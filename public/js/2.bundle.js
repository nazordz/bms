(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[2],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ItemComponent.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/ItemComponent.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    description: String,
    price: String,
    url: String,
    term: String,
    data: Object
  },
  data: function data() {
    return {
      currency: sessionStorage.getItem('currency') || 'Rp',
      subtotalAmount: 0,
      grandTotal: 0,
      discount: '',
      tax: '',
      discount_amount: 0,
      tax_amount: 0,
      term_and_condition: '',
      taxOption: null,
      discountOption: null,
      taxDetail: null,
      // for detail purpose
      discountDetail: null,
      taxes: [{
        tax_id: '',
        tax_name: '',
        id: '',
        text: '',
        tax_amount: 0,
        type: 1
      }],
      rows: [{
        options: null,
        selected: false,
        item_id: '',
        item_name: '',
        description: '',
        price: 0,
        discountType: 1,
        qty: 1,
        discount: 0,
        subtotal: 0
      }]
    };
  },
  mounted: function mounted() {
    var vm = this;

    if (this.data) {
      this.term_and_condition = this.data.term_and_condition;

      if (this.data.taxes) {
        this.taxes = this.data.taxes;
      }

      if (this.data.discount) {
        this.discountOption = {
          id: this.data.discount.discount_id,
          text: "".concat(this.data.discount.discount_amount, "%-").concat(this.data.discount.description)
        };
        this.discountDetail = {
          id: this.data.discount.discount_id,
          text: "".concat(this.data.discount.discount_amount, "%-").concat(this.data.discount.description)
        };
        this.discount_amount = this.data.discount.discount_amount;
      } else if (this.data.discountDetail) {
        this.discountOption = {
          id: this.data.discountDetail.id,
          text: "".concat(this.data.discountDetail.text)
        };
        this.discount_amount = this.data.discount_amount;
      }

      this.data.list_items && this.data.list_items.forEach(function (val, index) {
        var arr = {
          options: {
            id: val.item_id,
            text: val.item_name
          },
          description: val.description,
          price: val.price,
          qty: val.quantity || val.qty || 1,
          discountType: val.discount_type || val.discountType || 1,
          discount: val.discount,
          subtotal: val.subtotal,
          item_name: val.item_name
        };

        if (index < 1) {
          this.rows[0] = arr;
        } else {
          vm.rows.push(arr);
        }
      }, vm);
      this.calculateSubTotalAmount();
    }
  },
  methods: {
    addRow: function addRow() {
      var newRow = {
        options: null,
        selected: null,
        // item: '',
        description: '',
        price: 0,
        qty: 1,
        discountType: 1,
        discount: 0,
        subtotal: 0
      };
      this.rows.push(newRow);
    },
    detailRow: function detailRow(payload, index) {
      var description = payload.description,
          sell_price = payload.sell_price,
          text = payload.text;
      this.rows[index].description = description;
      this.rows[index].discountType = 1;
      this.rows[index].price = sell_price;
      this.rows[index].subtotal = sell_price;
      this.rows[index].item_name = text;
      this.calculateSubTotal(index);
    },
    calculateSubTotal: function calculateSubTotal(index) {
      var $event = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

      if ($event) {
        this.rows[index].price = $event.target.value.replace(/\./g, '');
      }

      var _this$rows$index = this.rows[index],
          price = _this$rows$index.price,
          qty = _this$rows$index.qty,
          discount = _this$rows$index.discount,
          discountType = _this$rows$index.discountType;
      var cut = discount / 100;
      var subtotal = price * qty - price * qty * cut;

      if (discountType == 2) {
        subtotal = price * qty - discount;
      }

      this.$set(this.rows[index], 'subtotal', subtotal);
      this.calculateSubTotalAmount();
    },
    calculateSubTotalAmount: function calculateSubTotalAmount() {
      var subAmount = this.rows.reduce(function (x, y) {
        return {
          subtotal: x.subtotal + y.subtotal
        };
      });
      this.subtotalAmount = subAmount.subtotal;
      this.calculateTotal();
    },
    calculateTotal: function calculateTotal() {
      // set to global
      window.items = {
        list_items: this.rows,
        term_and_condition: this.term_and_condition,
        discount_amount: this.discount_amount,
        discountDetail: this.discountDetail,
        taxes: this.taxes
      };
      var subtotalAmount = this.subtotalAmount,
          tax_amount = this.tax_amount,
          discount_amount = this.discount_amount;

      var taxDecrease = _.sumBy(this.taxes, function (o) {
        return o.type == 1 ? Number(o.tax_amount) : 0;
      });

      var taxIncrease = _.sumBy(this.taxes, function (o) {
        return o.type == 2 ? Number(o.tax_amount) : 0;
      });

      var disc = discount_amount / 100 * subtotalAmount;
      var amount = subtotalAmount - disc; // console.log({taxDecrease, taxIncrease});

      var taxDec = 0;
      var taxInc = 0;
      var taxes = 0;
      var pay = amount;

      if (taxDecrease > 0 && taxIncrease > 0) {
        taxDec = taxDecrease / 100 * subtotalAmount;
        taxInc = taxIncrease / 100 * subtotalAmount; // console.log({amount, taxDec, taxInc});
        // let inc = dec + taxInc;
        // let dec = amount + taxDec;

        pay = amount - taxDec + taxInc;
      } else if (taxDecrease > 0) {
        taxDec = taxDecrease / 100 * subtotalAmount;
        taxes = amount - taxDec;
        pay = taxes;
      } else if (taxIncrease > 0) {
        taxInc = taxIncrease / 100 * subtotalAmount;
        taxes = amount + taxInc;
        pay = taxes;
      }

      this.grandTotal = pay;
    },
    removeRow: function removeRow(index) {
      this.$delete(this.rows, index);
      this.calculateSubTotalAmount();
    },
    addTaxes: function addTaxes() {
      this.taxes.push({
        id: '',
        text: '',
        tax_amount: 0,
        type: 1
      });
    },
    rmvTaxes: function rmvTaxes(index) {
      this.$delete(this.taxes, index);
      this.calculateTotal();
    },
    taxAmount: function taxAmount(_ref, index) {
      var tax_amount = _ref.tax_amount,
          id = _ref.id,
          text = _ref.text,
          type = _ref.type,
          description = _ref.description;
      this.taxes[index] = {
        id: id,
        text: text,
        tax_id: id,
        description: text,
        tax_name: text,
        tax_amount: Number(tax_amount),
        type: type
      };
      this.calculateTotal();
    },
    discAmount: function discAmount(_ref2) {
      var discount_amount = _ref2.discount_amount,
          id = _ref2.id,
          text = _ref2.text;
      //{ discount_amount }
      this.discount_amount = discount_amount || 0;
      this.discountDetail = {
        id: id,
        text: text
      };
      this.calculateTotal();
    }
  },
  filters: {
    thousandSeparator: function thousandSeparator(num) {
      if (/^-?\d*$/.test(num)) {
        var res = num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return sessionStorage.getItem('thousand_separator') ? res.replace(/,/g, sessionStorage.getItem('thousand_separator')) : res;
      } else {
        return false;
      }
    }
  },
  watch: {
    discount: function discount(val) {
      if (!val) this.discount_amount = 0;
      this.calculateTotal();
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ItemComponent.vue?vue&type=style&index=0&id=70c3fdcf&lang=scss&scoped=true&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/ItemComponent.vue?vue&type=style&index=0&id=70c3fdcf&lang=scss&scoped=true& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".taxes-space[data-v-70c3fdcf] {\n  margin-bottom: 5px;\n  display: -ms-flexbox;\n  display: flex;\n}\n.taxes-space button[data-v-70c3fdcf] {\n  border-radius: 0;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ItemComponent.vue?vue&type=style&index=0&id=70c3fdcf&lang=scss&scoped=true&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/ItemComponent.vue?vue&type=style&index=0&id=70c3fdcf&lang=scss&scoped=true& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--8-2!../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../node_modules/vue-loader/lib??vue-loader-options!./ItemComponent.vue?vue&type=style&index=0&id=70c3fdcf&lang=scss&scoped=true& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ItemComponent.vue?vue&type=style&index=0&id=70c3fdcf&lang=scss&scoped=true&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ItemComponent.vue?vue&type=template&id=70c3fdcf&scoped=true&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/ItemComponent.vue?vue&type=template&id=70c3fdcf&scoped=true& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "table",
      {
        staticClass: "table table-sm table-condensed",
        attrs: { id: "tbItem" }
      },
      [
        _c("thead", [
          _c("tr", [
            _c("th", [_vm._v("Item")]),
            _vm._v(" "),
            _c("th", [_vm._v(_vm._s(_vm.description))]),
            _vm._v(" "),
            _c("th", [_vm._v(_vm._s(_vm.price))]),
            _vm._v(" "),
            _c("th", [_vm._v("Qty")]),
            _vm._v(" "),
            _c(
              "th",
              {
                staticStyle: { "text-align": "center" },
                attrs: { colspan: "2" }
              },
              [_vm._v("Discount")]
            ),
            _vm._v(" "),
            _c("th", [_vm._v("Subtotal")]),
            _vm._v(" "),
            _c("th")
          ])
        ]),
        _vm._v(" "),
        _c(
          "tbody",
          { attrs: { id: "body-item" } },
          _vm._l(_vm.rows, function(row, index) {
            return _c(
              "tr",
              {
                key: index,
                staticClass: "list-items",
                attrs: { id: "list-item-0" }
              },
              [
                _c(
                  "td",
                  { attrs: { width: "20%" } },
                  [
                    _c("select2", {
                      attrs: {
                        options: row.options,
                        name: "row[" + index + "][item_id]",
                        url: _vm.url,
                        urlCreate: "/item-list/add-form"
                      },
                      on: {
                        payloadItem: function($event) {
                          return _vm.detailRow($event, index)
                        }
                      },
                      model: {
                        value: row.item_id,
                        callback: function($$v) {
                          _vm.$set(row, "item_id", $$v)
                        },
                        expression: "row.item_id"
                      }
                    }),
                    _vm._v(" "),
                    _c("input", {
                      attrs: {
                        type: "hidden",
                        name: "row[" + index + "][item_name]"
                      },
                      domProps: { value: row.item_name }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c("td", { attrs: { width: "26%" } }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: row.description,
                        expression: "row.description"
                      }
                    ],
                    staticClass: "form-control row-description",
                    attrs: {
                      type: "text",
                      name: "row[" + index + "][description]"
                    },
                    domProps: { value: row.description },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(row, "description", $event.target.value)
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c(
                  "td",
                  { staticClass: "input-group", attrs: { width: "20%" } },
                  [
                    _c("div", { staticClass: "input-group-prepend" }, [
                      _c("span", { staticClass: "input-group-text" }, [
                        _vm._v(_vm._s(_vm.currency))
                      ])
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      staticClass: "form-control row-price",
                      attrs: {
                        min: "0",
                        type: "text",
                        name: "row[" + index + "][price]"
                      },
                      domProps: {
                        value: _vm._f("thousandSeparator")(row.price)
                      },
                      on: {
                        change: function($event) {
                          return _vm.calculateSubTotal(index, $event)
                        }
                      }
                    })
                  ]
                ),
                _vm._v(" "),
                _c("td", { attrs: { width: "8%" } }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: row.qty,
                        expression: "row.qty"
                      }
                    ],
                    staticClass: "form-control row-qty",
                    attrs: {
                      min: "0",
                      type: "number",
                      name: "row[" + index + "][quantity]"
                    },
                    domProps: { value: row.qty },
                    on: {
                      change: function($event) {
                        return _vm.calculateSubTotal(index)
                      },
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(row, "qty", $event.target.value)
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c("td", { attrs: { width: "9%" } }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: row.discount,
                        expression: "row.discount"
                      }
                    ],
                    staticClass: "form-control row-discount",
                    attrs: {
                      type: "number",
                      min: "0",
                      name: "row[" + index + "][discount]"
                    },
                    domProps: { value: row.discount },
                    on: {
                      change: function($event) {
                        return _vm.calculateSubTotal(index)
                      },
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(row, "discount", $event.target.value)
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c("td", { attrs: { width: "7%" } }, [
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: row.discountType,
                          expression: "row.discountType"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { name: "discount_type" },
                      on: {
                        change: [
                          function($event) {
                            var $$selectedVal = Array.prototype.filter
                              .call($event.target.options, function(o) {
                                return o.selected
                              })
                              .map(function(o) {
                                var val = "_value" in o ? o._value : o.value
                                return val
                              })
                            _vm.$set(
                              row,
                              "discountType",
                              $event.target.multiple
                                ? $$selectedVal
                                : $$selectedVal[0]
                            )
                          },
                          function($event) {
                            return _vm.calculateSubTotal(index)
                          }
                        ]
                      }
                    },
                    [
                      _c("option", { attrs: { value: "1" } }, [_vm._v("%")]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "2" } }, [
                        _vm._v(_vm._s(_vm.currency))
                      ])
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("td", { attrs: { width: "10%" } }, [
                  _c("input", {
                    staticClass:
                      "form-control row-subtotal text-right subtotal-row",
                    attrs: {
                      type: "text",
                      name: "row[" + index + "][subtotal]",
                      readonly: ""
                    },
                    domProps: {
                      value: _vm._f("thousandSeparator")(row.subtotal)
                    }
                  })
                ]),
                _vm._v(" "),
                index > 0
                  ? _c("td", [
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-sm btn-danger",
                          attrs: { type: "button" },
                          on: {
                            click: function($event) {
                              return _vm.removeRow(index)
                            }
                          }
                        },
                        [_c("i", { staticClass: "fas fa-trash" })]
                      )
                    ])
                  : _vm._e()
              ]
            )
          }),
          0
        )
      ]
    ),
    _vm._v(" "),
    _c(
      "button",
      {
        staticClass: "btn btn-sm btn-primary btn-icon text-light",
        attrs: { type: "button", id: "add-row" },
        on: {
          click: function($event) {
            return _vm.addRow()
          }
        }
      },
      [_c("i", { staticClass: "fas fa-plus" })]
    ),
    _vm._v(" "),
    _c("table", { staticClass: "table table-sm mt-4" }, [
      _c("tbody", { attrs: { id: "grid-inner" } }, [
        _c("tr", [
          _c("td", { attrs: { rowspan: "4", width: "30%" } }, [
            _c("div", { staticClass: "form-group col-lg-10" }, [
              _c("label", { attrs: { for: "" } }, [
                _vm._v(_vm._s(_vm.description))
              ]),
              _vm._v(" "),
              _c("textarea", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.term_and_condition,
                    expression: "term_and_condition"
                  }
                ],
                staticClass: "form-control",
                attrs: { name: "term_and_condition", rows: "6" },
                domProps: { value: _vm.term_and_condition },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.term_and_condition = $event.target.value
                  }
                }
              })
            ])
          ])
        ]),
        _vm._v(" "),
        _c("tr", [
          _c("td", { attrs: { align: "right", width: "5%" } }, [
            _vm._v("Subtotal")
          ]),
          _vm._v(" "),
          _c("td", { attrs: { align: "right", id: "subtotal-amount" } }, [
            _vm._v(
              _vm._s(_vm.currency) +
                " " +
                _vm._s(_vm._f("thousandSeparator")(_vm.subtotalAmount))
            )
          ])
        ]),
        _vm._v(" "),
        _c("tr", [
          _c("td", { attrs: { align: "right", width: "5%" } }, [
            _vm._v("Discount")
          ]),
          _vm._v(" "),
          _c(
            "td",
            { attrs: { width: "10%", align: "" } },
            [
              _c("select2", {
                attrs: {
                  name: "discount",
                  options: _vm.discountOption,
                  allowClear: true,
                  required: false,
                  urlCreate: "/discount",
                  url: "/select/discount"
                },
                on: {
                  payloadItem: function($event) {
                    return _vm.discAmount($event)
                  }
                },
                model: {
                  value: _vm.discount,
                  callback: function($$v) {
                    _vm.discount = $$v
                  },
                  expression: "discount"
                }
              })
            ],
            1
          )
        ]),
        _vm._v(" "),
        _c("tr", [
          _c("td", { attrs: { align: "right", width: "5%" } }, [
            _vm._v("Pajak")
          ]),
          _vm._v(" "),
          _c(
            "td",
            { attrs: { width: "10%", align: "" } },
            [
              _vm._l(_vm.taxes, function(tax, index) {
                return _c(
                  "div",
                  { key: index, staticClass: "taxes-space" },
                  [
                    _vm.taxes
                      ? _c("select2", {
                          attrs: {
                            name: "tax[]",
                            options: { id: tax.tax_id, text: tax.description },
                            required: true,
                            urlCreate: "/tax",
                            url: "/select/tax"
                          },
                          on: {
                            payloadItem: function($event) {
                              return _vm.taxAmount($event, index)
                            }
                          },
                          model: {
                            value: _vm.taxes[index].id,
                            callback: function($$v) {
                              _vm.$set(_vm.taxes[index], "id", $$v)
                            },
                            expression: "taxes[index].id"
                          }
                        })
                      : _c("select2", {
                          attrs: {
                            name: "tax[]",
                            required: true,
                            urlCreate: "/tax",
                            url: "/select/tax"
                          },
                          on: {
                            payloadItem: function($event) {
                              return _vm.taxAmount($event, index)
                            }
                          },
                          model: {
                            value: _vm.taxes[index].tax_id,
                            callback: function($$v) {
                              _vm.$set(_vm.taxes[index], "tax_id", $$v)
                            },
                            expression: "taxes[index].tax_id"
                          }
                        }),
                    _vm._v(" "),
                    index > 0
                      ? _c(
                          "button",
                          {
                            staticClass: "btn btn-sm btn-danger",
                            on: {
                              click: function($event) {
                                return _vm.rmvTaxes(index)
                              }
                            }
                          },
                          [_c("i", { staticClass: "fas fa-trash" })]
                        )
                      : _vm._e()
                  ],
                  1
                )
              }),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "btn btn-primary btn-secondary btn-sm btn-icon",
                  attrs: { type: "button" },
                  on: {
                    click: function($event) {
                      return _vm.addTaxes()
                    }
                  }
                },
                [_c("i", { staticClass: "fas fa-plus" })]
              )
            ],
            2
          )
        ]),
        _vm._v(" "),
        _c("tr", [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "td",
            { staticClass: "light", attrs: { align: "right", id: "total" } },
            [
              _c("strong", [
                _vm._v(
                  _vm._s(_vm.currency) +
                    " " +
                    _vm._s(_vm._f("thousandSeparator")(_vm.grandTotal))
                )
              ])
            ]
          )
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "td",
      {
        staticClass: "light",
        attrs: { align: "right", width: "5%", colspan: "2" }
      },
      [_c("strong", [_vm._v("Grand total")])]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./resources/js/components/ItemComponent.vue":
/*!***************************************************!*\
  !*** ./resources/js/components/ItemComponent.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ItemComponent_vue_vue_type_template_id_70c3fdcf_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ItemComponent.vue?vue&type=template&id=70c3fdcf&scoped=true& */ "./resources/js/components/ItemComponent.vue?vue&type=template&id=70c3fdcf&scoped=true&");
/* harmony import */ var _ItemComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ItemComponent.vue?vue&type=script&lang=js& */ "./resources/js/components/ItemComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _ItemComponent_vue_vue_type_style_index_0_id_70c3fdcf_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ItemComponent.vue?vue&type=style&index=0&id=70c3fdcf&lang=scss&scoped=true& */ "./resources/js/components/ItemComponent.vue?vue&type=style&index=0&id=70c3fdcf&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _ItemComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ItemComponent_vue_vue_type_template_id_70c3fdcf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ItemComponent_vue_vue_type_template_id_70c3fdcf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "70c3fdcf",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/ItemComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/ItemComponent.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/components/ItemComponent.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ItemComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./ItemComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ItemComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ItemComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/ItemComponent.vue?vue&type=style&index=0&id=70c3fdcf&lang=scss&scoped=true&":
/*!*************************************************************************************************************!*\
  !*** ./resources/js/components/ItemComponent.vue?vue&type=style&index=0&id=70c3fdcf&lang=scss&scoped=true& ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ItemComponent_vue_vue_type_style_index_0_id_70c3fdcf_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--8-2!../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../node_modules/vue-loader/lib??vue-loader-options!./ItemComponent.vue?vue&type=style&index=0&id=70c3fdcf&lang=scss&scoped=true& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ItemComponent.vue?vue&type=style&index=0&id=70c3fdcf&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ItemComponent_vue_vue_type_style_index_0_id_70c3fdcf_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ItemComponent_vue_vue_type_style_index_0_id_70c3fdcf_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ItemComponent_vue_vue_type_style_index_0_id_70c3fdcf_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ItemComponent_vue_vue_type_style_index_0_id_70c3fdcf_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ItemComponent_vue_vue_type_style_index_0_id_70c3fdcf_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/ItemComponent.vue?vue&type=template&id=70c3fdcf&scoped=true&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/ItemComponent.vue?vue&type=template&id=70c3fdcf&scoped=true& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ItemComponent_vue_vue_type_template_id_70c3fdcf_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./ItemComponent.vue?vue&type=template&id=70c3fdcf&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ItemComponent.vue?vue&type=template&id=70c3fdcf&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ItemComponent_vue_vue_type_template_id_70c3fdcf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ItemComponent_vue_vue_type_template_id_70c3fdcf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);