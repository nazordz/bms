(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["firm-component"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/NoteComponent.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/NoteComponent.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var bootstrap_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! bootstrap-vue */ "./node_modules/bootstrap-vue/esm/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['url', 'lang', 'data'],
  components: {
    BFormInput: bootstrap_vue__WEBPACK_IMPORTED_MODULE_0__["BFormInput"]
  },
  data: function data() {
    return {
      currency: sessionStorage.getItem('currency') || 'Rp',
      grandTotalAmount: 0,
      discount_amount: 0,
      rows: [{
        options: null,
        selected: false,
        item_id: '',
        item_name: '',
        description: '',
        price: 0,
        qty: 1,
        subtotal: 0
      }]
    };
  },
  mounted: function mounted() {
    var vm = this;

    if (this.data) {
      console.log(this.data.hasOwn);
      this.data.list_items && this.data.list_items.forEach(function (val, index) {
        var pricing = Number.isInteger(val.price) ? this.thousandSeparator(val.price) : val.price;
        var arr = {
          options: {
            id: val.item_id,
            text: val.item_name
          },
          description: val.description,
          price: pricing,
          qty: val.quantity || val.qty || 1,
          discountType: val.discount_type || val.discountType || 1,
          discount: val.discount,
          subtotal: val.subtotal,
          item_name: val.item_name
        };

        if (index < 1) {
          this.$set(this.rows, 0, arr);
        } else {
          vm.rows.push(arr);
        }
      }, vm);
      this.calculateTotalAmount();
    }
  },
  created: function created() {
    this.$i18n.locale = this.lang;
  },
  methods: {
    addRow: function addRow() {
      var newRow = {
        options: null,
        selected: null,
        description: '',
        price: 0,
        qty: 1,
        discountType: 1,
        discount: 0,
        subtotal: 0
      };
      this.rows.push(newRow);
    },
    detailRow: function detailRow(payload, index) {
      var description = payload.description,
          sell_price = payload.sell_price,
          text = payload.text;
      this.rows[index].description = description;
      this.rows[index].discountType = 1;
      this.rows[index].price = this.thousandSeparator(sell_price);
      this.rows[index].subtotal = sell_price;
      this.rows[index].item_name = text;
      this.calculateSubTotal(index);
    },
    handleCharacter: function handleCharacter($event) {
      var key = $event.which || $event.keyCode;

      if (key != 188 // Comma
      && key != 8 // Backspace
      && key != 9 // Tab
      && (key < 48 || key > 57) // Non digit
      && (key < 96 || key > 105) // Numpad Non digit
      ) {
          $event.preventDefault();
          return false;
        }
    },
    formatMoney: function formatMoney($event, index) {
      var nominal = $event.target.value;
      var number_string = nominal.replace(/[^,\d]/g, '').toString(),
          split = number_string.split(sessionStorage.getItem('thousand_separator')),
          sisa = split[0].length % 3,
          rupiah = split[0].substr(0, sisa),
          ribuan = split[0].substr(sisa).match(/\d{1,3}/gi);

      if (ribuan) {
        var separator = sisa ? sessionStorage.getItem('thousand_separator') : '';
        rupiah += separator + ribuan.join(sessionStorage.getItem('thousand_separator'));
      }

      this.rows[index].price = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    },
    calculateSubTotal: function calculateSubTotal(index) {
      var $event = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var _this$rows$index = this.rows[index],
          price = _this$rows$index.price,
          qty = _this$rows$index.qty,
          discount = _this$rows$index.discount,
          discountType = _this$rows$index.discountType;
      price = price.replace(/\.||\,/g, '');
      var cut = discount / 100;
      var subtotal = price * qty - price * qty * cut;

      if (discountType == 2) {
        subtotal = price * qty - discount;
      }

      this.$set(this.rows[index], 'subtotal', subtotal);
      this.calculateTotalAmount();
    },
    calculateTotalAmount: function calculateTotalAmount() {
      var subAmount = this.rows.reduce(function (x, y) {
        var xSubtotal = Number(x.subtotal.toString().replace(/\.||\,/g, ''));
        var ySubtotal = Number(y.subtotal.toString().replace(/\.||\,/g, ''));
        return {
          subtotal: xSubtotal + ySubtotal
        };
        return {
          subtotal: x.subtotal + y.subtotal
        };
      });
      this.grandTotalAmount = subAmount.subtotal;
      window.items = {
        list_items: this.rows,
        term_and_condition: this.term_and_condition,
        discount_amount: this.discount_amount,
        discountDetail: this.discountDetail,
        taxes: this.taxes
      };
    },
    removeRow: function removeRow(index) {
      this.$delete(this.rows, index);
      this.calculateTotalAmount();
    },
    thousandSeparator: function thousandSeparator(num) {
      if (/^-?\d*$/.test(num)) {
        var res = num.toLocaleString().replace(/,/g, sessionStorage.getItem('thousand_separator'));
        return res;
      } else {
        return false;
      }
    },
    hasOwn: function hasOwn(obj, key) {
      var myObject = new Object(obj); // myObject[key] = obj;

      return myObject.hasOwnProperty(key);
    }
  },
  filters: {
    thousandSeparator: function thousandSeparator(num) {
      if (/^-?\d*$/.test(num)) {
        var res = num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return sessionStorage.getItem('thousand_separator') ? res.replace(/,/g, sessionStorage.getItem('thousand_separator')) : res;
      } else {
        return false;
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/NoteComponent.vue?vue&type=template&id=5dae2960&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/NoteComponent.vue?vue&type=template&id=5dae2960& ***!
  \****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "table",
      {
        staticClass: "table table-sm table-condensed",
        attrs: { id: "tbItem" }
      },
      [
        _c("thead", [
          _c("tr", [
            _c("th", [_vm._v("Item")]),
            _vm._v(" "),
            _c("th", [_vm._v(_vm._s(_vm.$t("description")))]),
            _vm._v(" "),
            _c("th", [_vm._v(_vm._s(_vm.$t("price")))]),
            _vm._v(" "),
            _c("th", [_vm._v("Qty")]),
            _vm._v(" "),
            _c(
              "th",
              {
                staticStyle: { "text-align": "center" },
                attrs: { colspan: "2" }
              },
              [_vm._v("Discount")]
            ),
            _vm._v(" "),
            _c("th", [_vm._v("Subtotal")]),
            _vm._v(" "),
            _c("th")
          ])
        ]),
        _vm._v(" "),
        _c(
          "tbody",
          { attrs: { id: "body-item" } },
          _vm._l(_vm.rows, function(row, index) {
            return _c(
              "tr",
              {
                key: index,
                staticClass: "list-items",
                attrs: { id: "list-item-0" }
              },
              [
                _c(
                  "td",
                  { attrs: { width: "20%" } },
                  [
                    _c("select2", {
                      attrs: {
                        options: row.options,
                        name: "row[" + index + "][item_id]",
                        url: _vm.url,
                        urlCreate: "/item-list/add-form",
                        required: true
                      },
                      on: {
                        payloadItem: function($event) {
                          return _vm.detailRow($event, index)
                        }
                      },
                      model: {
                        value: row.item_id,
                        callback: function($$v) {
                          _vm.$set(row, "item_id", $$v)
                        },
                        expression: "row.item_id"
                      }
                    }),
                    _vm._v(" "),
                    _c("input", {
                      attrs: {
                        type: "hidden",
                        name: "row[" + index + "][item_name]"
                      },
                      domProps: { value: row.item_name }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c("td", { attrs: { width: "26%" } }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: row.description,
                        expression: "row.description"
                      }
                    ],
                    staticClass: "form-control row-description",
                    attrs: {
                      type: "text",
                      name: "row[" + index + "][description]"
                    },
                    domProps: { value: row.description },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(row, "description", $event.target.value)
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c(
                  "td",
                  { staticClass: "input-group", attrs: { width: "20%" } },
                  [
                    _c("div", { staticClass: "input-group-prepend" }, [
                      _c("span", { staticClass: "input-group-text" }, [
                        _vm._v(_vm._s(_vm.currency))
                      ])
                    ]),
                    _vm._v(" "),
                    _c("b-form-input", {
                      staticClass: "form-control row-price",
                      attrs: {
                        type: "text",
                        name: "row[" + index + "][price]"
                      },
                      on: {
                        keydown: function($event) {
                          return _vm.handleCharacter($event)
                        },
                        change: function($event) {
                          return _vm.calculateSubTotal(index, $event)
                        },
                        keyup: function($event) {
                          return _vm.formatMoney($event, index)
                        }
                      },
                      model: {
                        value: row.price,
                        callback: function($$v) {
                          _vm.$set(row, "price", $$v)
                        },
                        expression: "row.price"
                      }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c("td", { attrs: { width: "8%" } }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: row.qty,
                        expression: "row.qty"
                      }
                    ],
                    staticClass: "form-control row-qty",
                    attrs: {
                      min: "0",
                      type: "number",
                      name: "row[" + index + "][quantity]"
                    },
                    domProps: { value: row.qty },
                    on: {
                      change: function($event) {
                        return _vm.calculateSubTotal(index)
                      },
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(row, "qty", $event.target.value)
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c("td", { attrs: { width: "9%" } }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: row.discount,
                        expression: "row.discount"
                      }
                    ],
                    staticClass: "form-control row-discount",
                    attrs: {
                      type: "number",
                      min: "0",
                      name: "row[" + index + "][discount]"
                    },
                    domProps: { value: row.discount },
                    on: {
                      change: function($event) {
                        return _vm.calculateSubTotal(index)
                      },
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(row, "discount", $event.target.value)
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c("td", { attrs: { width: "7%" } }, [
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: row.discountType,
                          expression: "row.discountType"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { name: "discount_type" },
                      on: {
                        change: [
                          function($event) {
                            var $$selectedVal = Array.prototype.filter
                              .call($event.target.options, function(o) {
                                return o.selected
                              })
                              .map(function(o) {
                                var val = "_value" in o ? o._value : o.value
                                return val
                              })
                            _vm.$set(
                              row,
                              "discountType",
                              $event.target.multiple
                                ? $$selectedVal
                                : $$selectedVal[0]
                            )
                          },
                          function($event) {
                            return _vm.calculateSubTotal(index)
                          }
                        ]
                      }
                    },
                    [
                      _c("option", { attrs: { value: "1" } }, [_vm._v("%")]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "2" } }, [
                        _vm._v(_vm._s(_vm.currency))
                      ])
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("td", { attrs: { width: "10%" } }, [
                  _c("input", {
                    staticClass:
                      "form-control row-subtotal text-right subtotal-row",
                    attrs: {
                      type: "text",
                      name: "row[" + index + "][subtotal]",
                      readonly: ""
                    },
                    domProps: {
                      value: _vm._f("thousandSeparator")(row.subtotal)
                    }
                  })
                ]),
                _vm._v(" "),
                index > 0
                  ? _c("td", [
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-sm btn-danger",
                          attrs: { type: "button" },
                          on: {
                            click: function($event) {
                              return _vm.removeRow(index)
                            }
                          }
                        },
                        [_c("i", { staticClass: "fas fa-trash" })]
                      )
                    ])
                  : _vm._e()
              ]
            )
          }),
          0
        )
      ]
    ),
    _vm._v(" "),
    !_vm.hasOwn(_vm.data, "id")
      ? _c(
          "button",
          {
            staticClass: "btn btn-sm btn-primary btn-icon text-light",
            attrs: { type: "button", id: "add-row" },
            on: {
              click: function($event) {
                return _vm.addRow()
              }
            }
          },
          [_c("i", { staticClass: "fas fa-plus" })]
        )
      : _vm._e(),
    _vm._v(" "),
    _c("div", { staticClass: "col-lg-4 col-xs-12 offset-lg-8" }, [
      _c("table", { staticClass: "table table-sm mt-4" }, [
        _c("tbody", { attrs: { id: "grid-inner" } }, [
          _c("tr", [
            _c("td", { attrs: { align: "right", width: "10%" } }, [
              _vm._v("Total")
            ]),
            _vm._v(" "),
            _c("td", { attrs: { align: "right", id: "subtotal-amount" } }, [
              _vm._v(
                _vm._s(_vm.currency) +
                  " " +
                  _vm._s(_vm._f("thousandSeparator")(_vm.grandTotalAmount))
              )
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/NoteComponent.vue":
/*!***************************************************!*\
  !*** ./resources/js/components/NoteComponent.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _NoteComponent_vue_vue_type_template_id_5dae2960___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./NoteComponent.vue?vue&type=template&id=5dae2960& */ "./resources/js/components/NoteComponent.vue?vue&type=template&id=5dae2960&");
/* harmony import */ var _NoteComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./NoteComponent.vue?vue&type=script&lang=js& */ "./resources/js/components/NoteComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _NoteComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _NoteComponent_vue_vue_type_template_id_5dae2960___WEBPACK_IMPORTED_MODULE_0__["render"],
  _NoteComponent_vue_vue_type_template_id_5dae2960___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/NoteComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/NoteComponent.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/components/NoteComponent.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NoteComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./NoteComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/NoteComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NoteComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/NoteComponent.vue?vue&type=template&id=5dae2960&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/NoteComponent.vue?vue&type=template&id=5dae2960& ***!
  \**********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NoteComponent_vue_vue_type_template_id_5dae2960___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./NoteComponent.vue?vue&type=template&id=5dae2960& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/NoteComponent.vue?vue&type=template&id=5dae2960&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NoteComponent_vue_vue_type_template_id_5dae2960___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NoteComponent_vue_vue_type_template_id_5dae2960___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);