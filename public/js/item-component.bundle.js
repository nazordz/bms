(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["item-component"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ItemComponent.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/ItemComponent.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var bootstrap_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! bootstrap-vue */ "./node_modules/bootstrap-vue/esm/index.js");
/* harmony import */ var vue_currency_input__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue-currency-input */ "./node_modules/vue-currency-input/dist/vue-currency-input.esm.js");
/* harmony import */ var vuex_map_fields__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vuex-map-fields */ "./node_modules/vuex-map-fields/dist/index.esm.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    url: String,
    lang: String,
    data: Object
  },
  components: {
    BFormInput: bootstrap_vue__WEBPACK_IMPORTED_MODULE_1__["BFormInput"],
    CurrencyInput: vue_currency_input__WEBPACK_IMPORTED_MODULE_2__["CurrencyInput"]
  },
  directives: {
    currency: vue_currency_input__WEBPACK_IMPORTED_MODULE_2__["CurrencyDirective"]
  },
  data: function data() {
    return {
      subtotalAmount: 0,
      grandTotal: 0,
      discount: '',
      tax: '',
      discount_amount: 0,
      tax_amount: 0,
      description: '',
      term_and_condition: '',
      taxOption: null,
      discountOption: null,
      taxDetail: null,
      // for detail purpose
      discountDetail: null,
      subTotalDiscount: 0,
      taxes: [{
        tax_id: '',
        tax_name: '',
        id: '',
        text: '',
        tax_amount: 0,
        type: 1
      }],
      rows: [{
        options: null,
        selected: false,
        item_id: '',
        item_name: '',
        description: '',
        price: 0,
        discountType: 1,
        qty: 1,
        discount: 0,
        subtotal: 0
      }]
    };
  },
  mounted: function mounted() {
    var vm = this;

    if (this.data) {
      this.term_and_condition = this.data.term_and_condition;
      this.description = this.data.description;

      if (this.data.taxes.length) {
        this.taxes = this.data.taxes;
      }

      if (this.data.discount) {
        this.discountOption = {
          id: this.data.discount.discount_id,
          text: "".concat(this.data.discount.discount_amount, "%-").concat(this.data.discount.description)
        };
        this.discountDetail = {
          id: this.data.discount.discount_id,
          text: "".concat(this.data.discount.discount_amount, "%-").concat(this.data.discount.description)
        };
        this.discount_amount = this.data.discount.discount_amount;
      } else if (this.data.discountDetail) {
        this.discountOption = {
          id: this.data.discountDetail.id,
          text: "".concat(this.data.discountDetail.text)
        };
        this.discount_amount = this.data.discount_amount;
      }

      this.data.list_items && this.data.list_items.forEach(function (val, index) {
        // var pricing = Number.isInteger(val.price) ? this.thousandSeparator(val.price) : val.price;
        var arr = {
          options: {
            id: val.item_id,
            text: val.item_name
          },
          description: val.description,
          // price: this.thousandSeparator(val.price),
          price: this.currency == 1 ? val.price : val.price / this.data.rate,
          // coverted if currency isn't idr
          oldPrice: val.price,
          // price: pricing,
          qty: val.quantity || val.qty || 1,
          discountType: val.discount_type || val.discountType || 1,
          discount: val.discount,
          subtotal: this.currency == 1 ? val.subtotal : val.subtotal / this.data.rate,
          // converted
          item_name: val.item_name
        };

        if (index < 1) {
          this.rows[0] = arr;
        } else {
          vm.rows.push(arr);
        }
      }, vm);
      this.calculateSubTotalAmount();
    }
  },
  created: function created() {
    this.$i18n.locale = this.lang;
  },
  methods: {
    addRow: function addRow() {
      var newRow = {
        options: null,
        selected: null,
        // item: '',
        description: '',
        price: 0,
        qty: 1,
        discountType: 1,
        discount: 0,
        subtotal: 0
      };
      this.rows.push(newRow);
    },
    detailRow: function detailRow(payload, index) {
      var description = payload.description,
          sell_price = payload.sell_price,
          text = payload.text;
      this.rows[index].description = description;
      this.rows[index].discountType = 1;
      this.rows[index].oldPrice = sell_price;
      this.rows[index].item_name = text;

      if (this.currency != 1) {
        this.rows[index].price = sell_price / this.rate;
        this.rows[index].subtotal = sell_price / this.rate;
      } else {
        this.rows[index].price = sell_price;
        this.rows[index].subtotal = sell_price;
      }

      this.calculateSubTotal(index);
    },
    handleCharacter: function handleCharacter($event) {
      var key = $event.which || $event.keyCode;

      if (key != 188 // Comma
      && key != 8 // Backspace
      && key != 9 // Tab
      && (key < 48 || key > 57) // Non digit
      && (key < 96 || key > 105) // Numpad Non digit
      ) {
          $event.preventDefault();
          return false;
        }
    },
    formatMoney: function formatMoney($event, index) {
      var nominal = $event.target.value;
      var number_string = nominal.replace(/[^,\d]/g, '').toString(),
          split = number_string.split(sessionStorage.getItem('thousand_separator')),
          sisa = split[0].length % 3,
          rupiah = split[0].substr(0, sisa),
          ribuan = split[0].substr(sisa).match(/\d{1,3}/gi);

      if (ribuan) {
        var separator = sisa ? sessionStorage.getItem('thousand_separator') : '';
        rupiah += separator + ribuan.join(sessionStorage.getItem('thousand_separator'));
      }

      this.rows[index].price = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    },
    calculateSubTotal: function calculateSubTotal(index) {
      var $event = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var _this$rows$index = this.rows[index],
          price = _this$rows$index.price,
          qty = _this$rows$index.qty,
          discount = _this$rows$index.discount,
          discountType = _this$rows$index.discountType;
      var cut = discount / 100; // var subtotal = (price*qty) - ((price*qty) * cut);

      var subtotal = price * qty;

      if (discountType == 2) {
        subtotal = price * qty - discount;
      }

      this.$set(this.rows[index], 'subtotal', subtotal);
      this.calculateSubTotalAmount();
    },
    calculateSubTotalAmount: function calculateSubTotalAmount() {
      var subAmount = this.rows.reduce(function (x, y) {
        var xSubtotal = Number(x.subtotal);
        var ySubtotal = Number(y.subtotal);
        return {
          subtotal: xSubtotal + ySubtotal
        };
      });
      this.subtotalAmount = subAmount.subtotal || 0.00;
      this.subTotalDiscount = this.discount_amount / 100 * this.subtotalAmount;
      this.calculateTotal();
    },
    calculateTotal: function calculateTotal() {
      // set to global
      window.items = {
        list_items: this.rows,
        term_and_condition: this.term_and_condition,
        discount_amount: this.discount_amount,
        discountDetail: this.discountDetail,
        taxes: this.taxes
      };
      var subtotalAmount = this.subtotalAmount,
          tax_amount = this.tax_amount,
          discount_amount = this.discount_amount;

      var taxDecrease = _.sumBy(this.taxes, function (o) {
        return o.type == 1 ? Number(o.tax_amount) : 0;
      });

      var taxIncrease = _.sumBy(this.taxes, function (o) {
        return o.type == 2 ? Number(o.tax_amount) : 0;
      });

      var disc = discount_amount / 100 * subtotalAmount; // var amount = subtotalAmount - disc;

      var amount = subtotalAmount;
      var taxDec = 0;
      var taxInc = 0;
      var taxes = 0;
      var pay = amount;

      if (taxDecrease > 0 && taxIncrease > 0) {
        taxDec = taxDecrease / 100 * amount;
        taxInc = taxIncrease / 100 * amount;
        pay = amount - taxDec + taxInc;
      } else if (taxDecrease > 0) {
        taxDec = taxDecrease / 100 * amount;
        taxes = amount - taxDec;
        pay = taxes;
      } else if (taxIncrease > 0) {
        taxInc = taxIncrease / 100 * amount;
        taxes = amount + taxInc;
        pay = taxes;
      }
      /* f(this.currency != 1){
          pay = pay / this.rate;
      } */


      this.grandTotal = pay || 0.00;
    },
    removeRow: function removeRow(index) {
      this.$delete(this.rows, index);
      this.calculateSubTotalAmount();
    },
    addTaxes: function addTaxes() {
      this.taxes.push({
        id: '',
        text: '',
        tax_amount: 0,
        type: 1
      });
    },
    rmvTaxes: function rmvTaxes(index) {
      this.$delete(this.taxes, index);
      this.calculateTotal();
    },
    taxAmount: function taxAmount(_ref, index) {
      var tax_amount = _ref.tax_amount,
          id = _ref.id,
          text = _ref.text,
          type = _ref.type,
          description = _ref.description;
      this.taxes[index] = {
        id: id,
        text: text,
        tax_id: id,
        description: text,
        tax_name: text,
        tax_amount: Number(tax_amount),
        type: type
      };
      this.calculateTotal();
    },
    discAmount: function discAmount(_ref2) {
      var discount_amount = _ref2.discount_amount,
          id = _ref2.id,
          text = _ref2.text;
      this.discount_amount = discount_amount || 0;
      this.discountDetail = {
        id: id,
        text: text
      };
      this.subTotalDiscount = discount_amount / 100 * this.subtotalAmount;
      this.calculateTotal();
    },
    thousandSeparator: function thousandSeparator(num) {
      if (/^-?\d*$/.test(num)) {
        var res = num.toLocaleString().replace(/,/g, sessionStorage.getItem('thousand_separator'));
        return res;
      } else {
        return false;
      }
    }
  },
  filters: {
    thousandSeparator: function thousandSeparator(num) {
      return num.toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
      });
    }
  },
  watch: {
    discount: function discount(val) {
      if (!val) this.discount_amount = 0;
      this.calculateTotal();
    },
    rate: function rate(_rate) {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (_this.currency != 1) {
                  _this.rows.map(function (val, index) {
                    val.price = val.oldPrice / _rate;
                    val.subtotal = val.oldPrice / _rate * val.qty;
                    return val;
                  });
                } else {
                  _this.rows.map(function (val, index) {
                    val.price = val.oldPrice;
                    val.subtotal = val.oldPrice * val.qty;
                    return val;
                  });
                }

                _this.calculateSubTotalAmount();

              case 2:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    }
  },
  computed: _objectSpread({}, Object(vuex_map_fields__WEBPACK_IMPORTED_MODULE_3__["mapFields"])('currencyRate', ['currency', 'rate']), {
    options: function options() {
      return {
        value: 0,
        locale: 'de-DE',
        selectedCurrencyOption: 0,
        currencyCode: this.getCurrency,
        distractionFree: true,
        hideCurrencySymbol: true,
        hideGroupingSymbol: true,
        hideNegligibleDecimalDigits: true,
        prefix: null,
        suffix: null,
        precisionEnabled: false,
        precisionRangeEnabled: false,
        precisionFixed: 2,
        precisionRange: [0, 20],
        valueRangeEnabled: false,
        valueRange: [0, 9999],
        minActive: false,
        maxActive: false,
        autoDecimalMode: false,
        valueAsInteger: false,
        allowNegative: false
      };
    },
    getGrandTotal: function getGrandTotal() {
      return this.currency == 1 ? this.grandTotal : this.grandTotal * this.rate;
    },
    getSubTotal: function getSubTotal() {
      return this.currency == 1 ? this.subtotalAmount : this.subtotalAmount * this.rate;
    },
    getCurrency: function getCurrency() {
      var curr = this.currency;
      var res = 'IDR';

      switch (curr) {
        case "2" || false:
          res = 'SGD';
          break;

        case "3" || false:
          res = 'USD';
          break;

        case "4" || false:
          res = 'RM';
          break;
      }

      return res;
    },
    dpp: function dpp() {
      return this.subtotalAmount; //- this.subTotalDiscount;
    },
    taxIncrease: function taxIncrease() {
      if (this.taxes) {
        return _.sumBy(this.taxes, function (o) {
          return o.type == 2 ? Number(o.tax_amount) : 0;
        });
      } else {
        return 0;
      }
    },
    taxDecrease: function taxDecrease() {
      return _.sumBy(this.taxes, function (o) {
        return o.type == 1 ? Number(o.tax_amount) : 0;
      });
    }
  })
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ItemComponent.vue?vue&type=style&index=0&id=70c3fdcf&lang=scss&scoped=true&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/ItemComponent.vue?vue&type=style&index=0&id=70c3fdcf&lang=scss&scoped=true& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".taxes-space[data-v-70c3fdcf] {\n  margin-bottom: 5px;\n  display: -ms-flexbox;\n  display: flex;\n}\n.taxes-space button[data-v-70c3fdcf] {\n  border-radius: 0;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ItemComponent.vue?vue&type=style&index=0&id=70c3fdcf&lang=scss&scoped=true&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/ItemComponent.vue?vue&type=style&index=0&id=70c3fdcf&lang=scss&scoped=true& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--8-2!../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../node_modules/vue-loader/lib??vue-loader-options!./ItemComponent.vue?vue&type=style&index=0&id=70c3fdcf&lang=scss&scoped=true& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ItemComponent.vue?vue&type=style&index=0&id=70c3fdcf&lang=scss&scoped=true&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ItemComponent.vue?vue&type=template&id=70c3fdcf&scoped=true&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/ItemComponent.vue?vue&type=template&id=70c3fdcf&scoped=true& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "table",
      {
        staticClass: "table table-sm table-condensed",
        attrs: { id: "tbItem" }
      },
      [
        _c("thead", [
          _c("tr", [
            _c("th", [_vm._v("Item")]),
            _vm._v(" "),
            _c("th", [_vm._v(_vm._s(_vm.$t("description")))]),
            _vm._v(" "),
            _c("th", [_vm._v(_vm._s(_vm.$t("price")))]),
            _vm._v(" "),
            _c("th", [_vm._v("Qty")]),
            _vm._v(" "),
            _c("th", [_vm._v("Subtotal")]),
            _vm._v(" "),
            _c("th")
          ])
        ]),
        _vm._v(" "),
        _c(
          "tbody",
          { attrs: { id: "body-item" } },
          _vm._l(_vm.rows, function(row, index) {
            return _c(
              "tr",
              {
                key: index,
                staticClass: "list-items",
                attrs: { id: "list-item-0" }
              },
              [
                _c(
                  "td",
                  { attrs: { width: "20%" } },
                  [
                    _c("select2", {
                      attrs: {
                        options: row.options,
                        name: "row[" + index + "][item_id]",
                        url: _vm.url,
                        urlCreate: "/item-list/add-form",
                        required: true
                      },
                      on: {
                        payloadItem: function($event) {
                          return _vm.detailRow($event, index)
                        }
                      },
                      model: {
                        value: row.item_id,
                        callback: function($$v) {
                          _vm.$set(row, "item_id", $$v)
                        },
                        expression: "row.item_id"
                      }
                    }),
                    _vm._v(" "),
                    _c("input", {
                      attrs: {
                        type: "hidden",
                        name: "row[" + index + "][item_name]"
                      },
                      domProps: { value: row.item_name }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c("td", { attrs: { width: "26%" } }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: row.description,
                        expression: "row.description"
                      }
                    ],
                    staticClass: "form-control row-description",
                    attrs: {
                      type: "text",
                      name: "row[" + index + "][description]"
                    },
                    domProps: { value: row.description },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(row, "description", $event.target.value)
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c(
                  "td",
                  { attrs: { width: "20%" } },
                  [
                    _c("v-currency", {
                      on: {
                        change: function($event) {
                          return _vm.calculateSubTotal(index)
                        }
                      },
                      model: {
                        value: row.price,
                        callback: function($$v) {
                          _vm.$set(row, "price", $$v)
                        },
                        expression: "row.price"
                      }
                    }),
                    _vm._v(" "),
                    _c("input", {
                      attrs: {
                        type: "hidden",
                        name: "row[" + index + "][price]"
                      },
                      domProps: {
                        value: _vm.rate ? row.price * _vm.rate : row.price
                      }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c("td", { attrs: { width: "12%" } }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: row.qty,
                        expression: "row.qty"
                      }
                    ],
                    staticClass: "form-control row-qty",
                    attrs: {
                      min: "0",
                      type: "number",
                      name: "row[" + index + "][quantity]"
                    },
                    domProps: { value: row.qty },
                    on: {
                      change: function($event) {
                        return _vm.calculateSubTotal(index)
                      },
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(row, "qty", $event.target.value)
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c(
                  "td",
                  [
                    _c("v-currency", {
                      attrs: { readonly: "" },
                      model: {
                        value: row.subtotal,
                        callback: function($$v) {
                          _vm.$set(row, "subtotal", $$v)
                        },
                        expression: "row.subtotal"
                      }
                    }),
                    _vm._v(" "),
                    _c("input", {
                      attrs: {
                        type: "hidden",
                        name: "row[" + index + "][subtotal]"
                      },
                      domProps: {
                        value: _vm.rate ? row.subtotal * _vm.rate : row.subtotal
                      }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                index > 0
                  ? _c("td", [
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-sm btn-danger",
                          attrs: { type: "button" },
                          on: {
                            click: function($event) {
                              return _vm.removeRow(index)
                            }
                          }
                        },
                        [_c("i", { staticClass: "fas fa-trash" })]
                      )
                    ])
                  : _vm._e()
              ]
            )
          }),
          0
        )
      ]
    ),
    _vm._v(" "),
    _c(
      "button",
      {
        staticClass: "btn btn-sm btn-primary btn-icon text-light",
        attrs: { type: "button", id: "add-row" },
        on: {
          click: function($event) {
            return _vm.addRow()
          }
        }
      },
      [_c("i", { staticClass: "fas fa-plus" })]
    ),
    _vm._v(" "),
    _c("table", { staticClass: "table table-sm mt-4" }, [
      _c("tbody", { attrs: { id: "grid-inner" } }, [
        _c("tr", [
          _c("td", { attrs: { rowspan: "7", width: "30%" } }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "form-group col-lg-10" }, [
                _c("label", { attrs: { for: "" } }, [
                  _vm._v(_vm._s(_vm.$t("description")))
                ]),
                _vm._v(" "),
                _c("textarea", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.description,
                      expression: "description"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { name: "description", rows: "4" },
                  domProps: { value: _vm.description },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.description = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group col-lg-10" }, [
                _c("label", { attrs: { for: "" } }, [
                  _vm._v(_vm._s(_vm.$t("term_and_condition")))
                ]),
                _vm._v(" "),
                _c("textarea", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.term_and_condition,
                      expression: "term_and_condition"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { name: "term_and_condition", rows: "4" },
                  domProps: { value: _vm.term_and_condition },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.term_and_condition = $event.target.value
                    }
                  }
                })
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("tr", [
          _c("td", { attrs: { align: "right", colspan: "2", width: "5%" } }, [
            _vm._v("Subtotal")
          ]),
          _vm._v(" "),
          _c("td", { attrs: { align: "right", id: "subtotal-amount" } }, [
            _vm._v(
              "\n                    " +
                _vm._s(_vm._f("thousandSeparator")(_vm.subtotalAmount)) +
                "\n                    "
            ),
            _c("input", {
              attrs: { name: "subTotal", type: "hidden" },
              domProps: { value: _vm.getSubTotal }
            })
          ])
        ]),
        _vm._v(" "),
        _c("tr", [
          _c("td", { attrs: { align: "right", colspan: "2", width: "5%" } }, [
            _vm._v(_vm._s(_vm.$t("tax")))
          ]),
          _vm._v(" "),
          _c(
            "td",
            { attrs: { width: "10%", align: "" } },
            [
              _vm._l(_vm.taxes, function(tax, index) {
                return _c(
                  "div",
                  { key: index, staticClass: "taxes-space" },
                  [
                    _vm.taxes
                      ? _c("select2", {
                          attrs: {
                            name: "tax[" + index + "][id]",
                            options: { id: tax.tax_id, text: tax.description },
                            allowClear: true,
                            urlCreate: "/tax",
                            url: "/select/tax/2"
                          },
                          on: {
                            payloadItem: function($event) {
                              return _vm.taxAmount($event, index)
                            }
                          },
                          model: {
                            value: _vm.taxes[index].id,
                            callback: function($$v) {
                              _vm.$set(_vm.taxes[index], "id", $$v)
                            },
                            expression: "taxes[index].id"
                          }
                        })
                      : _c("select2", {
                          attrs: {
                            name: "tax[" + index + "][id]",
                            urlCreate: "/tax",
                            allowClear: true,
                            url: "/select/tax/2"
                          },
                          on: {
                            payloadItem: function($event) {
                              return _vm.taxAmount($event, index)
                            }
                          },
                          model: {
                            value: _vm.taxes[index].tax_id,
                            callback: function($$v) {
                              _vm.$set(_vm.taxes[index], "tax_id", $$v)
                            },
                            expression: "taxes[index].tax_id"
                          }
                        }),
                    _vm._v(" "),
                    index > 0
                      ? _c(
                          "button",
                          {
                            staticClass: "btn btn-sm btn-danger",
                            on: {
                              click: function($event) {
                                return _vm.rmvTaxes(index)
                              }
                            }
                          },
                          [_c("i", { staticClass: "fas fa-trash" })]
                        )
                      : _vm._e()
                  ],
                  1
                )
              }),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "btn btn-primary btn-secondary btn-sm btn-icon",
                  attrs: { type: "button" },
                  on: {
                    click: function($event) {
                      return _vm.addTaxes()
                    }
                  }
                },
                [_c("i", { staticClass: "fas fa-plus" })]
              )
            ],
            2
          )
        ]),
        _vm._v(" "),
        _c(
          "tr",
          [
            _vm._m(0),
            _vm._v(" "),
            _c(
              "td",
              { staticClass: "light", attrs: { align: "right", id: "total" } },
              [
                _c("strong", [
                  _vm._v(
                    "\n                        " +
                      _vm._s(_vm.getCurrency) +
                      "\n                        " +
                      _vm._s(_vm._f("thousandSeparator")(_vm.grandTotal)) +
                      "\n                    "
                  )
                ])
              ]
            ),
            _vm._v(" "),
            _c("input", {
              attrs: { type: "hidden", name: "grandTotal" },
              domProps: { value: _vm.getGrandTotal }
            }),
            _vm._v(" "),
            _vm._l(_vm.taxes, function(tax, index) {
              return _c(
                "div",
                { key: index, staticStyle: { display: "none" } },
                [
                  _c("input", {
                    attrs: {
                      name: "tax[" + index + "][amount]",
                      type: "hidden"
                    },
                    domProps: { value: tax.tax_amount }
                  }),
                  _vm._v(" "),
                  _c("input", {
                    attrs: { name: "tax[" + index + "][type]", type: "hidden" },
                    domProps: { value: tax.type }
                  })
                ]
              )
            })
          ],
          2
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "td",
      {
        staticClass: "light",
        attrs: { align: "right", width: "5%", colspan: "2" }
      },
      [_c("strong", [_vm._v("Grand total")])]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/ItemComponent.vue":
/*!***************************************************!*\
  !*** ./resources/js/components/ItemComponent.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ItemComponent_vue_vue_type_template_id_70c3fdcf_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ItemComponent.vue?vue&type=template&id=70c3fdcf&scoped=true& */ "./resources/js/components/ItemComponent.vue?vue&type=template&id=70c3fdcf&scoped=true&");
/* harmony import */ var _ItemComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ItemComponent.vue?vue&type=script&lang=js& */ "./resources/js/components/ItemComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _ItemComponent_vue_vue_type_style_index_0_id_70c3fdcf_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ItemComponent.vue?vue&type=style&index=0&id=70c3fdcf&lang=scss&scoped=true& */ "./resources/js/components/ItemComponent.vue?vue&type=style&index=0&id=70c3fdcf&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _ItemComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ItemComponent_vue_vue_type_template_id_70c3fdcf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ItemComponent_vue_vue_type_template_id_70c3fdcf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "70c3fdcf",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/ItemComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/ItemComponent.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/components/ItemComponent.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ItemComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./ItemComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ItemComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ItemComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/ItemComponent.vue?vue&type=style&index=0&id=70c3fdcf&lang=scss&scoped=true&":
/*!*************************************************************************************************************!*\
  !*** ./resources/js/components/ItemComponent.vue?vue&type=style&index=0&id=70c3fdcf&lang=scss&scoped=true& ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ItemComponent_vue_vue_type_style_index_0_id_70c3fdcf_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--8-2!../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../node_modules/vue-loader/lib??vue-loader-options!./ItemComponent.vue?vue&type=style&index=0&id=70c3fdcf&lang=scss&scoped=true& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ItemComponent.vue?vue&type=style&index=0&id=70c3fdcf&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ItemComponent_vue_vue_type_style_index_0_id_70c3fdcf_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ItemComponent_vue_vue_type_style_index_0_id_70c3fdcf_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ItemComponent_vue_vue_type_style_index_0_id_70c3fdcf_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ItemComponent_vue_vue_type_style_index_0_id_70c3fdcf_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ItemComponent_vue_vue_type_style_index_0_id_70c3fdcf_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/ItemComponent.vue?vue&type=template&id=70c3fdcf&scoped=true&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/ItemComponent.vue?vue&type=template&id=70c3fdcf&scoped=true& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ItemComponent_vue_vue_type_template_id_70c3fdcf_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./ItemComponent.vue?vue&type=template&id=70c3fdcf&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ItemComponent.vue?vue&type=template&id=70c3fdcf&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ItemComponent_vue_vue_type_template_id_70c3fdcf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ItemComponent_vue_vue_type_template_id_70c3fdcf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);