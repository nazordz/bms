<?php

return [
    'Dashboard'         => 'Dasbor',
    'Customer'          => 'Customer',
    'Settings'          => 'Pengaturan',
    'Users'             => 'User',
    'Edit_menu'         => 'Edit Menu',
    'Edit_menu_elements' => 'Edit menu element',
    'Edit_roles'        => 'Edit Peran',
    'Media'             => 'Media',

    'amount'   => 'Jumlah',
    'supplier' => 'Supplier',
    'bill'     => 'Tagihan',
    'run'      => 'Berjalan',

    'amountCustomer' => 'Jumlah Customer',
    'amountSupplier' => 'Jumlah Supplier',
    'nominalInvoice' => 'Nominal Invoice',
    'amountBillRunning' => 'Jumlah Tagihan Berjalan',

    'Supplier'        => 'Supplier',
    'Accounting'      => 'Akunting',
    'Asset'           => 'Aktiva',
    'journal'         => 'Jurnal',
    'Ledger'          => 'Buku Besar',
    'Balance'         => 'Neraca',
    'COA'             => 'COA',
    'Item_List'       => 'Daftar item',
    'Report'          => 'Laporan',
    'Selling'         => 'Penjualan',
    'Cost'            => 'Biaya',
    'ProfitAndLoss'   => 'Laba/Rugi',
    'Manage_discount' => 'Manage discount',
    'Manage_tax'      => 'Manage pajak',
    'Manage_admin'    => 'Manage admin',

    'name'       => 'Nama',
    'role'       => 'Akses',
    'created_at' => 'Dibuat pada',
    'action'     => 'Aksi',

    'addAdmin'  => 'Tambah Admin',
    'close'     => 'Tutup',
    'save'      => 'Simpan',
    'return'    => 'Kembali',
    'privilege' => 'Hak akses',

    'admin_customer'   => 'Admin customer',
    'admin_supplier'   => 'Admin supplier',
    'admin_accounting' => 'Admin akunting',
    'admin_item_list'  => 'Admin daftar item',
    'admin_laporan'    => 'Admin laporan',
    'admin_diskon'     => 'Admin discount',
    'admin_pajak'      => 'Admin pajak',
    'admin_setting'    => 'Admin setting',

    'password' => 'Sandi',
    'password_confirmation' => 'Konfirmasi sandi',

    'admin_created' => 'Admin telah dibuat!',
    'language' => 'Bahasa',
    'edit' => 'Ubah',

];
