@extends('dashboard.base')
@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
        @if (session('status-success'))
            <div class="alert alert-success">
                {{ session('status-success') }}
            </div>
        @endif
        @if (session('status-fail'))
            <div class="alert alert-danger">
                {{ session('status-fail') }}
                @if ($errors->any())
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ preg_replace('/[0-9]||\./', '', $error) }}</li>
                        @endforeach
                    </ul>
                @endif
            </div>
        @endif
            {{-- <div class="modal fade" id="modalPaid" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">@lang('dashboard.payment') INV{{ $data->serial }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="" id="formJournal" method="post">
                            <div class="modal-body">
                                <div class="form-group row">
                                    <label for="" class="col-lg-3">@lang('dashboard.payment_date')</label>
                                    <div class="col-lg-9">
                                        <div class="input-group">
                                            <input type="text" required name="payment_date" onchange="changeHiddenValue('paymentdate', this)" onkeydown="return false;" autocomplete="off" class="form-control datepicker" placeholder="">
                                            <div class="input-group-append">
                                                <div class="input-group-text"><i class="fas fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="" class="col-lg-3">@lang('dashboard.payment_ref')</label>
                                    <div class="col-lg-9">
                                        <input type="text" required onchange="changeHiddenValue('paymentref', this)" name="payment_ref" class="form-control" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="" class="col-lg-3">@lang('dashboard.account')</label>
                                    <div class="col-lg-9">
                                        <select id="selectAccount" style="bottom: 18%; left: 50%;" onchange="changeHiddenValue('accountid', this)" oninvalid="check(this, 'Choose Account')" name="account_id" class="form-control" placeholder="" required>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('dashboard.close')</button>
                                <button type="submit" class="btn btn-primary">@lang('dashboard.save')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> --}}
            <form action="{{ route('customer.invoice.update') }}" id="formInvoice" enctype="multipart/form-data" method="post">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-header-split">
                                    <div class="">
                                        Invoice INV{{$data->serializing}}
                                    </div>
                                    @if ($data->payment == "0")
                                        <div class="alert-header alert-danger">Void</div>
                                    @elseif($data->payment == "1")
                                        <div class="card-header-notif">
                                            <div class="alert-header alert-warning">Unpaid</div>
                                            @if ($data->status == "1")
                                                <div class="alert-header alert-warning">Draft</div>
                                            @elseif($data->status == "2")
                                                <div class="alert-header alert-success">Publish</div>
                                            @endif
                                        </div>

                                    @elseif($data->payment == "2")
                                        <div class="card-header-notif">
                                            <div class="alert-header alert-success">Paid</div>
                                            @if ($data->status == "1")
                                                <div class="alert-header alert-warning">Draft</div>
                                            @elseif($data->status == "2")
                                                <div class="alert-header alert-success">Publish</div>
                                            @endif
                                        </div>

                                    @endif
                                </div>
                            </div>
                            <div class="card-body">
                                @method('put')
                                @csrf
                                <input type="hidden" id="invoiceId" name="invoice_id" value="{{ $data->invoice_id }}">
                                <input type="hidden" id="serialInput" name="serial" value="{{ $data->serializing }}">
                                <div class="row">
                                    <div class="form-group col-lg-4">
                                        <label for="">Customer</label>
                                        <select id="selectCustomer" style="bottom: 18%; left: 50%;" oninvalid="check(this, 'Choose Customer')" onchange="setCustomValidity('')" {{$isFormDisabled}} name="customer_id" class="form-control" placeholder="" required>
                                            @if($data->customer)
                                                <option value="{{ $data->customer->customer_id }}"> {{ $data->customer->company.' - '.$data->customer->first_name.' '.$data->customer->last_name }}  </option>
                                            @endif
                                        </select>
                                        @if ($data)
                                                <input type="hidden" name="customer_name" id="input_customer_name" value="{{ $data->customer->company.' - '.$data->customer->first_name.' '.$data->customer->last_name }}" />
                                        @endif
                                        @if ($errors->first('customer_id'))
                                            <small class="text-danger">{{ $errors->first('customer_id') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="">No {{ __('dashboard.quotation')  }}</label>
                                        <select name="quotation_id" id="selectQuotationCustomer" class="form-control" {{$isFormDisabled}}>
                                            @if ($data->quotation)
                                                <option value="{{ $data->quotation->quotation_id }}">QTT{{ $data->quotation->serializing.' - '.$data->quotation->customer->company. ' - '. $data->quotation->customer->first_name }}</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="">No Booking Confirmation</label>
                                        <select id="selectBc" style="bottom: 18%; left: 50%;" {{$isFormDisabled}} name="bc_id" class="form-control" placeholder="">
                                            @if($data->booking_confirmation)
                                                <option value="{{ $data->booking_confirmation->bc_id }}"> {{ 'BC'.$data->booking_confirmation->serializing.' - '.$data->booking_confirmation->customer->company. ' - '. $data->booking_confirmation->customer->first_name }}  </option>
                                            @endif
                                        </select>
                                        @if ($errors->first('bc_id'))
                                            <small class="text-danger">{{ $errors->first('bc_id') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.term_of_payment')</label>
                                        <select name="term_of_payment" class="form-control" {{$isFormDisabled}}>
                                            <option {{ $data->term_of_payment == 'COD' }} value="COD">COD</option>
                                            <option {{ $data->term_of_payment == '30 Hari' ? 'selected' : '' }} value="30 Hari">30 Hari</option>
                                            <option {{ $data->term_of_payment == '60 Hari' ? 'selected' : '' }} value="60 Hari">60 Hari</option>
                                            <option {{ $data->term_of_payment == '90 Hari' ? 'selected' : '' }} value="90 Hari">90 Hari</option>
                                        </select>
                                        @if ($errors->first('payment'))
                                            <small class="text-danger">{{ $errors->first('payment') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.invoice_date')</label>
                                        <div class="input-group">
                                            <input type="text" name="invoice_date" id="invoiceDate" {{$isFormDisabled}} value="{{date_format(date_create($data->invoice_date), 'd-m-Y') }}" onkeydown="return false;" required class="form-control datepicker-invoice" placeholder="">
                                            <div class="input-group-append">
                                                <div class="input-group-text"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                                            </div>
                                        </div>
                                        @if ($errors->first('invoice_date'))
                                            <small class="text-danger">{{ $errors->first('invoice_date') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.due_date')</label>
                                        <div class="input-group">
                                            <input type="text" name="due_date" id="dueDate" {{$isFormDisabled}} value="{{date_format(date_create($data->due_date), 'd-m-Y') }}" onkeydown="return false;" required class="form-control" placeholder="">
                                            <div class="input-group-append">
                                                <div class="input-group-text"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                                            </div>
                                        </div>
                                        @if ($errors->first('due_date'))
                                            <small class="text-danger">{{ $errors->first('due_date') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.type')</label>
                                        <div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadioInline1" name="is_invoice" value="1" {{$isFormDisabled}} {{ $data->is_invoice == 1?'checked':'' }} class="custom-control-input">
                                                <label class="custom-control-label" for="customRadioInline1">Invoice</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadioInline2" name="is_invoice" value="0" {{$isFormDisabled}} {{ $data->is_invoice == 0?'checked':'' }} class="custom-control-input">
                                                <label class="custom-control-label" for="customRadioInline2">Debit Note</label>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <div class="form-group col-lg-4">
                                        <label for="">@lang('dashboard.term_of_payment')</label>
                                        <div class="input-group">
                                            <input type="number" id="termOfPayment" class="form-control" value="{{ $data->term_of_payment }}" name="term_of_payment">
                                            <div class="input-group-append">
                                                <div class="input-group-text">{{ __('dashboard.day') }}</div>
                                            </div>
                                        </div>
                                    </div> --}}

                                    {{-- <div class="form-group col-lg-4">
                                        <label for="">@lang('dashboard.created_at')</label>
                                        <v-datetimepicker name="created_time" value="{{ date_format(date_create($data->created_time), 'd-m-Y') }}"></v-datetimepicker>
                                    </div> --}}

                                    <v-rate-curr
                                        :data="{{$data}}"
                                        lang="{{Auth::user()->language}}"
                                        styling="col-lg-12"
                                        :disabled="{{($data->paidAmount > 0 || $data->status == 0) ? 'true': 'false'}}"
                                    />
                                </div>
                            </div>
                        </div>
                        <div id="accordion">
                            <div class="card">
                                <div class="card-header text-right" id="headingForm">
                                    <button class="btn btn-link btn-sm" type="button" onclick="$('.collapse').collapse('toggle');" style="cursor: pointer;" data-toggle="collapse" role="button" aria-expanded="false" data-controls="collapseForm">
                                        {{ __('dashboard.show') }} <i class="fas fa-angle-down"></i>
                                   </button>
                                </div>
                                <div class="collapse" data-parent="#accordion" id="collapseForm" aria-labelledby="headingForm">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="form-group col-lg-3">
                                                <label for="">Attn</label>
                                                <input type="text" class="form-control" id="inputAttn" {{$isFormDisabled}} name="attn" value="{{ $data->attn }}">
                                            </div>
                                            <div class="form-group col-lg-3">
                                                <label for="">@lang('dashboard.vessel')</label>
                                                <input type="text" class="form-control" id="inputVessel" {{$isFormDisabled}} name="vessel" value="{{ $data->vessel }}">
                                            </div>
                                            <div class="form-group col-lg-3">
                                                <label for="">@lang('dashboard.voyage')</label>
                                                <input type="text" class="form-control" id="inputVoyage" {{$isFormDisabled}} name="voyage" value="{{ $data->voyage }}">
                                            </div>
                                            <div class="form-group col-lg-3">
                                                <label for="">ETA</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control datepicker" onkeydown="return false;" {{$isFormDisabled}} name="eta" value="{{date_format(date_create($data->eta), 'd-m-Y') }}">
                                                    <div class="input-group-append">
                                                        <div class="input-group-text"><i class="fas fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-4">
                                                <label for="">B/L No</label>
                                                <input type="text" class="form-control" {{$isFormDisabled}} name="bl_no" id="inputBl_no" value="{{ $data->bl_no }}">
                                            </div>

                                            <div class="form-group col-lg-4">
                                                <label for="">@lang('dashboard.from')</label>
                                                <input type="text" class="form-control" {{$isFormDisabled}} name="from" id="inputFrom" value="{{ $data->from }}">
                                            </div>

                                            <div class="form-group col-lg-4">
                                                <label for="">@lang('dashboard.to')</label>
                                                <input type="text" class="form-control" {{$isFormDisabled}} name="to" id="inputTo" value="{{ $data->to }}">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                <item-component
                                    :data="{{$data}}"
                                    lang="{{ Auth::user()->language }}"
                                    url="{{ url('/select/item') }}"
                                    >
                                </item-component>
                            </div>
                        </div>

                    </div>

                    {{-- for paid action --}}
                    <input type="hidden" name="payment_date" id="paymentdate" value="">
                    <input type="hidden" name="payment_ref" id="paymentref" value="">
                    <input type="hidden" name="account_id" id="accountid" value="">
                    <input type="hidden" name="status" id="statusInv" value="">
                    <input type="submit" id="triggerSubmit" style="display:none;">
                    <input type="hidden" name="paymentInfo" id="paymentInfo" value="{{ $data->payment }}">

                    <div class="col-lg-10 col-xs-12">
                        @if ($data->status)
                            @if ($data->status == 1)
                                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                    <div class="btn-group" role="group">
                                        <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{ __('dashboard.save') }}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                            <button type="button" name="status" value="1" onclick="handleSubmit(this)" class="dropdown-item">Draft</button>
                                            <button type="button" name="status" value="2" onclick="handleSubmit(this)" class="dropdown-item">Publish</button>
                                        </div>
                                    </div>
                                </div>
                            @else
                                @if ($data->payment == 1 && $paymentAmount < 1)
                                    <button type="submit" class="btn btn-primary">{{ __('dashboard.save') }}</button>
                                @endif
                            @endif
                            <button class="btn btn-success btn-print" data-uuid="{{$data->uuid}}" type="button">{{ __('dashboard.print') }}</button>

                            @if ($data->status == 2)
                                <a href="/accounting/sales-create-payment/{{ $data->uuid }}" class="btn btn-warning text-white">@lang('dashboard.createPayment')</a>
                                <button type="button" class="btn btn-dark btn-create-inv" data-invoice="{{$data->invoice_id}}">@lang('dashboard.create_invoice')</button>
                                <button type="button" class="btn btn-primary btn-create-qtt" data-invoice="{{$data->invoice_id}}">@lang('dashboard.create_quotation')</button>
                                <button class="btn btn-info btn-create-cn" data-id="{{$data->invoice_id}}" type="button">{{ __('dashboard.create_credit_note') }}</button>
                            @endif

                            @if ($data->status == 1)
                                <button class="btn btn-danger btn-void" data-type="delete" data-id="{{$data->invoice_id}}" type="button">@lang('dashboard.cancel')</button>
                            @elseif($data->status ==2)
                                <button class="btn btn-danger btn-void" data-type="void" data-id="{{$data->invoice_id}}" type="button">Void</button>
                            @endif
                        @endif

                        @if ($data->void)
                            <div class="card">
                                <div class="card-header">
                                    @lang('dashboard.void_description')
                                </div>
                                <div class="card-body">
                                    <div class="col-lg-7">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                @lang('dashboard.void_by') :
                                            </div>
                                            <div class="col-lg-6">
                                                {{ $data->void->name }}
                                            </div>
                                            <div class="col-lg-6">
                                                @lang('dashboard.void_at') :
                                            </div>
                                            <div class="col-lg-6">
                                                {{ date_format(date_create($data->void_at), 'd-m-Y H:i:s') }}
                                            </div>
                                            <div class="col-lg-6">
                                                @lang('dashboard.reason') :
                                            </div>
                                            <div class="col-lg-6">
                                                {{ $data->void_note }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <a href="{{url('/customer/invoice') }}" class="btn btn-secondary">{{ __('dashboard.return') }}</a>
                    </div>
                </div>
            </form>
            <form action="{{ route('customer.invoice.void') }}" method="post" id="voidInvoice" class="hidden">
                <input type="hidden" name="invoice_id" id="invoiceVoidId" value="">
                <input type="hidden" name="type" id="invoiceType" value="">
                <input type="hidden" name="reason" id="reasonInput" value="">
                @csrf
                @method('patch')
            </form>
        </div>
    </div>

@endsection

@section('javascript')
<script src="{{ asset('js/app.js') }}" defer></script>
<script>
var isModalOpen = false;

function check(input, word){
    if (input.value == "") {
     input.setCustomValidity(word);
   } else  {
     input.setCustomValidity('');
   }
}
function changeHiddenValue(el, e){
    document.getElementById(el).value = e.value
}
function handleSubmit(el){
    changeHiddenValue('statusInv', el)
    $("#triggerSubmit").click();
}
$(document).ready(function () {
    $("#invoiceDate").change(function(){
        var invDate = moment($(this).val(), 'DD-MM-YYYY');
        $("#dueDate").datepicker('remove');
        $("#dueDate").datepicker({
            // startDate: invDate._d,
            autoclose: true,
            format: 'dd-mm-yyyy',
            language: 'id',
            weekStart: 1,
            orientation: 'bottom'
        });
    });

    $("#dueDate").change(function(){
        var now = moment();
        var dueDate = moment($(this).val(), 'DD-MM-YYYY');
        var jarak = moment().range(now, dueDate)
        var hasil = '';
        if(now.format('DD-MM-YYYY') == dueDate.format('DD-MM-YYYY')){
            hasil = 0;
        }else{
            hasil = jarak.diff('days') + 1;
        }

        $("#termOfPayment").val(hasil);
    });
    $('.btn-create-inv').click(function(e){
        e.preventDefault();
        window.onbeforeunload = null;
        let customer  = {
            customer_id: $("#selectCustomer").val(),
            customer_name: $("#input_customer_name").val()
        }
        customer = $.param(customer)
        let item = $.param(items);
        let form = $.param({
            quotation_name: 'QTT'+$("#inputSerial").val(),
            attn          : $("#inputAttn").val(),
            vessel        : $("#inputVessel").val(),
            voyage        : $("#inputVoyage").val(),
            bl_no         : $("#inputBl_no").val(),
            from          : $("#inputFrom").val(),
            to            : $("#inputTo").val(),
            copy_form: 'true'
        });
        location = `/customer/create-invoice?${customer}&${item}&${form}`;
    });
    $('.btn-create-qtt').click(function(e){
        e.preventDefault();
        window.onbeforeunload = null;
        let customer  = {
            customer_id: $("#selectCustomer").val(),
            customer_name: $("#input_customer_name").val()
        }
        customer = $.param(customer)
        let item = $.param(items);
        let form = $.param({
            attn          : $("#inputAttn").val(),
            vessel        : $("#inputVessel").val(),
            voyage        : $("#inputVoyage").val(),
            quotation_name: 'QTT'+$("#inputSerial").val(),
            bl_no         : $("#inputBl_no").val(),
            from          : $("#inputFrom").val(),
            to            : $("#inputTo").val(),
            copy_form: 'true'
        });
        location = `/customer/create-quotation?${customer}&${item}&${form}`;
    });
    $(".btn-create-cn").click(function(){
        window.onbeforeunload = null;
        let customer  = {
            customer_id: $("#selectCustomer").val(),
            customer_name: $("#input_customer_name").val()
        }
        customer = $.param(customer)
        let stuff = $.param(items);
        let invoice = $.param({
            invoice_id: $("#invoiceId").val(),
            serial    : $("#serialInput").val(),
            currency  : window.items.currency,
            payment   : $("#paymentInfo").val(),
            rate      : items.rate

        });
        location = `/customer/create-credit-note?items=${stuff}&${customer}&${invoice}`;

    })
    $("#formJournal").submit(function(e){
        e.preventDefault();
        $("#formInvoice").trigger('submit');
    })

    $("#modalPaid").on('shown.bs.modal', function(){
        $("#formJournal")[0].reset();
    });
    $("#modalPaid").on('hidden.bs.modal', function(){
        isModalOpen = false;
        $("#paymentdate, #paymentref, #accountid").val('');
    });
    $(".btn-void").click(function(){
        $("#invoiceVoidId").val($(this).data('id'));
        $("#invoiceType").val($(this).data('type'));
        Swal.fire({
            title: '{{__("dashboard.are_you_sure")}}',
            icon: 'warning',
            input: 'textarea',
            inputPlaceholder: '{{__("dashboard.reason")}}',
            inputAttributes: {
                'aria-label': '{{__("dashboard.reason")}}...'
            },
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
        }).then((result) => {
            console.log(result);

            if (result.value) {
                $("#reasonInput").val(result.value);
                $("#voidInvoice").trigger('submit');
            }
        });
    });

    $("#formInvoice").submit(function(e){
        window.onbeforeunload = null;
        // check if status is paid
        if($("#paymentInvoice").val() == "2" && !isModalOpen && $("#statusInv").val() == 2){
            e.preventDefault();
            $("#modalPaid").modal('show');
            isModalOpen = true;
        }else{
            return;
        }
    });

    $("#selectAccount").select2({
        width: "100%",
        placeholder: "Account",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/account') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10,
                    lock: { kas: '1.1.01', bank: '1.1.02', sales: '4.0.00', receivable: '1.1.03' }

				};
            },
            cache: true,
        },
    });
    $("#selectQuotationCustomer").select2({
        width: "100%",
        placeholder: "Quotation",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/qtt-customer') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
                    page_limit : 10,
                    customer_id: $("#selectCustomer").val()

				};
            },
            cache: true,
        },
    });

    $("#selectBc").select2({
        width: "100%",
        placeholder: "Booking Confirmation",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/booking-confirmation') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
                    page_limit : 10,
                    quotation_id: $("#selectQuotationCustomer").val()
				};
            },
            cache: true,
        },
    })
    .on('select2:select', ({ params }) => {
        // var $option = $("<option selected></option>").val(params.data.customer_id).text(params.data.customer);
        // $("#selectCustomer").append($option).trigger('change');
    });
    $(".btn-print").click(function(){
        window.onbeforeunload = null;
        // location = '/customer/print-invoice/'+$(this).data('id');
        window.open('/customer/print-invoice/'+$(this).data('uuid'), '_blank');
    });

    const date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $('.datepicker').datepicker({
        autoclose: true,
        format: {
            toDisplay(date, format, language){
                var tgl = moment(date);
                return tgl.format('DD-MM-Y')
            },
            toValue(date, format, language){
                var tgl = moment(date);
                return tgl.format('Y-MM-DD')
            }
        },
        language: 'id',
        weekStart: 1,
        // startDate: today,
        orientation: 'bottom'
    });
    $('#dueDate').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        language: 'id',
        weekStart: 1,
        // startDate: today,
        orientation: 'bottom'
    });
    $('.datepicker-invoice').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        language: 'id',
        weekStart: 1,
        orientation: 'bottom'
    });

    $("#selectCustomer").select2({
        width: "100%",
        placeholder: "Customers",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/customer') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        },
    });

    $("#selectCustomer").on('change', function(){
        $("#input_customer_name").val($(this).select2('data')[0].text);
    })





});
</script>
@endsection
