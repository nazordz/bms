@extends('dashboard.base')
@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
        @if (session('status-success'))
            <div class="alert alert-success">
                {{ session('status-success') }}
            </div>
        @endif
        @if (session('status-fail'))
            <div class="alert alert-danger">
                {{ session('status-fail') }}
                @if($errors->any())
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                @endif
            </div>
        @endif
            <div class="row">
                <form action="{{ route('customer.credit.note.update') }}" method="post">
                    <div class="col-lg-12">
                        @method('put')
                        @csrf
                        <div class="card">
                            <div class="card-header">
                                <div class="card-header-split">
                                    <div>
                                        Edit CN{{$data->serializing}}
                                    </div>
                                    @if ($data->status == 1)
                                        <div class="alert-header alert-warning">Draft</div>
                                    @elseif($data->status == 2)
                                        <div class="alert-header alert-success">Publish</div>
                                    @else
                                        <div class="alert-header alert-danger">Void</div>
                                    @endif
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-lg-3">
                                        <label for="">Customer</label>
                                        <input type="text" class="form-control" disabled value="{{  $data->customer->company.' - '.$data->customer->first_name }}">
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="">Invoice</label>
                                        <input type="text" class="form-control" disabled value="{{ $data->invoice? ('INV'.$data->invoice->serial.' - '.$data->customer->company.' - '.$data->customer->first_name):'' }}">
                                    </div>

                                    {{-- <div class="form-group col-lg-3">
                                        <label for="">Sales</label>
                                        <input type="text" class="form-control" disabled value="{{  $data->seller->name ?? '' }}">
                                    </div> --}}
                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.proof')</label>
                                        <div>
                                            @if ($data->status == 1)
                                                @if ($data->proof_picture)
                                                    <a href="{{ $data->proof_picture }}" download class="btn btn-md btn-primary">Download</a>
                                                @else
                                                    <input type="file" name="proof_picture" value="{{ old('proof_picture') }}" class="form-control-file">
                                                    {{-- @lang('dashboard.no_exists') --}}
                                                @endif
                                            @else
                                                @if ($data->proof_picture)
                                                    <a href="{{ $data->proof_picture }}" download class="btn btn-md btn-primary">Download</a>
                                                @else
                                                    {{-- <input type="file" name="proof_picture" value="{{ old('proof_picture') }}" class="form-control-file"> --}}
                                                    @lang('dashboard.no_exists')
                                                @endif

                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-3" id="paidFromAccount" style="{{$data &&  $data['payment'] == 1 ? 'display: none;':'display: block;' }}">
                                        <label for="">@lang('dashboard.paid_from_account')</label>
                                        <input type="text" value="{{ $data->paid_account ? $data->paid_account->name : ''}}" disabled class="form-control">
                                        {{-- <select style="bottom: 18%; left: 50%;" type="text" disabled name="paid_from_account" id="inputPaidFromAccount" class="form-control selectAccount" {{$data &&  $data['payment'] == 2 ? 'required':'' }}>
                                        </select> --}}
                                    </div>

                                    @if ($data)
                                        <v-rate-curr
                                            :data="{{$data}}"
                                            lang="{{Auth::user()->language}}"
                                            :disabled="{{$data['status'] == 2? "true": "false"}}"
                                            styling="col-lg-9"
                                        />
                                    @else
                                        <v-rate-curr
                                            lang="{{Auth::user()->language}}"
                                            styling="col-lg-9"
                                        />
                                    @endif

                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        @if ($data)
                                            <note-component
                                                :data="{{$data}}"
                                                lang="{{ Auth::user()->language }}"
                                                url="{{ url('/select/item') }}"
                                            ></note-component>
                                        @else
                                            <note-component
                                                lang="{{ Auth::user()->language }}"
                                                url="{{ url('/select/item') }}"
                                            ></note-component>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- watch this --}}
                    <input type="hidden" name="id" value="{{ $data->id }}">
                    <input type="hidden" name="serial" value="{{ $data->serial }}">
                    <input type="hidden" name="invoice_id" value="{{ $data->invoice_id }}">
                    <input type="hidden" name="status" id="statusInv" value="">
                    <input type="submit" id="triggerSubmit" style="display:none;">
                    <div class="col-lg-8 col-xs-12">
                        @if ($data->status == 1)
                            <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                <div class="btn-group" role="group">
                                    <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{ __('dashboard.save') }}
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                        <button type="button" name="status" value="1" onclick="handleSubmit(this)" class="dropdown-item">Draft</button>
                                        <button type="button" name="status" value="2" onclick="handleSubmit(this)" class="dropdown-item">Publish</button>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <a href="{{url('/customer/credit-note') }}" class="btn btn-secondary">{{ __('dashboard.return') }}</a>
                        <button class="btn btn-success btn-print" data-uuid="{{$data->uuid}}" type="button">{{ __('dashboard.print') }}</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection
@section('javascript')
<script src="{{ asset('js/app.js') }}" defer></script>
<script>
function handleCharacter(e) {
    var key = e.which || e.keyCode;
    if ( key != 188 // Comma
        && key != 8 // Backspace
        && key != 9 // Tab
        && (key < 48 || key > 57) // Non digit
        && (key < 96 || key > 105) // Numpad Non digit
        )
    {
        e.preventDefault();
        return false;
    }
}

function check(input){
    if (input.value == "") {
     input.setCustomValidity('Choose Sales');
   } else  {
     input.setCustomValidity('');
   }
}
function changeHiddenValue(el, e){
    document.getElementById(el).value = e.value
}
function handleSubmit(el){
    changeHiddenValue('statusInv', el)
    $("#triggerSubmit").click();
}
$(document).ready(function () {
    $(".btn-print").click(function(){
        window.open('/customer/print-credit-note/'+$(this).data('uuid'), '_blank');
    });
    $("#selectSeller").select2({
        width: "100%",
        placeholder: "Sales",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/sales') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        },
    });
    $("#selectSeller").on('change', function(){
        $("#input_seller_name").val($(this).select2('data')[0].text);
    })
    const date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        language: 'id',
        weekStart: 1,
        startDate: today
    });

    $("#selectCustomer").select2({
        width: "100%",
        placeholder: "Customers",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/customer') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        },
    });

    $("#selectCustomer").on('change', function(){
        $("#input_customer_name").val($(this).select2('data')[0].text);
    })

});
</script>
@endsection
