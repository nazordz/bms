@extends('dashboard.base')

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
            @if (session('status-success'))
                <div class="alert alert-success">
                    {{ session('status-success') }}
                </div>
            @endif
            @if (session('status-fail'))
                <div class="alert alert-danger">
                    {{ session('status-fail') }}
                </div>
            @endif
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-users"></i> Booking Confirmation
                        </div>
                        <div class="card-body">
                            <div class="col-lg-12 mb-5">
                                <a href="{{ url('/customer/create-booking-confirmation') }}" class="btn btn-primary pull-right" > @lang('dashboard.create_bc') </a>
                            </div>
                            <table id="tbBc" class="table table-responsive-lg table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Booking Ref</th>
                                        <th>Customer</th>
                                        <th>Status</th>
                                        {{-- <th>@lang('dashboard.created_at')</th> --}}
                                        <th>@lang('dashboard.action')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('javascript')
<script>
$(document).ready(function () {

    var myTable = $("#tbBc").DataTable({
        responsive: true,
        processing : true,
        serverSide : true,
        order      : [[4,"DESC"]],
        autoWidth  : false,
        searchDelay: 500,
        ajax : {
            url : "{{ url('/customer/table-booking-confirmation') }}"
        },
        columns:[
            {
                data: null,
                orderable  : false,
				searchable : false
            },
            {
                data: 'booking_ref',
                render(data, type, row){
                    return `<a href="/customer/show-booking-confirmation/${row.uuid}">${data}${serializing(row.created_at, row.serial)}</a>`;
                }
            },
            {
                data: 'company',
                name: 'customers.company',
                render(data, type, row){
                    return `${data} - ${row.first_name} ${row.last_name}`;
                }
            },
            {
                data: 'status',
                render(data){
                    res = '<div class="alert-tb alert-danger">Cancel</div>';
                    switch (Number(data)) {
                        case 1:
                            res = '<div class="alert-tb alert-warning">Proccess</div>';
                            break;

                        case 2:
                            res = '<div class="alert-tb alert-primary">Shipping</div>';
                            break;

                        case 3:
                            res = '<div class="alert-tb alert-success">Shipped</div>';
                            break;

                    }
                    return res;
                }
            },
            // {
            //     data: 'created_at',
            //     name: 'bc_customers.created_at',
            //     render(data){
            //         var tgl = moment(data);
            //         return tgl.format('DD-MM-Y');
            //     }
            // },
            {
                data: 'bc_id',
                render(data, type, row){
                    if(row.status){
                        return `<button class="btn btn-sm btn-danger btn-cancel" data-bc_id="${data}"><i class="fas fa-times"></i> Cancel</button>`;
                    }
                    return ``;
                }
            }


        ],
        fnCreatedRow(row, data, index) {
			if (myTable.page.info().start >= 10) {
				var panjang = myTable.page.info().length;
				var halaman = myTable.page.info().page;
				var i = 1;
				i = (halaman + 1) * panjang;
				i -= panjang - 1;
	        }else{
	        	var i = 1;
	        }
	        $('td', row).eq(0).html(index + i);
		},
        drawCallback(){
            $(".dataTables_length select").removeClass("form-control-sm");
            $(".btn-cancel").click(function(){
                var bc_id = $(this).data('bc_id');
                Swal.fire({
                    title: '{{__("dashboard.are_you_sure")}}',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: '{{ __("dashboard.cancel") }}',
                    confirmButtonText: '{{__("dashboard.confirm_cancel")}}'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: "post",
                            url: "{{ route('customer.bc.cancel') }}",
                            data: { _token: "{{csrf_token()}}", _method:'patch',  bc_id},
                            dataType: "json"
                        }).done(function(data){
                            myTable.draw();
                            Swal.fire( 'Success', '', 'success' );
                        }).catch(function(err){
                            console.log(err);
                        });
                    }
                });
            });

		}
    })
});
</script>
@endsection

