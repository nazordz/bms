@extends('dashboard.base')
@section('css')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection
@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
        @if (session('status-success'))
            <div class="alert alert-success">
                {{ session('status-success') }}
            </div>
        @endif
        @if (session('status-fail'))
            <div class="alert alert-danger">
                {{ session('status-fail') }}
                @if ($errors->any())
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ preg_replace('/[0-9]||\./', '', $error) }}</li>
                        @endforeach
                    </ul>
                @endif
            </div>
        @endif
            <div class="modal fade" id="modalPaid" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">@lang('dashboard.payment')</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="" id="formJournal" method="post">
                            <div class="modal-body">
                                <div class="form-group row">
                                    <label for="" class="col-lg-3">@lang('dashboard.payment_date')</label>
                                    <div class="col-lg-9">
                                        <div class="input-group">
                                            <input type="text" required name="payment_date" onchange="changeHiddenValue('paymentdate', this)" onkeydown="return false;" autocomplete="off" class="form-control datepicker" placeholder="">
                                            <div class="input-group-append">
                                                <div class="input-group-text"><i class="fas fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="" class="col-lg-3">@lang('dashboard.payment_ref')</label>
                                    <div class="col-lg-9">
                                        <input type="text" required onchange="changeHiddenValue('paymentref', this)" name="payment_ref" class="form-control" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="" class="col-lg-3">@lang('dashboard.account')</label>
                                    <div class="col-lg-9">
                                        <select id="selectAccount" style="bottom: 18%; left: 50%;" onchange="changeHiddenValue('accountid', this)" oninvalid="check(this, 'Choose Account')" name="account_id" class="form-control" placeholder="" required>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('dashboard.close')</button>
                                <button type="submit" class="btn btn-primary">@lang('dashboard.save')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <form action="{{ route('customer.invoice.store') }}" id="formInvoice" enctype="multipart/form-data" method="post">
                <div class="row">
                    <div class="col-lg-12">
                        <div id="cardForm" class="card">
                            <div class="card-header">
                                @lang('dashboard.create_invoice')
                            </div>
                            <div class="card-body">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-lg-4">
                                        <label for="">Customer</label>
                                        <select id="selectCustomer" style="bottom: 18%; left: 50%;" oninvalid="check(this, 'Choose Customer')" name="customer_id" onchange="setCustomValidity('')" class="form-control" placeholder="" required>
                                            @if(old('customer_id'))
                                                <option value="{{old('customer_id')}}">{{ old('customer_name') }}</option>
                                            @elseif($data)
                                                <option value="{{ $customer['customer_id'] }}">{{ $customer['customer_name'] }}</option>
                                            @endif
                                        </select>
                                        <input type="hidden" name="customer_name" id="input_customer_name" value="{{ $customer ? $customer['customer_name'] : old('customer_name')}}" />
                                        @if ($errors->first('customer_id'))
                                            <small class="text-danger">{{ $errors->first('customer_id') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="">No {{ __('dashboard.quotation')  }}</label>
                                        <select name="quotation_id" id="selectQuotationCustomer" class="form-control">
                                            @if (isset($data['quotation_id']))
                                                <option value="{{ $data['quotation_id'] }}">{{ $data['quotation_name'] }}</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="">No Booking Confirmation</label>
                                        <select id="selectBc" style="bottom: 18%; left: 50%;"  name="bc_id" class="form-control" placeholder="" >
                                            @if(old('bc_id'))
                                                <option value="{{old('bc_id')}}">{{ old('bc_name') }}</option>
                                            @endif
                                            @if(isset($data['bc_id']))
                                                <option value="{{ $data['bc_id'] }}">{{ $data['bc_name'] }}</option>
                                            @endif
                                        </select>
                                        <input type="hidden" name="bc_name" id="input_bc_name" value="{{ isset($data['bc_name']) ? $data['bc_name'] : old('bc_name')}}" />
                                        @if ($errors->first('bc_id'))
                                            <small class="text-danger">{{ $errors->first('bc_id') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.term_of_payment')</label>
                                        <select name="term_of_payment" class="form-control">
                                            <option value="COD">COD</option>
                                            <option value="30 Hari">30 Hari</option>
                                            <option value="60 Hari">60 Hari</option>
                                            <option value="90 Hari">90 Hari</option>
                                        </select>
                                        @if ($errors->first('payment'))
                                            <small class="text-danger">{{ $errors->first('payment') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.invoice_date')</label>
                                        <div class="input-group">
                                            <input type="text" required autocomplete="off" id="invoiceDate" value="{{old('invoice_date') ?? date('d-m-Y') }}" class="form-control datepicker-invoice" onkeydown="return false;" name="invoice_date">
                                            <div class="input-group-append">
                                                <div class="input-group-text"><i class="fas fa-calendar"></i></div>
                                            </div>
                                        </div>
                                        @if ($errors->first('invoice_date'))
                                            <small class="text-danger">{{ $errors->first('invoice_date') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.due_date')</label>
                                        <div class="input-group">
                                            <input type="text" required autocomplete="off" id="dueDate" value="{{old('due_date')}}" class="form-control" onkeydown="return false;" name="due_date">
                                            <div class="input-group-append">
                                                <div class="input-group-text"><i class="fas fa-calendar"></i></div>
                                            </div>
                                        </div>
                                        @if ($errors->first('due_date'))
                                            <small class="text-danger">{{ $errors->first('due_date') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.type')</label>
                                        <div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadioInline1" name="is_invoice" value="1" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadioInline1">Invoice</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadioInline2" name="is_invoice" value="0" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadioInline2">Debit Note</label>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- <div class="form-group col-lg-4">
                                        <label for="">@lang('dashboard.term_of_payment')</label>
                                        <div class="input-group">
                                            <input type="number" id="termOfPayment" class="form-control" value="{{ old('term_of_payment') }}" name="term_of_payment">
                                            <div class="input-group-append">
                                                <div class="input-group-text">{{ __('dashboard.day') }}</div>
                                            </div>
                                        </div>
                                    </div> --}}

                                    {{-- <div class="form-group col-lg-4">
                                        <label for="">@lang('dashboard.created_at')</label>
                                        <v-datetimepicker name="created_time" oninput="console.log('ada')" value="{{ old('created_time') }}"></v-datetimepicker>
                                    </div> --}}
                                    @if ($data)
                                        <v-rate-curr
                                            :data="{{$data}}"
                                            lang="{{Auth::user()->language}}"
                                            styling="col-lg-12"
                                        />
                                    @else
                                        <v-rate-curr
                                            lang="{{Auth::user()->language}}"
                                            styling="col-lg-12"
                                        />
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div id="accordion">
                            <div class="card">
                                <div class="card-header text-right" id="headingForm">
                                    <button class="btn btn-link btn-sm" type="button" onclick="$('.collapse').collapse('toggle');" style="cursor: pointer;" data-toggle="collapse" role="button" aria-expanded="false" data-controls="collapseForm">
                                        {{ __('dashboard.show') }} <i class="fas fa-angle-down"></i>
                                   </button>
                                </div>
                                <div class="collapse" data-parent="#accordion" id="collapseForm" aria-labelledby="headingForm">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="form-group col-lg-3">
                                                <label for="">Attn</label>
                                                <input type="text" class="form-control" id="attnInput" name="attn" value="{{  $data['attn']?? '' }}">
                                            </div>
                                            <div class="form-group col-lg-3">
                                                <label for="">@lang('dashboard.vessel')</label>
                                                <input type="text" class="form-control" id="vesselInput" name="vessel" value="{{ $data['vessel'] ?? '' }}">
                                            </div>
                                            <div class="form-group col-lg-3">
                                                <label for="">@lang('dashboard.voyage')</label>
                                                <input type="text" class="form-control" id="voyageInput" name="voyage" value="{{ $data['voyage'] ?? '' }}">
                                            </div>
                                            <div class="form-group col-lg-3">
                                                <label for="">ETA</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control datepicker" id="etaInput" onkeydown="return false;" autocomplete="off" name="eta">
                                                    <div class="input-group-append">
                                                        <div class="input-group-text"><i class="fas fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-4">
                                                <label for="">B/L No</label>
                                                <input type="text" class="form-control" id="BlNoinput" name="bl_no" value="{{ $data['bl_no']?? '' }}">
                                            </div>
                                            <div class="form-group col-lg-4">
                                                <label for="">@lang('dashboard.from')</label>
                                                <input type="text" class="form-control" id="fromInput" name="from" value="{{ $data['from']?? '' }}">
                                            </div>
                                            <div class="form-group col-lg-4">
                                                <label for="">@lang('dashboard.to')</label>
                                                <input type="text" class="form-control" id="toInput" name="to" value="{{ $data['to']?? '' }}">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="card mt-4">
                            <div class="card-body">
                                @if ($data)
                                    <item-component
                                        :data="{{ $data }}"
                                        lang="{{ Auth::user()->language }}"
                                        url="{{ url('/select/item') }}">
                                    </item-component>
                                @else
                                    <item-component
                                        lang="{{ Auth::user()->language }}"
                                        url="{{ url('/select/item') }}">
                                    </item-component>
                                @endif
                            </div>
                        </div>
                    </div>
                    {{-- for paid action --}}
                    <input type="hidden" name="payment_date" id="paymentdate" value="">
                    <input type="hidden" name="payment_ref" id="paymentref" value="">
                    <input type="hidden" name="account_id" id="accountid" value="">
                    <input type="hidden" name="status" id="statusInv" value="">
                    <input type="submit" id="triggerSubmit" style="display:none;">
                    <div class="col-lg-8 col-xs-12">
                        <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                            <div class="btn-group" role="group">
                                <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ __('dashboard.save') }}
                                </button>
                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                    <button type="button" name="status" value="1" onclick="handleSubmit(this)" class="dropdown-item">Draft</button>
                                    <button type="button" name="status" value="2" onclick="handleSubmit(this)" class="dropdown-item">Publish</button>
                                    {{-- <button type="submit" name="status" value="1" class="dropdown-item">Draft</button>
                                    <button type="button"  onclick="handleSubmit(this)" name="status" value="2" class="dropdown-item">Publish</button> --}}
                                </div>
                            </div>
                        </div>
                        <i class="    "></i>
                        <a href="{{url('/customer/invoice') }}" class="btn btn-secondary">{{ __('dashboard.return') }}</a>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection


@section('javascript')
<script src="{{ asset('js/app.js') }}" defer></script>
<script>
var isModalOpen = false;


function check(input, word){
    if (input.value == "") {
     input.setCustomValidity(word);
   } else  {
     input.setCustomValidity('');
   }
}
function changeHiddenValue(el, e){
    document.getElementById(el).value = e.value
}
function handleSubmit(el){
    changeHiddenValue('statusInv', el)
    // $("#formInvoice").trigger('submit');
    $("#triggerSubmit").click();
}

$(document).ready(function () {
    $("#invoiceDate").change(function(){
        var invDate = moment($(this).val(), 'DD-MM-YYYY');
        $("#dueDate").datepicker('remove');
        $("#dueDate").datepicker({
            // startDate: invDate._d,
            autoclose: true,
            format: 'dd-mm-yyyy',
            language: 'id',
            weekStart: 1,
            orientation: 'bottom'
        });
    });

    $("#formJournal").submit(function(e){
        e.preventDefault();
        $("#formInvoice").trigger('submit');
    })

    $("#modalPaid").on('shown.bs.modal', function(){
        $("#formJournal")[0].reset();
    });
    $("#modalPaid").on('hidden.bs.modal', function(){
        isModalOpen = false;
        $("#paymentdate, #paymentref, #accountid").val('');
    });
    $("#selectAccount").select2({
        width: "100%",
        placeholder: "Account",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/account') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10,
                    lock: { kas: '1.1.01', bank: '1.1.02', sales: '4.0.00', receivable: '1.1.03' }

				};
            },

            cache: true,
        },
    });

    $("#formInvoice").submit(function(e){
        window.onbeforeunload = null;
        // check if status is paid
        if($("#paymentInvoice").val() == "2" && !isModalOpen && $("#statusInv").val() == 2){
            e.preventDefault();
            $("#modalPaid").modal('show');
            isModalOpen = true;

        }else{

            return;
        }
    })
    const date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());

    // $("#createdTime").val(moment().format('DD-MM-YYYY'));
    $('.datepicker').datepicker({
        autoclose: true,
        format: {
            toDisplay(date, format, language){
                var tgl = moment(date);
                return tgl.format('DD-MM-Y')
            },
            toValue(date, format, language){
                var tgl = moment(date);
                return tgl.format('Y-MM-DD')
            }
        },
        language: 'id',
        weekStart: 1,
        // startDate: today,
        orientation: 'bottom'
    });
    $('#dueDate').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        language: 'id',
        weekStart: 1,
        // startDate: today,
        orientation: 'bottom'
    });
    $('.datepicker-invoice').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        language: 'id',
        weekStart: 1,
        orientation: 'bottom'
    });

    $("#selectQuotationCustomer").select2({
        width: "100%",
        placeholder: "",
        allowClear: true,
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/qtt-customer') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
                    page_limit : 10,
                    customer_id: $("#selectCustomer").val()

				};
            },
            cache: true,
        },
    });



    $("#selectBc").select2({
        width: "100%",
        allowClear: true,
        placeholder: "",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/booking-confirmation') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
                    page_limit : 10,
                    quotation_id: $("#selectQuotationCustomer").val()
				};
            },

            cache: true,
        },
    })
    .on('select2:select', ({ params }) => {
        var $option = $("<option selected></option>").val(params.data.customer_id).text(params.data.customer);
        $("#selectCustomer").append($option).trigger('change');
    });
    $("#selectCustomer").select2({
        width: "100%",
        placeholder: "Choose Customers",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/customer') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        },
    });

    $("#termOfPayment").change(function(){
        var tgl = moment().add($(this).val(), 'day').format('DD-MM-YYYY');
        $("#dueDate").val(tgl);
    })

    $("#selectCustomer").on('change', function(){
        $("#input_customer_name").val($(this).select2('data')[0].text);
    })
    $("#selectBc").on('change', function(){
        $("#input_bc_name").val($(this).select2('data')[0].text);
        const id = $(this).val()
        if (id) {
            $.ajax({
                type: "get",
                url: "/select/customer-bc-detail/"+id,
                dataType: "json"
            })
            .then((res) => {
                $("#vesselInput").val(res.vessel)
                $("#voyageInput").val(res.voyage)
                $("#etaInput").val(moment(res.eta_port_of_discharge).format('DD-MM-Y'))
            })
        }
    })
    $("#selectQuotationCustomer").change(function() {
        const id = $(this).val()
        if (id) {
            $.ajax({
                type: "get",
                url: "/select/customer-qtt-detail/"+id,
                dataType: "json"
            })
            .then(res => {
                // console.log(res);
                $("#attnInput").val(res.attn)
                $("#BlNoinput").val(res.bl_no)
                $("#fromInput").val(res.from)
                $("#toInput").val(res.to)
            })
        }
    })

});
</script>
@endsection
