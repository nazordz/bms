@extends('dashboard.base')
@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
        @if (session('status-success'))
            <div class="alert alert-success">
                {{ session('status-success') }}
            </div>
        @endif
        @if (session('status-fail'))
            <div class="alert alert-danger">
                {{ session('status-fail') }}
            </div>
        @endif
            <form action="{{ route('customer.bc.update') }}" id="formBc" enctype="multipart/form-data" method="post">
                <input type="hidden" name="serial" id="serialInput" value="{{ $data->serial }}">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                Edit Booking Confirmation {{ $data->serializing }}
                            </div>
                            <div class="card-body">
                                @csrf
                                @method('put')
                                <input type="hidden" name="bc_id" value="{{ $data->bc_id }}">
                                <div class="row">
                                    <div class="form-group col-lg-3">
                                        <label for="">Customer</label>
                                        <select id="selectCustomer" name="customer_id" class="form-control" placeholder="" required>
                                            @if($data->customer)
                                                <option value="{{$data->customer->customer_id}}">{{ $data->customer->customer_name }}</option>
                                            @endif
                                        </select>
                                        <input type="hidden" name="customer_name" id="input_customer_name" value="{{$data->customer->customer_name}}" />
                                        @if ($errors->first('customer_id'))
                                            <small class="text-danger">{{ $errors->first('customer_id') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group col-lg-3">
                                        <label for="">No {{ __('dashboard.quotation')  }}</label>
                                        <select name="quotation_id" id="selectQuotation" class="form-control">
                                            @if ($data->quotation)
                                                <option value="{{ $data->quotation->quotation_id }}">QTT{{ $data->quotation->serializing }}</option>
                                            @endif
                                        </select>
                                        @if ($data->quotation)
                                            <input type="hidden" name="quotation_name" id="input_quotation_name" value="QTT{{ $data->quotation->serializing }}" />
                                        @else
                                            <input type="hidden" name="quotation_name" id="input_quotation_name" value="{{old('quotation_name')}}" />
                                        @endif

                                    </div>

                                    <div class="form-group col-lg-2">
                                        <label for="">Status</label>
                                        <select name="status" value="" class="form-control">
                                            <option value="1" {{ $data->status == 1 ? 'selected':'' }}>Proccess</option>
                                            <option value="2" {{ $data->status == 2 ? 'selected':'' }}>Shipping</option>
                                            <option value="3" {{ $data->status == 3 ? 'selected':'' }}>Shipped</option>
                                            <option value="0" {{ $data->status == 0 ? 'selected':'' }}>Cancel</option>
                                        </select>
                                        @if ($errors->first('status'))
                                            <small class="text-danger">{{ $errors->first('status') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-2">
                                        <label for="">Booking Ref</label>
                                        <select name="booking_ref" required class="form-control">
                                            <option value="BTMSIN" {{ $data->booking_ref == 'BTMSIN' ? 'selected': '' }}>BTMSIN</option>
                                            <option value="SINBTM" {{ $data->booking_ref == 'SINBTM' ? 'selected': '' }}>SINBTM</option>
                                            <option value="BTMIDN" {{ $data->booking_ref == 'BTMIDN' ? 'selected': '' }}>BTMIDN</option>
                                            <option value="IDNBTM" {{ $data->booking_ref == 'IDNBTM' ? 'selected': '' }}>IDNBTM</option>
                                            <option value="BTMTF" {{ $data->booking_ref == 'BTMTF' ? 'selected': '' }}>BTMTF</option>
                                            <option value="TFBTM" {{ $data->booking_ref == 'TFBTM' ? 'selected': '' }}>TFBTM</option>
                                        </select>
                                        @if ($errors->first('status'))
                                            <small class="text-danger">{{ $errors->first('status') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-2">
                                        <label for="">File</label>
                                        <input type="file" name="file" class="form-control-file">
                                        @if ($errors->first('file'))
                                            <small class="text-danger">{{ $errors->first('file') }}</small>
                                        @endif
                                        @if($data->file)
                                            <a class="font-sm" href="{{ $data->file }}" download>{{ __('dashboard.previous_file').' '.(explode('/', $data->file)[3]) }}</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">
                                Booking Data
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-lg-4">
                                        <label for="">Vessel</label>
                                        <input type="text" name="vessel" value="{{ $data->vessel }}" required id="inputVessel" class="form-control" value="{{old('vessel')}}" />
                                        @if ($errors->first('vessel'))
                                            <small class="text-danger">{{ $errors->first('vessel') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="">Voyage</label>
                                        <input type="text" name="voyage" value="{{ $data->voyage }}" required id="inputVoyage" class="form-control" value="{{old('voyage')}}" />
                                        @if ($errors->first('voyage'))
                                            <small class="text-danger">{{ $errors->first('voyage') }}</small>
                                        @endif
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Port of Loading</label>
                                            <input type="text" name="port_of_loading" value="{{ $data->port_of_loading }}" required class="form-control" value="{{old('port_of_loading')}}" />
                                            @if ($errors->first('port_of_loading'))
                                                <small class="text-danger">{{ $errors->first('port_of_loading') }}</small>
                                            @endif

                                        </div>
                                        <div class="form-group">
                                            <label for="">Port of Discharge</label>
                                            <input type="text" name="port_of_discharge" value="{{ $data->port_of_discharge }}" required class="form-control" value="{{old('port_of_discharge')}}" />
                                            @if ($errors->first('port_of_discharge'))
                                                <small class="text-danger">{{ $errors->first('port_of_discharge') }}</small>
                                            @endif
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="">ETD Port of Loading</label>
                                        <input type="text" name="etd_port_of_loading" value="{{ $data->etd_port_of_loading? date_format(date_create($data->etd_port_of_loading), 'd-m-Y') :'' }}" class="form-control datepicker" required />
                                        @if ($errors->first('etd_port_of_loading'))
                                            <small class="text-danger">{{ $errors->first('etd_port_of_loading') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="">ETA Port of Dischage</label>
                                        <input type="text" name="eta_port_of_discharge" value="{{ $data->eta_port_of_discharge? date_format(date_create($data->eta_port_of_discharge), 'd-m-Y') :'' }}" class="form-control datepicker" required />
                                        @if ($errors->first('eta_port_of_discharge'))
                                            <small class="text-danger">{{ $errors->first('eta_port_of_discharge') }}</small>
                                        @endif
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Sin Permit</label>
                                            <input type="number" min="0" max="99" name="sin_permit" value="{{ $data->sin_permit }}" class="form-control inputPermit" required value="{{old('sin_permit')}}" />
                                            @if ($errors->first('sin_permit'))
                                                <small class="text-danger">{{ $errors->first('sin_permit') }}</small>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="">Batam Permit</label>
                                            <input type="number" min="0" max="99" name="batam_permit" value="{{ $data->batam_permit }}" class="form-control inputPermit" required value="{{old('batam_permit')}}" />
                                            @if ($errors->first('batam_permit'))
                                                <small class="text-danger">{{ $errors->first('batam_permit') }}</small>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="">Consignee</label>
                                        <input type="text" name="consignee" required class="form-control" value="{{ $data? $data['consignee']: old('consignee')}}" />
                                        @if ($errors->first('consignee'))
                                            <small class="text-danger">{{ $errors->first('consignee') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="">Shipper</label>
                                        <input type="text" name="shipper" required class="form-control" value="{{ $data? $data['shipper']: old('shipper')}}" />
                                        @if ($errors->first('shipper'))
                                            <small class="text-danger">{{ $errors->first('shipper') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                <booking-component :rows="{{ $data->item }}"></booking-component>
                                <div class="mt-4">
                                    {{-- <div class="row">
                                        <label for="" class="col-lg-1 ml-3 mr-5">Total</label>
                                        <div class="col-lg-5 ml-4">
                                            <input type="text" name="total" value="{{ $data->total }}" class="form-control" placeholder="" required>
                                            <v-currency required="required" onchange="handleTotal(event)" :value="Number({{ $data->total }})"/>
                                        </div>
                                        <input type="hidden" name="total" value="{{ $data->total }}" id="totalInput">
                                    </div> --}}
                                    <div class="form-group col-lg-7 mt-2">
                                        <label for="">Special Remark</label>
                                        <textarea name="special_remark" class="form-control" cols="20" rows="4">{{ $data->special_remark }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-xs-12">
                        <button class="btn btn-primary" type="submit">{{ __('dashboard.save') }}</button>
                        <button data-id="{{ $data->bc_id }}" class="btn btn-success btn-print" type="button">{{ __('dashboard.print') }}</button>
                        <button type="button" class="btn btn-info btn-create-inv" data-bc_id="{{ $data->bc_id }}">@lang('dashboard.create_invoice')</button>
                        <button type="button" class="btn btn-warning text-white btn-create-bc" data-bc_id="{{ $data->bc_id }}">@lang('dashboard.create_bc')</button>
                        <a href="{{url('/customer/booking-confirmation') }}" class="btn btn-secondary">{{ __('dashboard.return') }}</a>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection


@section('javascript')
<script src="{{ asset('js/app.js') }}" defer></script>
<script>
function handleTotal(e){
    if(event.detail){
        $("#totalInput").val(event.detail.numberValue)
    }
}
function getBookingRef(code){
    var res = '';
    switch (Number(code)) {
        case 1:
            res = 'BTMSIN';
            break;
        case 2:
            res = 'SINBTM';
            break;
        case 3:
            res = 'BTMIDN';
            break;
        case 4:
            res = 'IDNBTM';
            break;
        case 5:
            res = 'BTMTF';
            break;
        case 6:
            res = 'TFBTM';
            break;
    }
    return res;
}
$(document).ready(function () {
    $('.btn-create-bc').click(function(){
        // window.onbeforeunload = null;
        let customer  = {
            customer_id: $("#selectCustomer").val(),
            customer_name: $("#input_customer_name").val()
        }
        customer = $.param(customer)
        // let item = $.param(items);
        let form = $.param({
            quotation_id: $("#selectQuotation").val(),
            serial: String($("#input_quotation_name").val()).replace('QTT', ''),
            vessel: $("#inputVessel").val(),
            voyage: $("#inputVoyage").val(),
            items : window.items,
            copy_form: 'true'
        });
        location = `/customer/create-booking-confirmation?${customer}&${form}`;
    });
    $('.btn-create-inv').click(function(e){
        e.preventDefault();

        // window.onbeforeunload = null;
        let bc = {
            bc_id: $(this).data('bc_id'),
            bc_name: 'BC'+$('#serialInput').val()
        };
        bc = $.param(bc);
        let customer  = {
            customer_id: $("#selectCustomer").val(),
            customer_name: $("#input_customer_name").val()
        };
        customer = $.param(customer);

        var serial = $("#input_quotation_name").val();
        let quotation = {
            quotation_id: $("#selectQuotation").val(),
            quotation_name: serial
        };
        quotation = $.param(quotation);
        let form = $.param({
            vessel: $("#inputVessel").val(),
            voyage: $("#inputVoyage").val(),
            serial: $("#inputSerial").val()
        });
        location = `/customer/create-invoice?${bc}&${customer}&${quotation}&${form}`;
    });

    $(".btn-print").click(function(){
        // window.onbeforeunload = null;
        // location = '/customer/print-booking-confirmation/'+$(this).data('id');
        window.open('/customer/print-booking-confirmation/'+$(this).data('id'), '_blank');
        // window.onbeforeunload = function() {
        //     return 'Are you sure that you want to leave this page?';
        // };
    });

    // $("#formBc").submit(function(){
    //     window.onbeforeunload = null;
    //     return;
    // })
    $(".inputPermit").keyup(function(){
        if($(this).val() > 99){
            $(this).val(99);
        }
    })
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        language: 'id',
        weekStart: 1,
    });

    $("#selectCustomer").select2({
        width: "100%",
        placeholder: "Customers",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/customer') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        },
    });
    $("#selectQuotation").select2({
        width: "100%",
        allowClear: true,
        placeholder: "Choose",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/qtt-customer') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },
            cache: true,
        },
    });
    $("#selectCustomer").on('change', function(){
        $("#input_customer_name").val($(this).select2('data')[0].text);
    })
    $("#selectQuotation").on('change', function(){
        $("#input_quotation_name").val($(this).select2('data')[0].text);
    })

});
</script>
@endsection
