@extends('dashboard.base')

@section('content')

    <div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('dashboard.Item_List')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">@lang('dashboard.first_name')</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="input_first_name"></div>

                                    <div class="col-lg-4">@lang('dashboard.last_name')</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="input_last_name"></div>

                                    <div class="col-lg-4">@lang('dashboard.company')</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="input_company"></div>

                                    <div class="col-lg-4">@lang('dashboard.province')</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="input_province"></div>

                                    <div class="col-lg-4">@lang('dashboard.regency')</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="input_regency"></div>

                                    <div class="col-lg-4">@lang('dashboard.village')</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="input_village"></div>

                                    <div class="col-lg-4">@lang('dashboard.address')</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="input_address"></div>

                                    <div class="col-lg-4">Email</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="input_email"></div>

                                    <div class="col-lg-4">@lang('dashboard.phone')</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="input_phone"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('dashboard.close')</button>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="animated fadeIn">
            @if (session('status-success'))
                <div class="alert alert-success">
                    {{ session('status-success') }}
                </div>
            @endif
            @if (session('status-fail'))
                <div class="alert alert-danger">
                    {{ session('status-fail') }}
                </div>
            @endif
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-users"></i> Purchase Order
                        </div>
                        <div class="card-body">
                            <div class="col-lg-12 mb-5">
                                <a href="{{ url('/customer/create-purchase-order') }}" class="btn btn-primary pull-right" > Tambah PO </a>
                            </div>
                            <table id="tbPo" class="table table-responsive-lg table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>NO PO</th>
                                        <th>Customer</th>
                                        <th>@lang('dashboard.term_and_condition')</th>
                                        <th>status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('javascript')
<script>
$(document).ready(function () {

    var myTable = $("#tbPo").DataTable({
        responsive: true,
        processing : true,
        serverSide : true,
        order      : [[2,"ASC"]],
        autoWidth  : false,
        searchDelay: 500,
        ajax : {
            url : "{{ url('/customer/table-purchase-order') }}"
        },
        columns:[
            {
                data: null,
                orderable  : false,
				searchable : false
            },
            {
                data : 'po_id',
                name : 'purchase_order_customers.po_id',
                render(data, type, row){
                    return `<a href="/customer/show-purchase-order/${row.uuid}" class="stretched-link">PO${data}</a>`;
                }
            },
            { data : 'company', name : 'customers.company' },
            { data : 'term_and_condition', name : 'purchase_order_customers.term_and_condition' },
            {
                data : 'status',
                name : 'purchase_order_customers.status',
                render(data){
                    var status = 'void';
                    switch (data) {
                        case '0':
                            status = 'void';
                            break;
                        case '1':
                            status = 'shipping';
                            break;
                        case '2':
                            status = 'delivered';
                            break;
                        default:
                            break;
                    }
                    return  status;
                },
            }

        ],
        fnCreatedRow(row, data, index) {
			if (myTable.page.info().start >= 10) {
				var panjang = myTable.page.info().length;
				var halaman = myTable.page.info().page;
				var i = 1;
				i = (halaman + 1) * panjang;
				i -= panjang - 1;
	        }else{
	        	var i = 1;
	        }
	        $('td', row).eq(0).html(index + i);
		},
        drawCallback(){
            $(".dataTables_length select").removeClass("form-control-sm");
            $(".btn-delete").click(function(){
                var customer_id = $(this).data('customer_id');
                Swal.fire({
                    title: '{{__("dashboard.are_you_sure")}}',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: '{{ __("dashboard.cancel") }}',
                    confirmButtonText: '{{__("dashboard.confirm_delete")}}'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: "post",
                            url: "{{ route('customer.delete') }}",
                            data: { _token: "{{csrf_token()}}", _method:'delete',  customer_id},
                            dataType: "json"
                        }).done(function(data){
                            myTable.draw();
                            Swal.fire( '{{__("dashboard.deleted")}}', '', 'success' );
                        }).catch(function(err){
                            console.log(err);
                        });
                    }
                });
            });
            $(".btn-detail").click(function(){
                const customer_id = $(this).data('customer_id');
                $.ajax({
                    type: "get",
                    url: "{{url('customer/show-customer')}}/"+customer_id,
                    dataType: "json"
                })
                .done(function(data){
                    console.log(data);
                    $("#input_first_name").text(data.first_name);
                    $("#input_last_name").text(data.last_name);
                    $("#input_company").text(data.company);
                    $("#input_province").text(data.province.name);
                    $("#input_regency").text(data.regency.name);
                    $("#input_village").text(data.village.name);
                    $("#input_address").text(data.address);
                    $("#input_email").text(data.email)
                    $("#input_phone").text(data.phone)
                    $("#modalDetail").modal('show');
                });
            });

            $(".btn-detail").on('hidden.bs.modal', function(e){
                $("#item_picture").css('display', 'none')
                $(".info-detail").empty();
            })

		}
    })
});
</script>
@endsection

