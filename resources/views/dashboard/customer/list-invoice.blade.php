@extends('dashboard.base')

@section('content')

    <div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('dashboard.quotation')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">Customer</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="input_customer"></div>

                                    <div class="col-lg-4">Sales</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="input_sales"></div>

                                    <div class="col-lg-4">@lang('dashboard.term_and_condition')</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="input_term_and_condition"></div>

                                    <div class="col-lg-4">@lang('dashboard.valid_until')</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="input_valid_until"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('dashboard.close')</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalExport" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Export</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" id="printTable">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                          <label for="">@lang('dashboard.start_date')</label>
                          <div class="input-group">
                                <input type="text" name="start_date" autocomplete="off" id="start_date_print" class="form-control datepicker" placeholder="">
                                <div class="input-group-append">
                                    <div class="input-group-text"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                                </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="">@lang('dashboard.to_date')</label>
                          <div class="input-group">
                                <input type="text" name="to_date" autocomplete="off" id="to_date_print" class="form-control datepicker" placeholder="">
                                <div class="input-group-append">
                                    <div class="input-group-text"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                                </div>
                          </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('dashboard.close')</button>
                        <button type="submit" class="btn btn-primary">@lang('dashboard.print')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="animated fadeIn">
            @if (session('status-success'))
                <div class="alert alert-success">
                    {{ session('status-success') }}
                </div>
            @endif
            @if (session('status-fail'))
                <div class="alert alert-danger">
                    {{ session('status-fail') }}
                </div>
            @endif
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <ul class="nav nav-pills">
                                <li class="nav-item">
                                    <a class="nav-link active filterStatus" data-status="4" href="#">@lang('dashboard.posted')</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link filterStatus" data-status="1" href="#">Draft</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link filterStatus" data-status="2" href="#">Publish</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link filterStatus" data-status="0" href="#">Void</a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="col-lg-12 mb-5 pr-0">
                                <a href="{{ url('/customer/create-invoice') }}" class="btn btn-primary" > {{ __('dashboard.create') }}</a>
                                <div class="pull-right col-lg-6 mt-4 mt-lg-0">
                                    <div class="row pull-right">
                                        <div class="col-lg-6 form-inline px-0">
                                            <div class="form-group offset-lg-2 ">
                                                <div class="col-lg-8">
                                                    <input type="text" name="start_date" value="" autocomplete="off" id="start_date_input" placeholder="@lang('dashboard.start_date')" class="form-control datepicker">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 form-inline px-0">
                                            <div class="form-group offset-lg-2 ">
                                                <div class="col-lg-8">
                                                    <input type="text" name="to_date" value="" autocomplete="off" id="to_date_input" placeholder="@lang('dashboard.to_date')" class="form-control datepicker">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <table id="tbQuotation" class="table table-responsive-lg table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>@lang('dashboard.code')</th>
                                        <th>{{ __('dashboard.company') }}</th>
                                        <th>Customer</th>
                                        <th>Publish</th>
                                        <th>Status</th>
                                        <th>@lang('dashboard.type')</th>
                                        <th>{{ __('dashboard.created_at') }}</th>
                                        <th>{{ __('dashboard.action') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('javascript')
<script>

$(document).ready(function () {
    var status = 4;

    const date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $('.datepicker').datepicker({
        autoclose: true,
        format: {
            toDisplay(date, format, language){
                var tgl = moment(date);
                return tgl.format('DD-MM-Y')
            },
            toValue(date, format, language){
                var tgl = moment(date);
                return tgl.format('Y-MM-DD')
            }
        },
        language: 'id',
        weekStart: 1,
        orientation: 'bottom'
        // startDate: today
    });

    $(".datepicker").change(function(){
        myTable.draw();
    })
    var myTable = $("#tbQuotation").DataTable({
        responsive: true,
        processing : true,
        serverSide : true,
        order      : [[7,"DESC"]],
        autoWidth  : false,
        searchDelay: 500,
        ajax : {
            url : "{{ url('/customer/table-invoice') }}",
            data(data){
                return {
                    ...data,
                    status,
                    start_date: $("#start_date_input").val(),
                    to_date: $("#to_date_input").val(),
                }

            }
        },
        columns:[
            {
                data: null,
                orderable  : false,
				searchable : false
            },
            {
                data: 'serial', name: 'invoice_customers.serial',
                render(data, type, row){
                    return `<a href="/customer/show-invoice/${row.uuid}">${(row.is_invoice ? 'INV':'DN')+serializing(row.invoice_date, row.serial)}</a>`;
                }
            },
            { data: 'company', name: 'customers.company' },
            {
                data : 'first_name',
                name : 'customers.first_name',
                render(data, type, row){
                    return `${data} ${row.last_name}`
                }
            },
            {
                data: 'status', name: 'invoice_customers.status',
                render(data){
                    var res = '';
                    switch (Number(data)) {
                        case 1:
                            res = '<div class="alert-tb alert-warning">Draft</div>';
                            break;
                        case 2:
                            res = '<div class="alert-tb alert-success">Publish</div>';
                            break;
                        default:
                            res = '<div class="alert-tb alert-danger">Void</div>';
                            break;
                    }
                    return res;
                }
            },
            {
                data: 'payment', name: 'invoice_customers.payment',
                render(data, type, row){
                    var res = '';
                    if(Number(row.status) == 2){
                        switch (Number(data)) {
                            case 1:
                                res = '<div class="alert-tb alert-warning">Unpaid</div>';
                                break;
                            case 2:
                                res = '<div class="alert-tb alert-success">Paid</div>';
                                break;
                            default:
                            res = '<div class="alert-tb alert-danger">Void</div>'
                        }
                    }
                    return res;
                }
            },
            {
                data: 'is_invoice', name: 'invoice_customers.is_invoice',
                render(data) {
                    return Boolean(data) ? 'Invoice' : 'Debit Note'
                }
            },
            {
                data : 'created_at',
                name : 'invoice_customers.created_at',
                render(data){
                    var date = moment(data).locale('id');
                    return date.format('dddd, DD-MM-Y');
                }
            },
            {
                data: null,
                searchable:false,
                orderable : false,
                render(data, type, row){
                    // if(row.payment != "2" || (row.status == "1" && row.payment == "2")){
                    if(Number(row.status) == 1){
                        return ` <button class="btn btn-danger btn-delete btn-sm" data-type="delete" data-invoice_id="${row.invoice_id}"><i class="fa fa-times"></i> Cancel</button>`
                    }else if(Number(row.status) == 2){
                        return ` <button class="btn btn-danger btn-delete btn-sm" data-type="void" data-invoice_id="${row.invoice_id}"><i class="fa fa-times"></i> Void</button>`
                    }else{
                        return ``;
                    }
                }
            }
        ],
        fnCreatedRow(row, data, index) {
			if (myTable.page.info().start >= 10) {
				var panjang = myTable.page.info().length;
				var halaman = myTable.page.info().page;
				var i = 1;
				i = (halaman + 1) * panjang;
				i -= panjang - 1;
	        }else{
	        	var i = 1;
	        }
	        $('td', row).eq(0).html(index + i);
		},
        drawCallback(){
            $(".dataTables_length select").removeClass("form-control-sm");
            $(".btn-delete").click(function(){
                var invoice_id = $(this).data('invoice_id');
                var type = $(this).data('type');
                Swal.fire({
                    title: '{{__("dashboard.are_you_sure")}}',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    input: 'textarea',
                    inputPlaceholder: '{{__("dashboard.reason")}}',
                    inputAttributes: {
                        'aria-label': '{{__("dashboard.reason")}}...'
                    },
                    cancelButtonColor: '#d33',
                    cancelButtonText: '{{ __("dashboard.cancel") }}',
                    confirmButtonText: 'Okay'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: "post",
                            url: "{{ route('customer.invoice.void') }}",
                            data: { _token: "{{csrf_token()}}", _method:'patch',  invoice_id, type, reason: result.value},
                            dataType: "json"
                        }).done(function(data){
                            myTable.draw();
                            Swal.fire( 'Invoice Voided', '', 'success' );
                        }).catch(function(err){
                            console.log(err);
                        });
                    }
                });
            });

            $("#printTable").submit(function(e){
                e.preventDefault();
                const start = $("#start_date_print").val();
                const to    = $("#to_date_print").val();
                $("#printTable")[0].reset()
                $("#modalExport").modal('hide');
                // location = `/customer/export-quotation/${start}/${to}`;
                window.open(`/customer/export-quotation/${start}/${to}`, '_blank');
            })

            $(".btn-detail").click(function(){
                const quotation_id = $(this).data('quotation_id');
                $.ajax({
                    type: "get",
                    url: "{{url('customer/show-quotation')}}/"+quotation_id,
                    dataType: "json"
                })
                .done(function(data){
                    $("#input_customer").text(data.customer.first_name+' '+data.customer.last_name);
                    $("#input_sales").text(data.sales);
                    $("#input_term_and_condition").text(data.term_and_condition);

                    var date = moment(data.valid_until).locale('id');
                    $("#input_valid_until").text(date.format('DD-MM-Y'));
                    $("#modalDetail").modal('show');
                });
            });

		}
    });

    $(".filterStatus").click(function(e){
        e.preventDefault();
        status = $(this).data('status');
        $('.nav-link').removeClass('active')
        $(this).addClass('active');
        myTable.draw();
    });
});
</script>
@endsection

