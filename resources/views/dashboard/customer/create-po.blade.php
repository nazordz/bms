@extends('dashboard.base')
@section('content')

    <!-- Modal -->
    <div class="modal fade" id="modalCategory" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('dashboard.addCategory')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="post" id="createCategory">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">{{ __('dashboard.name') }}</label>
                            <input type="text" name="name" id="inputNameCategory" class="form-control" placeholder="" aria-describedby="helpId">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="submitCategory" class="btn btn-primary">{{ __('dashboard.save') }}</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('dashboard.return') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="animated fadeIn">
        @if (session('status-success'))
            <div class="alert alert-success">
                {{ session('status-success') }}
            </div>
        @endif
        @if (session('status-fail'))
            <div class="alert alert-danger">
                {{ session('status-fail') }}
            </div>
        @endif
            <form action="{{ route('customer.po.store') }}" enctype="multipart/form-data" method="post">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                @lang('dashboard.create_po')
                            </div>
                            <div class="card-body">
                                @csrf
                                @if(request()->has('quotation'))
                                    <input type="hidden" name="quotation_id" value="{{ request()->quotation }}">
                                @endif
                                <div class="row">
                                    <div class="form-group col-lg-4">
                                        <label for="">Customer</label>
                                        <select id="selectCustomer" name="customer_id" class="form-control" placeholder="" required>
                                            @if(old('customer_id'))
                                                <option value="{{old('customer_id')}}">{{ old('customer_name') }}</option>
                                            @elseif($data)
                                                <option value="{{ $customer['customer_id'] }}">{{ $customer['customer_name'] }}</option>
                                            @endif
                                        </select>
                                        <input type="hidden" name="customer_name" id="input_customer_name" value="{{old('customer_name')}}" />
                                        @if ($errors->first('customer_id'))
                                            <small class="text-danger">{{ $errors->first('customer_id') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="">@lang('dashboard.shipped')</label>
                                        <select name="status" id="" class="form-control">
                                            <option value="1">Shipping</option>
                                            <option value="2">Delivered</option>
                                            <option value="0">Void</option>
                                        </select>
                                        @if ($errors->first('status'))
                                            <small class="text-danger">{{ $errors->first('status') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="">@lang('dashboard.image_po')</label>
                                        <div class="input-group">
                                            <input type="file" name="file" class="form-control-file">
                                        </div>
                                        @if ($errors->first('file'))
                                            <small class="text-danger">{{ $errors->first('file') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                @if (request()->has('quotation'))
                                    <item-component
                                        :data="{{ $data }}"
                                        url="{{ url('/select/item') }}"
                                        lang="{{ Auth::user()->language }}">
                                    </item-component>
                                @else
                                    <item-component
                                        description="@lang('dashboard.description')"
                                        lang="{{ Auth::user()->language }}">
                                    </item-component>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-xs-12">
                        <button class="btn btn-success" type="submit">{{ __('dashboard.save') }}</button>
                        <a href="{{url('/customer/purchase-order') }}" class="btn btn-secondary">{{ __('dashboard.return') }}</a>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection


@section('javascript')
<script src="{{ asset('js/app.js') }}" defer></script>

<script>

$(document).ready(function () {
    /*window.onbeforeunload = function() {
        return 'Are you sure that you want to leave this page?';
    };*/

    const date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $('.datepicker').datepicker({
autoclose: true,
        format: 'dd-mm-yyyy',
        language: 'id',
        weekStart: 1,
        startDate: today
    });

    $("#selectCustomer").select2({
        width: "100%",
        placeholder: "Customers",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/customer') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        },
    });

    $("#selectCustomer").on('change', function(){
        $("#input_customer_name").val($(this).select2('data')[0].text);
    })

});
</script>
@endsection
