@extends('dashboard.base')
@section('content')

    <!-- Modal -->
    <div class="modal fade" id="modalCategory" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('dashboard.addCategory')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="post" id="createCategory">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">{{ __('dashboard.name') }}</label>
                            <input type="text" name="name" id="inputNameCategory" class="form-control" placeholder="" aria-describedby="helpId">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="submitCategory" class="btn btn-primary">{{ __('dashboard.save') }}</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('dashboard.return') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="animated fadeIn">
        @if (session('status-success'))
            <div class="alert alert-success">
                {{ session('status-success') }}
            </div>
        @endif
        @if (session('status-fail'))
            <div class="alert alert-danger">
                {{ session('status-fail') }}
            </div>
        @endif
            <form action="{{ route('customer.quotation.update') }}" id="formQuotation" enctype="multipart/form-data" method="post">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-header-split">
                                    <div class="">
                                        {{-- @lang('dashboard.quotation') QTT{{ serializing($data->quotation_date,$data->serial) }} --}}
                                        @lang('dashboard.quotation') QTT{{ $data->serializing }}
                                        <input type="hidden" name="serial" id="inputSerial" value="{{ $data->serial }}">
                                    </div>
                                    @if ($data->status == 0)
                                        <div class="alert-header alert-danger">Void</div>
                                    @elseif($data->status == 1)
                                        <div class="alert-header alert-warning">Draft</div>
                                    @elseif($data->status == 2)
                                        <div class="alert-header alert-success">Publish</div>
                                    @endif
                                </div>
                            </div>
                            <div class="card-body">
                                @method('put')
                                @csrf
                                <input type="hidden" name="quotation_id" value="{{ $data->quotation_id }}">
                                <div class="row">
                                    <div class="form-group col-lg-3">
                                        <label for="">Customer</label>
                                        <select id="selectCustomer" style="bottom: 18%; left: 50%;" onchange="setCustomValidity('')" oninvalid="check(this, 'Choose Customer')" name="customer_id" class="form-control" {{ ($data->status == 0) ? 'disabled' : '' }} placeholder="" required>
                                            @if($data->customer)
                                                <option value="{{ $data->customer->customer_id }}"> {{ ($data->customer->company? $data->customer->company.' - ': '').$data->customer->first_name.' '.$data->customer->last_name }}  </option>
                                            @endif
                                        </select>
                                        @if ($data)
                                            <input type="hidden" name="customer_name" id="input_customer_name" value="{{ $data->customer->company.' - '.$data->customer->first_name.' '.$data->customer->last_name }}" />
                                        @endif
                                        @if ($errors->first('customer_id'))
                                            <small class="text-danger">{{ $errors->first('customer_id') }}</small>
                                        @endif
                                    </div>

                                    {{-- <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.sales')</label>
                                        <select id="selectSeller" name="seller_id" style="bottom: 18%; left: 50%;" class="form-control" {{ ($data->status == 0) ? 'disabled' : '' }} placeholder="">
                                            @if($data->seller)
                                                <option value="{{$data->seller->id}}">{{ $data->seller->name }}</option>
                                            @endif
                                        </select>
                                        <input type="hidden" name="saller_name" id="input_seller_name" value="{{$data->seller? $data->seller->name : ''}}" />
                                        @if ($errors->first('seller_id'))
                                            <small class="text-danger">{{ $errors->first('seller_id') }}</small>
                                        @endif
                                    </div> --}}
                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.term_of_payment')</label>
                                        <select name="term_of_payment" class="form-control">
                                            <option {{ $data->term_of_payment == 'COD' ? 'selected':'' }} value="COD">COD</option>
                                            <option {{ $data->term_of_payment == '30 Hari' ? 'selected':'' }} value="30 Hari">30 Hari</option>
                                            <option {{ $data->term_of_payment == '60 Hari' ? 'selected':'' }} value="60 Hari">60 Hari</option>
                                            <option {{ $data->term_of_payment == '90 Hari' ? 'selected':'' }} value="90 Hari">90 Hari</option>
                                        </select>
                                        @if ($errors->first('payment'))
                                            <small class="text-danger">{{ $errors->first('payment') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.quotation_date')</label>
                                        <div class="input-group">
                                            <input type="text" {{ ($data->status == 0) ? 'disabled' : '' }} name="quotation_date" id="quotation_date" value="{{date_format(date_create($data->quotation_date), 'd-m-Y') }}" onkeydown="return false;" autocomplete="off" required class="form-control datepicker" placeholder="">
                                            <div class="input-group-append">
                                                <div class="input-group-text"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                                            </div>
                                        </div>
                                        @if ($errors->first('quotation_date'))
                                            <small class="text-danger">{{ $errors->first('quotation_date') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.valid_until')</label>
                                        <div class="input-group">
                                            <input type="text" {{ ($data->status == 0) ? 'disabled' : '' }} name="valid_until" id="dueDate" value="{{date_format(date_create($data->valid_until), 'd-m-Y') }}" onkeydown="return false;" autocomplete="off" required class="form-control datepicker" placeholder="">
                                            <div class="input-group-append">
                                                <div class="input-group-text"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                                            </div>
                                        </div>
                                        @if ($errors->first('valid_until'))
                                            <small class="text-danger">{{ $errors->first('valid_until') }}</small>
                                        @endif
                                    </div>

                                    {{--<div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.term_of_payment')</label>
                                        <div class="input-group">
                                            <input type="number" id="termOfPayment" value="{{$data->term_of_payment}}" class="form-control" {{ ($data->status == 0) ? 'disabled' : '' }} name="term_of_payment">
                                            <div class="input-group-append">
                                                <div class="input-group-text">{{ __('dashboard.day') }}</div>
                                            </div>
                                        </div>
                                    </div> --}}
                                    {{-- <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.created_at')</label>
                                        <v-datetimepicker {{ ($data->status == 0) ? 'disabled' : '' }} name="created_time" value="{{ old('created_time') }}"></v-datetimepicker>
                                    </div> --}}
                                    <v-rate-curr
                                        :data="{{$data}}"
                                        lang="{{Auth::user()->language}}"
                                        styling="col-lg-9"
                                        :disabled="{{ ($data->status == 0) ? 'true' : 'false' }}"
                                    />
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                <div class="row">

                                    <div class="form-group col-lg-3">
                                        <label for="">Attn</label>
                                        <input type="text" class="form-control" id="inputAttn" name="attn" required {{ ($data->status == 0) ? 'disabled' : '' }} value="{{ $data->attn }}">
                                    </div>
{{--
                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.vessel')</label>
                                        <input type="text" class="form-control" id="inputVessel" name="vessel" required {{ ($data->status == 0) ? 'disabled' : '' }} value="{{ $data->vessel }}">
                                    </div>

                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.voyage')</label>
                                        <input type="text" class="form-control" id="inputVoyage" name="voyage" required {{ ($data->status == 0) ? 'disabled' : '' }} value="{{ $data->voyage }}">
                                    </div>

                                    <div class="form-group col-lg-3">
                                        <label for="">ETA</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control datepicker" onkeydown="return false;" name="eta" required {{ ($data->status == 0) ? 'disabled' : '' }} value="{{date_format(date_create($data->eta), 'd-m-Y') }}">
                                            <div class="input-group-append">
                                                <div class="input-group-text"><i class="fas fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-4">
                                        <label for="">B/L No</label>
                                        <input type="text" class="form-control" id="inputBl_no" name="bl_no" required {{ ($data->status == 0) ? 'disabled' : '' }} value="{{ $data->bl_no }}">
                                    </div> --}}

                                    <div class="form-group col-lg-4">
                                        <label for="">@lang('dashboard.from')</label>
                                        <input type="text" class="form-control" id="inputFrom" name="from" required {{ ($data->status == 0) ? 'disabled' : '' }} value="{{ $data->from }}">
                                    </div>

                                    <div class="form-group col-lg-4">
                                        <label for="">@lang('dashboard.to')</label>
                                        <input type="text" class="form-control" id="inputTo" name="to" required {{ ($data->status == 0) ? 'disabled' : '' }} value="{{ $data->to }}">
                                    </div>

                                </div>

                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                <item-component
                                    :data="{{$data}}"
                                    url="{{ url('/select/item') }}"
                                    lang="{{ Auth::user()->language }}">
                                </item-component>
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-8 col-xs-12">
                        @if ($data->status)
                            @if ($data->status == 1)
                                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                    <div class="btn-group" role="group">
                                        <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{ __('dashboard.save') }}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                            <button type="submit" name="status" value="1" class="dropdown-item">Draft</button>
                                            <button type="submit" name="status" value="2" class="dropdown-item">Publish</button>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <button type="submit" name="status" value="2" class="btn btn-primary">@lang('dashboard.save')</button>
                                <button type="button" class="btn btn-dark btn-create-qtt" data-quotation="{{$data->quotation_id}}">@lang('dashboard.create_quotation')</button>
                                <button type="button" class="btn btn-warning btn-create-bc text-white" data-quotation="{{$data->quotation_id}}">@lang('dashboard.create_booking_confirmation')</button>
                                <button type="button" class="btn btn-primary btn-create" data-quotation="{{$data->quotation_id}}">@lang('dashboard.create_invoice')</button>

                            @endif
                            <button class="btn btn-success btn-print" data-id="{{$data->uuid}}" type="button">{{ __('dashboard.print') }}</button>
                            {{-- @if (count($data->booking_confirmation))
                                <div class="btn-group" role="group">
                                    <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        @lang('dashboard.see_booking_confirmation')
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                        @foreach ($data->booking_confirmation as $item)
                                            <a class="dropdown-item" href="{{ url('customer/show-booking-confirmation/'.$item->uuid) }}">BC{{ $item->serial }}</a>
                                        @endforeach
                                        <a class="dropdown-item btn-create-bc" href="" data-quotation="{{ $data->quotation_id }}">@lang('dashboard.create')</a>
                                    </div>
                                </div>
                            @else --}}
                                {{-- <button type="button" class="btn btn-secondary btn-create-bc" data-quotation="{{$data->quotation_id}}">@lang('dashboard.create_booking_confirmation')</button> --}}
                            {{-- @endif
                             --}}

                            {{-- @if (count($data->invoice))
                                <div class="btn-group" role="group">
                                    <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        @lang('dashboard.see_invoice')
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                        @foreach ($data->invoice as $item)
                                            <a class="dropdown-item" href="{{ url('customer/show-invoice/'.$item->uuid) }}">INV{{ $item->serial }}</a>
                                        @endforeach
                                        <a class="dropdown-item btn-create" href="#" data-quotation="{{ $data->quotation_id }}">@lang('dashboard.create')</a>
                                    </div>
                                </div>
                            @else --}}
                                {{-- <button type="button" class="btn btn-secondary btn-create" data-quotation="{{$data->quotation_id}}">@lang('dashboard.create_invoice')</button> --}}
                            {{-- @endif --}}

                        @endif
                        <a href="{{url('/customer/list-quotation') }}" class="btn btn-secondary">{{ __('dashboard.return') }}</a>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection


@section('javascript')
<script src="{{ asset('js/app.js') }}" defer></script>
<script>
function check(input, word){
    if (input.value == "") {
     input.setCustomValidity(word);
   } else  {
     input.setCustomValidity('');
   }
}
$(document).ready(function () {
    /*window.onbeforeunload = function() {
        return 'Are you sure that you want to leave this page?';
    };*/
    $("#formQuotation").submit(function(){
        window.onbeforeunload = null;
        return;
    })

    $("#quotation_date").change(function(){
        var qttDate = moment($(this).val(), 'DD-MM-YYYY');
        // qttDate = qttDate.format('YYYY-MM-DD');
        console.log(qttDate);
        // $("#dueDate").datepicker('setStartDate', qttDate);

        $("#dueDate").datepicker('remove');
        $("#dueDate").datepicker({
            startDate: qttDate._d,
            autoclose: true,
            format: 'dd-mm-yyyy',
            language: 'id',
            weekStart: 1,
            orientation: 'bottom'
        });
    });

    $(".btn-print").click(function(){
        window.onbeforeunload = null;
        window.open('/customer/print-quotation/'+$(this).data('id'), '_blank');
        // window.onbeforeunload = function() {
        //     return 'Are you sure that you want to leave this page?';
        // };
    });
    $('.btn-create-qtt').click(function(e){
        e.preventDefault();
        window.onbeforeunload = null;
        let customer  = {
            customer_id: $("#selectCustomer").val(),
            customer_name: $("#input_customer_name").val()
        }
        customer = $.param(customer)
        let item = $.param(items);
        let form = $.param({
            attn          : $("#inputAttn").val(),
            vessel        : $("#inputVessel").val(),
            voyage        : $("#inputVoyage").val(),
            quotation_name: 'QTT'+$("#inputSerial").val(),
            bl_no         : $("#inputBl_no").val(),
            from          : $("#inputFrom").val(),
            to            : $("#inputTo").val(),
            copy_form: 'true'
        });
        location = `/customer/create-quotation?${customer}&${item}&${form}`;
    });
    $('.btn-create').click(function(e){
        e.preventDefault();
        window.onbeforeunload = null;
        let quotation = $(this).data('quotation');
        let customer  = {
            customer_id: $("#selectCustomer").val(),
            customer_name: $("#input_customer_name").val()
        }
        customer = $.param(customer)
        let item = $.param(items);
        let form = $.param({
            attn          : $("#inputAttn").val(),
            vessel        : $("#inputVessel").val(),
            voyage        : $("#inputVoyage").val(),
            quotation_name: 'QTT'+$("#inputSerial").val(),
            bl_no         : $("#inputBl_no").val(),
            from          : $("#inputFrom").val(),
            to            : $("#inputTo").val(),
            copy_form: 'true'
        });
        location = `/customer/create-invoice?quotation_id=${quotation}&${customer}&${item}&${form}`;
    });
    $('.btn-create-bc').click(function(){
        window.onbeforeunload = null;
        let quotation = $(this).data('quotation');
        let customer  = {
            customer_id: $("#selectCustomer").val(),
            customer_name: $("#input_customer_name").val()
        }
        customer = $.param(customer)
        // let item = $.param(items);
        let form = $.param({
            vessel: $("#inputVessel").val(),
            voyage: $("#inputVoyage").val(),
            serial: $("#inputSerial").val(),
            copy_form: 'true'
        });
        location = `/customer/create-booking-confirmation?quotation_id=${quotation}&${customer}&${form}`;
    });

    $("#dueDate").change(function(){
        var now = moment();
        var dueDate = moment($(this).val(), 'DD-MM-YYYY');
        var jarak = moment().range(now, dueDate)
        var hasil = '';
        if(now.format('DD-MM-YYYY') == dueDate.format('DD-MM-YYYY')){
            hasil = 0;
        }else{
            hasil = jarak.diff('days') + 1;
        }

        $("#termOfPayment").val(hasil);
    });

    $("#termOfPayment").change(function(){
        var tgl = moment().add($(this).val(), 'day').format('DD-MM-YYYY');
        $("#dueDate").val(tgl);
    })

    $("#selectSeller").select2({
        width: "100%",
        placeholder: "Sales",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/sales') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        },
    });

    const date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $('.datepicker').datepicker({
autoclose: true,
        format: {
            toDisplay(date, format, language){
                var tgl = moment(date);
                return tgl.format('DD-MM-Y')
            },
            toValue(date, format, language){
                var tgl = moment(date);
                return tgl.format('Y-MM-DD')
            }
        },
        language: 'id',
        weekStart: 1,
        startDate: today
    });

    $("#selectCustomer").select2({
        width: "100%",
        placeholder: "Customers",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/customer') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        },
    });

    $("#selectQuotation").select2({
        width: "100%",
        placeholder: "",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/qtt-customer') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },
            cache: true,
        },
    });

    $("#selectCustomer").on('change', function(){
        $("#input_customer_name").val($(this).select2('data')[0].text);
    })
    $("#selectSeller").on('change', function(){
        $("#input_seller_name").val($(this).select2('data')[0].text);
    })

});
</script>
@endsection
