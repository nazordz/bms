@extends('dashboard.base')
@section('content')

    <!-- Modal -->
    <div class="modal fade" id="modalCategory" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('dashboard.addCategory')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="post" id="createCategory">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">{{ __('dashboard.name') }}</label>
                            <input type="text" name="name" id="inputNameCategory" class="form-control" placeholder="" aria-describedby="helpId">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="submitCategory" class="btn btn-primary">{{ __('dashboard.save') }}</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('dashboard.return') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="animated fadeIn">
        @if (session('status-success'))
            <div class="alert alert-success">
                {{ session('status-success') }}
            </div>
        @endif
        @if (session('status-fail'))
            <div class="alert alert-danger">
                {{ session('status-fail') }}
            </div>
        @endif
            <form action="{{ route('customer.store') }}" id="formCustomer" enctype="multipart/form-data" method="post">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="card">
                            <div class="card-header">
                                @lang('dashboard.create_customer')
                            </div>
                            <div class="card-body">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label for="">@lang('dashboard.first_name')</label>
                                        <input type="text" name="first_name" required value="{{old('first_name')}}" class="form-control" placeholder="">
                                        @if ($errors->first('first_name'))
                                            <small class="text-danger">{{ $errors->first('first_name') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label for="">@lang('dashboard.last_name')</label>
                                        <input type="text" name="last_name" value="{{old('last_name')}}" class="form-control" placeholder="">
                                        @if ($errors->first('last_name'))
                                            <small class="text-danger">{{ $errors->first('last_name') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-12 mb-4">
                                        <label for="">@lang('dashboard.company_name')</label>
                                        <input type="text" name="company" required value="{{old('company')}}" class="form-control" placeholder="">
                                        @if ($errors->first('company'))
                                            <small class="text-danger">{{ $errors->first('company') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group col-lg-4 mt-3">
                                        <label for="">@lang('dashboard.nation')</label>
                                        <select id="selectNation" name="nation" class="form-control" placeholder="">
                                            <option value="Indonesia">Indonesia</option>
                                            <option value="Singapore">Singapore</option>
                                            <option value="Malaysia">Malaysia</option>
                                            <option value="United State">United State</option>
                                            <option value="China">China</option>
                                        </select>
                                        @if ($errors->first('nation'))
                                            <small class="text-danger">{{ $errors->first('nation') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <label for="">@lang('dashboard.address')</label>
                                        <input type="text" name="address" value="{{ old('address') }}" class="form-control" placeholder="">
                                        @if ($errors->first('address'))
                                            <small class="text-danger">{{ $errors->first('address') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-4 mt-3">
                                        <label for="">@lang('dashboard.province')</label>
                                        <select id="selectProvince" name="province_id" class="form-control" placeholder="">
                                            @if(old('province_id'))
                                                <option value="{{old('province_id')}}">{{ old('province_name') }}</option>
                                            @endif
                                        </select>
                                        <input type="hidden" name="province_name" id="input_province_name" value="{{old('province_id')}}" />
                                        @if ($errors->first('province_id'))
                                            <small class="text-danger">{{ $errors->first('province_id') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-4  mt-3">
                                        <label for="">@lang('dashboard.regency')</label>
                                        <select id="selectRegency" name="regency_id" class="form-control" placeholder="">
                                            @if(old('regency_id'))
                                                <option value="{{old('regency_id')}}">{{ old('regency_name') }}</option>
                                            @endif
                                        </select>
                                        <input type="hidden" name="regency_name" id="input_regency_name" value="{{old('regency_id')}}" />
                                        @if ($errors->first('regency_id'))
                                            <small class="text-danger">{{ $errors->first('regency_id') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="">@lang('dashboard.district')</label>
                                        <select id="selectDistrict" name="district_id" class="form-control" placeholder="">
                                            @if(old('district_id'))
                                                <option value="{{old('district_name')}}">{{ old('district_name') }}</option>
                                            @endif
                                        </select>
                                        <input type="hidden" name="district_name" id="input_district_name" value="{{old('district_id')}}" />
                                        @if ($errors->first('district_id'))
                                            <small class="text-danger">{{ $errors->first('district_id') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="">@lang('dashboard.village')</label>
                                        <select id="selectVillage" name="village_id" class="form-control" placeholder="">
                                            @if(old('village_id'))
                                                <option value="{{old('village_id')}}">{{ old('village_name') }}</option>
                                            @endif
                                        </select>
                                        <input type="hidden" name="village_name" id="input_village_name" value="{{old('village_id')}}" />
                                        @if ($errors->first('village_id'))
                                            <small class="text-danger">{{ $errors->first('village_id') }}</small>
                                        @endif
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-5">
                        <div class="card">
                            <div class="card-body">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="form-group col-lg-6">
                                            <label for="" class="">Email</label>
                                            <input type="email" name="email" value="{{ old('email') }}"  class="form-control" placeholder="">
                                            @if ($errors->first('email'))
                                                <small class="text-danger">{{ $errors->first('email') }}</small>
                                            @endif
                                        </div>
                                        <div class="form-group col-lg-6">
                                            <label for="" class="">{{ __('dashboard.phone') }}</label>
                                            <input type="text" name="phone" value="{{ old('phone') }}" id="phoneNumber" class="form-control" placeholder="">
                                            @if ($errors->first('phone'))
                                                <small  class="text-danger">{{ $errors->first('phone') }}</small>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-xs-12">
                        <button class="btn btn-primary" type="submit">{{ __('dashboard.save') }}</button>
                        <a href="{{url('/customer/list-customer') }}" class="btn btn-secondary">{{ __('dashboard.return') }}</a>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection


@section('javascript')
<script>
$("#formCustomer").submit(function(){
    return;
})
setInputFilter(document.getElementById("phoneNumber"), function(value) {
    return /^-?\d*$/.test(value);
});
$(document).ready(function () {
    $("#selectNation").change(function(){
        if($(this).val() != 'Indonesia'){
            $("#selectProvince, #selectRegency, #selectDistrict, #selectVillage").val('').trigger('change').prop('disabled', true);
        }else{
            $("#selectProvince, #selectRegency, #selectDistrict, #selectVillage").prop('disabled', false);
        }
    });
    $("#selectProvince").select2({
        width: "100%",
        placeholder: "{{__('dashboard.province')}}",
        theme: 'bootstrap4',
        allowClear: true,
        ajax: {
            url: " {{ url('/select/province') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        },
    });
    $("#selectProvince").change(function(){
        $("#selectRegency, #selectDistrict, #selectVillage").val('').trigger('change');
    })
    $("#selectRegency").select2({
        width: "100%",
        placeholder: "{{__('dashboard.regency')}}",
        theme: 'bootstrap4',
        allowClear: true,
        ajax: {
            url: " {{ url('/select/regency') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
                    province_id: $("#selectProvince").val(),
					page_limit : 10
				};
            },

            cache: true,
        },
    });
    $("#selectRegency").change(function(){
        $("#selectDistrict, #selectVillage").val('').trigger('change');
    })
    $("#selectDistrict").select2({
        width: "100%",
        placeholder: "{{__('dashboard.district')}}",
        theme: 'bootstrap4',
        allowClear: true,
        ajax: {
            url: " {{ url('/select/district') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
                    regency_id: $("#selectRegency").val(),
					page_limit : 10
				};
            },

            cache: true,
        },
    });
    $("#selectDistrict").change(function(){
        $("#selectVillage").val("").trigger('change');
    })
    $("#selectVillage").select2({
        width: "100%",
        placeholder: "{{__('dashboard.village')}}",
        theme: 'bootstrap4',
        allowClear: true,
        ajax: {
            url: " {{ url('/select/village') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
                    district_id: $("#selectDistrict").val(),
					page_limit : 10
				};
            },

            cache: true,
        },
    });

    $("#selectProvince").on('change', function(){
        $("#input_province_name").val($(this).text());
    })
    $("#selectRegency").on('change', function(){
        $("#input_regency_name").val($(this).text());
    })
    $("#selectDistrict").on('change', function(){
        $("#input_district_name").val($(this).text());
    })
    $("#selectVillage").on('change', function(){
        $("#input_village_name").val($(this).text());
    })

});
</script>
@endsection
