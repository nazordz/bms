@extends('dashboard.base')

@section('content')

    <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Sales</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="formSeller">
                    @csrf
                    <input type="hidden" name="seller_id" id="sellerIdInput" value="">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">@lang('dashboard.name')</label>
                            <div class="col-sm-8">
                                <input type="text" name="name" min="0" required id="sellerNameInput" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-submit btn-secondary" data-dismiss="modal">@lang('dashboard.close')</button>
                        <button type="submit" class="btn btn-submit btn-primary">@lang('dashboard.save')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-tag"></i> @lang('dashboard.seller')
                        </div>
                        <div class="card-body">
                            <div class="col-lg-12 mb-5">
                                <button onclick="openModal()" class="btn btn-primary pull-right" > {{ __('dashboard.addSeller') }}</button>
                            </div>
                            <table id="tbSeller" class="table table-responsive-lg table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>@lang('dashboard.seller')</th>
                                        <th>{{ __('dashboard.action') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('javascript')
<script>
function openModal(){
    $("#modalAdd").modal('show')
    $("#sellerIdInput").val('');
    $("#sellerNameInput").val('');
}
$(document).ready(function () {
    $("#formSeller").submit(function(e){
        e.preventDefault();
        $(".btn-submit").prop('disabled', true);
        $.ajax({
            type: "post",
            url: "{{url('/customer/store-seller')}}",
            data: $(this).serialize(),
            dataType: "json"
        })
        .done(function(response) {
            $("#modalAdd").modal('hide');
            Swal.fire( '{{__("dashboard.success_saved")}}', '', 'success' );
        })
        .fail((e) => {
            Swal.fire( '{{ __("dashboard.fail_saved") }}', '', 'warning' );
        })
        .always(function(){
            $(".btn-submit").prop('disabled', false);
            myTable.draw();
        });
    });


    var myTable = $("#tbSeller").DataTable({
        responsive: true,
        processing : true,
        serverSide : true,
        order      : [[2,"ASC"]],
        autoWidth  : false,
        searchDelay: 500,
        ajax : {
            url : "{{ url('/customer/table-seller') }}"
        },
        columns:[
            {
                data: null,
                orderable  : false,
				searchable : false
            },
            { data: 'name' },
            {
                data: null,
                searchable:false,
                orderable : false,
                render(data, type, row){
                    return `<button data-id="${row.id}" class="btn btn-success btn-edit btn-sm mr-2"><i class="fa fa-edit"></i> {{__('dashboard.edit')}}</a>
                            <button class="btn btn-danger btn-delete btn-sm" data-id="${row.id}"><i class="fa fa-trash"></i> {{__('dashboard.delete')}}</button>`
                }
            }
        ],
        fnCreatedRow(row, data, index) {
			if (myTable.page.info().start >= 10) {
				var panjang = myTable.page.info().length;
				var halaman = myTable.page.info().page;
				var i = 1;
				i = (halaman + 1) * panjang;
				i -= panjang - 1;
	        }else{
	        	var i = 1;
	        }
	        $('td', row).eq(0).html(index + i);
		},
        drawCallback(){
            $(".dataTables_length select").removeClass("form-control-sm");
            $(".btn-delete").click(function(){
                var id = $(this).data('id');
                Swal.fire({
                    title: '{{__("dashboard.are_you_sure")}}',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: '{{ __("dashboard.cancel") }}',
                    confirmButtonText: '{{__("dashboard.confirm_delete")}}'
                }).then(function(result) {
                    if (result.value) {
                        $.ajax({
                            type: "post",
                            url: "{{ url('/customer/delete-seller')}}",
                            data: { _token: "{{csrf_token()}}", _method:'delete',  id},
                            dataType: "json"
                        }).done(function(data){
                            myTable.draw();
                            Swal.fire( '{{__("dashboard.deleted")}}', '', 'success' );
                        }).catch(function(err){
                            console.log(err);
                        });
                    }
                });
            });

            $(".btn-edit").click(function(){
                const id = $(this).data('id');
                $.ajax({
                    type: "get",
                    url: "{{url('customer/show-seller')}}/"+id,
                    dataType: "json"
                })
                .done(function({id, name}){
                    $("#sellerIdInput").val(id);
                    $("#sellerNameInput").val(name);
                    $("#modalAdd").modal('show');
                });
            });
		}
    })
});
</script>
@endsection

