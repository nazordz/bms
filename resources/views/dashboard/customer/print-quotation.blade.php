<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Quotation Customer</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        body{
            font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
            color:#333;
            text-align:left;
            font-size:18px;
            margin:0;
        }

        caption{
            font-size:16px;
            margin-bottom: 5px;
        }
        caption h3{
            margin: 0;
        }
        table{
            border:1px solid #333;
            border-collapse:collapse;
            margin:20px auto;
            width: 300px;
            height: auto;
            top: 3%;
            position: relative;
            font-family: sans-serif;
            padding-right: 50px
        }
        td, tr, th{
            padding:12px;
            font-size: 10px;
            border:1px solid #333;
            width:50px;
        }
        th{
            background-color: #f0f0f0;
        }
        h4, p{
            margin:0px;
        }
        .author{
            font-size: 12px;
        }
        .container{
            margin:0 auto;
            width:100%;
            height:auto;
            background-color:#fff;
            float: left;
            /* display: flex;
            flex-direction: column; */
        }
        .body-print{
            top: 15%;
            position: relative;
        }
        .print-header{
            text-align: center;
            z-index: 999;
            min-height: 50px;
            margin: 0 auto;
            width: 100%;
            margin: 0 auto;
        }
        .print-header div{
            display: inline;
        }
        .header-image{
            max-width: 120px;
        }
        .footer-print{
            text-align: center;
            z-index: 999;
            margin: 0 auto;
            bottom: 0;
            position: relative;
            max-width: 120px;
            display: block;
            margin-top: 50px;

        }
        .footer-regard{
            min-width: 170px;
            text-align:center;
            float: right;
        }
        .footer-down{
            margin-top: 100px;
        }
        .footer-image{
            max-width: 482px;
        }
    </style>
</head>
<body>
    @if ($web->header)
        <header class="print-header">
            <div>
                <img src="{{ public_path().$web->header }}" class="header-image" alt="header">
            </div>
            <div style="margin: 0 100px;">
                <b>Quotation</b>
            </div>
        </header>
    @endif
    <div class="container">
        <div>
            <table>
                <tbody>
                    <tr>
                        <th colspan="3"><strong>No QTT{{ $data->serial }}</strong></th>
                        <th colspan="2">dibuat {{ date_format(date_create($data->invoice_date), 'd-m-Y') }}</th>
                        <th colspan="2">Valid sampai {{ date_format(date_create($data->valid_until), 'd-m-Y') }}</th>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <h4>Perusahaan: </h4>
                            <p class="author">BMS.<br>
                                {{$web->address}}<br>
                                {{$web->phone}}<br>
                                {{$web->email}}
                            </p>
                        </td>
                        <td colspan="4">
                            <h4>Pelanggan: </h4>
                            <div class="author" style="max-width:200px;">
                                {{ $data->first_name." ".$data->last_name }}<br>
                                {{ $data->provinces }}
                                {{ $data->regencies }}
                                {{ $data->districts }}
                                {{ $data->villages }}<br />
                                {{-- {!! nl2br(str_replace(',', '\n', $data->address)) !!}<br> --}}
                                {{ $data->address }}<br>
                                {{ $data->phone }} <br>
                                {{ $data->email }}
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>Item</th>
                        <th>Deskripsi</th>
                        <th>Harga</th>
                        <th>Qty</th>
                        <th>Discount</th>
                        <th>Discount % / $</th>
                        <th>Subtotal</th>
                    </tr>
                    @foreach ($items as $item)
                    <tr>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->description }}</td>
                        <td>{{ $currency }} {{ number_format($item->price, 0, ",", $thousand_separator) }}</td>
                        <td>{{ $item->quantity }}</td>
                        <td>{{ $item->discount }}</td>
                        <td>{{ $item->discount_type == 1 ? '%' : $currency }}</td>
                        <td>{{$currency}} {{ number_format($item->subtotal, 0, ',', $thousand_separator) }}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <th colspan="6">Subtotal</th>
                        <td>{{$currency}} {{ number_format(collect($items)->sum('subtotal'), 0, ',', $thousand_separator)}}</td>
                    </tr>
                    @php
                        $subtotal = collect($items)->sum('subtotal');
                        $disc     = $data->discount_amount;
                        $amount   = ($disc/100)*$subtotal > 0 ? ($disc/100)*$subtotal :$subtotal;
                    @endphp
                    <tr>
                        <th colspan="5">Discount</th>
                        <td>{{ $data->disDesc}} {{ $data->discount_amount }}%</td>
                        <td>{{ $currency }} {{ number_format(($disc/100)*$subtotal > 0 ? ($disc/100)*$subtotal : 0, 0, ',', $thousand_separator) }}</td>
                    </tr>
                    @for ($i = 0; $i < count($taxes); $i++)
                        <tr>
                            @if ($i == 0)
                                <th colspan="5" rowspan="{{count($taxes)}}">Pajak</th>
                            @endif
                            @if ($taxes[$i]->type == 1)
                                <td> {{$taxes[$i]->description}} ({{$taxes[$i]->tax_amount}}%) </td>
                                <td>{{ $currency.' ('.number_format(($taxes[$i]->tax_amount/100)*$subtotal, 0, ',', $thousand_separator) }})</td>
                            @else
                                <td> {{$taxes[$i]->description}} {{$taxes[$i]->tax_amount}}% </td>
                                <td>{{ $currency.' '.number_format(($taxes[$i]->tax_amount/100)*$subtotal, 0, ',', $thousand_separator) }}</td>
                            @endif
                        </tr>
                    @endfor
                    <tr>
                        @php
                            $subtotal = collect($items)->sum('subtotal');
                            $disc     = $data->discount_amount;
                            $cut    = (($disc/100)*$subtotal > 0) ? ($disc/100)*$subtotal :0;
                            $taxDecrease = collect($taxes)->sum(function($tax){
                                return $tax->type == 1 ? $tax->tax_amount:0;
                            });
                            $taxIncrease = collect($taxes)->sum(function($tax){
                                return $tax->type == 2 ? $tax->tax_amount:0;
                            });
                            $amount = $subtotal-$cut;
                            $pay = $amount;
                            if($taxDecrease > 0 && $taxIncrease > 0){
                                $taxDec = ($taxDecrease/100)*$amount;
                                $taxInc = ($taxIncrease/100)*$amount;
                                $pay    = ($amount-$taxDec) + $taxInc;
                            }else if($taxDecrease > 0){
                                $taxDec  = ($taxDecrease/100)*$amount;
                                $taxes  = $amount - $taxDec;
                                $pay  =  $taxes;
                            }else if($taxIncrease > 0){
                                $taxInc  = ($taxIncrease/100)*$amount;
                                $taxes  = $amount + $taxInc;
                                $pay  =  $taxes;
                            }
                        @endphp
                        <th colspan="6">Total</th>
                        <td>
                            {{$currency}} {{number_format($pay, 0, ',', $thousand_separator)}}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7">
                            <h3>Catatan:</h3>
                            <p>
                                {{ $data->term_and_condition }}
                            </p>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table style="border:none;">
                <tbody style="border:none; margin-left: 390px;">
                    <tr style="border:none;">
                        <td style="border:none;" colspan="10"></td>
                        <td style="border:none; padding-top:0;">
                            <div style="text-align: right;">
                                <p style="margin-bottom: 80px;">Hormat Kami</p>
                                <p> {{ $web->behalf_of }} </p>
                                <p> {{ $web->behalf_of_position }} </p>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        {{-- <div class="footer-print">
            @if ($web->footer)
                <div class="footer-down">
                    <img src="{{ public_path().$web->footer }}" class="footer-image" alt="footer">
                </div>
            @endif
        </div> --}}
    </div>
</body>
</html>
