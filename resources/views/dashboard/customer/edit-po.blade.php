@extends('dashboard.base')
@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
        @if (session('status-success'))
            <div class="alert alert-success">
                {{ session('status-success') }}
            </div>
        @endif
        @if (session('status-fail'))
            <div class="alert alert-danger">
                {{ session('status-fail') }}
            </div>
        @endif
            <form action="{{ route('customer.po.update') }}" enctype="multipart/form-data" method="post">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                @lang('dashboard.create_po')
                            </div>
                            <div class="card-body">
                                @csrf
                                <input type="hidden" name="po_id" value="{{ $data->po_id }}">
                                <div class="row">
                                    <div class="form-group col-lg-4">
                                        <label for="">Customer</label>
                                        <select id="selectCustomer" name="customer_id" class="form-control" placeholder="" required>
                                            @if($data->customer)
                                                <option value="{{$data->customer->customer_id}}">{{ $data->customer->company.' - '. $data->customer->first_name.' '.$data->customer->last_name }}</option>
                                            @endif
                                        </select>
                                        @if ($data->customer)
                                            <input type="hidden" name="customer_name" id="input_customer_name" value="{{ $data->customer->company.' - '. $data->customer->first_name.' '.$data->customer->last_name }}" />
                                        @else
                                            <input type="hidden" name="customer_name" id="input_customer_name" value="{{old('customer_name')}}" />
                                        @endif
                                        @if ($errors->first('customer_id'))
                                            <small class="text-danger">{{ $errors->first('customer_id') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="">@lang('dashboard.shipped')</label>
                                        <select name="status" id="" class="form-control">
                                            <option value="1" {{ $data->status == '1' ? 'selected':'' }}>Shipping</option>
                                            <option value="2" {{ $data->status == '2' ? 'selected':'' }}>Delivered</option>
                                            <option value="0" {{ $data->status == '0' ? 'selected':'' }}>Void</option>
                                        </select>
                                        @if ($errors->first('status'))
                                            <small class="text-danger">{{ $errors->first('status') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="">@lang('dashboard.image_po')</label>
                                        <div>
                                            @if ($data->file)
                                                <a href="{{ $data->file }}" class="btn my-3 btn-sm btn-secondary" download> Download file</a>
                                            @endif
                                            <input type="file" name="file" class="form-control-file">
                                        </div>
                                        @if ($errors->first('file'))
                                            <small class="text-danger">{{ $errors->first('file') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                <item-component
                                    :data="{{$data}}"
                                    url="{{ url('/select/item') }}"
                                    lang="{{ Auth::user()->language }}">
                                </item-component>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-xs-12">
                        <button class="btn btn-success" type="submit">{{ __('dashboard.save') }}</button>
                        @if ($data->quotation)
                            <a class="btn btn-secondary" href="{{ url('customer/show-quotation/'.$data->quotation->quotation_id) }}">@lang('dashboard.see') @lang('dashboard.quotation')</a>
                        @endif
                        <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                            <div class="btn-group" role="group">
                                <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Invoice
                                </button>
                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                    @if ($data->invoice)
                                        @foreach ($data->invoice as $invoice)
                                    <a href="{{ url('customer/show-invoice/'.$invoice->invoice_id) }}" class="dropdown-item">@lang('dashboard.see') INV{{$invoice->invoice_id}}</a>
                                        @endforeach
                                    @endif
                                    {{-- <a href="{{ url('customer/create-invoice?po='.$data->po_id) }}" class="dropdown-item">@lang('dashboard.addInvoice')</a> --}}
                                    <button type="button" class="btn btn-link btn-create" data-purchase_order="{{$data->po_id}}">@lang('dashboard.create_invoice')</button>

                                </div>
                            </div>
                        </div>

                        <a href="{{url('/customer/purchase-order') }}" class="btn btn-secondary">{{ __('dashboard.return') }}</a>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection


@section('javascript')
<script src="{{ asset('js/app.js') }}" defer></script>

<script>

$(document).ready(function () {
    /*window.onbeforeunload = function() {
        return 'Are you sure that you want to leave this page?';
    };*/
    $('.btn-create').click(function(){
        let purchase_order = $(this).data('purchase_order');
        let customer  = {
            customer_id: $("#selectCustomer").val(),
            customer_name: $("#input_customer_name").val()
        }
        customer = $.param(customer)
        let item = $.param(items);
        location = `/customer/create-invoice?purchase_order=${purchase_order}&${customer}&${item}`;
    });

    const date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $('.datepicker').datepicker({
autoclose: true,
        format: 'dd-mm-yyyy',
        language: 'id',
        weekStart: 1,
        startDate: today
    });

    $("#selectCustomer").select2({
        width: "100%",
        placeholder: "Customers",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/customer') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        },
    });

    $("#selectCustomer").on('change', function(){
        // console.log($(this).select2('data')[0].text);

        $("#input_customer_name").val($(this).select2('data')[0].text);
    })

});
</script>
@endsection
