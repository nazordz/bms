@extends('dashboard.base')
@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
        @if (session('status-success'))
            <div class="alert alert-success">
                {{ session('status-success') }}
            </div>
        @endif
        @if (session('status-fail'))
            <div class="alert alert-danger">
                {{ session('status-fail') }}
            </div>
        @endif
            <form action="{{ route('customer.bc.store') }}" id="store-bc" enctype="multipart/form-data" method="post">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                @lang('dashboard.create_bc')
                            </div>
                            <div class="card-body">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-lg-3">
                                        <label for="">Customer</label>
                                        <select id="selectCustomer" name="customer_id" class="form-control" placeholder="" required>
                                            @if ($data)
                                                <option value="{{ $data['customer_id'] }}">{{ $data['customer_name'] }}</option>
                                            @else
                                                @if(old('customer_id'))
                                                    <option value="{{old('customer_id')}}">{{ old('customer_name') }}</option>
                                                @endif
                                            @endif

                                        </select>
                                        <input type="hidden" name="customer_name" id="input_customer_name" value="{{old('customer_name')}}" />
                                        @if ($errors->first('customer_id'))
                                            <small class="text-danger">{{ $errors->first('customer_id') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="">No {{ __('dashboard.quotation')  }}</label>
                                        <select name="quotation_id" id="selectQuotation" class="form-control">
                                            @if ($data)
                                                <option value="{{ $data['quotation_id'] }}">QTT{{ $data['serial'] }}</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-2">
                                        <label for="">Status</label>
                                        <select name="status" id="" class="form-control">
                                            <option value="1">Proccess</option>
                                            <option value="2">Shipping</option>
                                            <option value="3">Shipped</option>
                                            <option value="0">Cancel</option>
                                        </select>
                                        @if ($errors->first('status'))
                                            <small class="text-danger">{{ $errors->first('status') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-2">
                                        <label for="">Booking Ref</label>
                                        <select name="booking_ref" required class="form-control">
                                            <option value="BTMSIN">BTMSIN</option>
                                            <option value="SINBTM">SINBTM</option>
                                            <option value="BTMIDN">BTMIDN</option>
                                            <option value="IDNBTM">IDNBTM</option>
                                            <option value="BTMTF">BTMTF</option>
                                            <option value="TFBTM">TFBTM</option>
                                        </select>
                                        @if ($errors->first('status'))
                                            <small class="text-danger">{{ $errors->first('status') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-2">
                                        <label for="">File</label>
                                        <input type="file" name="file" class="form-control-file">
                                        @if ($errors->first('file'))
                                            <small class="text-danger">{{ $errors->first('file') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">
                                Booking Data
                            </div>
                            <div class="card-body">
                                <div class="row">

                                    <div class="form-group col-lg-4">
                                        <label for="">Vessel</label>
                                        <input type="text" name="vessel" required class="form-control" value="{{ $data? $data['vessel']: old('vessel')}}" />
                                        @if ($errors->first('vessel'))
                                            <small class="text-danger">{{ $errors->first('vessel') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="">Voyage</label>
                                        <input type="text" name="voyage" required class="form-control" value="{{$data ? $data['voyage']: old('voyage')}}" />
                                        @if ($errors->first('voyage'))
                                            <small class="text-danger">{{ $errors->first('voyage') }}</small>
                                        @endif
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Port of Loading</label>
                                            <input type="text" name="port_of_loading" required class="form-control" value="{{old('port_of_loading')}}" />
                                            @if ($errors->first('port_of_loading'))
                                                <small class="text-danger">{{ $errors->first('port_of_loading') }}</small>
                                            @endif

                                        </div>
                                        <div class="form-group">
                                            <label for="">Port of Discharge</label>
                                            <input type="text" name="port_of_discharge" required class="form-control" value="{{old('port_of_discharge')}}" />
                                            @if ($errors->first('port_of_discharge'))
                                                <small class="text-danger">{{ $errors->first('port_of_discharge') }}</small>
                                            @endif
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="">ETD Port of Loading</label>
                                        <input type="text" onkeydown="return false;" name="etd_port_of_loading" class="form-control datepicker" required autocomplete="off" value="{{old('etd_port_of_loading')}}" />
                                        @if ($errors->first('etd_port_of_loading'))
                                            <small class="text-danger">{{ $errors->first('etd_port_of_loading') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="">ETA Port of Dischage</label>
                                        <input type="text" onkeydown="return false;" name="eta_port_of_discharge" class="form-control datepicker" required autocomplete="off" value="{{old('eta_port_of_discharge')}}" />
                                        @if ($errors->first('eta_port_of_discharge'))
                                            <small class="text-danger">{{ $errors->first('eta_port_of_discharge') }}</small>
                                        @endif
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Sin Permit</label>
                                            <input type="number" min="0" max="99" name="sin_permit" id="sin_permit" class="form-control inputPermit" required value="{{old('sin_permit')}}" />
                                            @if ($errors->first('sin_permit'))
                                                <small class="text-danger">{{ $errors->first('sin_permit') }}</small>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="">Batam Permit</label>
                                            <input type="number" min="0" max="99" name="batam_permit" class="form-control inputPermit" required value="{{old('batam_permit')}}" />
                                            @if ($errors->first('batam_permit'))
                                                <small class="text-danger">{{ $errors->first('batam_permit') }}</small>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="">Consignee</label>
                                        <input type="text" name="consignee" required class="form-control" value="{{ $data? $data['consignee']: old('consignee')}}" />
                                        @if ($errors->first('consignee'))
                                            <small class="text-danger">{{ $errors->first('consignee') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="">Shipper</label>
                                        <input type="text" name="shipper" required class="form-control" value="{{ $data? $data['shipper']: old('shipper')}}" />
                                        @if ($errors->first('shipper'))
                                            <small class="text-danger">{{ $errors->first('shipper') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                @if ($data)
                                    <booking-component :rows="{{json_encode($data['items'], true)}}"></booking-component>
                                @else
                                    <booking-component></booking-component>
                                @endif
                                <div class="mt-4">
                                    <div class="row">
                                        {{-- <div class="col-lg-12 mt-2">
                                            <label for="" class="col-lg-1 mx-0 px-0">@lang('dashboard.rate')</label>
                                            <div class="col-lg-3 mx-0 px-0">
                                                <select name="rate" class="form-control">
                                                    <option value="1">IDR</option>
                                                    <option value="2">SGD</option>
                                                    <option value="3">USD</option>
                                                    <option value="4">RM</option>
                                                </select>
                                            </div>
                                        </div> --}}
                                        {{-- <div class="col-lg-12 mt-2">
                                            <label for="" class="col-lg-1 mx-0 px-0">Total</label>
                                            <div class="col-lg-5 mx-0 px-0">
                                                <v-currency id="totalBc" required="required" onchange="handleTotal(event)" />
                                            </div>
                                        </div> --}}
                                        <div class="form-group col-lg-7 mt-2">
                                            <label for="">Special Remark</label>
                                            <textarea name="special_remark" class="form-control" cols="20" rows="4"></textarea>
                                        </div>
                                    </div>
                                    {{-- <input type="hidden" name="total" id="inputTotal"> --}}
                                    {{-- <div class="form-group col-lg-7 mt-2">
                                        <label for="">Special Remark</label>
                                        <textarea name="special_remark" class="form-control" cols="20" rows="4"></textarea>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-xs-12">
                        <button class="btn btn-primary" type="submit">{{ __('dashboard.save') }}</button>
                        <a href="{{url('/customer/booking-confirmation') }}" class="btn btn-secondary">{{ __('dashboard.return') }}</a>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection


@section('javascript')
<script src="{{ asset('js/app.js') }}" defer></script>

<script>
function handleTotal(event){
    if(event.detail){
        $("#inputTotal").val(event.detail.numberValue)
    }
}

$(document).ready(function () {
    /*window.onbeforeunload = function() {
        return 'Are you sure that you want to leave this page?';
    };*/
    // $("#store-bc").submit(function(){
    //     window.onbeforeunload = null;
    //     return;
    // })
    // $(".inputPermit").keyup(function(){
    //     if($(this).val() > 99){
    //         $(this).val(99);
    //     }
    // })
    var number = document.getElementsByClassName('inputPermit');

    // Listen for input event on numInput.
    for (let index = 0; index < number.length; index++) {
        const el = number[index];
        el.onkeydown = function(e) {
            if(!((e.keyCode > 95 && e.keyCode < 106)
            || (e.keyCode > 47 && e.keyCode < 58)
            || e.keyCode == 8)) {
                return false;
            }
        }

    }
    const date = new Date();
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        language: 'id',
        weekStart: 1,
    });

    $("#selectQuotation").select2({
        width: "100%",
        placeholder: "",
        theme: 'bootstrap4',
        allowClear: true,
        ajax: {
            url: " {{ url('/select/qtt-customer') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
                    page_limit : 10,
                    customer_id: $("#selectCustomer").val()
				};
            },
            cache: true,
        },
    }).on('select2:select', function({ params }){
        const { data } = params;
        var tax = (data.price * (Number(data.tax)/100));
        // var total = data.price + tax;
        // $("#totalBc").val(total.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}));
        console.log(data);

    });
    $("#selectCustomer").select2({
        width: "100%",
        placeholder: "Customers",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/customer') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        },
    });

    $("#selectCustomer").on('change', function(){
        $("#input_customer_name").val($(this).select2('data').text);
    })

});
</script>
@endsection
