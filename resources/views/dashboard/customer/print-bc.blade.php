<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Booking Confirmation Customer</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        body{
            font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
            color:#333;
            text-align:left;
            font-size:18px;
            margin:0;
        }
        table{
            border:1px solid #333;
            border-collapse:collapse;
            margin:30px auto;
            width: auto;
            height: auto;
            top: 3%;
            position: relative;
            font-family: sans-serif;
        }
        td, tr, th{
            padding:12px;
            font-size: 10px;
            border:1px solid #333;
            width:100px;
            /* width:50px; */
        }
        th{
            background-color: #f0f0f0;
        }
        h4, p{
            margin:0px;
        }
        .author{
            font-size: 14px;
        }
        .container{
            margin:0 auto;
            width:100%;
            height:auto;
            background-color:#fff;
            float: left;
            /* display: flex;
            flex-direction: column; */
        }
        .body-print{
            top: 15%;
            position: relative;
        }
        .header-print{
            text-align: center;
            z-index: 999;
            min-height: 300px;
            margin: 0 auto;
            width: 100%;
            margin: 0 auto;
        }
        .header-image{
            max-width: 100px;
        }
        .footer-print{
            text-align: center;
            z-index: 999;
            margin: 0 auto;
            bottom: 0;
            position: relative;
            max-width: 300px;
            display: block;
            margin-top: 50px;

        }
        .footer-regard{
            min-width: 170px;
            text-align:center;
            float: right;
        }
        .footer-down{
            margin-top: 100px;
        }
        .footer-image{
            max-width: 482px;
        }

        .contact{
            width: 100%;
        }
        .contact-field{
            display: inline;
            width: 20%;
        }
        .contact-data{
            display: inline;
            width: 80%;
        }
    </style>
</head>
<body>
    <div class="container">
        <div>
            <table style="border-collapse: collapse;">
                <tbody>
                    <tr>
                        <td colspan="2">
                            <center>
                                @if($web->logo)
                                    {{-- <img src="{{ (public_path().$web->logo)  }}" class="header-image" alt="Logo"> --}}
                                    <img src="{{ ($web->logo)  }}" class="header-image" alt="Logo">
                                @endif
                            </center>
                        </td>
                        <td colspan="2">
                            <h3>Booking Confirmation</h3>
                        </td>
                        <td colspan="3">
                            <h3>Booking Ref</h3>
                            <p>{{$data->booking_ref. date('y', strtotime($data->created_at)) .str_pad($data->serial, 4, '0', STR_PAD_LEFT)}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div class="contact">
                                <div class="contact-field">Date :</div>
                                <div class="contact-data">{{ $data->created_at }}</div>
                            </div>
                            <div class="contact">
                                <div class="contact-field">Billing Party :</div>
                                <div class="contact-data">{{ $data->customer->company }}</div>
                            </div>
                            <div class="contact">
                                <div class="contact-field">Email :</div>
                                <div class="contact-data">{{ $data->customer->email}}</div>
                            </div>
                            <div class="contact">
                                we are pleased to confirm your booking as per below.
                            </div>
                        </td>
                        <td colspan="4">
                            <div class="contact">
                                <div class="contact-field">
                                    Attention:
                                </div>
                                <div class="contact-data">
                                    {{$data->customer->first_name}} {{ $data->customer->last_name }}
                                </div>
                            </div>
                            <div class="contact">
                                <div class="contact-field">
                                    Contact No:
                                </div>
                                <div class="contact-data">
                                    {{$data->customer->phone}}
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <h4>SHIPPER</h4>
                            <p>{{$data->shipper}}</p>
                        </td>
                        <td colspan="4">
                            <h4>CONSIGNEE</h4>
                            <P>{{$data->consignee}}</P>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="contact">
                                <div class="contact-field">
                                    VESSEL:
                                </div>
                                <div class="contact-data">
                                    {{$data->vessel}}
                                </div>
                            </div>
                        </td>
                        <td colspan="2">
                            <div class="contact">
                                <div class="contact-field">
                                    VOYAGE:
                                </div>
                                <div class="contact-data">
                                    {{$data->voyage}}
                                </div>
                            </div>
                        </td>
                        <td colspan="3">
                            <div class="contact">
                                <div class="contact-field">
                                    PORT OF LOADING:
                                </div>
                                <div class="contact-data">
                                    {{$data->port_of_loading}}
                                </div>
                            </div>
                            <div class="contact">
                                <div class="contact-field">
                                    PORT OF DISCHARGE:
                                </div>
                                <div class="contact-data">
                                    {{$data->port_of_discharge}}
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="contact">
                                <div class="contact-field">
                                    ETD PORT OF LOADING:
                                </div>
                                <div class="contact-data">
                                    {{$data->etd_port_of_loading}}
                                </div>
                            </div>
                        </td>
                        <td colspan="2">
                            <div class="contact">
                                <div class="contact-field">
                                    ETD PORT OF LOADING:
                                </div>
                                <div class="contact-data">
                                    {{$data->etd_port_of_loading}}
                                </div>
                            </div>
                        </td>
                        <td colspan="3">
                            <div class="contact">
                                <div class="contact-field">
                                    SIN PERMIT :
                                </div>
                                <div class="contact-data">
                                    {{$data->sin_permit}}
                                </div>
                            </div>
                            <div class="contact">
                                <div class="contact-field">
                                    BATAM PERMIT :
                                </div>
                                <div class="contact-data">
                                    {{$data->batam_permit}}
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>QTY</th>
                        <th>FORWARDING SERVICE</th>
                        <th>SHIPMENT TYPE</th>
                        <th>CONTAINER SIZE/DESCRIPTION OF GOODS</th>
                        <th colspan="3">CONTAINER NO:</th>
                    </tr>
                    @foreach ($data->item as $item)
                        <tr>
                            <td>{{$item->qty}}</td>
                            {{-- <td>{{$item->forwarding_service}}</td> --}}
                            @if ($item->forwarding_service == 1)
                                <td>LCL</td>
                            @endif
                            @if ($item->forwarding_service == 2)
                                <td>FCL</td>
                            @endif
                            @if ($item->forwarding_service == 3)
                                <td>BREAKBULK</td>
                            @endif
                            @if ($item->shipment_type == 1)
                                <td>COC</td>
                            @elseif ($item->shipment_type == 2)
                                <td>SOC</td>
                            @else
                                <td></td>
                            @endif

                            <td>{{$item->container_size}}</td>
                            <td colspan="3">{{$item->container_no}}</td>
                        </tr>
                    @endforeach
                    {{-- <tr>
                        <td colspan="7">
                            <h4>Total</h4>
                            <p>{{ $data->total }}</p>
                        </td>
                    </tr> --}}
                    <tr>
                        <td colspan="7">
                            <h4>Special Remark</h4>
                            <p>{{ $data->special_remark }}</p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7">
                            <p>Please submit your shipping instruction 24 hours before vessel ETA Singapore and Notify us immediately if any details </p>
                            <p>above are incorrect.</p>
                            <p>carriers shall not be held reponsible for any claim watsover that may arise as a result</p>
                            <p>to avoid any damage claim arising, please ensure the condition of the container (s) you receive are clean and good condition. </p>
                        </td>
                    </tr>
                </tbody>

            </table>
            <table style="border:none;width: 600px;">
                <tbody style="border:none;">
                    <tr style="border:none;">
                        <td style="border:none;" colspan="10"></td>
                        <td style="border:none;">
                            <div style="text-align: right;">
                                <p style="margin-bottom: 80px;">Hormat Kami</p>
                                <p> {{ $web->behalf_of }} </p>
                                <p> {{ $web->behalf_of_position }} </p>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <script>
        print()
    </script>
</body>
</html>
