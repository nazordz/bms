@extends('dashboard.base')
@section('css')
{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> --}}
{{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" /> --}}
<style>
    #bodyItem .select2-selection .select2-selection--single{
        height: calc(1.8rem) !important;
    }
</style>
@endsection
@section('content')
    <div class="container-fluid" id="form-quotation">
        <div class="animated fadeIn">
        @if (session('status-success'))
            <div class="alert alert-success">
                {{ session('status-success') }}
            </div>
        @endif
        @if (session('status-fail'))
            <div class="alert alert-danger">
                {{ session('status-fail') }}
                @if ($errors->any())
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ preg_replace('/[0-9]||\./', '', $error) }}</li>
                        @endforeach
                    </ul>
                @endif
            </div>
        @endif
            <form action="{{ route('customer.qotation.store') }}" id="formQuotation" enctype="multipart/form-data" method="post">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                @lang('dashboard.create_quotation')
                            </div>
                            <div class="card-body">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-lg-3">
                                        <label for="">Customer</label>
                                        <select id="selectCustomer" name="customer_id" style="bottom: 18%; left: 50%;" onchange="setCustomValidity('')" oninvalid="check(this, 'Choose Customer')" class="form-control" placeholder="" required>
                                            @if(old('customer_id'))
                                                <option value="{{old('customer_id')}}">{{ old('customer_name') }}</option>
                                            @elseif($data)
                                                <option value="{{ $customer['customer_id'] }}">{{ $customer['customer_name'] }}</option>
                                            @endif
                                        </select>
                                        <input type="hidden" name="customer_name" id="input_customer_name" value="{{ $customer ? $customer['customer_name'] : old('customer_name')}}" />
                                        @if ($errors->first('customer_id'))
                                            <small class="text-danger">{{ $errors->first('customer_id') }}</small>
                                        @endif
                                    </div>

                                    {{-- <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.sales')</label>
                                        <select id="selectSeller" name="seller_id" style="bottom: 18%; left: 50%;"  class="form-control" placeholder="">
                                            @if(old('seller_id'))
                                                <option value="{{old('seller_id')}}">{{ old('saller_name') }}</option>
                                            @endif
                                        </select>
                                        <input type="hidden" name="saller_name" id="input_seller_name" value="{{old('saller_name')}}" />
                                    </div> --}}
                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.term_of_payment')</label>
                                        <select name="term_of_payment" class="form-control">
                                            <option value="COD">COD</option>
                                            <option value="30 Hari">30 Hari</option>
                                            <option value="60 Hari">60 Hari</option>
                                            <option value="90 Hari">90 Hari</option>
                                        </select>
                                        @if ($errors->first('payment'))
                                            <small class="text-danger">{{ $errors->first('payment') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.quotation_date')</label>
                                        <div class="input-group">
                                            <input type="text" autocomplete="off" id="quotation_date" name="quotation_date" value="{{ old('quotation_date') ?? date('d-m-Y') }}" onkeydown="return false;" required class="form-control datepicker-quotation" placeholder="">
                                            <div class="input-group-append">
                                                <div class="input-group-text"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                                            </div>
                                        </div>
                                        @if ($errors->first('quotation_date'))
                                            <small class="text-danger">{{ $errors->first('quotation_date') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.valid_until')</label>
                                        <div class="input-group">
                                            <input type="text" autocomplete="off" id="dueDate" name="valid_until" value="{{ old('valid_until') }}" onkeydown="return false;" required class="form-control" placeholder="">
                                            <div class="input-group-append">
                                                <div class="input-group-text"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                                            </div>
                                        </div>
                                        @if ($errors->first('valid_until'))
                                            <small class="text-danger">{{ $errors->first('valid_until') }}</small>
                                        @endif
                                    </div>

                                    {{-- <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.term_of_payment')</label>
                                        <div class="input-group">
                                            <input type="number" id="termOfPayment" class="form-control" name="term_of_payment">
                                            <div class="input-group-append">
                                                <div class="input-group-text">{{ __('dashboard.day') }}</div>
                                            </div>
                                        </div>
                                    </div> --}}

                                    @if ($data)
                                        <v-rate-curr
                                            :data="{{$data}}"
                                            lang="{{Auth::user()->language}}"
                                            styling="col-lg-9"
                                        />
                                    @else
                                        <v-rate-curr
                                            lang="{{Auth::user()->language}}"
                                            styling="col-lg-9"
                                        />
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                <div class="row">

                                    <div class="form-group col-lg-4">
                                        <label for="">Attn</label>
                                        <input type="text" class="form-control" name="attn" value="{{  $data['attn']?? old('attn') }}">
                                    </div>

                                    <div class="form-group col-lg-4">
                                        <label for="">@lang('dashboard.from')</label>
                                        <input type="text" class="form-control" name="from" value="{{ $data['from'] ?? old('from') }}">
                                    </div>

                                    <div class="form-group col-lg-4">
                                        <label for="">@lang('dashboard.to')</label>
                                        <input type="text" class="form-control" name="to" value="{{ $data['to'] ?? old('to') }}">
                                    </div>

                                </div>

                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                @if ($data)
                                    <item-component
                                        :data="{{ $data }}"
                                        lang="{{ Auth::user()->language }}"
                                        url="{{ url('/select/item') }}">
                                    </item-component>
                                @else
                                    <item-component
                                        lang="{{ Auth::user()->language }}"
                                        url="{{ url('/select/item') }}">
                                    </item-component>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-8 col-xs-12">
                        <div class="btn-group" role="group" aria-label="Button group with nested dropdown">

                            <div class="btn-group" role="group">
                                <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ __('dashboard.save') }}
                                </button>
                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                    <button type="submit" name="status" value="1" class="dropdown-item">Draft</button>
                                    <button type="submit" name="status" value="2" class="dropdown-item">Publish</button>
                                </div>
                            </div>
                        </div>
                        <a href="{{url('/customer/list-quotation') }}" class="btn btn-secondary">{{ __('dashboard.return') }}</a>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection


@section('javascript')
<script src="{{ asset('js/app.js') }}" defer></script>

<script type="text/javascript">
function handleCreatedDate(e){
    console.log('tes:',e);

}
// $('#datetimepicker1').datetimepicker({
//     format: 'L'
// });
$(document).ready(function () {
    /*window.onbeforeunload = function() {
        return 'Are you sure that you want to leave this page?';
    };*/

    $("#quotation_date").change(function(){
        var qttDate = moment($(this).val(), 'DD-MM-YYYY');
        $("#dueDate").datepicker('remove');
        $("#dueDate").datepicker({
            // startDate: qttDate._d,
            autoclose: true,
            format: 'dd-mm-yyyy',
            language: 'id',
            weekStart: 1,
            orientation: 'bottom'
        });
    });

    $("#dueDate").change(function(){
        var now = moment();
        var dueDate = moment($(this).val(), 'DD-MM-YYYY');
        var jarak = moment().range(now, dueDate)
        var hasil = '';
        if(now.format('DD-MM-YYYY') == dueDate.format('DD-MM-YYYY')){
            hasil = 0;
        }else{
            hasil = jarak.diff('days') + 1;
        }

        $("#termOfPayment").val(hasil);
    });

    // $("#termOfPayment").change(function(){
    //     var tgl = moment().add($(this).val(), 'day').format('DD-MM-YYYY');
    //     $("#dueDate").val(tgl);
    // })


    $("#formQuotation").submit(function(){
        window.onbeforeunload = null;
        return;
    })
    var i = 0;
    const date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        language: 'id',
        weekStart: 1,
        startDate: today,
        orientation: 'bottom'
    });
    $('#dueDate').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        language: 'id',
        weekStart: 1,
        // startDate: today,
        orientation: 'bottom'
    });
    $('.datepicker-quotation').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        language: 'id',
        weekStart: 1,
        // startDate: today,
        orientation: 'bottom'
    });

    $("#selectCustomer").select2({
        width: "100%",
        placeholder: "Customers",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/customer') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        },
    });
    $("#selectSeller").select2({
        width: "100%",
        placeholder: "Sales",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/sales') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        },
    });

    $("#selectCustomer").on('change', function(){
        $("#input_customer_name").val($(this).select2('data')[0].text);
    })
    $("#selectSeller").on('change', function(){
        $("#input_seller_name").val($(this).select2('data')[0].text);
    })

    $("#select-tax").select2({
        width: "100%",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/tax') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        },
    }).on('select2:open', () => {
        $(".select2-results:not(:has(button))").append(`<a href="/tax" class="select2-create">{{ __('dashboard.create_new_one') }}</a>`);
    });

    $("#select-discount").select2({
        width: "100%",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/discount') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        },
    }).on('select2:open', () => {
        $(".select2-results:not(:has(button))").append(`<a href="/discount" class="select2-create">{{ __('dashboard.create_new_one') }}</a>`);
    });



});
</script>
@endsection
