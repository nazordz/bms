@extends('dashboard.base')
@section('content')

    <!-- Modal -->
    <div class="modal fade" id="modalCategory" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('dashboard.addCategory')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="post" id="createCategory">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">{{ __('dashboard.name') }}</label>
                            <input type="text" name="name" id="inputNameCategory" class="form-control" placeholder="" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="submitCategory" class="btn btn-primary">{{ __('dashboard.save') }}</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('dashboard.return') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="animated fadeIn">
        @if (session('status-success'))
            <div class="alert alert-success">
                {{ session('status-success') }}
            </div>
        @endif
        @if (session('status-fail'))
            <div class="alert alert-danger">
                {{ session('status-fail') }}
            </div>
        @endif
            <form action="{{ route('item-list.update') }}" id="itemForm" enctype="multipart/form-data" method="post">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="card">
                            <div class="card-header">
                                Item
                            </div>
                            <div class="card-body">
                                @csrf
                                @method('put')
                                <input type="hidden" name="uuid" value="{{ $item->uuid }}">
                                <div class="col-lg-12">
                                    {{-- <div class="form-group row">
                                        <label for="" class="">{{ __('dashboard.code') }} Item</label>
                                        <input type="text" name="code" value="{{ $item->item_id }}" required  class="form-control" placeholder="">
                                        @if ($errors->first('code'))
                                            <small class="text-danger">{{ $errors->first('code') }}</small>
                                        @endif
                                    </div> --}}
                                    <div class="form-group row">
                                        <label for="" class="">{{ __('dashboard.nameItem') }}</label>
                                        <input type="text" name="name" value="{{ $item->name }}" required  class="form-control" placeholder="">
                                        @if ($errors->first('name'))
                                            <small class="text-danger">{{ $errors->first('name') }}</small>
                                        @endif
                                    </div>
                                    {{-- <div class="form-group row">
                                        <label for="" class="">{{ __('dashboard.category') }}</label>
                                        <select id="selectCategory" style="bottom: 40%; left: 10%;" name="category_id" class="form-control" placeholder="{{__('dashboard.category')}} Item" required>
                                            <option value="{{$item->category['id']}}">{{$item->category['name']}}</option>
                                        </select>
                                    </div> --}}
                                    <div class="form-group row">
                                        <label for="">@lang('dashboard.description')</label>
                                        <textarea class="form-control" name="description" rows="4">{{ $item->description }}</textarea>
                                        @if ($errors->first('description'))
                                            <small class="text-danger">{{ $errors->first('description') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-5">
                        {{-- <div class="card">
                            <div class="card-header">
                                @lang('dashboard.price')
                            </div>
                            <div class="card-body">
                                <form method="POST" class="" action="/item-list/create">
                                    @csrf
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="form-group col-lg-6">
                                                <label for="" class="">{{ __('dashboard.sell_price') }}</label>
                                                <input type="text"  onkeydown="handleCharacter(event)" onkeyup="formatMoney(this)" name="sell_price" value="{{ number_format($item->sell_price, 0, ',', $thousand_separator)}}" required min="1"  class="form-control" placeholder="">
                                                @if ($errors->first('sell_price'))
                                                    <small class="text-danger">{{ $errors->first('sell_price') }}</small>
                                                @endif
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="">Unit</label>
                                                <select class="form-control" name="unit">
                                                    <option value="0" {{ $item->unit == 0 ? 'selected': '' }}>Pcs</option>
                                                    <option value="1" {{ $item->unit == 1 ? 'selected': '' }}>Kg</option>
                                                    <option value="2" {{ $item->unit == 2 ? 'selected': '' }}>Per Set</option>
                                                    <option value="3" {{ $item->unit == 3 ? 'selected': '' }}>Per Container</option>
                                                    <option value="4" {{ $item->unit == 4 ? 'selected': '' }}>Per Unit</option>
                                                    <option value="5" {{ $item->unit == 5 ? 'selected': '' }}>Per Trip</option>
                                                    <option value="6" {{ $item->unit == 6 ? 'selected': '' }}>Lump Sum</option>
                                                    <option value="7" {{ $item->unit == 7 ? 'selected': '' }}>Per Shipment</option>
                                                    <option value="8" {{ $item->unit == 8 ? 'selected': '' }}>Per Revenue Ton</option>
                                                    <option value="9" {{ $item->unit == 9 ? 'selected': '' }}>Per M3</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div> --}}

                        <div class="card">
                            <div class="card-header">
                                @lang('dashboard.image')
                            </div>
                            <div class="card-body">
                                @if ($item->picture)
                                    <img src="{{ $item->picture }}" style="max-width:150px;" alt="img old">
                                @endif
                                <div class="form-group">
                                    {{-- <label for="">@lang('dashboard.image')</label> --}}
                                    <input type="file" class="form-control-file" name="picture" accept="image/*" placeholder="" aria-describedby="fileHelpId">
                                    @if ($errors->first('picture'))
                                        <small class="text-danger">{{ $errors->first('picture') }}</small>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-xs-12">
                        <button class="btn btn-primary" type="submit">{{ __('dashboard.save') }}</button>
                        <a href="{{url('/item-list') }}" class="btn btn-secondary">{{ __('dashboard.return') }}</a>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection


@section('javascript')
<script>
    function handleCharacter(e) {
    var key = e.which || e.keyCode;
    if ( key != 188 // Comma
        && key != 8 // Backspace
        && key != 9 // Tab
        && (key < 48 || key > 57) // Non digit
        && (key < 96 || key > 105) // Numpad Non digit
        )
    {
        e.preventDefault();
        return false;
    }
}
function formatMoney(el){
    var nominal = el.value.toString();
    var number_string = nominal.replace(/[^,\d]/g, '').toString(),
        split	= number_string.split(sessionStorage.getItem('thousand_separator')),
        sisa 	= split[0].length % 3,
        rupiah 	= split[0].substr(0, sisa),
        ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);

    if (ribuan) {
        var separator = sisa ? sessionStorage.getItem('thousand_separator') : '';
        rupiah += separator + ribuan.join(sessionStorage.getItem('thousand_separator'));
    }
    el.value = (split[1] != undefined ? rupiah + ',' + split[1] : rupiah);
}
function callCategoryModal(){

    $("#modalCategory").modal('show')
    $("#selectCategory").select2("close")
}
$(document).ready(function () {
    $("#itemForm").submit(function(){
        window.onbeforeunload = null;
        return;
    })
    $("#selectCategory").select2({
        width: "100%",
        placeholder: "{{__('dashboard.category')}} Item",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/category') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        },
        // language: {
        //     noResults(){
        //         return `<button id="no-results-btn" onClick="callCategoryModal()">Not exists, Create one ?</button>`;
        //     }
        // },
        escapeMarkup: function(markup){
            return markup;
        }
    })
    .on('select2:open', () => {
        $(".select2-results:not(:has(button))").append(`<button id="no-results-btn" onClick="callCategoryModal()">{{ __('dashboard.create_new_one') }}</button>`);
    });

    $("#createCategory").submit(function(e){
        e.preventDefault();
        $("#submitCategory").prop('disabled', true);
        $.ajax({
            type: "post",
            url: "{{url('/category/create')}}",
            data: $(this).serialize(),
            dataType: "json"
        })
        .done(function({data}){
            $("#inputNameCategory").val('');
            const defaultOpt = $(`<option value="${data.id}" selected>${data.name}</option>`).val(data.id);
            $("#selectCategory").append(defaultOpt).trigger('change');
            $("#modalCategory").modal('hide');
            Swal.fire( '{{__("dashboard.success_saved")}}', '', 'success' );
        })
        .fail(e => {
            Swal.fire( '{{__("dashboard.category_exists")}}', '', 'warning' );
        });
        $("#submitCategory").prop('disabled', false);

    })


});
</script>
@endsection
