@extends('dashboard.base')

@section('content')

    <div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('dashboard.Item_List')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row">
                                    {{-- <div class="col-lg-4">@lang('dashboard.code')</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="item_code"></div> --}}

                                    <div class="col-lg-4">@lang('dashboard.item_name')</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="item_name"></div>

                                    {{-- <div class="col-lg-4">@lang('dashboard.category_name')</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="item_category"></div> --}}

                                    <div class="col-lg-4">@lang('dashboard.description')</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="item_description"></div>

                                    {{-- <div class="col-lg-4">@lang('dashboard.sell_price')</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="item_sell_price"></div> --}}

                                    {{-- <div class="col-lg-4">@lang('dashboard.buy_price')</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="item_buy_price"></div> --}}

                                    {{-- <div class="col-lg-4">Unit</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="item_unit"></div> --}}

                                    <div class="col-lg-4">@lang('dashboard.image')</div>
                                    <div class="col-lg-1">:</div>
                                    <img src="" id="item_picture" alt="img" style="display:none;" class="col-lg-6" width="300">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('dashboard.close')</button>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="animated fadeIn">
            @if (session('status-success'))
                <div class="alert alert-success">
                    {{ session('status-success') }}
                </div>
            @endif
            @if (session('status-fail'))
                <div class="alert alert-danger">
                    {{ session('status-fail') }}
                </div>
            @endif
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-users"></i> Item
                        </div>
                        <div class="card-body">
                            <div class="col-lg-12 mb-5">
                                <a href="{{ url('/item-list/add-form') }}" class="btn btn-primary pull-right" > {{ __('dashboard.addItem') }}</a>
                            </div>
                            <table id="tbItem" class="table table-responsive-lg table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        {{-- <th>{{__('dashboard.code')}}</th> --}}
                                        <th>{{__('dashboard.name')}}</th>
                                        {{-- <th>{{__('dashboard.sell_price')}}</th>
                                        <th>{{__('dashboard.buy_price')}}</th> --}}
                                        <th>{{ __('dashboard.action') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('javascript')
<script>
    function getItemName(unit){
        unit = Number(unit)
        switch (unit) {
                case 0:
                    return `Pcs`
                    break;
                case 1:
                    return `Kg`
                    break;
                case 2:
                    return `Per Set`
                    break;
                case 3:
                    return `Per Container`
                    break;
                case 4:
                    return `Per Unit`
                    break;
                case 5:
                    return `Per Trip`
                    break;
                case 6:
                    return `Lump Sum`
                    break;
                case 7:
                    return `Per Shipment`
                    break;
                case 8:
                    return `Per Revenue Ton`
                    break;
                case 9:
                    return `Per M3`
                    break;
            }
    }
    function showDetail(id){
        $("#item_picture").css('display', 'none')
        $(".info-detail").empty();

        const item_id = id;
        $.ajax({
            type: "get",
            url: "{{url('item-list/show')}}/"+item_id,
            dataType: "json"
        })
        .done(function(data){
            console.log(data);
            $("#item_code").text(data.item_id);
            $("#item_name").text(data.name);
            $("#item_category").text(data.category.name);
            $("#item_description").text(data.description);
            // $("#item_sell_price").text(data.sell_price);
            // $("#item_buy_price").text(data.buy_price);

            // $("#item_unit").text(getItemName(data.unit));
            data.picture && $("#item_picture").css('display', 'block').prop('src', data.picture)
        });
        $("#modalDetail").modal('show');
    }

$(document).ready(function () {

    var myTable = $("#tbItem").DataTable({
        responsive: true,
        processing : true,
        serverSide : true,
        order      : [[2,"ASC"]],
        autoWidth  : false,
        searchDelay: 500,
        ajax : {
            url : "{{ url('/item-list/table') }}"
        },
        columns:[
            {
                data: null,
                orderable  : false,
				searchable : false
            },
            // { data : 'item_id', name : 'list_items.item_id' },
            { data : 'name', name : 'list_items.name' },
            // { data : 'sell_price', name : 'list_items.sell_price',
            //     render(data){
            //         return `${sessionStorage.getItem('currency')} ${thousandSeparator(data)}`
            //     }
            // },
            // { data : 'buy_price', name : 'list_items.buy_price',
            //     render(data){
            //         return `${sessionStorage.getItem('currency')} ${thousandSeparator(data)}`
            //     }
            // },
            {
                data: null,
                searchable:false,
                orderable : false,
                render(data, type, row){
                    return `<button class="btn-detail btn btn-sm btn-primary" onclick="showDetail('${row.uuid}')"><i class="fa fa-eye"></i> Lihat</button>
                            <a href="/item-list/edit/${row.uuid}" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> {{__('dashboard.edit')}}</a>
                            <button class="btn btn-danger btn-delete btn-sm" data-id="${row.uuid}"><i class="fa fa-trash"></i> {{__('dashboard.delete')}}</button>`
                }
            }
        ],
        fnCreatedRow(row, data, index) {
			if (myTable.page.info().start >= 10) {
				var panjang = myTable.page.info().length;
				var halaman = myTable.page.info().page;
				var i = 1;
				i = (halaman + 1) * panjang;
				i -= panjang - 1;
	        }else{
	        	var i = 1;
	        }
	        $('td', row).eq(0).html(index + i);
		},
        drawCallback(){
            $(".dataTables_length select").removeClass("form-control-sm");
            $(".btn-delete").click(function(){
                var id = $(this).data('id');
                Swal.fire({
                    title: '{{__("dashboard.are_you_sure")}}',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: '{{ __("dashboard.cancel") }}',
                    confirmButtonText: '{{__("dashboard.confirm_delete")}}'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: "post",
                            url: "{{ route('item-list.delete') }}",
                            data: { _token: "{{csrf_token()}}", _method:'delete',  id},
                            dataType: "json"
                        }).done(function(data){
                            myTable.draw();
                            Swal.fire( '{{__("dashboard.deleted")}}', '', 'success' );
                        }).catch(function(err){
                            console.log(err);
                        });
                    }
                });
            });


            // $(".btn-detail").click(function(){
            //     const item_id = $(this).data('item_id');
            //     $.ajax({
            //         type: "get",
            //         url: "{{url('item-list/show')}}/"+item_id,
            //         dataType: "json"
            //     })
            //     .done(function(data){
            //         console.log(data);
            //         $("#item_code").text(data.item_id);
            //         $("#item_name").text(data.name);
            //         $("#item_category").text(data.category.name);
            //         $("#item_description").text(data.description);
            //         $("#item_sell_price").text(data.sell_price);
            //         $("#item_buy_price").text(data.buy_price);
            //         $("#item_unit").text((data.unit == 0 ? "Pcs" : "Kg"));
            //         data.picture && $("#item_picture").css('display', 'block').prop('src', data.picture)
            //     });
            //     $("#modalDetail").modal('show');
            // });
		}
        // $("#modalDetail").on('hidden.bs.modal', function(e){
        //     $("#item_picture").css('display', 'none')
        //     console.log('modal closed');

        //     $(".info-detail").empty();
        // })
    })
});
</script>
@endsection

