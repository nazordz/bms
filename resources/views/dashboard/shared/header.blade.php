<div class="c-wrapper">
    <header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    <button class="c-header-toggler c-class-toggler d-lg-none mr-auto" type="button" data-target="#sidebar" data-class="c-sidebar-show">
        <span class="c-header-toggler-icon"></span>
    </button>
    <a class="c-header-brand d-sm-none" href="#">
        <img class="c-header-brand" src="{{ $nav_logo ?? '/img/def-header.png' }}" width="60" height="60"  alt="BMS Logo">
    </a>
    <button class="c-header-toggler c-class-toggler ml-3 d-md-down-none" type="button" data-target="#sidebar" data-class="c-sidebar-lg-show" responsive="true"><span class="c-header-toggler-icon"></span></button>

    <a class="c-header-brand d-lg-none c-header-brand-sm-up-center" href="#">
        <div class="company-name">
            {{ session()->get('company_name') }}
        </div>
    </a>

    <ul class="c-header-nav d-md-down-none">
        <li class="c-header-nav-item dropdown px-3">
            <div class="company-name">
                {{ session()->get('company_name') }}
            </div>
        </li>
    </ul>
    <?php
        // use App\MenuBuilder\FreelyPositionedMenus;
        // if(isset($appMenus['top menu'])){
        //     FreelyPositionedMenus::render( $appMenus['top menu'] , 'c-header-', 'd-md-down-none');
        // }
    ?>
    <ul class="c-header-nav ml-auto mr-4">
        {{--
        <li class="c-header-nav-item d-md-down-none mx-2">
            <a class="c-header-nav-link">
            <svg class="c-icon">
                <use
                xlink:href="/assets/icons/coreui/free-symbol-defs.svg#cui-envelope-open"
                ></use></svg
            ></a>
        </li>
        --}}
        <li class="c-header-nav-item">
            <div class="c-header-nav-link">{{ Auth::user()->name }}</div>
        </li>
        <li class="c-header-nav-item dropdown">
            <a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" >
                @lang('dashboard.language')
                @if (Auth::user()->language == 'id')
                    (ID)
                @else
                    (EN)
                @endif
                <i class="fa fa-caret-down" style="margin-left:5px;" aria-hidden="true"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right pt-0">
                <form action="/language" method="post">
                    @csrf
                    <button type="submit" class="dropdown-item" style="cursor: pointer;" data-lang="en"> Language </button type="button">
                    <input type="hidden" name="language" value="en">
                </form>
                <form action="/language" method="post">
                    @csrf
                    <input type="hidden" name="language" value="id">
                    <button type="submiy" class="dropdown-item" style="cursor: pointer;"> Bahasa </button type="button">
                </form>
            </div>
        </li>

        {{-- @role('superadmin')
            <li class="c-header-nav-item dropdown">
                <div
                    class="c-header-nav-link"
                    role="button"
                    aria-haspopup="true"
                    aria-expanded="false"
                >
                    <div class="c-avatar">
                        <img
                            class="c-avatar-img"
                            src="/assets/img/avatars/userIcon.png"
                            alt="user@email.com"
                        />
                    </div>
                </div>
            </li>
        @else --}}
            <li class="c-header-nav-item dropdown">
                <a
                    class="c-header-nav-link"
                    data-toggle="dropdown"
                    href="#"
                    role="button"
                    aria-haspopup="true"
                    aria-expanded="false"
                >
                    <div class="c-avatar">
                    <img
                        class="c-avatar-img"
                        src="/assets/img/avatars/userIcon.png"
                        alt="user@email.com"
                    />
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-right pt-0">
                    <div class="dropdown-header bg-light py-2"><strong>Settings</strong></div>
                    <a class="dropdown-item" href="{{ url('profile/')}}">
                    <svg class="c-icon mr-2">
                        <use
                        xlink:href="/assets/icons/coreui/free-symbol-defs.svg#cui-user"
                        ></use>
                    </svg>
                    @lang('dashboard.profile')
                    </a>
                    @if (Auth::user()->menuroles == 'superadmin')
                        <a class="dropdown-item" href="{{ url('/company') }}">
                            <i class="fas fa-building mr-2"></i> @lang('dashboard.company')
                        </a>
                    @endif

                    <form action="/logout" method="POST">
                        @csrf
                        <button type="submit" class="dropdown-item" href="#">
                            <svg class="c-icon mr-2">
                                <use xlink:href="/assets/icons/coreui/free-symbol-defs.svg#cui-account-logout"></use>
                            </svg>
                            Logout
                        </button>
                    </form>
                </div>
            </li>
        {{-- @endrole --}}
    </ul>

    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        <?php $segments = ''; ?>
        {{-- @for($i = 1; $i <= count(Request::segments()); $i++) --}}
        @for($i = 1; $i <= 2; $i++)
            <?php $segments .= '/'. Request::segment($i); ?>
            @if($i < count(Request::segments()))
                <li class="breadcrumb-item">{{ Request::segment($i) }}</li>
            @else
                <li class="breadcrumb-item active">{{ Request::segment($i) }}</li>
            @endif
        @endfor
        </ol>
    </div>
</header>
