@extends('dashboard.base')

@section('content')

    <div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('dashboard.quotation')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">supplier</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="input_supplier"></div>

                                    <div class="col-lg-4">Sales</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="input_sales"></div>

                                    <div class="col-lg-4">@lang('dashboard.term_and_condition')</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="input_term_and_condition"></div>

                                    <div class="col-lg-4">@lang('dashboard.valid_until')</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="input_valid_until"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('dashboard.close')</button>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="animated fadeIn">
            @if (session('status-success'))
                <div class="alert alert-success">
                    {{ session('status-success') }}
                </div>
            @endif
            @if (session('status-fail'))
                <div class="alert alert-danger">
                    {{ session('status-fail') }}
                </div>
            @endif
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-users"></i> Credit Note
                        </div>
                        <div class="card-body">
                            <div class="col-lg-12 mb-4 pr-0">
                                <a href="{{ url('/supplier/create-credit-note') }}" class="btn btn-primary" > {{ __('dashboard.addCreditNote') }}</a>
                                <div class="pull-right col-lg-6 mt-4 mt-lg-0">
                                    <div class="row pull-right">
                                        <div class="col-lg-6 form-inline px-0">
                                            <div class="form-group offset-lg-2 ">
                                                <div class="col-lg-8">
                                                    <input type="text" name="start_date" value="" id="start_date_input" placeholder="@lang('dashboard.start_date')" class="form-control datepicker" autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 form-inline px-0">
                                            <div class="form-group offset-lg-2 ">
                                                <div class="col-lg-8">
                                                    <input type="text" name="to_date" value="" id="to_date_input" placeholder="@lang('dashboard.to_date')" class="form-control datepicker" autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <table id="tbCredit" class="table table-responsive-lg table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>{{ __('dashboard.credit_note_number') }}</th>
                                        <th>{{ __('dashboard.company') }}</th>
                                        <th>supplier</th>
                                        <th>Note</th>
                                        <th>Sales</th>
                                        <th>Status</th>
                                        <th>{{ __('dashboard.created_at') }}</th>
                                        <th>@lang('dashboard.action')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('javascript')
<script>

$(document).ready(function () {
    const date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $('.datepicker').datepicker({
        autoclose: true,
        format: {
            toDisplay(date, format, language){
                var tgl = moment(date);
                return tgl.format('DD-MM-Y')
            },
            toValue(date, format, language){
                var tgl = moment(date);
                return tgl.format('Y-MM-DD')
            }
        },
        language: 'id',
        weekStart: 1,
        orientation: 'bottom',
        // startDate: today
    });

    $(".datepicker").change(function(){
        myTable.draw();
    })
    var myTable = $("#tbCredit").DataTable({
        responsive: true,
        processing : true,
        serverSide : true,
        order      : [[7,"DESC"]],
        autoWidth  : false,
        searchDelay: 500,
        ajax : {
            url : "{{ url('/supplier/table-credit-note') }}",
            data(data){
                return {
                    ...data,
                    start_date: $("#start_date_input").val(),
                    to_date: $("#to_date_input").val(),
                }

            }
        },
        columns:[
            {
                data: null,
                orderable  : false,
				searchable : false
            },
            {
                data: 'id', name: 'credit_note_suppliers.id',
                render(data, type, row){
                    return `<a href="/supplier/credit-note/${row.uuid}/show">CN${serializing(row.created_at, row.serial)}</a>`;
                }
            },
            { data: 'company', name: 'suppliers.company' },
            {
                data: 'first_name',
                name: 'suppliers.first_name',
                render(data, type, row){
                    return `${data} ${row.last_name}`
                }
            },
            { data : 'note', name : 'credit_note_sustomers.note' },
            { data: 'sales', name: 'sellers.name' },
            {
                data: 'status', name: 'credit_note_suppliers.status',
                render(data){
                    return Boolean(data) ? 'Posted': 'Void';
                }
            },
            {
                data : 'created_at',
                name : 'credit_note_suppliers.created_at',
                render(data){
                    var date = moment(data).locale('id');
                    return date.format('dddd, DD-MM-Y');
                }
            },
            {
                data: null,
                width: '10%',
                orderable: false,
                searchable: false,
                render(data, type, row){
                    var res = ``;
                    if(Boolean(row.status)){
                        res += `<button class="btn btn-danger btn-delete btn-sm" data-id="${row.id}"><i class="fa fa-times"></i> Void</button>`;
                    }
                    return res;
                }
            }
        ],
        fnCreatedRow(row, data, index) {
			if (myTable.page.info().start >= 10) {
				var panjang = myTable.page.info().length;
				var halaman = myTable.page.info().page;
				var i = 1;
				i = (halaman + 1) * panjang;
				i -= panjang - 1;
	        }else{
	        	var i = 1;
	        }
	        $('td', row).eq(0).html(index + i);
		},
        drawCallback(){
            $(".dataTables_length select").removeClass("form-control-sm");
            // $(".btn-print").click(function(){
            //     var id = $(this).data('id');

            // });
            $(".btn-delete").click(function(){
                var id = $(this).data('id');
                Swal.fire({
                    title: '{{__("dashboard.are_you_sure")}}',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: '{{ __("dashboard.cancel") }}',
                    confirmButtonText: 'Okay'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: "post",
                            url: "{{ route('supplier.credit-note.delete') }}",
                            data: { _token: "{{csrf_token()}}", _method:'delete',  id},
                            dataType: "json"
                        }).done(function(data){
                            myTable.draw();
                            Swal.fire( `{{__('dashboard.credit_note_deleted')}} `, '', 'success' );
                        }).catch(function(err){
                            console.log(err);
                        });
                    }
                });
            });

            $("#printTable").submit(function(e){
                e.preventDefault();
                const start = $("#start_date_print").val();
                const to    = $("#to_date_print").val();
                $("#printTable")[0].reset()
                $("#modalExport").modal('hide');
                location = `/supplier/export-quotation/${start}/${to}`;
            })

            $(".btn-detail").click(function(){
                const quotation_id = $(this).data('quotation_id');
                $.ajax({
                    type: "get",
                    url: "{{url('supplier/show-quotation')}}/"+quotation_id,
                    dataType: "json"
                })
                .done(function(data){
                    $("#input_supplier").text(data.supplier.first_name+' '+data.supplier.last_name);
                    $("#input_sales").text(data.sales);
                    $("#input_term_and_condition").text(data.term_and_condition);

                    var date = moment(data.valid_until).locale('id');
                    $("#input_valid_until").text(date.format('DD-MM-Y'));
                    $("#modalDetail").modal('show');
                });
            });

		}
    })
});
</script>
@endsection

