@extends('dashboard.base')
@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
        @if (session('status-success'))
            <div class="alert alert-success">
                {{ session('status-success') }}
            </div>
        @endif
        @if (session('status-fail'))
            <div class="alert alert-danger">
                {{ session('status-fail') }}
            </div>
        @endif
            <form action="{{ route('supplier.po.update') }}" id="formPo" enctype="multipart/form-data" method="post">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-header-split">
                                    <div>
                                        Edit PO{{$data->serial}}
                                    </div>
                                    @if ($data->status == 1)
                                            <div class="alert-header alert-warning">Draft</div>
                                    @elseif($data->status == 2)
                                        <div class="card-header-notif">
                                            <div class="alert-header alert-success">Publish</div>
                                        </div>
                                    @else
                                        <div class="alert-header alert-danger">Void</div>
                                    @endif
                                </div>
                            </div>
                            <div class="card-body">
                                @csrf
                                <input type="hidden" name="po_id" value="{{ $data->po_id }}">
                                <input type="hidden" id="serialPo" name="serial" value="{{ $data->serial }}">
                                <div class="row">
                                    <div class="form-group col-lg-3">
                                        <label for="">supplier</label>
                                        <select id="selectSupplier" style="bottom: 18%; left: 50%;" onchange="setCustomValidity('')" oninvalid="check(this, 'Choose Supplier')" name="supplier_id" {{ $isFormDisabled }} class="form-control" placeholder="" required>
                                            @if($data->supplier)
                                                <option value="{{$data->supplier->supplier_id}}">{{ $data->supplier->company.' - '. $data->supplier->first_name.' '.$data->supplier->last_name }}</option>
                                            @endif
                                        </select>
                                        @if ($data->supplier)
                                            <input type="hidden" name="supplier_name" id="input_supplier_name" value="{{$data->supplier->company.' - '. $data->supplier->first_name.' '.$data->supplier->last_name}}" />
                                        @else
                                            <input type="hidden" name="supplier_name" id="input_supplier_name" value="{{old('supplier_name')}}" />
                                        @endif
                                        @if ($errors->first('supplier_id'))
                                            <small class="text-danger">{{ $errors->first('supplier_id') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group col-lg-3">
                                        <label for="">{{__('dashboard.quotation')}}</label>
                                        <select id="selectQuotation" style="bottom: 18%; left: 50%;" name="quotation_id" {{ $isFormDisabled }} class="form-control" placeholder="">
                                            @if($data->quotation)
                                                <option value="{{ $data->quotation->quotation_id }}">QTT{{ $data->quotation->serial.' - '.$data->supplier->company.' - '. $data->supplier->first_name.' '.$data->supplier->last_name }}</option>
                                            @endif
                                        </select>
                                        @if ($data->quotation)
                                            <input type="hidden" name="quotation_name" id="input_quotation_name" value="QTT{{ $data->quotation->serial.' - '.$data->supplier->company.' - '. $data->supplier->first_name.' '.$data->supplier->last_name }}" />
                                        @else
                                            <input type="hidden" name="quotation_name" id="input_quotation_name" value="" />
                                        @endif

                                        @if ($errors->first('quotation_id'))
                                            <small class="text-danger">{{ $errors->first('quotation_id') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.po_date')</label>
                                        <div class="input-group">
                                            <input type="text" name="po_date" {{ $isFormDisabled }} class="form-control datepicker-po" autocomplete="off" value="{{ $data ? date_format(date_create($data->po_date), 'd-m-Y') :date('d-m-Y') }}" required onkeydown="return false;">
                                            <div class="input-group-append">
                                                <div class="input-group-text"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.shipping_date')</label>
                                        <div class="input-group">
                                            <input type="text" name="shipping_date" {{ $isFormDisabled }} class="form-control datepicker-shipping" autocomplete="off" value="{{ $data ? date_format(date_create($data->shipping_date), 'd-m-Y') :date('d-m-Y') }}" required onkeydown="return false;">
                                            <div class="input-group-append">
                                                <div class="input-group-text"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                    @if ($data)
                                        <v-rate-curr :disabled="{{$data->status == 2 ?'true':'false'}}" :data="{{$data}}" lang="{{Auth::user()->language}}" styling="col-lg-12" />
                                    @else
                                        <v-rate-curr lang="{{Auth::user()->language}}" :disabled="{{$data->status == 2 ?'true':'false'}}" styling="col-lg-12" />
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                <item-component
                                    :data="{{$data}}"
                                    url="{{ url('/select/item') }}"
                                    lang="{{ Auth::user()->language }}">
                                </item-component>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-xs-12">
                        @if ($data->status == 1)
                            <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                <div class="btn-group" role="group">
                                    <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{ __('dashboard.save') }}
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                        <button type="submit" name="status" value="1" class="dropdown-item">Draft</button>
                                        <button type="submit" name="status" value="2" class="dropdown-item">Publish</button>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-danger btn-void" data-type="cancel" data-id="{{$data->po_id}}" type="button">@lang('dashboard.cancel')</button>

                        @else
                            {{-- <button class="btn btn-primary" type="submit">{{ __('dashboard.save') }}</button> --}}
                            <button type="button" class="btn btn-dark btn-create-qtt" data-invoice="{{$data->invoice_id}}">@lang('dashboard.create_quotation')</button>
                            <button type="button" class="btn btn-info btn-create-po" data-quotation="{{$data->quotation_id}}">@lang('dashboard.create_po')</button>
                            <button class="btn btn-warning btn-create-inv text-white" data-id="{{ $data->po_id }}" type="button">{{ __('dashboard.create_invoice') }}</button>
                            <button class="btn btn-danger btn-void" data-type="void" data-id="{{$data->po_id}}" type="button">Void</button>
                        @endif
                        <button class="btn btn-success btn-print" data-id="{{ $data->uuid }}" type="button">{{ __('dashboard.print') }}</button>
                        <a href="{{url('/supplier/purchase-order') }}" class="btn btn-secondary">{{ __('dashboard.return') }}</a>
                    </div>
                </div>
            </form>
            <form action="{{ route('supplier.po.delete') }}" method="post" id="voidInvoice" class="hidden">
                <input type="hidden" name="po_id" id="poId" value="{{ $data->invoice_id}}">
                <input type="hidden" name="type" id="invoiceType" value="">
                <input type="hidden" name="reason" id="reasonInput" value="">
                @csrf
                @method('delete')
            </form>
        </div>
    </div>

@endsection


@section('javascript')
<script src="{{ asset('js/app.js') }}" defer></script>

<script>
function check(input, word){
    if (input.value == "") {
     input.setCustomValidity(word);
   } else  {
     input.setCustomValidity('');
   }
}
$(document).ready(function () {
    $(".btn-void").click(function(){
        $("#poId").val($(this).data('id'));s
        $("#invoiceType").val($(this).data('type'));

        Swal.fire({
            title: 'Are you sure ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            input: 'textarea',
            inputPlaceholder: '{{__("dashboard.reason")}}',
            inputAttributes: {
                'aria-label': '{{__("dashboard.reason")}}...'
            },
            cancelButtonColor: '#d33',
        }).then((result) => {
            if (result.value) {
                $("#reasonInput").val(result.value);
                $("#voidInvoice").trigger('submit');
            }
        });
    });

    $("#formPo").submit(function(){
        return;
    })
    $('.datepicker-po').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        language: 'id',
        orientation: 'bottom',
        weekStart: 1,
    });
    $('.datepicker-shipping').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        language: 'id',
        orientation: 'bottom',
        weekStart: 1,
        startDate: today
    });
    $(".btn-print").click(function(){
        // location = '/supplier/print-purchase-order/'+$(this).data('id');
        window.open('/supplier/print-purchase-order/'+$(this).data('id'), '_blank');
    });
    $('.btn-create-qtt').click(function(e){
        e.preventDefault();
        window.onbeforeunload = null;
        let supplier  = {
            supplier_id: $("#selectSupplier").val(),
            supplier_name: $("#input_supplier_name").val()
        }
        supplier = $.param(supplier)
        let item = $.param(items);
        let form = $.param({
            quotation_id: $('#selectQuotation').val(),
            quotation_name:  $('#input_quotation_name').val(),

            copy_form: 'true'
        });
        location = `/supplier/create-quotation?${supplier}&${item}&${form}`;
    });
    $('.btn-create-po').click(function(e){
        e.preventDefault();
        window.onbeforeunload = null;
        let supplier  = {
            supplier_id: $("#selectSupplier").val(),
            supplier_name: $("#input_supplier_name").val()
        }
        supplier = $.param(supplier)
        let item = $.param(items);
        let form = $.param({
            quotation_id: $('#selectQuotation').val(),
            quotation_name:  $('#input_quotation_name').val(),
            copy_form: 'true'
        });
        location = `/supplier/create-purchase-order?${supplier}&${item}&${form}`;
    });
    $('.btn-create-inv').click(function(){
        // let purchase_order = $(this).data('id');
        let purchase_order = {
            po_id: $(this).data('id'),
            po_name: $("#serialPo").val()
        };
        let supplier  = {
            supplier_id: $("#selectSupplier").val(),
            supplier_name: $("#input_supplier_name").val()
        }
        var quotation = {
            quotation_id: $('#selectQuotation').val(),
            quotation_name:  $('#input_quotation_name').val()
        }
        purchase_order = $.param(purchase_order);
        quotation = $.param(quotation);
        supplier = $.param(supplier);
        let item = $.param(items);
        location = `/supplier/create-invoice?${purchase_order}&${supplier}&${item}&${quotation}&copy_form=true`;
    });

    const date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        language: 'id',
        weekStart: 1,
        startDate: today
    });
    $("#selectQuotation").select2({
        width: "100%",
        placeholder: "{{ __('dashboard.quotation') }}",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/qtt-supplier') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
                    page_limit : 10,
                    supplier: $("#selectSupplier").val()
				};
            },

            cache: true,
        },
    });
    $("#selectSupplier").select2({
        width: "100%",
        placeholder: "{{__('dashboard.supplier')}}",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/supplier') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        },
    });

    $("#selectSupplier").on('change', function(){
        $("#input_supplier_name").val($(this).select2('data')[0].text);
    })
    $("#selectQuotation").on('change', function(){
        $("#quotation_id").val($(this).select2('data')[0].text);
    })

});
</script>
@endsection
