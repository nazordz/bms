@extends('dashboard.base')
@section('content')

    <div class="modal fade" id="modalPaid" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('dashboard.payment')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" id="formJournal" method="post">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="" class="col-lg-3">@lang('dashboard.payment_date')</label>
                            <div class="col-lg-9">
                                <div class="input-group">
                                    <input type="text" required name="payment_date" onchange="changeHiddenValue('paymentdate', this)" onkeydown="return false;" autocomplete="off" class="form-control datepicker" placeholder="">
                                    <div class="input-group-append">
                                        <div class="input-group-text"><i class="fas fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-lg-3">@lang('dashboard.payment_ref')</label>
                            <div class="col-lg-9">
                                <input type="text" required onchange="changeHiddenValue('paymentref', this)" name="payment_ref" class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-lg-3">@lang('dashboard.account')</label>
                            <div class="col-lg-9">
                                {{-- <select id="selectAccount" style="bottom: 18%; left: 50%;" onchange="changeHiddenValue('accountid', this)" oninvalid="check(this, 'Choose Account')" name="account_id" class="form-control" placeholder="" required>
                                </select> --}}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('dashboard.close')</button>
                        <button type="submit" class="btn btn-primary">@lang('dashboard.save')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="animated fadeIn">
        @if (session('status-success'))
            <div class="alert alert-success">
                {{ session('status-success') }}
            </div>
        @endif
        @if (session('status-fail'))
            <div class="alert alert-danger">
                {{ session('status-fail') }}
            </div>
        @endif
            <form action="{{ route('supplier.invoice.store') }}" id="formInvoice" enctype="multipart/form-data" method="post">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                @lang('dashboard.create_invoice')
                            </div>
                            <div class="card-body">
                                @csrf
                                @if(request()->has('po_id'))
                                    <input type="hidden" name="po_id" value="{{ request()->po_id }}">
                                @endif
                                <div class="row">
                                    <div class="form-group col-lg-3">
                                        <label for="">Supplier</label>
                                        <select id="selectSupplier" style="bottom: 18%; left: 50%;" required onchange="setCustomValidity('')" oninvalid="check(this, 'Choose Supplier')" name="supplier_id" class="form-control" placeholder="" required>
                                            @if(old('supplier_id'))
                                                <option value="{{old('supplier_id')}}">{{ old('supplier_name') }}</option>
                                            @elseif($data)
                                                <option value="{{ $supplier['supplier_id'] }}">{{ $supplier['supplier_name'] }}</option>
                                            @endif
                                        </select>
                                        <input type="hidden" name="supplier_name" id="input_supplier_name" value="{{$supplier ? $supplier['supplier_name'] : old('supplier_name')}}" />
                                        @if ($errors->first('supplier_id'))
                                            <small class="text-danger">{{ $errors->first('supplier_id') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group col-lg-3">
                                        <label for="">{{__('dashboard.quotation')}}</label>
                                        <select id="selectQuotationSupplier" style="bottom: 18%; left: 50%;" name="quotation_id" class="form-control" placeholder="" >
                                            @if(isset($data['quotation_id']))
                                                <option value="{{ $data['quotation_id'] }}">
                                                    {{ $data['quotation_name'].' - '.$supplier['supplier_name'] }}
                                                </option>
                                            @endif
                                        </select>
                                        {{-- @if ($data->quotation)
                                            <input type="hidden" name="quotation_name" id="input_quotation_name" value="QTT{{ $data->quotation->serial.' - '.$data->supplier->company.' - '. $data->supplier->first_name.' '.$data->supplier->last_name }}" />
                                        @else
                                            <input type="hidden" name="quotation_name" id="input_quotation_name" value="" />
                                        @endif --}}

                                        @if ($errors->first('quotation_id'))
                                            <small class="text-danger">{{ $errors->first('quotation_id') }}</small>
                                        @endif
                                    </div>

                                    @if (request()->has('po_id'))
                                        <div class="form-group col-lg-3">
                                            <label for="">Purchase Order No</label>
                                            <input type="text" disabled class="form-control" value="PO{{ request()->po_name }}">
                                        </div>
                                    @else
                                        <div class="form-group col-lg-3">
                                            <label for="selectPo">Purchase Order No</label>
                                            <select id="selectPo" style="bottom: 18%; left: 50%;"  name="po_id" class="form-control" placeholder="" >
                                                @if(old('po_id'))
                                                    <option value="{{old('po_id')}}">PO{{ old('po_id') }}</option>
                                                @endif
                                            </select>
                                        </div>
                                    @endif

                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.term_of_payment')</label>
                                        <select name="term_of_payment" class="form-control">
                                            <option value="COD">COD</option>
                                            <option value="30 Hari">30 Hari</option>
                                            <option value="60 Hari">60 Hari</option>
                                            <option value="90 Hari">90 Hari</option>
                                        </select>
                                        @if ($errors->first('payment'))
                                            <small class="text-danger">{{ $errors->first('payment') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.invoice_date')</label>
                                        <div class="input-group">
                                            <input type="text" required autocomplete="off" id="invoiceDate" value="{{old('invoice_date') ?? date('d-m-Y') }}" class="form-control datepicker-invoice" onkeydown="return false;" name="invoice_date">
                                            <div class="input-group-append">
                                                <div class="input-group-text"><i class="fas fa-calendar"></i></div>
                                            </div>
                                        </div>
                                        @if ($errors->first('invoice_date'))
                                            <small class="text-danger">{{ $errors->first('invoice_date') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.due_date')</label>
                                        <div class="input-group">
                                            <input type="text" required class="form-control datepicker" autocomplete="off" required onkeydown="return false;" name="due_date" />
                                            <div class="input-group-append">
                                                <div class="input-group-text"><i class="fas fa-calendar"></i></div>
                                            </div>
                                        </div>
                                        @if ($errors->first('due_date'))
                                            <small class="text-danger">{{ $errors->first('due_date') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="">File Invoice</label>
                                        <div>
                                            <input type="file" name="file" value="{{ old('file') }}" class="form-control-file" placeholder="">
                                        </div>
                                        @if ($errors->first('file'))
                                            <small class="text-danger">{{ $errors->first('file') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.purchase_account')</label>
                                        <select name="purchase_account" required="required" class="form-control" id="selectAccount">
                                        </select>
                                        @if ($errors->first('purchase_account'))
                                            <small class="text-danger">{{ $errors->first('purchase_account') }}</small>
                                        @endif
                                    </div>

                                    @if ($data)
                                        <v-rate-curr
                                            :data="{{$data}}"
                                            lang="{{Auth::user()->language}}"
                                            styling="col-lg-9"
                                        />
                                    @else
                                        <v-rate-curr
                                            lang="{{Auth::user()->language}}"
                                            styling="col-lg-9"
                                        />
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div id="accordion">
                            <div class="card">
                                <div class="card-header text-right" id="headingForm">
                                    <button class="btn btn-link btn-sm" type="button" onclick="$('.collapse').collapse('toggle');" style="cursor: pointer;" data-toggle="collapse" role="button" aria-expanded="false" data-controls="collapseForm">
                                        {{ __('dashboard.show') }} <i class="fas fa-angle-down"></i>
                                   </button>
                                </div>
                                <div class="collapse" data-parent="#accordion" id="collapseForm" aria-labelledby="headingForm">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="form-group col-lg-3">
                                                <label for="">Attn</label>
                                                <input type="text" class="form-control" id="attnInput" name="attn" value="{{  $data['attn']?? '' }}">
                                            </div>

                                            <div class="form-group col-lg-3">
                                                <label for="">@lang('dashboard.vessel')</label>
                                                <input type="text" class="form-control" id="vesselInput" name="vessel" value="{{ $data['vessel'] ?? '' }}">
                                            </div>

                                            <div class="form-group col-lg-3">
                                                <label for="">@lang('dashboard.voyage')</label>
                                                <input type="text" class="form-control" id="voyageInput" name="voyage" value="{{ $data['voyage'] ?? '' }}">
                                            </div>

                                            <div class="form-group col-lg-3">
                                                <label for="">ETA</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control datepicker" onkeydown="return false;" autocomplete="off" id="etaInput" name="eta">
                                                    <div class="input-group-append">
                                                        <div class="input-group-text"><i class="fas fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-4">
                                                <label for="">B/L No</label>
                                                <input type="text" class="form-control" id="bl_noInput" name="bl_no" value="{{ $data['bl_no']?? '' }}">
                                            </div>

                                            <div class="form-group col-lg-4">
                                                <label for="">@lang('dashboard.from')</label>
                                                <input type="text" class="form-control" id="fromInput" name="from" value="{{ $data['from']?? '' }}">
                                            </div>

                                            <div class="form-group col-lg-4">
                                                <label for="">@lang('dashboard.to')</label>
                                                <input type="text" class="form-control" id="toInput" name="to" value="{{ $data['to']?? '' }}">
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                @if ($data)
                                    <item-component
                                        :data="{{ $data }}"
                                        url="{{ url('/select/item') }}"
                                        lang="{{ Auth::user()->language }}">
                                    </item-component>
                                @else
                                    <item-component
                                        url="{{ url('/select/item') }}"
                                        lang="{{ Auth::user()->language }}">
                                    </item-component>
                                @endif
                            </div>
                        </div>
                    </div>

                    {{-- for paid action --}}
                    <input type="hidden" name="status" id="statusInv" value="">
                    <div class="col-lg-8 col-xs-12">
                        <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                            <div class="btn-group" role="group">
                                <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ __('dashboard.save') }}
                                </button>
                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                    {{-- <button type="button" name="status" value="1" onclick="handleSubmit(this)" class="dropdown-item">Draft</button>
                                    <button type="button" name="status" value="2" onclick="handleSubmit(this)" class="dropdown-item">Publish</button> --}}
                                    <button type="submit" name="status" value="1" class="dropdown-item">Draft</button>
                                    <button type="submit" name="status" value="2" class="dropdown-item">Publish</button>
                                </div>
                            </div>
                        </div>
                        <a href="{{url('/supplier/invoice') }}" class="btn btn-secondary">{{ __('dashboard.return') }}</a>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection


@section('javascript')
<script src="{{ asset('js/app.js') }}" defer></script>

<script>
var isModalOpen = false;

function check(input, word){
    if (input.value == "") {
     input.setCustomValidity(word);
   } else  {
     input.setCustomValidity('');
   }
}
function changeHiddenValue(el, e){
    document.getElementById(el).value = e.value
}
function handleSubmit(el){
    changeHiddenValue('statusInv', el)
    $("#formInvoice").trigger('submit');
}
$(document).ready(function () {
    $("#selectAccount").select2({
        width: "100%",
        placeholder: "Choose Account",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/account') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        }
    });
    $("#formJournal").submit(function(e){
        e.preventDefault();
        $("#formInvoice").trigger('submit');
    })

    $("#modalPaid").on('shown.bs.modal', function(){
        $("#formJournal")[0].reset();
    });
    $("#modalPaid").on('hidden.bs.modal', function(){
        isModalOpen = false;
        $("#paymentdate, #paymentref, #accountid").val('');
    });
    $("#formInvoice").submit(function(e){
        window.onbeforeunload = null;
        // check if status is paid
        if($("#paymentInvoice").val() == "2" && !isModalOpen && $("#statusInv").val() == 2){
            e.preventDefault();
            $("#modalPaid").modal('show');
            isModalOpen = true;

        }else{

            return;
        }
    })
    const date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        language: 'id',
        weekStart: 1,
        // startDate: today,
        orientation: 'bottom'
    });
    $('.datepicker-invoice').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        language: 'id',
        weekStart: 1,
        // startDate: today,`
        orientation: 'bottom'
    });
    $("#selectQuotationSupplier").select2({
        width: "100%",
        placeholder: "{{ __('dashboard.quotation') }}",
        allowClear: true,
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/qtt-supplier') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
                    page_limit : 10,
                    supplier: $("#selectSupplier").val()
				};
            },

            cache: true,
        },
    });
    $("#selectPo").select2({
        allowClear: true,
        width: "100%",
        placeholder: "Po Supplier",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/po-supplier') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
                    page_limit : 10,
                    quotation: $("#selectQuotationSupplier").val()
				};
            },

            cache: true,
        },
    })

    $("#selectSupplier").select2({
        width: "100%",
        placeholder: "{{__('dashboard.supplier')}}",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/supplier') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        },
    });

    $("#selectSupplier").on('change', function(){
        $("#input_supplier_name").val($(this).select2('data')[0].text);
    })
    $("#selectQuotationSupplier").change(function() {
        const id = $(this).val()
        if (id) {
            $.ajax({
                type: "get",
                url: "/select/supplier-qtt-detail/"+id,
                dataType: "json"
            })
            .then(res => {
                // console.log(res);
                $("#attnInput").val(res.attn)
                $("#BlNoinput").val(res.bl_no)
                $("#fromInput").val(res.from)
                $("#toInput").val(res.to)
            })
        }
    })
});
</script>
@endsection
