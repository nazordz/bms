@extends('dashboard.base')

@section('content')

    <div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('dashboard.Item_List')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">@lang('dashboard.first_name')</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="input_first_name"></div>

                                    <div class="col-lg-4">@lang('dashboard.last_name')</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="input_last_name"></div>

                                    <div class="col-lg-4">@lang('dashboard.company')</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="input_company"></div>

                                    <div class="col-lg-4">@lang('dashboard.province')</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="input_province"></div>

                                    <div class="col-lg-4">@lang('dashboard.regency')</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="input_regency"></div>

                                    <div class="col-lg-4">@lang('dashboard.village')</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="input_village"></div>

                                    <div class="col-lg-4">@lang('dashboard.address')</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="input_address"></div>

                                    <div class="col-lg-4">Email</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="input_email"></div>

                                    <div class="col-lg-4">@lang('dashboard.phone')</div>
                                    <div class="col-lg-1">:</div>
                                    <div class="col-lg-6" class="info-detail" id="input_phone"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('dashboard.close')</button>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="animated fadeIn">
            @if (session('status-success'))
                <div class="alert alert-success">
                    {{ session('status-success') }}
                </div>
            @endif
            @if (session('status-fail'))
                <div class="alert alert-danger">
                    {{ session('status-fail') }}
                </div>
            @endif
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-users"></i> Supplier
                        </div>
                        <div class="card-body">
                            <div class="col-lg-12 mb-5">
                                <a href="{{ url('/supplier/create-supplier') }}" class="btn btn-primary pull-right" > {{ __('dashboard.create_supplier') }}</a>
                            </div>
                            <table id="tbsupplier" class="table table-responsive-lg table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>{{__('dashboard.name')}}</th>
                                        <th>{{__('dashboard.company')}}</th>
                                        <th>{{ __('dashboard.action') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('javascript')
<script>
$(document).ready(function () {

    var myTable = $("#tbsupplier").DataTable({
        responsive: true,
        processing : true,
        serverSide : true,
        order      : [[2,"ASC"]],
        autoWidth  : false,
        searchDelay: 500,
        ajax : {
            url : "{{ url('/supplier/table-supplier') }}"
        },
        columns:[
            {
                data: null,
                orderable  : false,
				searchable : false
            },
            { data : 'first_name', name : 'suppliers.first_name' },
            { data : 'company', name : 'suppliers.company' },
            {
                data: null,
                searchable:false,
                orderable : false,
                render(data, type, row){
                    return `<button class="btn-detail btn btn-sm btn-primary" data-supplier_id="${row.supplier_id}"><i class="fa fa-eye"></i> Lihat</button>
                            <a href="/supplier/edit-supplier/${row.supplier_id}" class="btn btn-success btn-sm"><i class="fa fa-cogs"></i> {{__('dashboard.edit')}}</a>
                            <button class="btn btn-danger btn-delete btn-sm" data-supplier_id="${row.supplier_id}"><i class="fa fa-trash"></i> {{__('dashboard.delete')}}</button>`
                }
            }
        ],
        fnCreatedRow(row, data, index) {
			if (myTable.page.info().start >= 10) {
				var panjang = myTable.page.info().length;
				var halaman = myTable.page.info().page;
				var i = 1;
				i = (halaman + 1) * panjang;
				i -= panjang - 1;
	        }else{
	        	var i = 1;
	        }
	        $('td', row).eq(0).html(index + i);
		},
        drawCallback(){
            $(".dataTables_length select").removeClass("form-control-sm");
            $(".btn-delete").click(function(){
                var supplier_id = $(this).data('supplier_id');
                Swal.fire({
                    title: '{{__("dashboard.are_you_sure")}}',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: '{{ __("dashboard.cancel") }}',
                    confirmButtonText: '{{__("dashboard.confirm_delete")}}'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: "post",
                            url: "{{ route('supplier.delete') }}",
                            data: { _token: "{{csrf_token()}}", _method:'delete',  supplier_id},
                            dataType: "json"
                        }).done(function(data){
                            myTable.draw();
                            Swal.fire( '{{__("dashboard.deleted")}}', '', 'success' );
                        }).catch(function(err){
                            console.log(err);
                        });
                    }
                });
            });
            $(".btn-detail").click(function(){
                const supplier_id = $(this).data('supplier_id');
                $.ajax({
                    type: "get",
                    url: "{{url('supplier/show-supplier')}}/"+supplier_id,
                    dataType: "json"
                })
                .done(function(data){
                    console.log(data);
                    $("#input_first_name").text(data.first_name);
                    $("#input_last_name").text(data.last_name);
                    $("#input_company").text(data.company);
                    $("#input_province").text(data.province.name);
                    $("#input_regency").text(data.regency.name);
                    $("#input_village").text(data.village.name);
                    $("#input_address").text(data.address);
                    $("#input_email").text(data.email)
                    $("#input_phone").text(data.phone)
                    $("#modalDetail").modal('show');
                });
            });

            $(".btn-detail").on('hidden.bs.modal', function(e){
                $("#item_picture").css('display', 'none')
                $(".info-detail").empty();
            })

		}
    })
});
</script>
@endsection

