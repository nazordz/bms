@extends('dashboard.base')
@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
        @if (session('status-success'))
            <div class="alert alert-success">
                {{ session('status-success') }}
            </div>
        @endif
        @if (session('status-fail'))
            <div class="alert alert-danger">
                {{ session('status-fail') }}
                @if($errors->any())
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                @endif
            </div>
        @endif
            <form action="{{ route('supplier.credit.note.store') }}" id="creditForm" enctype="multipart/form-data" method="post">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                @lang('dashboard.addCreditNote')
                            </div>
                            <div class="card-body">
                                @csrf
                                @if ($data && $data['payment'])
                                    <input type="hidden" name="payment" id="paymentInfo" value="{{ $data['payment'] }}">
                                @else
                                    <input type="hidden" name="payment" id="paymentInfo" value="">
                                @endif

                                <div class="row">
                                    <div class="form-group col-lg-3">
                                        <label for="">Suplier</label>
                                        <select id="selectSupplier" name="supplier_id" class="form-control" placeholder="" required>
                                            @if(old('supplier_id'))
                                                <option value="{{old('supplier_id')}}">{{ old('supplier_name') }}</option>
                                            @endif
                                            @if ($data)
                                                <option value="{{$data['supplier_id']}}">{{ $data['supplier_name'] }}</option>
                                            @endif
                                        </select>
                                        <input type="hidden" name="supplier_name" id="input_supplier_name" value="{{old('supplier_name')}}" />
                                        @if ($errors->first('supplier_id'))
                                            <small class="text-danger">{{ $errors->first('supplier_id') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="">Invoice</label>
                                        <select id="selectInvoice" name="invoice_id" class="form-control" placeholder="" required>
                                            @if(old('invoice_id'))
                                                <option value="{{old('invoice_id')}}">{{ old('serial') }}</option>
                                            @endif
                                            @if ($data)
                                                <option value="{{$data['invoice_id']}}">INV{{ $data['serial'] }} - {{ $data['supplier_name'] }}</option>
                                            @endif
                                        </select>
                                        @if ($data)
                                            <input type="hidden" name="serial" id="input_serial" value="INV{{ $data['serial'].' - '.$data['supplier_name'] }}" />
                                        @else
                                            <input type="hidden" name="serial" id="input_serial" value="{{old('serial')}}" />
                                        @endif
                                        @if ($errors->first('invoice_id'))
                                            <small class="text-danger">{{ $errors->first('invoice_id') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group col-lg-3">
                                        <label for="">Sales</label>
                                        <select id="selectSeller" name="seller_id" style="bottom: 18%; left: 50%;" onchange="check(this)" class="form-control" placeholder="" required>
                                            @if(old('seller_id'))
                                                <option value="{{old('seller_id')}}">{{ old('saller_name') }}</option>
                                            @endif
                                        </select>
                                        <input type="hidden" name="saller_name" id="input_seller_name" value="{{old('saller_name')}}" />
                                    </div>


                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.proof')</label>
                                        <input type="file" name="proof_picture" value="{{ old('proof_picture') }}" class="form-control-file">
                                        @if ($errors->first('proof_picture'))
                                            <small class="text-danger">{{ $errors->first('proof_picture') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group col-lg-3" id="paidFromAccount" style="{{$data &&  $data['payment'] == 1 ? 'display: none;':'display: block;' }}">
                                        <label for="">@lang('dashboard.paid_from_account')</label>
                                        <select style="bottom: 18%; left: 50%;" type="text" name="paid_from_account" id="inputPaidFromAccount" class="form-control selectAccount" {{$data &&  $data['payment'] == 2 ? 'required':'' }}>
                                        </select>
                                    </div>

                                    @if ($data)
                                        <v-rate-curr
                                            :data="{{$data}}"
                                            lang="{{Auth::user()->language}}"
                                            styling="col-lg-9"
                                        />
                                    @else
                                        <v-rate-curr
                                            lang="{{Auth::user()->language}}"
                                            styling="col-lg-9"
                                        />
                                    @endif
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">

                                        @if ($data)
                                            <note-component
                                                :data="{{$data}}"
                                                lang="{{ Auth::user()->language }}"
                                                url="{{ url('/select/item') }}"
                                            ></note-component>
                                        @else
                                            <note-component
                                                lang="{{ Auth::user()->language }}"
                                                url="{{ url('/select/item') }}"
                                            ></note-component>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <input type="submit" id="triggerSubmit" style="display:none;">
                    <input type="hidden" name="status" id="statusInv" value="">

                    <div class="col-lg-8 col-xs-12">
                        <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                            <div class="btn-group" role="group">
                                <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ __('dashboard.save') }}
                                </button>
                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                    <button type="button" name="status" value="1" onclick="handleSubmit(this)" class="dropdown-item">Draft</button>
                                    <button type="button" name="status" value="2" onclick="handleSubmit(this)" class="dropdown-item">Publish</button>
                                </div>
                            </div>
                        </div>
                        <a href="{{url('/supplier/credit-note') }}" class="btn btn-secondary">{{ __('dashboard.return') }}</a>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection


@section('javascript')
<script src="{{ asset('js/app.js') }}" defer></script>

<script>
function handleTotal(event){
    if(event.detail){
        $("#inputTotal").val(event.detail.numberValue)
    }
}
function changeHiddenValue(el, e){
    document.getElementById(el).value = e.value
}
function handleSubmit(el){
    changeHiddenValue('statusInv', el)
    $("#triggerSubmit").click();
}
function handleCharacter(e) {
    var key = e.which || e.keyCode;
    if ( key != 188 // Comma
        && key != 8 // Backspace
        && key != 9 // Tab
        && (key < 48 || key > 57) // Non digit
        && (key < 96 || key > 105) // Numpad Non digit
        )
    {
        e.preventDefault();
        return false;
    }
}
function check(input){
    console.log(input);

    if (input.value == "") {
     input.setCustomValidity('Choose Sales');
   } else  {
     input.setCustomValidity('');
   }
}

$(document).ready(function () {
    /*window.onbeforeunload = function() {
        return 'Are you sure that you want to leave this page?';
    };*/
    $("#creditForm").submit(function(){
        window.onbeforeunload = null;
        return;
    });
    $("#selectSeller").select2({
        width: "100%",
        placeholder: "Sales",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/sales') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        },
    });
    $("#selectSeller").on('change', function(){
        $("#input_seller_name").val($(this).select2('data')[0].text);
    });

    $(".selectAccount").select2({
        width: "100%",
        placeholder: "Choose Account",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/account') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },
            cache: true,
        }
    });

    const date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        language: 'id',
        weekStart: 1,
        startDate: today
    });

    $("#selectSupplier").select2({
        width: "100%",
        placeholder: "Suppliers",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/supplier') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        },
    });
    $("#selectInvoice").select2({
        width: "100%",
        placeholder: "Suppliers",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/inv-supplier') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
                    page_limit : 10,
                    supplier: $("#selectSupplier").val()
				};
            },

            cache: true,
        },
    })
    .on('select2:select', function({ params }){
        const { data } = params;
        $("#paymentInfo").val(data.payment);
        if(data.payment == 1){
            $("#paidFromAccount").hide();
            $(".selectAccount").select2('val', '');
            $(".selectAccount").prop('required', true);
        }else if(data.payment == 2){
            $("#paidFromAccount").show();
            $(".selectAccount").prop('required', false);

        }

    });

    $("#selectSupplier").on('change', function(){
        $("#input_supplier_name").val($(this).select2('data')[0].text);
    })
    $("#selectInvoice").on('change', function(){
        $("#input_serial").val($(this).select2('data')[0].text);
    })

});
</script>
@endsection
