<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kredit Note Supplier</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        body{
            font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
            color:#333;
            text-align:left;
            font-size:18px;
            margin:0;
        }

        caption{
            font-size:14px;
            margin-bottom:0;
        }
        table{
            border:1px solid #333;
            border-collapse:collapse;
            margin:30px auto;
            width:300px;
            height: auto;
            top: 3%;
            position: relative;
            font-family: sans-serif;
        }
        td, tr, th{
            padding:10px;
            font-size: 10px;
            border:1px solid #333;
            width:50px;
        }
        th{
            background-color: #f0f0f0;
        }
        h4, p{
            margin:0px;
        }
        .author{
            font-size: 12px;
        }
        .container{
            margin:0 auto;
            width:100%;
            height:auto;
            background-color:#fff;
            float: left;
        }
        .body-print{
            top: 15%;
            position: relative;
        }
        .header-print{
            text-align: center;
            z-index: 999;
            min-height: 300px;
            margin: 0 auto;
            width: 100%;
            margin: 0 auto;
        }
        .header-image{
            max-width: 300px;
        }
        .footer-print{
            text-align: center;
            z-index: 999;
            margin: 0 auto;
            bottom: 0;
            position: relative;
            max-width: 300px;
            display: block;
            margin-top: 50px;

        }
        .footer-regard{
            min-width: 170px;
            text-align:center;
            float: right;
        }
        .footer-down{
            margin-top: 100px;
        }
        .footer-image{
            max-width: 482px;
        }
    </style>
</head>
<body>
    @if ($web->header)
        <header class="print-header">
            <center>
                <img src="{{ public_path().$web->header }}" class="header-image" alt="header">
            </center>
        </header>
    @endif
    <div class="container">
        <div>
            <table>
                <caption><b>Nota Kredit</b></caption>
                <tbody>
                    <tr>
                        <th>NO Nota Kredit</th>
                        <td colspan="2">
                            CN{{$data->id}}
                        </td>
                    </tr>
                    <tr>
                        <th>Perusahaan</th>
                        <td colspan="2">
                            {{ $data->supplier->company }}
                        </td>
                    </tr>
                    <tr>
                        <th>Nama</th>
                        <td colspan="2">
                            {{ $data->supplier->first_name.' '.$data->supplier->last_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>Sales</th>
                        <td colspan="2">
                            {{$data->sales}}
                        </td>
                    </tr>
                    <tr>
                        <th>Catatan</th>
                        <td colspan="2">
                            {{$data->note}}
                        </td>
                    </tr>
                    <tr>
                        <th>Total</th>
                        <td colspan="2">
                            {{ $currency }} {{ number_format($data->total, 0, ",", $thousand_separator) }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <table style="border:none;width: 300px;">
                <tbody style="border:none;">
                    <tr style="border:none;">
                        <td style="border:none;" colspan="10"></td>
                        <td style="border:none;">
                            <div style="text-align: right;">
                                <p style="margin-bottom: 80px;">Hormat Kami</p>
                                <p> {{ $web->behalf_of }} </p>
                                <p> {{ $web->behalf_of_position }} </p>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="footer-print">
            @if ($web->footer)
                <div class="footer-down">
                    <img src="{{ public_path().$web->footer }}" class="footer-image" alt="footer">
                </div>
            @endif
        </div>
    </div>
</body>
</html>
