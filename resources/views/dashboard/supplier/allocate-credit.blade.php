@extends('dashboard.base')
@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
        @if ($data)
            <allocate-credit
                :data="{{ $data }}"
                lang="{{ Auth::user()->language }}">
            </allocate-credit>
        @else
            <allocate-credit
                lang="{{ Auth::user()->language }}">
            </allocate-credit>
        @endif

        </div>
    </div>

@endsection


@section('javascript')
<script src="{{ asset('js/app.js') }}" defer></script>

<script>
</script>
@endsection
