@extends('dashboard.base')
@section('content')

    <!-- Modal -->
    <div class="modal fade" id="modalCategory" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('dashboard.addCategory')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="post" id="createCategory">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">{{ __('dashboard.name') }}</label>
                            <input type="text" name="name" id="inputNameCategory" class="form-control" placeholder="" aria-describedby="helpId">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="submitCategory" class="btn btn-primary">{{ __('dashboard.save') }}</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('dashboard.return') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="animated fadeIn">
        @if (session('status-success'))
            <div class="alert alert-success">
                {{ session('status-success') }}
            </div>
        @endif
        @if (session('status-fail'))
            <div class="alert alert-danger">
                {{ session('status-fail') }}
            </div>
        @endif
            <form action="{{ route('supplier.debit.note.store') }}" id="formDebit" enctype="multipart/form-data" method="post">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                @lang('dashboard.addDebitNote')
                            </div>
                            <div class="card-body">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-lg-3">
                                        <label for="">Supplier</label>
                                        <select id="selectSupplier" name="supplier_id" class="form-control" placeholder="" required>
                                            @if(old('supplier_id'))
                                                <option value="{{old('supplier_id')}}">{{ old('supplier_name') }}</option>
                                            @elseif ($data)
                                                <option value="{{$data['supplier_id']}}">{{ $data['supplier_name'] }}</option>
                                            @endif
                                        </select>
                                        <input type="hidden" name="supplier_name" id="input_supplier_name" value="{{old('supplier_name')}}" />
                                        @if ($errors->first('supplier_id'))
                                            <small class="text-danger">{{ $errors->first('supplier_id') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="">Invoice</label>
                                        <select id="selectInvoice" name="invoice_id" class="form-control" placeholder="" required>
                                            @if(old('invoice_id'))
                                                <option value="{{old('invoice_id')}}">{{ old('serial') }}</option>
                                            @elseif ($data)
                                                <option value="{{$data['invoice_id']}}">INV{{ $data['serial'] }} - {{ $data['supplier_name'] }}</option>
                                            @endif
                                        </select>
                                        @if ($data)
                                            <input type="hidden" name="serial" id="input_serial" value="INV{{ $data['serial'].' - '.$data['supplier_name'] }}" />
                                        @else
                                            <input type="hidden" name="serial" id="input_serial" value="{{old('serial')}}" />
                                        @endif
                                        @if ($errors->first('invoice_id'))
                                            <small class="text-danger">{{ $errors->first('invoice_id') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.proof')</label>
                                        <input type="file" name="proof_picture" value="{{ old('proof_picture') }}" class="form-control-file">
                                        @if ($errors->first('proof_picture'))
                                            <small class="text-danger">{{ $errors->first('proof_picture') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="">Account</label>
                                        <select id="selectAccount" name="account_id" class="form-control" placeholder="" required>
                                            @if(old('supplier_credit_note_id'))
                                                <option value="{{old('account_id')}}">{{ old('input_account') }}</option>
                                            @endif
                                        </select>
                                        <input type="hidden" name="input_account" id="input_account" value="{{old('input_account')}}" />
                                        @if ($errors->first('account_id'))
                                            <small class="text-danger">{{ $errors->first('account_id') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.debit_note_date')</label>
                                        <div class="input-group">
                                            <input type="text" required autocomplete="off" id="invoiceDate" value="{{old('debit_note_date') ?? date('d-m-Y') }}" class="form-control datepicker-debit" onkeydown="return false;" name="debit_note_date">
                                            <div class="input-group-append">
                                                <div class="input-group-text"><i class="fas fa-calendar"></i></div>
                                            </div>
                                        </div>
                                        @if ($errors->first('debit_note_date'))
                                            <small class="text-danger">{{ $errors->first('debit_note_date') }}</small>
                                        @endif
                                    </div>
                                    @if ($data)
                                        <v-rate-curr
                                            :data="{{$data}}"
                                            lang="{{Auth::user()->language}}"
                                            styling="col-lg-9"
                                            :readonly="{{($data['copy_form'] == 'true') ? 'true': 'false'}}"
                                        />
                                    @else
                                        <v-rate-curr
                                            lang="{{Auth::user()->language}}"
                                            styling="col-lg-9"
                                        />
                                    @endif

                                </div>

                                <div class="row">
                                    <div class="col-lg-12">

                                        @if ($data)
                                            <note-component
                                                :data="{{$data}}"
                                                lang="{{ Auth::user()->language }}"
                                                url="{{ url('/select/item') }}"
                                            ></note-component>
                                        @else
                                            <note-component
                                                lang="{{ Auth::user()->language }}"
                                                url="{{ url('/select/item') }}"
                                            ></note-component>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="submit" id="triggerSubmit" style="display:none;">
                    <input type="hidden" name="status" id="statusInv" value="">

                    <div class="col-lg-8 col-xs-12">
                        <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                            <div class="btn-group" role="group">
                                <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ __('dashboard.save') }}
                                </button>
                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                    {{-- <button type="button" name="status" value="1" onclick="handleSubmit(this)" class="dropdown-item">Draft</button>
                                    <button type="button" name="status" value="2" onclick="handleSubmit(this)" class="dropdown-item">Publish</button> --}}
                                    <button type="submit" name="status" value="1" class="dropdown-item">Draft</button>
                                    <button type="submit" name="status" value="2" class="dropdown-item">Publish</button>
                                </div>
                            </div>
                        </div>
                        <a href="{{url('/supplier/debit-note') }}" class="btn btn-secondary">{{ __('dashboard.return') }}</a>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection


@section('javascript')
<script src="{{ asset('js/app.js') }}" defer></script>

<script>
function changeHiddenValue(el, e){
    document.getElementById(el).value = e.value
}
function handleSubmit(el){
    changeHiddenValue('statusInv', el)
    $("#triggerSubmit").click();
}
$(document).ready(function () {
    $("#formDebit").submit(function(){
        window.onbeforeunload = null;
        return;
    })

    const date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        language: 'id',
        weekStart: 1,
        startDate: today,
        orientation: 'bottom'
    });
    $('.datepicker-debit').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        language: 'id',
        weekStart: 1,
        // startDate: today,`
        orientation: 'bottom'
    });

    $("#selectSupplier").select2({
        width: "100%",
        placeholder: "{{__('dashboard.supplier')}}",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/supplier') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        },
    });
    $("#selectInvoice").select2({
        width: "100%",
        placeholder: "Invoice",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/inv-supplier') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
                    page_limit : 10,
                    supplier: $("#selectSupplier").val()
				};
            },

            cache: true,
        },
    });

    $("#selectAccount").select2({
        width: "100%",
        placeholder: "Choose Account",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/account') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        }
    });


    $("#selectAccount").on('change', function(){
        $("#input_account").val($(this).select2('data')[0].text);
    })
    $("#selectInvoice").on('change', function(){
        $("#input_serial").val($(this).select2('data')[0].text);
    })
    $("#selectSupplier").on('change', function(){
        $("#input_supplier_name").val($(this).select2('data')[0].text);
    })

});
</script>
@endsection
