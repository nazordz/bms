@extends('dashboard.base')
@section('content')

    <!-- Modal -->
    <div class="modal fade" id="modalCategory" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('dashboard.addCategory')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="post" id="createCategory">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">{{ __('dashboard.name') }}</label>
                            <input type="text" name="name" id="inputNameCategory" class="form-control" placeholder="" aria-describedby="helpId">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="submitCategory" class="btn btn-primary">{{ __('dashboard.save') }}</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('dashboard.return') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="animated fadeIn">
        @if (session('status-success'))
            <div class="alert alert-success">
                {{ session('status-success') }}
            </div>
        @endif
        @if (session('status-fail'))
            <div class="alert alert-danger">
                {{ session('status-fail') }}
            </div>
        @endif
            <form action="{{ route('supplier.po.store') }}" id="formPo" enctype="multipart/form-data" method="post">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                @lang('dashboard.create_po')
                            </div>
                            <div class="card-body">
                                @csrf
                                @if(request()->has('quotation_id'))
                                    <input type="hidden" name="quotation_id" value="{{ request()->quotation_id }}">
                                @endif
                                <div class="row">
                                    <div class="form-group col-lg-3">
                                        <label for="">Supplier</label>
                                        <select id="selectSupplier" style="bottom: 18%; left: 50%;" oninvalid="check(this, 'Choose Supplier')" onchange="setCustomValidity('')" name="supplier_id" class="form-control" placeholder="" required>
                                            @if(old('supplier_id'))
                                                <option value="{{old('supplier_id')}}">{{ old('supplier_name') }}</option>
                                            @elseif($supplier)
                                                <option value="{{ $supplier['supplier_id'] }}">{{ $supplier['supplier_name'] }}</option>
                                            @endif
                                        </select>
                                        @if ($supplier)
                                            <input type="hidden" name="supplier_name" id="input_supplier_name" value="{{$supplier['supplier_name']}}" />
                                        @else
                                            <input type="hidden" name="supplier_name" id="input_supplier_name" value="{{old('supplier_name')}}" />
                                        @endif
                                        @if ($errors->first('supplier_id'))
                                            <small class="text-danger">{{ $errors->first('supplier_id') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group col-lg-3">
                                        <label for="">{{__('dashboard.quotation')}}</label>
                                        <select id="selectQuotation" style="bottom: 18%; left: 50%;" name="quotation_id" class="form-control" placeholder="">
                                            @if(old('quotation_id'))
                                                <option value="{{old('quotation_id')}}">{{ old('quotation_name') }}</option>
                                            @elseif($data)
                                                <option value="{{ $data['quotation_id'] }}">{{ $data['quotation_name'].' - '.$data['supplier_name'] }}</option>
                                            @endif
                                        </select>
                                        <input type="hidden" name="quotation_name" id="input_quotation_name" value="{{$data? $data['quotation_name'].' - '.$data['supplier_name'] : ''}}" />

                                        @if ($errors->first('quotation_id'))
                                            <small class="text-danger">{{ $errors->first('quotation_id') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.po_date')</label>
                                        <div class="input-group">
                                            <input type="text" name="po_date" class="form-control datepicker-po" autocomplete="off" value="{{ date('d-m-Y') }}" required onkeydown="return false;">
                                            <div class="input-group-append">
                                                <div class="input-group-text"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.shipping_date')</label>
                                        <div class="input-group">
                                            <input type="text" name="shipping_date" class="form-control datepicker-shipping" autocomplete="off" value="" required onkeydown="return false;">
                                            <div class="input-group-append">
                                                <div class="input-group-text"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                    @if ($data)
                                        <v-rate-curr :data="{{$data}}" lang="{{Auth::user()->language}}" styling="col-lg-12" />
                                    @else
                                        <v-rate-curr lang="{{Auth::user()->language}}" styling="col-lg-12" />
                                    @endif
                                    {{-- <div class="form-group col-lg-4">
                                        <label for="">@lang('dashboard.shipped')</label>
                                        <select name="status" id="" class="form-control">
                                            <option value="1">Shipping</option>
                                            <option value="2">Delivered</option>
                                            <option value="0">Void</option>
                                        </select>
                                        @if ($errors->first('status'))
                                            <small class="text-danger">{{ $errors->first('status') }}</small>
                                        @endif
                                    </div> --}}
                                </div>
                            </div>
                        </div>

                        {{-- <div class="card">
                            <div class="card-body">
                                <div class="row">

                                    <div class="form-group col-lg-3">
                                        <label for="">Attn</label>
                                        <input type="text" class="form-control" id="inputAttn" name="attn" required
                                             value="{{ $data ? $data['attn']: '' }}">
                                    </div>

                                    <div class="form-group col-lg-4">
                                        <label for="">@lang('dashboard.from')</label>
                                        <input type="text" class="form-control" id="inputFrom" name="from" required
                                             value="{{ $data ? $data['from']: '' }}">
                                    </div>

                                    <div class="form-group col-lg-4">
                                        <label for="">@lang('dashboard.to')</label>
                                        <input type="text" class="form-control" id="inputTo" name="to" required
                                             value="{{ $data ? $data['to']: '' }}">
                                    </div>

                                </div>

                            </div>
                        </div> --}}

                        <div class="card">
                            <div class="card-body">
                                @if (request()->has('quotation_id'))
                                    <item-component
                                        :data="{{ $data }}"
                                        url="{{ url('/select/item') }}"
                                        lang="{{ Auth::user()->language }}">
                                    </item-component>
                                @else
                                    <item-component
                                        url="{{ url('/select/item') }}"
                                        lang="{{ Auth::user()->language }}">
                                    </item-component>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-xs-12">
                        <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                            <div class="btn-group" role="group">
                                <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ __('dashboard.save') }}
                                </button>
                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                    <button type="submit" name="status" value="1" class="dropdown-item">Draft</button>
                                    <button type="submit" name="status" value="2" class="dropdown-item">Publish</button>
                                </div>
                            </div>
                        </div>
                        <a href="{{url('/supplier/purchase-order') }}" class="btn btn-secondary">{{ __('dashboard.return') }}</a>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection

@section('javascript')
<script src="{{ asset('js/app.js') }}" defer></script>
<script>
function check(input, word){
    if (input.value == "") {
     input.setCustomValidity(word);
   } else  {
     input.setCustomValidity('');
   }
}
$(document).ready(function () {
    /*window.onbeforeunload = function() {
        return 'Are you sure that you want to leave this page?';
    };*/
    $("#formPo").submit(function(){
        window.onbeforeunload = null;
        return;
    })
    const date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $('.datepicker-po').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        language: 'id',
        orientation: 'bottom',
        weekStart: 1,
    });
    $('.datepicker-shipping').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        language: 'id',
        orientation: 'bottom',
        weekStart: 1,
        startDate: today
    });

    $("#selectQuotation").select2({
        width: "100%",
        placeholder: "{{ __('dashboard.quotation') }}",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/qtt-supplier') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
                    page_limit : 10,
                    supplier: $("#selectSupplier").val()
				};
            },

            cache: true,
        },
    });
    $("#selectSupplier").select2({
        width: "100%",
        placeholder: "{{__('dashboard.supplier')}}",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/supplier') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        },
    });

    $("#selectSupplier").on('change', function(){
        $("#input_supplier_name").val($(this).select2('data')[0].text);
    })
    $("#selectQuotation").on('change', function(){
        $("#input_quotation_name").val($(this).select2('data')[0].text);
    })

});
</script>
@endsection
