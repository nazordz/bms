@extends('dashboard.base')
@section('content')

<!-- Modal -->
<div class="modal fade" id="modalCategory" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('dashboard.addCategory')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post" id="createCategory">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">{{ __('dashboard.name') }}</label>
                        <input type="text" name="name" id="inputNameCategory" class="form-control" placeholder=""
                            aria-describedby="helpId">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="submitCategory"
                        class="btn btn-primary">{{ __('dashboard.save') }}</button>
                    <button type="button" class="btn btn-secondary"
                        data-dismiss="modal">{{ __('dashboard.return') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="animated fadeIn">
        @if (session('status-success'))
        <div class="alert alert-success">
            {{ session('status-success') }}
        </div>
        @endif
        @if (session('status-fail'))
        <div class="alert alert-danger">
            {{ session('status-fail') }}
        </div>
        @endif
        <form action="{{ route('supplier.quotation.update') }}" id="formQuotation" enctype="multipart/form-data"
            method="post">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-header-split">
                                <div class="">
                                    @lang('dashboard.quotation') QTT{{ $data->serializing }}
                                    <input type="hidden" name="serial" id="inputSerial" value="{{ $data->serial }}">
                                </div>
                                @if ($data->status == 0)
                                <div class="alert-header alert-danger">Void</div>
                                @elseif($data->status == 1)
                                <div class="alert-header alert-warning">Draft</div>
                                @elseif($data->status == 2)
                                <div class="alert-header alert-success">Publish</div>
                                @endif
                            </div>
                        </div>
                        <div class="card-body">
                            @method('put')
                            @csrf
                            <input type="hidden" name="quotation_id" value="{{ $data->quotation_id }}">
                            <div class="row">
                                <div class="form-group col-lg-3">
                                    <label for="">Supplier</label>
                                    <select id="selectSupplier" name="supplier_id" style="bottom: 18%; left: 50%;"
                                        onchange="setCustomValidity('')" oninvalid="check(this, 'Choose Supplier')"
                                        class="form-control" placeholder="" required>
                                        @if($data->supplier)
                                        <option value="{{ $data->supplier->supplier_id }}">
                                            {{ $data->supplier->company.' - '.$data->supplier->first_name.' '.$data->supplier->last_name }}
                                        </option>
                                        @endif
                                    </select>
                                    @if ($data)
                                    <input type="hidden" name="supplier_name" id="input_supplier_name"
                                        value="{{ $data->supplier->company.' - '.$data->supplier->first_name.' '.$data->supplier->last_name }}" />
                                    @endif
                                    @if ($errors->first('supplier_id'))
                                    <small class="text-danger">{{ $errors->first('supplier_id') }}</small>
                                    @endif
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="">@lang('dashboard.quotation_date')</label>
                                    <div class="input-group">
                                        <input type="text" {{ ($data->status == 0) ? 'disabled' : '' }}
                                            name="quotation_date" id="quotation_date"
                                            value="{{date_format(date_create($data->quotation_date), 'd-m-Y') }}"
                                            onkeydown="return false;" autocomplete="off" required
                                            class="form-control datepicker" placeholder="">
                                        <div class="input-group-append">
                                            <div class="input-group-text"><i class="fa fa-calendar"
                                                    aria-hidden="true"></i></div>
                                        </div>
                                    </div>
                                    @if ($errors->first('quotation_date'))
                                    <small class="text-danger">{{ $errors->first('quotation_date') }}</small>
                                    @endif
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="">@lang('dashboard.valid_until')</label>
                                    <div class="input-group">
                                        <input type="text" name="valid_until"
                                            value="{{date_format(date_create($data->valid_until), 'd-m-Y') }}"
                                            onkeydown="return false;" required class="form-control datepicker"
                                            placeholder="">
                                        <div class="input-group-append">
                                            <div class="input-group-text"><i class="fa fa-calendar"
                                                    aria-hidden="true"></i></div>
                                        </div>
                                    </div>
                                    @if ($errors->first('valid_until'))
                                    <small class="text-danger">{{ $errors->first('valid_until') }}</small>
                                    @endif
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="">@lang('dashboard.term_of_payment')</label>
                                    <select name="term_of_payment" class="form-control">
                                        <option {{ $data->term_of_payment == 'COD' ? 'selected': '' }} value="COD">COD</option>
                                        <option {{ $data->term_of_payment == '30 Hari' ? 'selected': '' }} value="30 Hari">30 Hari</option>
                                        <option {{ $data->term_of_payment == '60 Hari' ? 'selected': '' }} value="60 Hari">60 Hari</option>
                                        <option {{ $data->term_of_payment == '90 Hari' ? 'selected': '' }} value="90 Hari">90 Hari</option>
                                    </select>
                                    @if ($errors->first('payment'))
                                        <small class="text-danger">{{ $errors->first('payment') }}</small>
                                    @endif
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="">File</label>
                                    <div>
                                        @if ($data->file)
                                        <a download href="{{ $data->file }}"
                                            class="btn btn-sm btn-secondary mb-2">download</a>
                                        @endif
                                        <input type="file" name="file" value="{{ old('file') }}"
                                            class="form-control-file" placeholder="">
                                    </div>
                                    @if ($errors->first('file'))
                                    <small class="text-danger">{{ $errors->first('file') }}</small>
                                    @endif
                                </div>



                                <v-rate-curr :data="{{$data}}" lang="{{Auth::user()->language}}" styling="col-lg-9"
                                    :disabled="{{ ($data->status == 0) ? 'true' : 'false' }}" />
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <div class="row">

                                <div class="form-group col-lg-3">
                                    <label for="">Attn</label>
                                    <input type="text" class="form-control" id="inputAttn" name="attn" required
                                        {{ ($data->status == 0) ? 'disabled' : '' }} value="{{ $data->attn }}">
                                </div>

                                <div class="form-group col-lg-4">
                                    <label for="">@lang('dashboard.from')</label>
                                    <input type="text" class="form-control" id="inputFrom" name="from" required
                                        {{ ($data->status == 0) ? 'disabled' : '' }} value="{{ $data->from }}">
                                </div>

                                <div class="form-group col-lg-4">
                                    <label for="">@lang('dashboard.to')</label>
                                    <input type="text" class="form-control" id="inputTo" name="to" required
                                        {{ ($data->status == 0) ? 'disabled' : '' }} value="{{ $data->to }}">
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <item-component :data="{{$data}}" url="{{ url('/select/item') }}"
                                lang="{{ Auth::user()->language }}">
                            </item-component>
                        </div>
                    </div>

                </div>

                <div class="col-lg-8 col-xs-12">
                    @if ($data->status)
                        @if ($data->status == 1)
                        <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                            <div class="btn-group" role="group">
                                <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ __('dashboard.save') }}
                                </button>
                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                    <button type="submit" name="status" value="1" class="dropdown-item">Draft</button>
                                    <button type="submit" name="status" value="2" class="dropdown-item">Publish</button>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-danger btn-delete" data-type="cancel" data-quotation_id="{{$data->quotation_id}}" type="button">Cancel</button>

                        @else
                            <button type="submit" name="status" value="2" class="btn btn-primary">{{ __('dashboard.save') }}</button>
                            <button type="button" class="btn btn-dark btn-create-qtt" data-quotation="{{$data->quotation_id}}">@lang('dashboard.create_quotation')</button>
                            <button type="button" class="btn btn-info btn-create-po" data-quotation="{{$data->quotation_id}}">@lang('dashboard.create_po')</button>
                            <button type="button" class="btn btn-warning text-white btn-create-inv" data-quotation="{{$data->quotation_id}}">@lang('dashboard.create_invoice')</button>
                            {{-- @if ($data->payment != 2 || ($data->status == 1 && $data->payment == 2)) --}}
                                <button class="btn btn-danger btn-delete" data-type="void" data-quotation_id="{{$data->quotation_id}}" type="button">Void</button>
                            {{-- @endif --}}
                        @endif
                        {{-- <button class="btn btn-success btn-print" data-uuid="{{$data->uuid}}" type="button">{{ __('dashboard.print') }}</button> --}}
                    @endif
                    <a href="{{url('/supplier/list-quotation') }}" class="btn btn-secondary">{{ __('dashboard.return') }}</a>
                </div>
            </div>
        </form>

    </div>
</div>

@endsection


@section('javascript')
<script src="{{ asset('js/app.js') }}" defer></script>
<script>
    function check(input, word){
    if (input.value == "") {
     input.setCustomValidity(word);
   } else  {
     input.setCustomValidity('');
   }
}
$(document).ready(function () {

    $(".btn-print").click(function(){
        // location = '/supplier/print-quotation/'+$(this).data('id');
        window.open('/supplier/print-quotation/'+$(this).data('uuid'), '_blank');
    });
    $(".btn-delete").click(function(){
        var quotation_id = $(this).data('quotation_id');
        var type = $(this).data('type');
        Swal.fire({
            title: '{{__("dashboard.are_you_sure")}}',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: '{{ __("dashboard.cancel") }}',
            confirmButtonText: 'Okay'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "post",
                    url: "{{ route('supplier.quotation.delete') }}",
                    data: { _token: "{{csrf_token()}}", _method:'delete',  quotation_id, type},
                    dataType: "json"
                }).done(function(data){
                    var response = 'Quotation Voided';
                    if(type == 'cancel') response = 'Quotation Canceled';
                    Swal.fire( response, '', 'success' );
                    setTimeout(() => {
                        location = '/supplier/list-quotation';
                    }, 2000);
                }).catch(function(err){
                    console.log(err);
                });
            }
        });
    });
    $("#formQuotation").submit(function(){
        window.onbeforeunload = null;
        return;
    });
    $('.btn-create-qtt').click(function(e){
        e.preventDefault();
        window.onbeforeunload = null;
        let supplier  = {
            supplier_id: $("#selectSupplier").val(),
            supplier_name: $("#input_supplier_name").val()
        }
        supplier = $.param(supplier)
        let item = $.param(items);
        let form = $.param({
            attn          : $("#inputAttn").val(),
            vessel        : $("#inputVessel").val(),
            voyage        : $("#inputVoyage").val(),
            quotation_name: 'QTT'+$("#inputSerial").val(),
            bl_no         : $("#inputBl_no").val(),
            from          : $("#inputFrom").val(),
            to            : $("#inputTo").val(),
            copy_form: 'true'
        });
        location = `/supplier/create-quotation?${supplier}&${item}&${form}`;
    });
    $('.btn-create-inv').click(function(e){
        e.preventDefault();
        window.onbeforeunload = null;
        let quotation = $(this).data('quotation');
        let supplier  = {
            supplier_id: $("#selectSupplier").val(),
            supplier_name: $("#input_supplier_name").val()
        }
        supplier = $.param(supplier)
        let item = $.param(items);
        let form = $.param({
            attn          : $("#inputAttn").val(),
            vessel        : $("#inputVessel").val(),
            voyage        : $("#inputVoyage").val(),
            quotation_name: 'QTT'+$("#inputSerial").val(),
            bl_no         : $("#inputBl_no").val(),
            from          : $("#inputFrom").val(),
            to            : $("#inputTo").val(),
            copy_form: 'true'
        });
        location = `/supplier/create-invoice?quotation_id=${quotation}&${supplier}&${item}&${form}`;
    });
    $('.btn-create-po').click(function(){
        let quotation = $(this).data('quotation');
        let supplier  = {
            supplier_id: $("#selectSupplier").val(),
            supplier_name: $("#input_supplier_name").val()
        }
        supplier = $.param(supplier)
        let item = $.param(items);
        let form = $.param({
            attn: $("#inputAttn").val(),
            from: $("#inputFrom").val(),
            to  : $("#inputTo").val(),
            quotation_name: 'QTT'+$("#inputSerial").val(),
            copy_form: 'true'
        });
        location = `/supplier/create-purchase-order?quotation_id=${quotation}&${supplier}&${item}&${form}`;
    });
    const date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $('.datepicker').datepicker({
        autoclose: true,
        format: {
            toDisplay(date, format, language){
                var tgl = moment(date);
                return tgl.format('DD-MM-Y')
            },
            toValue(date, format, language){
                var tgl = moment(date);
                return tgl.format('Y-MM-DD')
            }
        },
        language: 'id',
        weekStart: 1,
        startDate: today,
        orientation: 'bottom'

    });

    $("#selectSupplier").select2({
        width: "100%",
        placeholder: "{{__('dashboard.supplier')}}",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/supplier') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        },
    });

    $("#selectSupplier").on('change', function(){
        $("#input_supplier_name").val($(this).select2('data')[0].text);
    })





});
</script>
@endsection
