@extends('dashboard.base')
@section('content')
    <div class="modal fade" id="modalPaid" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('dashboard.payment')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" id="formJournal" method="post">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="" class="col-lg-3">@lang('dashboard.payment_date')</label>
                            <div class="col-lg-9">
                                <div class="input-group">
                                    <input type="text" required name="payment_date" onchange="changeHiddenValue('paymentdate', this)" onkeydown="return false;" autocomplete="off" class="form-control datepicker" placeholder="">
                                    <div class="input-group-append">
                                        <div class="input-group-text"><i class="fas fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-lg-3">@lang('dashboard.payment_ref')</label>
                            <div class="col-lg-9">
                                <input type="text" required onchange="changeHiddenValue('paymentref', this)" name="payment_ref" class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-lg-3">@lang('dashboard.account')</label>
                            <div class="col-lg-9">
                                <select id="selectAccount" style="bottom: 18%; left: 50%;" onchange="changeHiddenValue('accountid', this)" oninvalid="check(this, 'Choose Account')" name="account_id" class="form-control" placeholder="" required>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('dashboard.close')</button>
                        <button type="submit" class="btn btn-primary">@lang('dashboard.save')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="animated fadeIn">
        @if (session('status-success'))
            <div class="alert alert-success">
                {{ session('status-success') }}
            </div>
        @endif
        @if (session('status-fail'))
            <div class="alert alert-danger">
                {{ session('status-fail') }}
                @if ($errors->any())
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
            </div>
        @endif
            <form action="{{ route('supplier.invoice.update') }}" id="formInvoice" enctype="multipart/form-data" method="post">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-header-split">
                                    <div class="">
                                        Invoice INV{{$data->serializing}}
                                    </div>
                                    @if ($data->status == 1)
                                        <div class="alert-header alert-warning">Draft</div>
                                    @elseif($data->status == 2)
                                        <div class="card-header-notif">
                                            <div class="alert-header alert-success">Publish</div>
                                            @if ($data->payment == 2)
                                                <div class="alert-header alert-success">Paid</div>
                                            @else
                                                <div class="alert-header alert-warning">Unpaid</div>
                                            @endif
                                        </div>
                                    @else
                                        <div class="alert-header alert-danger">Void</div>
                                    @endif
                                </div>
                            </div>
                            <div class="card-body">
                                @method('put')
                                @csrf
                                <input type="hidden" id="serialInput" name="serial" value="{{ $data->serializing }}">
                                <input type="hidden" name="invoice_id" value="{{ $data->invoice_id }}">
                                <div class="row">
                                    <div class="form-group col-lg-3">
                                        <label for="">supplier</label>
                                        <select id="selectSupplier" name="supplier_id" {{ $isFormDisabled }} style="bottom: 18%; left: 50%;" onchange="setCustomValidity('')" oninvalid="check(this, 'Choose Supplier')" class="form-control" placeholder="" required>
                                            @if($data->supplier)
                                                <option value="{{ $data->supplier->supplier_id }}"> {{ $data->supplier->company.' - '.$data->supplier->first_name.' '.$data->supplier->last_name }}  </option>
                                            @endif
                                        </select>
                                        @if ($data)
                                                <input type="hidden" name="supplier_name" id="input_supplier_name" value="{{ $data->supplier->company.' - '.$data->supplier->first_name.' '.$data->supplier->last_name }}" />
                                        @endif
                                        @if ($errors->first('supplier_id'))
                                            <small class="text-danger">{{ $errors->first('supplier_id') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group col-lg-3">
                                        <label for="">{{__('dashboard.quotation')}}</label>
                                        <select id="selectQuotation" style="bottom: 18%; left: 50%;" {{ $isFormDisabled }} name="quotation_id" class="form-control" placeholder="">
                                            @if($data->quotation)
                                                <option value="{{ $data->quotation->quotation_id }}">QTT{{ $data->quotation->serializing.' - '.$data->supplier->company.' - '. $data->supplier->first_name.' '.$data->supplier->last_name }}</option>
                                            @endif
                                        </select>
                                        {{-- @if ($data->quotation)
                                            <input type="hidden" name="quotation_name" id="input_quotation_name" value="QTT{{ $data->quotation->serial.' - '.$data->supplier->company.' - '. $data->supplier->first_name.' '.$data->supplier->last_name }}" />
                                        @else
                                            <input type="hidden" name="quotation_name" id="input_quotation_name" value="" />
                                        @endif --}}

                                        @if ($errors->first('quotation_id'))
                                            <small class="text-danger">{{ $errors->first('quotation_id') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group col-lg-3">
                                        <label for="selectPo">Purchase Order No</label>
                                        <select id="selectPo" style="bottom: 18%; left: 50%;" {{ $isFormDisabled }} name="po_id" class="form-control" placeholder="" >
                                            @if($data->purchase_order)
                                                <option value="{{$data->purchase_order->po_id}}">PO{{ $data->purchase_order->serializing }} - {{ $data->supplier->company }} - {{ $data->supplier->first_name }}</option>
                                            @endif
                                        </select>
                                    </div>

                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.term_of_payment')</label>
                                        <select name="term_of_payment" class="form-control" {{$isFormDisabled}}>
                                            <option {{ $data->term_of_payment == 'COD' }} value="COD">COD</option>
                                            <option {{ $data->term_of_payment == '30 Hari' ? 'selected' : '' }} value="30 Hari">30 Hari</option>
                                            <option {{ $data->term_of_payment == '60 Hari' ? 'selected' : '' }} value="60 Hari">60 Hari</option>
                                            <option {{ $data->term_of_payment == '90 Hari' ? 'selected' : '' }} value="90 Hari">90 Hari</option>
                                        </select>
                                        @if ($errors->first('payment'))
                                            <small class="text-danger">{{ $errors->first('payment') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.invoice_date')</label>
                                        <div class="input-group">
                                            <input type="text" name="invoice_date" {{ $isFormDisabled }} value="{{date_format(date_create($data->invoice_date), 'd-m-Y') }}" onkeydown="return false;" autocomplete="off" required class="form-control datepicker-invoice" placeholder="">
                                            <div class="input-group-append">
                                                <div class="input-group-text"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                                            </div>
                                        </div>
                                        @if ($errors->first('invoice_date'))
                                            <small class="text-danger">{{ $errors->first('invoice_date') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.due_date')</label>
                                        <div class="input-group">
                                            <input type="text" {{ $isFormDisabled }} name="due_date" value="{{date_format(date_create($data->due_date), 'd-m-Y') }}" onkeydown="return false;" autocomplete="off" required class="form-control datepicker" placeholder="">
                                            <div class="input-group-append">
                                                <div class="input-group-text"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                                            </div>
                                        </div>
                                        @if ($errors->first('due_date'))
                                            <small class="text-danger">{{ $errors->first('due_date') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group col-lg-3">
                                        <label for="">File Invoice</label>
                                        <div>
                                            @if ($data->file)
                                               <a download href="{{ $data->file }}" class="btn btn-sm btn-secondary mb-2">download</a>
                                            @endif
                                            <input type="file" {{ $isFormDisabled }} name="file" value="{{ old('file') }}" class="form-control-file" placeholder="">
                                        </div>
                                        @if ($errors->first('file'))
                                            <small class="text-danger">{{ $errors->first('file') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="">@lang('dashboard.purchase_account')</label>
                                        <select name="purchase_account" class="form-control selectAccount" required {{$isFormDisabled}}>
                                            @if ($data->purchaseAccount)
                                                <option value="{{$data->purchase_account}}">{{ $data->purchaseAccount->id.' '.$data->purchaseAccount->name }}</option>
                                            @endif
                                        </select>
                                        @if ($errors->first('purchase_account'))
                                            <small class="text-danger">{{ $errors->first('purchase_account') }}</small>
                                        @endif
                                    </div>
                                    @if ($data)
                                        <v-rate-curr
                                            :data="{{$data}}"
                                            lang="{{Auth::user()->language}}"
                                            styling="col-lg-9"
                                            :disabled="{{($data->paidAmount > 0 || $data->status == 0) ? 'true': 'false'}}"
                                        />
                                    @else
                                        <v-rate-curr
                                            lang="{{Auth::user()->language}}"
                                            styling="col-lg-9"
                                        />
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div id="accordion">
                            <div class="card">
                                <div class="card-header text-right" id="headingForm">
                                    <button class="btn btn-link btn-sm" type="button" onclick="$('.collapse').collapse('toggle');" style="cursor: pointer;" data-toggle="collapse" role="button" aria-expanded="false" data-controls="collapseForm">
                                        {{ __('dashboard.show') }} <i class="fas fa-angle-down"></i>
                                   </button>
                                </div>
                                <div class="collapse" data-parent="#accordion" id="collapseForm" aria-labelledby="headingForm">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="form-group col-lg-3">
                                                <label for="">Attn</label>
                                                <input type="text" class="form-control" {{$isFormDisabled}} id="inputAttn" name="attn" value="{{ $data->attn }}">
                                            </div>

                                            <div class="form-group col-lg-3">
                                                <label for="">@lang('dashboard.vessel')</label>
                                                <input type="text" class="form-control" id="inputVessel" {{$isFormDisabled}} name="vessel" value="{{ $data->vessel }}">
                                            </div>

                                            <div class="form-group col-lg-3">
                                                <label for="">@lang('dashboard.voyage')</label>
                                                <input type="text" class="form-control" id="inputVoyage" {{$isFormDisabled}} name="voyage" value="{{ $data->voyage }}">
                                            </div>

                                            <div class="form-group col-lg-3">
                                                <label for="">ETA</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control datepicker" onkeydown="return false;" {{$isFormDisabled}} name="eta" value="{{date_format(date_create($data->eta), 'd-m-Y') }}">
                                                    <div class="input-group-append">
                                                        <div class="input-group-text"><i class="fas fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-4">
                                                <label for="">B/L No</label>
                                                <input type="text" class="form-control" {{$isFormDisabled}} id="inputBl_no" name="bl_no" value="{{ $data->bl_no }}">
                                            </div>

                                            <div class="form-group col-lg-4">
                                                <label for="">@lang('dashboard.from')</label>
                                                <input type="text" class="form-control" {{$isFormDisabled}} id="inputFrom" name="from" value="{{ $data->from }}">
                                            </div>

                                            <div class="form-group col-lg-4">
                                                <label for="">@lang('dashboard.to')</label>
                                                <input type="text" class="form-control" {{$isFormDisabled}} id="inputTo" name="to" value="{{ $data->to }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                <item-component
                                    :data="{{$data}}"
                                    url="{{ url('/select/item') }}"
                                    lang="{{ Auth::user()->language }}">
                                </item-component>
                            </div>
                        </div>

                    </div>
                    {{-- for paid action --}}
                    <input type="hidden" name="payment_date" id="paymentdate" value="">
                    <input type="hidden" name="payment_ref" id="paymentref" value="">
                    <input type="hidden" name="account_id" id="accountid" value="">
                    <input type="hidden" name="status" id="statusInv" value="">

                    <div class="col-lg-10 col-xs-12">
                        {{-- <button class="btn btn-primary" type="submit">{{ __('dashboard.save') }}</button> --}}
                        @if ($data->status == 1)
                            <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                <div class="btn-group" role="group">
                                    <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{ __('dashboard.save') }}
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                        {{-- <button type="button" name="status" value="1" onclick="handleSubmit(this)" class="dropdown-item">Draft</button>
                                        <button type="button" name="status" value="2" onclick="handleSubmit(this)" class="dropdown-item">Publish</button>                                    </div> --}}
                                        <button type="submit" name="status" value="1" class="dropdown-item">Draft</button>
                                        <button type="submit" name="status" value="2" class="dropdown-item">Publish</button>                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-success btn-print" data-uuid="{{$data->uuid}}" type="button">{{ __('dashboard.print') }}</button>

                        @elseif($data->status == 2)
                            @if ($data->payment == 1 && $paymentAmount < 1)
                                <button type="submit" name="status" value="2" class="btn btn-primary">{{ __('dashboard.save') }}</button>
                            @endif
                            @if ($data->payment == 1)
                                <a href="/accounting/purchase-create-payment/{{ $data->uuid }}" class="btn btn-warning text-white">@lang('dashboard.createPayment')</a>
                            @endif
                            <button type="button" class="btn btn-dark btn-create-qtt" data-invoice="{{$data->invoice_id}}">@lang('dashboard.create_quotation')</button>
                            <button type="button" class="btn btn-info btn-create-po" data-invoice="{{$data->invoice}}">@lang('dashboard.create_po')</button>
                            <button type="button" class="btn btn-warning btn-create-inv text-white" data-invoice="{{$data->invoice_id}}">@lang('dashboard.create_invoice')</button>
                            <button class="btn btn-primary btn-create-dn" data-id="{{$data->invoice_id}}" type="button">{{ __('dashboard.create_debit_note') }}</button>
                            <button class="btn btn-success btn-print" data-uuid="{{$data->uuid}}" type="button">{{ __('dashboard.print') }}</button>

                        @else
                            @if ($data->void)
                                <div class="card">
                                    <div class="card-header">
                                        @lang('dashboard.void_description')
                                    </div>
                                    <div class="card-body">
                                        <div class="col-lg-7">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    @lang('dashboard.void_by') :
                                                </div>
                                                <div class="col-lg-6">
                                                    {{ $data->void->name }}
                                                </div>
                                                <div class="col-lg-6">
                                                    @lang('dashboard.void_at') :
                                                </div>
                                                <div class="col-lg-6">
                                                    {{ date_format(date_create($data->void_at), 'd-m-Y H:i:s') }}
                                                </div>
                                                <div class="col-lg-6">
                                                    @lang('dashboard.reason') :
                                                </div>
                                                <div class="col-lg-6">
                                                    {{ $data->void_note }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endif

                        @if ($data->status == 1)
                            <button class="btn btn-danger btn-void" data-type="delete" data-id="{{$data->invoice_id}}" type="button">@lang('dashboard.cancel')</button>
                        @elseif($data->status ==2)
                            <button class="btn btn-danger btn-void" data-type="void" data-id="{{$data->invoice_id}}" type="button">Void</button>
                        @endif

                        <a href="{{url('/supplier/invoice') }}" class="btn btn-secondary">{{ __('dashboard.return') }}</a>
                    </div>
                </div>
            </form>
            <form action="{{ route('supplier.invoice.delete') }}" method="post" id="voidInvoice" class="hidden">
                <input type="hidden" name="invoice_id" id="invoiceId" value="{{ $data->invoice_id}}">
                <input type="hidden" name="type" id="invoiceType" value="">
                <input type="hidden" name="reason" id="reasonInput" value="">
                @csrf
                @method('delete')
            </form>
        </div>
    </div>

@endsection


@section('javascript')
<script src="{{ asset('js/app.js') }}" defer></script>
<script>
var isModalOpen = false;

function check(input, word){
    if (input.value == "") {
     input.setCustomValidity(word);
   } else  {
     input.setCustomValidity('');
   }
}
function changeHiddenValue(el, e){
    document.getElementById(el).value = e.value
}
function handleSubmit(el){
    changeHiddenValue('statusInv', el)
    $("#formInvoice").trigger('submit');
}
$(document).ready(function () {
    $(".selectAccount").select2({
        width: "100%",
        placeholder: "Choose Account",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/account') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        }
    });
    $("#formJournal").submit(function(e){
        e.preventDefault();
        $("#formInvoice").trigger('submit');
    })

    $("#modalPaid").on('shown.bs.modal', function(){
        $("#formJournal")[0].reset();
    });
    $("#modalPaid").on('hidden.bs.modal', function(){
        isModalOpen = false;
        $("#paymentdate, #paymentref, #accountid").val('');
    });
    /*window.onbeforeunload = function() {
        return 'Are you sure that you want to leave this page?';
    };*/
    $(".btn-void").click(function(){
        $("#invoiceId").val($(this).data('id'));
        $("#invoiceType").val($(this).data('type'));

        Swal.fire({
            title: 'Are you sure ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            input: 'textarea',
            inputPlaceholder: '{{__("dashboard.reason")}}',
            inputAttributes: {
                'aria-label': '{{__("dashboard.reason")}}...'
            },
            cancelButtonColor: '#d33',
        }).then((result) => {
            if (result.value) {
                $("#reasonInput").val(result.value);
                $("#voidInvoice").trigger('submit');
            }
        });
    });
    $('.btn-create-po').click(function(e){
        e.preventDefault();
        window.onbeforeunload = null;
        let supplier  = {
            supplier_id: $("#selectSupplier").val(),
            supplier_name: $("#input_supplier_name").val()
        }
        supplier = $.param(supplier)
        let item = $.param(items);
        let form = $.param({
            quotation_id: $('#selectQuotation').val(),
            quotation_name:  $('#input_quotation_name').val(),
            copy_form: 'true'
        });
        location = `/supplier/create-purchase-order?${supplier}&${item}&${form}`;
    });
    $('.btn-create-inv').click(function(e){
        e.preventDefault();
        window.onbeforeunload = null;
        let supplier  = {
            supplier_id: $("#selectSupplier").val(),
            supplier_name: $("#input_supplier_name").val()
        }
        supplier = $.param(supplier)
        let item = $.param(items);
        let form = $.param({
            quotation_name: 'QTT'+$("#inputSerial").val(),
            attn          : $("#inputAttn").val(),
            vessel        : $("#inputVessel").val(),
            voyage        : $("#inputVoyage").val(),
            bl_no         : $("#inputBl_no").val(),
            from          : $("#inputFrom").val(),
            to            : $("#inputTo").val(),
            copy_form: 'true'
        });
        location = `/supplier/create-invoice?${supplier}&${item}&${form}`;
    });
    $('.btn-create-qtt').click(function(e){
        e.preventDefault();
        window.onbeforeunload = null;
        let supplier  = {
            supplier_id: $("#selectSupplier").val(),
            supplier_name: $("#input_supplier_name").val()
        }
        supplier = $.param(supplier)
        let item = $.param(items);
        let form = $.param({
            quotation_name: 'QTT'+$("#inputSerial").val(),
            attn          : $("#inputAttn").val(),
            vessel        : $("#inputVessel").val(),
            voyage        : $("#inputVoyage").val(),
            bl_no         : $("#inputBl_no").val(),
            from          : $("#inputFrom").val(),
            to            : $("#inputTo").val(),
            copy_form: 'true'
        });
        location = `/supplier/create-quotation?${supplier}&${item}&${form}`;
    });
    $(".btn-create-dn").click(function(){
        let supplier  = {
            supplier_id: $("#selectSupplier").val(),
            supplier_name: $("#input_supplier_name").val()
        }
        supplier = $.param(supplier)
        let stuff = $.param(items);
        let invoice = $.param({
            invoice_id: $("#invoiceId").val(),
            serial    : $("#serialInput").val(),
            currency  : window.items.currency,
            payment   : $("#paymentInfo").val(),
            copy_form: 'true',
            rate      : items.rate
        });
        location = `/supplier/create-debit-note?items=${stuff}&${supplier}&${invoice}`;

    })

    $("#formInvoice").submit(function(e){
        window.onbeforeunload = null;
        // check if status is paid
        if($("#paymentInvoice").val() == "2" && !isModalOpen && $("#statusInv").val() == 2){
            e.preventDefault();
            $("#modalPaid").modal('show');
            isModalOpen = true;
        }else{
            return;
        }
    });

    $("#selectAccount").select2({
        width: "100%",
        placeholder: "Account",
        allowClear: true,
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/account') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10,
                    lock: ['1.1.01', '1.1.02']
				};
            },
            cache: true,
        },
    });
    $(".btn-print").click(function(){
        location = '/supplier/print-invoice/'+$(this).data('uuid');
    });
    $("#selectQuotation").select2({
        width: "100%",
        placeholder: "{{ __('dashboard.quotation') }}",
        allowClear: true,
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/qtt-supplier') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
                    page_limit : 10,
                    supplier: $("#selectSupplier").val()
				};
            },

            cache: true,
        },
    });

    $("#selectPo").select2({
        width: "100%",
        placeholder: "Po Supplier",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/po-supplier') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10,
                    quotation: $("#selectQuotation").val()
				};
            },

            cache: true,
        },
    });

    const date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $('.datepicker').datepicker({
        autoclose: true,
        format: {
            toDisplay(date, format, language){
                var tgl = moment(date);
                return tgl.format('DD-MM-Y')
            },
            toValue(date, format, language){
                var tgl = moment(date);
                return tgl.format('Y-MM-DD')
            }
        },
        language: 'id',
        weekStart: 1,
        // startDate: today,
        orientation: 'bottom'

    });
    $('.datepicker-invoice').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        language: 'id',
        weekStart: 1,
        // startDate: today,
        orientation: 'bottom'
    });
    $("#selectSupplier").select2({
        width: "100%",
        placeholder: "{{__('dashboard.supplier')}}",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/supplier') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        },
    });

    $("#selectSupplier").on('change', function(){
        $("#input_supplier_name").val($(this).select2('data')[0].text);
    })





});
</script>
@endsection
