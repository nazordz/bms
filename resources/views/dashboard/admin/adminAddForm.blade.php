@extends('dashboard.base')

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
            @if (session('status-success'))
                <div class="alert alert-success">
                    {{ session('status-success') }}
                </div>
            @endif
            @if (session('status-fail'))
                <div class="alert alert-danger">
                    {{ session('status-fail') }}
                </div>
            @endif

            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i> {{ __('dashboard.addAdmin') }}</div>
                        <div class="card-body">
                            <br>
                            <form method="POST" class="" id="adminForm" action="/admin/create">
                                @csrf
                                <div class="col-lg-8">
                                    <div class="form-group row">
                                        <label for="" class="col-sm-2 col-form-label">{{ __('dashboard.name') }}</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="name" value="{{ old('name') }}" required class="form-control" placeholder="">
                                            @if ($errors->first('name'))
                                                <small id="helpId" class="text-danger">{{ $errors->first('name') }}</small>
                                            @endif

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-sm-2 col-form-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="email" name="email" value="{{ old('email') }}" required class="form-control" placeholder="">
                                            @if ($errors->first('email'))
                                                <small id="" class="text-danger">{{ $errors->first('email') }}</small>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-sm-2 col-form-label">@lang('dashboard.phone')</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="phone" id="phoneNumber" value="{{ old('phone') }}" required class="form-control" placeholder="">
                                            @if ($errors->first('phone'))
                                                <small class="text-danger">{{ $errors->first('phone') }}</small>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-sm-2 col-form-label">@lang('dashboard.password')</label>
                                        <div class="col-sm-10">
                                            <input type="password" name="password" required min="5" class="form-control" placeholder="">
                                            @if ($errors->first('password'))
                                                <small id="" class="text-danger">{{ $errors->first('password') }}</small>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-sm-2 col-form-label">@lang('dashboard.password_confirmation')</label>
                                        <div class="col-sm-10">
                                            <input type="password" name="password_confirmation" required min="5" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                    <div class="col-sm-2 mb-4">
                                        @lang('dashboard.privilege')
                                    </div>
                                    <div class="col-lg-8">
                                        <table id="myTable" class="table table-responsive-m table-striped table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td width="5%">
                                                        <input type="checkbox" name="role[]" value="admin_customer" class="form-control check-admin">
                                                    </td>
                                                    <td>
                                                        @lang('dashboard.admin_customer')
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" name="role[]" value="admin_supplier" class="form-control check-admin">
                                                    </td>
                                                    <td>
                                                        @lang('dashboard.admin_supplier')
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" name="role[]" value="admin_accounting" class="form-control check-admin">
                                                    </td>
                                                    <td>
                                                        @lang('dashboard.admin_accounting')
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" name="role[]" value="admin_item_list" class="form-control check-admin">
                                                    </td>
                                                    <td>
                                                        @lang('dashboard.admin_item_list')
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" name="role[]" value="admin_laporan" class="form-control check-admin">
                                                    </td>
                                                    <td>
                                                        @lang('dashboard.admin_laporan')
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" name="role[]" value="admin_diskon" class="form-control check-admin">
                                                    </td>
                                                    <td>
                                                        @lang('dashboard.admin_diskon')
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" name="role[]" value="admin_pajak" class="form-control check-admin">
                                                    </td>
                                                    <td>
                                                        @lang('dashboard.admin_pajak')
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" name="role[]" value="admin_setting" class="form-control check-admin">
                                                    </td>
                                                    <td>
                                                        @lang('dashboard.admin_setting')
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                <div class="col-lg-8 col-xs-12">
                                    <button class="btn btn-success" type="submit">{{ __('dashboard.save') }}</button>
                                    <a href="{{url('/admin') }}" class="btn btn-secondary">{{ __('dashboard.return') }}</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')
<script src="/js/app.js" defer></script>
<script>
    $(document).ready(function () {
        window.onbeforeunload = function() {
            return 'Are you sure that you want to leave this page?';
        };
        setInputFilter(document.getElementById("phoneNumber"), function(value) {
            return /^-?\d*$/.test(value);
        });
        $("#adminForm").submit(function(e){
            window.onbeforeunload = null;
            var finded = false;
            $(this).serializeArray().forEach(function(val, index){
                finded = (val.name == 'role[]') && true;
            }, finded)

            if(finded){
                return;
            }else{
                Swal.fire( '{{__("dashboard.choice_role")}}', '', 'warning' );
            }
            e.preventDefault();
        })
    });
</script>
@endsection
