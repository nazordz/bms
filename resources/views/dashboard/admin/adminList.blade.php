@extends('dashboard.base')

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
            @if (session('status-success'))
                <div class="alert alert-success">
                    {{ session('status-success') }}
                </div>
            @endif
            @if (session('status-fail'))
                <div class="alert alert-danger">
                    {{ session('status-fail') }}
                </div>
            @endif
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-users"></i> Admin
                        </div>
                        <div class="card-body">
                            <div class="col-lg-12 mb-5">
                                <a href="{{ url('/admin/add-form') }}" class="btn btn-primary pull-right" > {{ __('dashboard.addAdmin') }}</a>
                            </div>
                            <table id="tbAdmin" class="table table-responsive-lg table-responsive-sm table-striped">
                                <thead>
                                    <tr>
                                    <th>No</th>
                                    <th data-priority="1">{{__('dashboard.name')}}</th>
                                    <th class="">Email</th>
                                    <th class="">{{__('dashboard.phone')}}</th>
                                    <th class="">{{__('dashboard.role')}}</th>
                                    <th class="" width="10%">{{ __('dashboard.suspend') }}</th>
                                    <th class="">{{ __('dashboard.created_at') }}</th>
                                    <th>{{ __('dashboard.action') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('javascript')
<script>
$(document).ready(function () {
    var myTable = $("#tbAdmin").DataTable({
        processing : true,
        serverSide : true,
        responsive : true,
        order      : [[6,"DESC"]],
        autoWidth  : false,
        searchDelay: 500,
        ajax : {
            url : "{{ url('/admin/table') }}"
        },
        columns:[
            {
                data: null,
                orderable  : false,
				searchable : false
            },
            { data : 'name', name : 'users.name' },
            { data : 'email', name : 'users.email' },
            { data : 'phone', name : 'users.phone' },
            {
                data : 'menuroles',
                name : 'users.menuroles',
                render(data){
                    var text = data.replace('user,', '');
                    text = text.replace(/_/gi, ' ');
                    text = text.split(',');
                    res = '';
                    text.forEach(role => {
                        res += `<li>${role}</li>`;
                    })
                    return `<ul>${res}</ul>`;

                }
            },
            {
                data: 'active',
                render(data, type, row){
                    var active = data == 1 ? 'checked': ''

                    return `<input type="checkbox" class="admin-active" data-toggle="toggle" data-size="xs" data-uuid="${row.uuid}" data-on="{{ __('dashboard.active') }}" data-off="{{ __('dashboard.suspend') }}" class="admin-active" ${active} />`;
                }
            },
            { data : 'created_at', name : 'users.created_at' },
            {
                data: null,
                searchable:false,
                orderable : false,
                render(data, type, row){
                    return `<a href="/admin/show/${row.uuid}" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> {{__('dashboard.edit')}}</a>
                            <button class="btn btn-danger btn-delete btn-sm" data-uuid="${row.uuid}"><i class="fa fa-trash"></i> {{__('dashboard.delete')}}</button> `
                }
            }
        ],
        fnCreatedRow(row, data, index) {
			if (myTable.page.info().start >= 10) {
				var panjang = myTable.page.info().length;
				var halaman = myTable.page.info().page;
				var i = 1;
				i = (halaman + 1) * panjang;
				i -= panjang - 1;
	        }else{
	        	var i = 1;
	        }
	        $('td', row).eq(0).html(index + i);
		},
        drawCallback(){
            $(".dataTables_length select").removeClass("form-control-sm");
            $(".btn-delete").click(function(){
                var uuid = $(this).data('uuid');
                Swal.fire({
                    title: '{{__("dashboard.are_you_sure")}}',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: '{{ __("dashboard.cancel") }}',
                    confirmButtonText: '{{__("dashboard.confirm_delete")}}'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: "post",
                            url: "/admin/delete",
                            data: { _token: "{{csrf_token()}}", _method:'delete',  uuid},
                            dataType: "json"
                        }).done(function(data){
                            myTable.draw();
                            Swal.fire( '{{__("dashboard.deleted")}}', '', 'success' );
                        }).catch(function(err){
                            console.log(err);
                        });
                    }
                });
            });
			$(".admin-active").bootstrapToggle();
            $(".admin-active").change(function(){
                var status = $(this).prop('checked') ? 1 : 0;
                var uuid     = $(this).data('uuid')

                $.ajax({
                    type: "patch",
                    url: "{{url('/admin/status')}}",
                    data: { status, uuid, _token: '{{ csrf_token() }}' },
                    dataType: "json"
                })
                .done(function(data){
                    Swal.fire(
                        '{{ __("dashboard.success") }}',
                        '{{ __("dashboard.success_change") }}',
                        'success'
                    );
                    myTable.draw();
                })
                .catch(function(err){
                    console.log(err);

                });
            })
		}
    })
    .on('responsive-display', function(e, datatable, row, showHide, update){
        $(".admin-active").bootstrapToggle();
        $(".admin-active").change(function(){
            var status = $(this).prop('checked') ? 1 : 0;
            var uuid     = $(this).data('uuid')

            $.ajax({
                type: "patch",
                url: "{{url('/admin/status')}}",
                data: { status, uuid, _token: '{{ csrf_token() }}' },
                dataType: "json"
            })
            .done(function(data){
                Swal.fire(
                    '{{ __("dashboard.success") }}',
                    '{{ __("dashboard.success_change") }}',
                    'success'
                );
                myTable.draw();
            })
            .catch(function(err){
                console.log(err);

            });
        })
    })
});
</script>
@endsection

