@extends('dashboard.base')
@php
    $role ='';
    function roles($roles, $find){
        $roles = explode(',', $roles);
        foreach ($roles as $key => $value) {
            if($find == $value)  return true;
        }
        return false;
    }
@endphp
@section('content')

        <div class="container-fluid">
          <div class="animated fadeIn">
            @if (session('status-success'))
                <div class="alert alert-success">
                    {{ session('status-success') }}
                </div>
            @endif
            @if (session('status-fail'))
                <div class="alert alert-danger">
                    {{ session('status-fail') }}
                </div>
            @endif
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i> {{ __('dashboard.editAdmin') }}</div>
                        <div class="card-body">
                            <br>
                            <form method="POST" id="adminForm" class="" action="/admin/update/{{ $user->uuid }}">
                                @method('put')
                                @csrf
                                <div class="col-lg-8">
                                    <div class="form-group row">
                                        <label for="" class="col-sm-2 col-form-label">{{ __('dashboard.name') }}</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="name" value="{{ $user->name}}" required id="" class="form-control" placeholder="">
                                            @if ($errors->first('name'))
                                                <small id="helpId" class="text-danger">{{ $errors->first('name') }}</small>
                                            @endif

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-sm-2 col-form-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="email" name="email" value="{{ $user->email }}" required id="" class="form-control" placeholder="">
                                            @if ($errors->first('email'))
                                                <small id="" class="text-danger">{{ $errors->first('email') }}</small>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-sm-2 col-form-label">@lang('phone')</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="phone" value="{{ $user->phone }}" required id="phoneNumber" class="form-control" placeholder="">
                                            @if ($errors->first('phone'))
                                                <small class="text-danger">{{ $errors->first('phone') }}</small>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label active">
                                            {{ __('dashboard.password') }} ?
                                            <input type="checkbox" name="" id="isPassword" autocomplete="off">
                                        </label>
                                    </div>
                                    <input type="hidden" name="insert_password" id="insertPassword" value="0">
                                    <div class="form-group row form-password" style="display:none;">
                                        <label for="" class="col-sm-2 col-form-label">@lang('dashboard.password')</label>
                                        <div class="col-sm-10">
                                            <input type="password" name="password" id="" min="5" class="form-control password-input" placeholder="">
                                            @if ($errors->first('password'))
                                                <small id="" class="text-danger">{{ $errors->first('password') }}</small>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row form-password" style="display:none;">
                                        <label for="" class="col-sm-2 col-form-label">@lang('dashboard.password_confirmation')</label>
                                        <div class="col-sm-10">
                                            <input type="password" name="password_confirmation" id="" min="5" class="form-control password-input" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                    <div class="col-sm-2 mb-4">
                                        @lang('dashboard.privilege')
                                    </div>
                                    <div class="col-lg-8">
                                        <table id="myTable" class="table table-responsive-m table-striped table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td width="6%">
                                                        <input type="checkbox" name="role[]" {{roles($user->menuroles, 'admin_customer')?'checked':''}} value="admin_customer" class="form-control">
                                                    </td>
                                                    <td>
                                                        @lang('dashboard.admin_customer')
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" name="role[]" {{roles($user->menuroles, 'admin_supplier')?'checked':''}} value="admin_supplier" class="form-control">
                                                    </td>
                                                    <td>
                                                        @lang('dashboard.admin_supplier')
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" name="role[]" {{roles($user->menuroles, 'admin_accounting')?'checked':''}} value="admin_accounting" class="form-control">
                                                    </td>
                                                    <td>
                                                        @lang('dashboard.admin_accounting')
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" name="role[]" {{roles($user->menuroles, 'admin_item_list')?'checked':''}} value="admin_item_list" class="form-control">
                                                    </td>
                                                    <td>
                                                        @lang('dashboard.admin_item_list')
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" name="role[]" {{roles($user->menuroles, 'admin_laporan')?'checked':''}} value="admin_laporan" class="form-control">
                                                    </td>
                                                    <td>
                                                        @lang('dashboard.admin_laporan')
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" name="role[]" {{roles($user->menuroles, 'admin_diskon')?'checked':''}} value="admin_diskon" class="form-control">
                                                    </td>
                                                    <td>
                                                        @lang('dashboard.admin_diskon')
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" name="role[]" {{roles($user->menuroles, 'admin_pajak')?'checked':''}} value="admin_pajak" class="form-control">
                                                    </td>
                                                    <td>
                                                        @lang('dashboard.admin_pajak')
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" name="role[]" {{roles($user->menuroles, 'admin_setting')?'checked':''}} value="admin_setting" class="form-control">
                                                    </td>
                                                    <td>
                                                        @lang('dashboard.admin_setting')
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                <div class="col-lg-8 col-xs-12">
                                    <button class="btn btn-success" type="submit">{{ __('dashboard.save') }}</button>
                                    <a href="{{url('/admin') }}" class="btn btn-secondary">{{ __('dashboard.return') }}</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>

@endsection


@section('javascript')
<script>
$(document).ready(function () {
    /*window.onbeforeunload = function() {
        return 'Are you sure that you want to leave this page?';
    };*/
    setInputFilter(document.getElementById("phoneNumber"), function(value) {
        return /^-?\d*$/.test(value);
    });
    $("#isPassword").change(function(){
        $(".password-input").val('')

        if($(this).prop('checked')){
            $(".form-password").show()
            $("#insertPassword").val(1)
        }else{
            $(".form-password").hide()
            $("#insertPassword").val(0)
        }
    })

    $("#adminForm").submit(function(e){
        window.onbeforeunload = null;
        var finded = false;
        $(this).serializeArray().forEach(function(val, index){
            finded = (val.name == 'role[]') && true;
        }, finded)

        if(finded){
            return;
        }else{
            Swal.fire( '{{__("dashboard.choice_role")}}', '', 'warning' );
        }
        e.preventDefault();
    })
});
</script>
@endsection
