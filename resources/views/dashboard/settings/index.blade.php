@extends('dashboard.base')
@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
        @if (session('status-success'))
            <div class="alert alert-success">
                {{ session('status-success') }}
            </div>
        @endif
        @if (session('status-fail'))
            <div class="alert alert-danger">
                {{ session('status-fail') }}
            </div>
        @endif
            <form action="{{ route('setting.config') }}" enctype="multipart/form-data" method="post">
                <div class="row">
                    <div class="col-lg-7 col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                {{__('dashboard.company')}}
                            </div>
                            <div class="card-body">
                                @csrf
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label for="" class="">{{ __('dashboard.address') }}</label>
                                        <textarea class="form-control" name="address" rows="4">{{ $setting->address }}</textarea>
                                        @if ($errors->first('address'))
                                            <small class="text-danger">{{ $errors->first('address') }}</small>
                                        @endif
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-lg-6 mr-1 row">
                                            <label for="" class="">{{ __('dashboard.phone') }}</label>
                                            <input type="text" name="phone" id="phoneNumber" value="{{ $setting->phone }}" class="form-control numericInput" placeholder="">
                                            @if ($errors->first('phone'))
                                                <small class="text-danger">{{ $errors->first('phone') }}</small>
                                            @endif
                                        </div>
                                        <div class="form-group col-lg-6 row">
                                            <label for="" class="">Email</label>
                                            <input type="email" name="email" value="{{ $setting->email }}" class="form-control" placeholder="">
                                            @if ($errors->first('email'))
                                                <small class="text-danger">{{ $errors->first('email') }}</small>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="form-group col-lg-6 row">
                                            <label for="" class="">{{ __('dashboard.behalf_of') }}</label>
                                            <input type="text" name="behalf_of" value="{{ $setting->behalf_of }}" class="form-control" placeholder="">
                                            @if ($errors->first('behalf_of'))
                                                <small class="text-danger">{{ $errors->first('behalf_of') }}</small>
                                            @endif
                                        </div>
                                        <div class="form-group col-lg-6 mx-1 row">
                                            <label for="" class="">{{ __('dashboard.behalf_of_position') }}</label>
                                            <input type="text" name="behalf_of_position" value="{{ $setting->behalf_of_position }}" class="form-control" placeholder="">
                                            @if ($errors->first('behalf_of_position'))
                                                <small class="text-danger">{{ $errors->first('behalf_of_position') }}</small>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-lg-8 row mr-1">
                                            <label for="" class="">{{ __('dashboard.account_owner') }}</label>
                                            <input type="text" class="form-control"  name="account_owner" value="{{ $setting->account_owner }}">
                                            @if ($errors->first('account_owner'))
                                                <small class="text-danger">{{ $errors->first('account_owner') }}</small>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">
                                {{-- @lang('dashboard.primary_account') --}}
                                Bank Indonesia
                            </div>
                            <div class="card-body">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="form-group col-lg-6 row mr-1">
                                            <label for="" class="">{{ __('dashboard.bank_account') }} </label>
                                            <input type="text" class="form-control" name="bank_account" value="{{ $setting->bank_account }}">
                                            @if ($errors->first('bank_account'))
                                                <small class="text-danger">{{ $errors->first('bank_account') }}</small>
                                            @endif
                                        </div>
                                        <div class="form-group col-lg-6 row">
                                            <label for="" class="">{{ __('dashboard.company_account') }} </label>
                                            <input type="text" class="form-control numericInput" placeholder="Ex: BCA" id="accountNumber" name="account" value="{{ $setting->account }}">
                                            @if ($errors->first('account'))
                                                <small class="text-danger">{{ $errors->first('account') }}</small>
                                            @endif
                                        </div>
                                        <div class="form-group col-lg-6 row mr-1">
                                            <label for="" class="">{{ __('dashboard.bank_code') }} </label>
                                            <input type="text" class="form-control" placeholder="" name="bank_code" value="{{ $setting->bank_code }}">
                                            @if ($errors->first('bank_code'))
                                                <small class="text-danger">{{ $errors->first('bank_code') }}</small>
                                            @endif
                                        </div>
                                        <div class="form-group col-lg-6 row">
                                            <label for="" class="">{{ __('dashboard.swift_code') }} </label>
                                            <input type="text" class="form-control" placeholder="" name="swift_code" value="{{ $setting->swift_code }}">
                                            @if ($errors->first('swift_code'))
                                                <small class="text-danger">{{ $errors->first('swift_code') }}</small>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                {{-- @lang('dashboard.secondary_account') --}}
                                Bank Singapore
                            </div>
                            <div class="card-body">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="form-group col-lg-6 row mr-1">
                                            <label for="" class="">{{ __('dashboard.bank_account') }} </label>
                                            <input type="text" class="form-control" name="bank_account_secondary" value="{{ $setting->bank_account_secondary }}">
                                            @if ($errors->first('bank_account_secondary'))
                                                <small class="text-danger">{{ $errors->first('bank_account_secondary') }}</small>
                                            @endif
                                        </div>
                                        <div class="form-group col-lg-6 row">
                                            <label for="" class="">{{ __('dashboard.company_account') }} </label>
                                            <input type="text" class="form-control numericInput" placeholder="Ex: BCA" name="account_secondary" value="{{ $setting->account_secondary }}">
                                            @if ($errors->first('account_secondary'))
                                                <small class="text-danger">{{ $errors->first('account_secondary') }}</small>
                                            @endif
                                        </div>
                                        <div class="form-group col-lg-6 row mr-1">
                                            <label for="" class="">{{ __('dashboard.bank_code') }} </label>
                                            <input type="text" class="form-control" placeholder="" name="bank_code_secondary" value="{{ $setting->bank_code_secondary }}">
                                            @if ($errors->first('bank_code_secondary'))
                                                <small class="text-danger">{{ $errors->first('bank_code_secondary') }}</small>
                                            @endif
                                        </div>
                                        <div class="form-group col-lg-6 row">
                                            <label for="" class="">{{ __('dashboard.swift_code') }} </label>
                                            <input type="text" class="form-control" placeholder="" name="swift_code_secondary" value="{{ $setting->swift_code_secondary }}">
                                            @if ($errors->first('swift_code_secondary'))
                                                <small class="text-danger">{{ $errors->first('swift_code_secondary') }}</small>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">
                                @lang('dashboard.format_number')
                            </div>
                            <div class="card-body">
                                @csrf
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="form-group col-lg-3">
                                            <label for="" class="">{{ __('dashboard.currency') }}</label>
                                            <select class="form-control" name="currency">
                                                <option value="IDR" {{ $setting->currency == 'IDR' ? 'selected': '' }}>IDR</option>
                                                <option value="USD" {{ $setting->currency == 'USD' ? 'selected': '' }}>USD</option>
                                            </select>
                                            @if ($errors->first('currency'))
                                                <small class="text-danger">{{ $errors->first('currency') }}</small>
                                            @endif
                                        </div>
                                        <div class="form-group col-lg-3">
                                            <label for="">{{__('dashboard.thousand_separator')}}</label>
                                            <select class="form-control" name="thousand_separator">
                                                <option value="." {{ $setting->thousand_separator == '.' ? 'selected' : '' }}>Dot (.)</option>
                                                <option value="," {{ $setting->thousand_separator == ',' ? 'selected' : '' }}>Comma (,)</option>
                                            </select>
                                            @if ($errors->first('thousand_separator'))
                                                <small class="text-danger">{{ $errors->first('thousand_separator') }}</small>
                                            @endif
                                        </div>
                                        <div class="form-group col-lg-3">
                                            <label for="">{{__('dashboard.decimal_separator')}}</label>
                                            <select class="form-control" name="decimal_separator">
                                                <option value="," {{ $setting->decimal_separator == ',' ? 'selected' : '' }}>Comma (,)</option>
                                                <option value="." {{ $setting->decimal_separator == '.' ? 'selected' : '' }}>Dot (.)</option>
                                            </select>
                                            @if ($errors->first('decimal_separator'))
                                                <small class="text-danger">{{ $errors->first('decimal_separator') }}</small>
                                            @endif
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-5 col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                Logo
                            </div>
                            <div class="card-body row">
                                <div class="form-group col-lg-6">
                                    <label for="">Favicon</label>
                                    @if($setting->logo)
                                    <div>
                                        <img src="{{ $setting->logo }}" style="max-width:150px;" alt="img old">
                                    </div>
                                    @endif
                                    <small>*@lang('dashboard.resolution') 192x192 px</small>
                                    <input type="file" class="form-control-file" name="logo" accept="image/*" placeholder="" aria-describedby="fileHelpId">
                                    @if ($errors->first('logo'))
                                        <small class="text-danger">{{ $errors->first('logo') }}</small>
                                    @endif
                                </div>
                                <div class="form-group col-lg-6">
                                    <label for="">logo</label>
                                    @if($setting->nav_logo)
                                    <div>
                                        <img src="{{ $setting->nav_logo }}" style="max-width:150px;" alt="img old">
                                    </div>
                                    @endif
                                    <small>*@lang('dashboard.resolution') 120x50 px</small>
                                    <input type="file" class="form-control-file" name="nav_logo" accept="image/*" placeholder="" aria-describedby="fileHelpId">
                                    @if ($errors->first('nav_logo'))
                                        <small class="text-danger">{{ $errors->first('nav_logo') }}</small>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">
                                @lang('dashboard.letterhead')
                            </div>
                            <div class="card-body row">
                                <div class="form-group col-lg-6">
                                    <label for="">Header</label>
                                    @if ($setting->header)
                                        <div>
                                            <img src="{{ $setting->header }}" style="max-width:150px;" alt="img old">
                                        </div>
                                    @endif
                                    <small>*width 400px</small>
                                    <input type="file" class="form-control-file" name="header" accept="image/*" placeholder="" aria-describedby="fileHelpId">
                                    @if ($errors->first('header'))
                                        <small class="text-danger">{{ $errors->first('header') }}</small>
                                    @endif
                                </div>
                                <div class="form-group col-lg-6">
                                    <label for="">Footer</label>
                                    @if ($setting->footer)
                                        <div>
                                            <img src="{{ $setting->footer }}" style="max-width:150px;" alt="img old">
                                        </div>
                                    @endif
                                    <small>*width 400px</small>
                                    <input type="file" class="form-control-file" name="footer" accept="image/*" placeholder="" aria-describedby="fileHelpId">
                                    @if ($errors->first('footer'))
                                        <small class="text-danger">{{ $errors->first('footer') }}</small>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-xs-12 mb-5">
                        <button class="btn btn-primary" type="submit">{{ __('dashboard.save') }}</button>
                        {{-- <a href="{{url('/item-list') }}" class="btn btn-secondary">{{ __('dashboard.return') }}</a> --}}
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection


@section('javascript')
<script>
    function setInputFilter(textbox, inputFilter) {
        ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
            textbox.addEventListener(event, function() {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            } else {
                this.value = "";
            }
            });
        });
    }
    setInputFilter(document.getElementById("phoneNumber"), function(value) {
        return /^-?\d*$/.test(value);
    });
    setInputFilter(document.getElementById("accountNumber"), function(value) {
        return /^-?\d*$/.test(value);
    });
</script>
@endsection
