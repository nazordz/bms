@extends('dashboard.base')

@section('content')
    <profit-loss-list lang="{{ Auth::user()->language }}"></profit-loss-list>
@endsection

@section('javascript')
<script src="/js/app.js" defer></script>
@endsection
