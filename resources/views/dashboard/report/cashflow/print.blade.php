<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Print Cashflow</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    </head>
    <body>
        <div id="app">
            <v-print-cashflow lang="{{Auth::user()->language}}" :web="{{ $web }}" start_date="{{ $start_date }}" to_date="{{ $to_date }}"></v-print-cashflow>
        </div>
        <script src="{{ asset('js/app.js') }}" defer></script>
    </body>
</html>
