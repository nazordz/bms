@extends('dashboard.base')

@section('content')
    <cashflow-list lang="{{Auth::user()->language}}"></cashflow-list>
@endsection

@section('javascript')
<script src="/js/app.js" defer></script>
@endsection
