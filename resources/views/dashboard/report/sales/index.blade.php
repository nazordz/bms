@extends('dashboard.base')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        @lang('dashboard.Selling')
                    </div>
                    <div class="card-body">
                        <div class="list-group">
                            <a href="{{ url('report/selling/invoice') }}" class="list-group-item list-group-item-action">
                                <i class="fas fa-file-alt"></i> @lang('dashboard.based_invoice')
                            </a>
                            <a href="{{ url('report/selling/booking-confirmation') }}" class="list-group-item list-group-item-action">
                                <i class="fas fa-truck" aria-hidden="true"></i> @lang('dashboard.based_bc')
                            </a>
                            <a href="{{ url('report/selling/age-of-receivable') }}" class="list-group-item list-group-item-action">
                                <i class="fas fa-chart-line"></i> @lang('dashboard.based_age_of_receivable')
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('javascript')
<script src="{{ asset('js/app.js') }}" defer></script>
@endsection

