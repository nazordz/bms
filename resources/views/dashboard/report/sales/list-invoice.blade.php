@extends('dashboard.base')
@section('content')
    <sales-invoice lang="{{ Auth::user()->language }}"></sales-invoice>
@endsection
@section('javascript')
<script src="/js/app.js"></script>
@endsection
