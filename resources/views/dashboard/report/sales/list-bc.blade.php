@extends('dashboard.base')
@section('content')
    <sales-bc lang="{{ Auth::user()->language }}"></sales-bc>
@endsection
@section('javascript')
<script src="/js/app.js"></script>
@endsection
