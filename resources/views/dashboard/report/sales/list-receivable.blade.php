@extends('dashboard.base')
@section('content')
    <sales-receivable lang="{{ Auth::user()->language }}"></sales-receivable>
@endsection
@section('javascript')
<script src="/js/app.js"></script>
@endsection
