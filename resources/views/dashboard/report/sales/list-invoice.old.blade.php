@extends('dashboard.base')

@section('content')
<div class="container-fluid">
    <div class="animated fadeIn">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        @lang('dashboard.Selling')
                    </div>
                    <div class="card-body">
                        <div class="col-lg-12 mb-5">
                            <div class="row pull-right">
                                <div class="form-inline">
                                    <select name="start_date" id="currency_input" placeholder="@lang('dashboard.currency')" class="form-control mr-2">
                                        <option value="1" selected>IDR</option>
                                        <option value="2">SGD</option>
                                        <option value="3">USD</option>
                                        <option value="4">RM</option>
                                    </select>
                                    <input type="text" name="start_date" value="" id="start_date_input" placeholder="@lang('dashboard.start_date')" class="form-control datepicker mr-2" autocomplete="off">
                                    <input type="text" name="to_date" value="" id="to_date_input" placeholder="@lang('dashboard.to_date')" class="form-control datepicker" autocomplete="off">

                                </div>
                            </div>
                        </div>
                        <table id="myTable" class="table table-responsive-lg table-striped">
                            <thead>
                                <tr>
                                    <th>NO INV</th>
                                    <th>@lang('dashboard.invoice_date')</th>
                                    <th>Customer</th>
                                    <th>@lang('dashboard.due_date')</th>
                                    <th>subtotal</th>
                                    <th>@lang('dashboard.tax')</th>
                                    <th>Nominal @lang('dashboard.tax')</th>
                                    <th>Total</th>
                                    <th>@lang('dashboard.paid')</th>
                                    <th>@lang('dashboard.balance')</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script>
    $(document).ready(function () {
        var myTable = $("#myTable").DataTable({
            responsive: true,
            processing : true,
            serverSide : true,
            order      : [[0, "desc"]],
            autoWidth  : false,
            searchDelay: 500,
            ajax : {
                url : "{{ url('/report/selling/table-invoice') }}",
                data(data){
                    data.start_date = $("#start_date_input").val();
                    data.to_date = $("#to_date_input").val();
                    data.currency = $("#currency_input").val();
                }
            },
            columns: [
                {
                    data: 'serial', name: 'invoice_customers.serial',
                    render(data){ return 'INV'+data }
                },
                {
                    data: 'invoice_date', name: 'invoice_customers.invoice_date'
                },
                {
                    data: 'company',
                    name: 'customers.company',
                    render(data, type, row){
                        return data + ' - ' + row.first_name;
                    }
                },
                { data: 'due_date', name: 'invoice_customers.due_date' },
                {
                    data: 'subtotal',
                    searchable: false,
                    render(data, type, row){
                        // blum ditambah pajak
                        if(row.currency > 1){
                            data /= row.rate
                            return Number(data).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
                        }else{
                            return Number(data).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
                        }
                    }
                },
                {
                    data: 'tax_amount',
                    name: 'tax.tax_amount',
                    render(data, type, row){
                        return `${row.description}-${data}%`;
                    }
                },
                {
                    data: 'tax_amount',
                    name: 'tax.tax_amount',
                    render(data, type, row){
                        if(row.currency > 1){
                            var tax = Number(row.subtotal*(data/100));
                            tax /= row.rate
                        }else{
                            var tax = Number(row.subtotal*(data/100));
                        }
                        return tax.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
                    }
                },
                {
                    data: 'subtotal', searchable: false,
                    render(data, type, row){
                        data = Number(data);
                        let tax = Number(data*(row.tax_amount/100));
                        var result = (data + tax);
                        if(row.currency > 1){
                            result /= row.rate;
                        }
                        return result.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
                    }
                },
                {
                    data: 'paid',
                    searchable: false,
                    render(data){
                        return Number(data).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
                    }
                },
                {
                    data: 'subtotal',
                    orderable: false,
                    searchable: false,
                    render(data, type ,row){
                        data = Number(data);
                        let tax = Number(data*(row.tax_amount/100));
                        var result = data + tax;
                        if(row.currency > 1){
                            result /= row.rate;
                        }

                        let balance = result - Number(row.paid);
                        balance = balance < 0 ? 0 : balance;
                        return balance.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
                    }
                }

            ],
            fnCreatedRow(row, data, index) {
                if (myTable.page.info().start >= 10) {
                    var panjang = myTable.page.info().length;
                    var halaman = myTable.page.info().page;
                    var i = 1;
                    i = (halaman + 1) * panjang;
                    i -= panjang - 1;
                }else{
                    var i = 1;
                }
                $('td', row).eq(0).html(index + i);
            },
            drawCallback(){
                $(".dataTables_length select").removeClass("form-control-sm");

            }
        });
        $("#start_date_input,#to_date_input,#currency_input").change(function(){
            myTable.draw();
        });

        $('.datepicker').datepicker({
            autoclose: true,
            format: {
                toDisplay(date, format, language){
                    var tgl = moment(date);
                    return tgl.format('DD-MM-Y')
                },
                toValue(date, format, language){
                    var tgl = moment(date);
                    return tgl.format('Y-MM-DD')
                }
            },
            language: 'id',
            weekStart: 1,
            orientation: 'bottom'
        });
    });
</script>
@endsection

