<table>
    <thead>
        <tr>
            <th>NO Ref</th>
            <th>@lang('dashboard.date') BC</th>
            <th>Customer</th>
            <th>Total</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($items as $item)
            <tr>
                <td>{{ $item->booking_ref.'-'.$item->serial }}</td>
                <td>{{ $item->created_at }}</td>
                <td>{{ $item->company.' '.$item->first_name }}</td>
                <td>{{ $item->total ?? 0 }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
