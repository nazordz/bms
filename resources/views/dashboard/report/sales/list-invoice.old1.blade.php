@extends('dashboard.base')

@section('content')
<div class="container-fluid">
    <div class="animated fadeIn">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        @lang('dashboard.Selling')
                    </div>
                    <div class="card-body">
                        <div class="col-lg-12 mb-5">
                            <div class="row">
                                <div class="form-group col-lg-2">
                                    <label for="">@lang('dashboard.currency')</label>
                                    <select name="start_date" id="currency_input" placeholder="@lang('dashboard.currency')" class="form-control mr-2">
                                        <option value="1" selected>IDR</option>
                                        <option value="2">SGD</option>
                                        <option value="3">USD</option>
                                        <option value="4">RM</option>
                                    </select>
                                </div>
                                <div class="col-lg-2">
                                    <label for="">@lang('dashboard.start_date')</label>
                                    <input type="text" name="start_date" value="" id="start_date_input" placeholder="@lang('dashboard.start_date')" class="form-control datepicker mr-2" autocomplete="off">
                                </div>
                                <div class="col-lg-2">
                                    <label for="">@lang('dashboard.to_date')</label>
                                    <input type="text" name="to_date" value="" id="to_date_input" placeholder="@lang('dashboard.to_date')" class="form-control datepicker" autocomplete="off">
                                </div>
                                <div class="col-lg-3">
                                    <label for="">Customer</label>
                                    <input type="text" name="customer" value="" id="customer_input" placeholder="@lang('dashboard.customer_name')" class="form-control" autocomplete="off">
                                </div>
                                <div class="col-lg-2">
                                    <button class="btn btn-primary btn-sm">@lang('dashboard.show')</button>
                                </div>

                            </div>
                        </div>
                        <table id="myTable" class="table table-responsive-lg table-striped">
                            <thead>
                                <tr>
                                    <th>NO INV</th>
                                    <th>@lang('dashboard.invoice_date')</th>
                                    <th>Customer</th>
                                    <th>@lang('dashboard.due_date')</th>
                                    <th>subtotal</th>
                                    <th>@lang('dashboard.tax')</th>
                                    <th>Nominal @lang('dashboard.tax')</th>
                                    <th>Total</th>
                                    <th>@lang('dashboard.paid')</th>
                                    <th>@lang('dashboard.balance')</th>
                                </tr>
                            </thead>
                            <tbody id="myTableBody"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script>
function loadTable(){
    var data = '';
    $.ajax({
        type: "get",
        url: "/report/selling/table-invoice",
        data: {
            currency  : $("#currency_input").val(),
            start_date: $("#start_date_input").val(),
            to_date   : $("#to_date_input").val(),
            customer  : $("#customer_input").val()
        },
        dataType: "json"
    }).done(function(data){
        data = '<tr>'
        $.each(data, (key, value) => {

            if(value.currency > 1){
                value.subtotal /= value.rate
                value.subtotal = Number(value.subtotal).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
            }else{
                value.subtotal = Number(value.subtotal).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
            }

            if(value.currency > 1){
                var tax = Number(value.subtotal*(tax_amount/100));
                tax /= value.rate
            }else{
                var tax = Number(value.subtotal*(tax_amount/100));
            }
            tax = tax.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});

            value.subtotal = Number(value.subtotal);
            let tax = Number(value.subtotal*(value.tax_amount/100));
            var result = (value.subtotal + tax);
            if(value.currency > 1){
                result /= value.rate;
            }
            result = result.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});

            data = Number(data);
            let tax = Number(data*(row.tax_amount/100));
            var result = data + tax;
            if(row.currency > 1){
                result /= row.rate;
            }

            let balance = result - Number(row.paid);
            balance = balance < 0 ? 0 : balance;
            return balance.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});

            data += `
                <td>INV${value.serial}</td>
                <td>${value.invoice_date}</td>
                <td>${value.company+ ' - ' + row.first_name}</td>
                <td>${value.due_date}</td>
                <td>${value.subtotal}</td>
                <td>${value.description}-${value.tax_amount}%</td>
                <td>${tax}</td>
                <td>${result}</td>
                <td>${Number(value.paid).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2})}</td>
                <td>${value.}</td>
            `;
        });
        data += '</tr>';
        console.log(data);

    });
}
$(document).ready(function () {


    $('.datepicker').datepicker({
        autoclose: true,
        format: {
            toDisplay(date, format, language){
                var tgl = moment(date);
                return tgl.format('DD-MM-Y')
            },
            toValue(date, format, language){
                var tgl = moment(date);
                return tgl.format('Y-MM-DD')
            }
        },
        language: 'id',
        weekStart: 1,
        orientation: 'bottom'
    });
});
</script>
@endsection

