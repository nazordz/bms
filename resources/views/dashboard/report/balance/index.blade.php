@extends('dashboard.base')

@section('content')
    <balance-list lang="{{Auth::user()->language}}"></balance-list>
@endsection

@section('javascript')
<script src="/js/app.js" defer></script>
@endsection
