@extends('dashboard.base')

@section('content')
    <ledger-list lang="{{Auth::user()->language}}"></ledger-list>
@endsection

@section('javascript')
<script src="/js/app.js" defer></script>
@endsection
