<table>
    <thead>
        <tr>
            <th>{{__('dashboard.account')}}</th>
            <th>{{__('dashboard.beginning_balance')}}</th>
            <th>{{__('dashboard.debit')}}</th>
            <th>{{__('dashboard.credit')}}</th>
            <th>{{__('dashboard.ending_balance')}}</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $item)
            <tr>
                <td>{{ $item->id.' '.$item->name }}</td>
                <td>{{ $item->beginning }}</td>
                <td>{{ $item->debit }}</td>
                <td>{{ $item->credit }}</td>
                <td>{{ $item->ending }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
