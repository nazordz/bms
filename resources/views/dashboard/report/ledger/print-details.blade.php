<table>
    <thead>
        <tr>
            <th>{{__('dashboard.date')}}</th>
            <th>{{__('dashboard.description')}}</th>
            <th>{{__('dashboard.debet')}}</th>
            <th>{{__('dashboard.credit')}}</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $el)
            <tr>
                <td>{{ $el->id.' '.$el->name }}</td>
                <td>{{ __('dashboard.beginning_balance') }}</td>
                <td>{{ $el->beginning_balance_type == 'debit' ? $el->beginning:''}}</td>
                <td>{{ $el->beginning_balance_type == 'credit' ? '('.$el->beginning.')':''}}</td>
            </tr>
            @if (count($el->journal_details))
                @foreach ($el->journal_details as $item)
                    <tr>
                        <td>{{$el->transaction_at }}</td>
                        <td>{{$item->description }}</td>
                        <td>{{$item->debet }}</td>
                        <td>{{$item->credit }}</td>
                    </tr>
                @endforeach
            @endif
            <tr>
                <td></td>
                <td>{{__('dashboard.ending_balance') }}</td>
                <td>{{ $el->ending_balance_type == 'debit' ?$el->ending:''}}</td>
                <td>{{ $el->ending_balance_type == 'credit' ?$el->ending:''}}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

        @endforeach
    </tbody>
</table>
