<table>
    <thead>
        <tr>
            <th>No Purchase order</th>
            <th>@lang('dashboard.date')</th>
            <th>Supplier</th>
            <th>Total</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($purchases as $purchase)
            <tr>
                <td>PO{{ $purchase->serial }}</td>
                <td>{{ $purchase->po_date }}</td>
                <td>{{ $purchase->company.' '.$purchase->first_name }}</td>
                <td>{{ $purchase->total ?? 0 }}</td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>{{ $purchases->sum('total') }}</td>
        </tr>
    </tfoot>
</table>
