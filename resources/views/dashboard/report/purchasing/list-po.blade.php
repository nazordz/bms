@extends('dashboard.base')
@section('content')
    <purchase-po lang="{{ Auth::user()->language }}"></purchase-po>
@endsection
@section('javascript')
<script src="/js/app.js"></script>
@endsection
