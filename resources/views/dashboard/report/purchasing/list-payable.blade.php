@extends('dashboard.base')
@section('content')
    <purchase-payable lang="{{ Auth::user()->language }}"></purchase-payable>
@endsection
@section('javascript')
<script src="/js/app.js"></script>
@endsection
