@extends('dashboard.base')
@section('content')
    <purchase-invoice lang="{{ Auth::user()->language }}"></purchase-invoice>
@endsection
@section('javascript')
<script src="/js/app.js"></script>
@endsection
