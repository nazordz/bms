<table>
    <thead>
        <tr>
            <th>Customer</th>
            <th>No Invoice</th>
            <th>@lang('dashboard.date')</th>
            <th>@lang('dashboard.today')</th>
            <th>@lang('dashboard.1_15_day')</th>
            <th>@lang('dashboard.16_30_day')</th>
            <th>@lang('dashboard.31_45_day')</th>
            <th>@lang('dashboard.46_60_day')</th>
            <th>@lang('dashboard.61_90_day')</th>
            <th>@lang('dashboard.90_above')</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($invoices as $invoice)
            <tr>
                <td>{{ $invoice->supplier->company.' '.$invoice->supplier->first_name }}</td>
                <td>INV{{ $invoice->serial }}</td>
                <td>{{ $invoice->invoice_date }}</td>
                <td>{{ $invoice->today }}</td>
                <td>{{ $invoice->is1_15 }}</td>
                <td>{{ $invoice->is16_30 }}</td>
                <td>{{ $invoice->is31_45 }}</td>
                <td>{{ $invoice->is46_60 }}</td>
                <td>{{ $invoice->is61_90 }}</td>
                <td>{{ $invoice->is90_above }}</td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>{{ $invoices->sum('today') }}</td>
            <td>{{ $invoices->sum('is1_15') }}</td>
            <td>{{ $invoices->sum('is16_30') }}</td>
            <td>{{ $invoices->sum('is31_45') }}</td>
            <td>{{ $invoices->sum('is46_60') }}</td>
            <td>{{ $invoices->sum('is61_90') }}</td>
            <td>{{ $invoices->sum('is90_above') }}</td>
        </tr>
    </tfoot>
</table>
