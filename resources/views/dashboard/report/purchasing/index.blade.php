@extends('dashboard.base')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        @lang('dashboard.Purchasing')
                    </div>
                    <div class="card-body">
                        <div class="list-group">
                            <a href="{{ url('report/purchasing/invoice') }}" class="list-group-item list-group-item-action">
                                <i class="fas fa-file-alt"></i> @lang('dashboard.based_invoice')
                            </a>
                            <a href="{{ url('report/purchasing/purchase-order') }}" class="list-group-item list-group-item-action">
                                <i class="fas fa-truck" aria-hidden="true"></i> @lang('dashboard.based_po')
                            </a>
                            <a href="{{ url('report/purchasing/age-of-payable') }}" class="list-group-item list-group-item-action">
                                <i class="fas fa-chart-line"></i> @lang('dashboard.based_age_of_payable')
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
<script src="{{ asset('js/app.js') }}" defer></script>
@endsection

