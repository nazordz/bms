<table>
    <thead>
        <tr>
            <th>No Invoice</th>
            <th>@lang('dashboard.date')</th>
            <th>Customer</th>
            <th>@lang('dashboard.due_date')</th>
            <th>Total</th>
            <th>Nominal @lang('dashboard.tax')</th>
            <th>@lang('dashboard.total_bill')</th>
            <th>@lang('dashboard.paid_amount')</th>
            <th>@lang('dashboard.balance')</th>
            <th>@lang('dashboard.description')</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($invoices as $invoice)
            <tr>
                <td>INV{{ $invoice->serial }}</td>
                <td>{{ $invoice->invoice_date }}</td>
                <td>{{ $invoice->name }}</td>
                <td>{{ $invoice->due_date }}</td>
                <td>{{ $invoice->total ?? 0 }}</td>
                <td>{{ $invoice->nominal_tax ?? 0 }}</td>
                <td>{{ $invoice->total_bill ?? 0 }}</td>
                <td>{{ $invoice->paid ?? 0 }}</td>
                <td>{{ $invoice->balance ?? 0 }}</td>
                <td>{{ $invoice->keterangan }}</td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>{{ $invoices->sum('total') }}</td>
            <td>{{ $invoices->sum('nominal_tax') }}</td>
            <td>{{ $invoices->sum('total_bill') }}</td>
            <td>{{ $invoices->sum('paid') }}</td>
            <td>{{ $invoices->sum('balance') }}</td>
        </tr>
    </tfoot>
</table>
