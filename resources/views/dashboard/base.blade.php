<!DOCTYPE html>
<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="BMS - Account application">
    <meta name="author" content="Nazor">
    <meta name="keyword" content="Bank Management System,Accounting,">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>BMS</title>
    <link rel="apple-touch-icon" sizes="57x57" href="/img/def-fav.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/def-fav.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/def-fav.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/def-fav.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/def-fav.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/def-fav.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/def-fav.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/def-fav.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/def-fav.png">
    {{-- <link rel="icon" type="image/png" sizes="192x192" href="assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/favicon/favicon-16x16.png"> --}}
    <link rel="shortcut icon" href="{{$logo ?? '/img/def-fav.png'}}" type="image/*">
    {{-- <link rel="manifest" href="assets/favicon/manifest.json"> --}}
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/img/def-fav.png">
    <meta name="theme-color" content="#ffffff">
    <meta charset = "UTF-8" />
    <!-- Icons-->
    <link href="{{ asset('css/free.min.css') }}" rel="stylesheet"> <!-- icons -->
    <link href="{{ asset('css/flag-icon.min.css') }}" rel="stylesheet"> <!-- icons -->
    <link rel="stylesheet" href="{{ asset('css/fontawesome/all.min.css') }}">
    <!-- Main styles for this application-->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/pace.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap4-toggle.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sweetalert2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/select2-bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    {{-- <script src="{{ asset('js/jquery.min.js') }}"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="{{ asset('js/moment.js') }}"></script>

    {{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
    <link rel="stylesheet" href="https://rawgit.com/tempusdominus/bootstrap-4/master/build/css/tempusdominus-bootstrap-4.css" /> --}}

    @yield('css')

    <!-- Global site tag (gtag.js) - Google Analytics-->
    <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-118965717-3"></script>
    <script>
      window.dataLayer = window.dataLayer || [];

      function gtag() {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());
      // Shared ID
      gtag('config', 'UA-118965717-3');
      // Bootstrap ID
      gtag('config', 'UA-118965717-5');
    </script>

    {{-- <link href="{{ asset('css/coreui-chartjs.css') }}" rel="stylesheet"> --}}
  </head>



  <body class="c-app">
      <div id="app">

          <div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">

            @include('dashboard.shared.nav-builder')

            @include('dashboard.shared.header')

            <div class="c-body">

                <main class="c-main">

                @yield('content')

              </main>
            </div>
            @include('dashboard.shared.footer')

          </div>
      </div>



    <!-- CoreUI and necessary plugins-->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
    <script src="{{ asset('js/datatables.min.js') }}"></script>
    {{-- <script src="{{ asset('js/pace.min.js') }}"></script> --}}
    <script src="{{ asset('js/coreui.bundle.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap4-toggle.min.js') }}"></script>
    <script src="{{ asset('js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('js/select2.full.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('locales/bootstrap-datepicker.id.min.js') }}"></script>
    <script src="{{ asset('js/currency.min.js') }}"></script>
    <script src="{{ asset('js/moment-with-locales.js') }}"></script>

    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-range/4.0.2/moment-range.js"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-range/2.2.0/moment-range.min.js"></script>
    <script>
    const thousand_separator = "{{$thousand_separator}}";
    const decimal_separator = "{{$decimal_separator}}";
    const logo = "{{$logo}}";
    const nav_logo = "{{$nav_logo}}";
    const currency = "{{$currency}}";
    logo && sessionStorage.setItem('logo', logo);
    nav_logo && sessionStorage.setItem('nav_logo', nav_logo);
    currency && sessionStorage.setItem('currency', currency);
    thousand_separator && sessionStorage.setItem('thousand_separator', thousand_separator);
    decimal_separator && sessionStorage.setItem('decimal_separator', decimal_separator);


    function setInputFilter(textbox, inputFilter) {
        ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
            textbox.addEventListener(event, function() {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            } else {
                this.value = "";
            }
            });
        });
    }
    function thousandSeparator(num){

        if (/^-?\d*$/.test(num)) {
            var res =  num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return sessionStorage.getItem('thousand_separator') ? res.replace(/,/g, sessionStorage.getItem('thousand_separator')) : res;
        }else{
            return false;
        }
    }
    function serializing(date, serial) {
        var year = String (new Date(date).getFullYear()).substr(2,4);
        var sequence = String (serial).padStart(4, '0')
        return year + sequence
    }
    </script>
    @yield('javascript')

  </body>
</html>
