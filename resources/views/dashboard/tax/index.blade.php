@extends('dashboard.base')

@section('content')

    <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('dashboard.tax')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="formAdd">
                    @csrf
                    <input type="hidden" name="tax_id" id="taxIdInput" value="">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">@lang('dashboard.tax_amount')</label>
                            <div class="col-sm-8 input-group">
                                <input type="number" name="tax_amount" step=".01" min="0" required id="taxAmountInput" class="form-control">
                                <div class="input-group-append">
                                    <div class="input-group-text">%</div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">@lang('dashboard.description')</label>
                            <div class="col-sm-8">
                                <input type="text" name="description" min="0" required id="taxDescriptionInput" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">@lang('dashboard.type')</label>
                            <div class="col-sm-8">
                                <select name="type" id="taxTypeInput" class="form-control">
                                    <option value="1">@lang('dashboard.decrease')</option>
                                    <option value="2">@lang('dashboard.increase')</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">@lang('dashboard.account_payable')</label>
                            <div class="col-sm-8">
                                <select type="text" name="account_payable" required id="accountPayable" class="form-control selectAccount">
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">@lang('dashboard.account_receivable')</label>
                            <div class="col-sm-8">
                                <select type="text" name="account_receivable" required id="accountReceivable" class="form-control selectAccount">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-submit btn-secondary" data-dismiss="modal">@lang('dashboard.close')</button>
                        <button type="submit" class="btn btn-submit btn-primary">@lang('dashboard.save')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-tag"></i> @lang('dashboard.tax')
                        </div>
                        <div class="card-body">
                            <div class="col-lg-12 mb-5">
                                <button onclick="openModal()" class="btn btn-primary pull-right" > {{ __('dashboard.addTax') }}</button>
                            </div>
                            <table id="tbTax" class="table table-responsive-lg table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>@lang('dashboard.description')</th>
                                        <th>{{__('dashboard.tax_amount')}}</th>
                                        <th>{{__('dashboard.type')}}</th>
                                        <th>{{ __('dashboard.action') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('javascript')
<script>
function openModal(){
    $("#modalAdd").modal('show')
    $("#taxIdInput").val('');
    $("#taxAmountInput").val('');
    $("#taxDescriptionInput").val('');
    $("#taxTypeInput").val("1");
    $(".selectAccount").select2('val', '');
}
$(document).ready(function () {
    $("#formAdd").submit(function(e){
        e.preventDefault();
        $(".btn-submit").prop('disabled', true);
        $.ajax({
            type: "post",
            url: "{{url('/tax/store')}}",
            data: $(this).serialize(),
            dataType: "json"
        })
        .done(function(response) {
            $("#modalAdd").modal('hide');
            Swal.fire( '{{__("dashboard.success_saved")}}', '', 'success' );
        })
        .fail((e) => {
            Swal.fire( '{{__("dashboard.tax_exists")}}', '', 'warning' );
        })
        .always(function(){
            $(".btn-submit").prop('disabled', false);
            myTable.draw();
        });
    });


    var myTable = $("#tbTax").DataTable({
        responsive: true,
        processing : true,
        serverSide : true,
        order      : [[2,"ASC"]],
        autoWidth  : false,
        searchDelay: 500,
        ajax : {
            url : "{{ url('/tax/table') }}"
        },
        columns:[
            {
                data: null,
                orderable  : false,
				searchable : false
            },
            { data: 'description' },
            {
                data : 'tax_amount',
                name : 'tax.tax_amount',
                render(data){
                    return `${data} %`;
                }
            },
            {
                data : 'type',
                name : 'tax.type',
                render(data){
                    return data == 1?"{{__('dashboard.decrease')}}":"{{__('dashboard.increase')}}";
                }
            },
            {
                data: null,
                searchable:false,
                orderable : false,
                render(data, type, row){
                    return `<button data-tax_id="${row.tax_id}" class="btn btn-success btn-edit btn-sm mr-2"><i class="fa fa-edit"></i> {{__('dashboard.edit')}}</a>
                            <button class="btn btn-danger btn-delete btn-sm" data-tax_id="${row.tax_id}"><i class="fa fa-trash"></i> {{__('dashboard.delete')}}</button>`
                }
            }
        ],
        fnCreatedRow(row, data, index) {
			if (myTable.page.info().start >= 10) {
				var panjang = myTable.page.info().length;
				var halaman = myTable.page.info().page;
				var i = 1;
				i = (halaman + 1) * panjang;
				i -= panjang - 1;
	        }else{
	        	var i = 1;
	        }
	        $('td', row).eq(0).html(index + i);
		},
        drawCallback(){
            $(".dataTables_length select").removeClass("form-control-sm");
            $(".btn-delete").click(function(){
                var tax_id = $(this).data('tax_id');
                Swal.fire({
                    title: '{{__("dashboard.are_you_sure")}}',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: '{{ __("dashboard.cancel") }}',
                    confirmButtonText: '{{__("dashboard.confirm_delete")}}'
                }).then(function(result) {
                    if (result.value) {
                        $.ajax({
                            type: "post",
                            url: "{{ route('tax.delete') }}",
                            data: { _token: "{{csrf_token()}}", _method:'delete',  tax_id},
                            dataType: "json"
                        }).done(function(data){
                            myTable.draw();
                            Swal.fire( '{{__("dashboard.deleted")}}', '', 'success' );
                        }).catch(function(err){
                            console.log(err);
                        });
                    }
                });
            });
            $(".btn-edit").click(function(){
                const tax_id = $(this).data('tax_id');
                $.ajax({
                    type: "get",
                    url: "{{url('tax/show')}}/"+tax_id,
                    dataType: "json"
                })
                .done(function(data){
                    $("#taxIdInput").val(data.tax_id);
                    $("#taxAmountInput").val(data.tax_amount);
                    $("#taxDescriptionInput").val(data.description);
                    $("#taxTypeInput").val(data.type);

                    if(data.payable_account){
                        var payable = $("<option selected></option>").val(data.payable_account.id).text(data.payable_account.name);
                        $("#accountPayable").append(payable).trigger('change');
                    }
                    if(data.receivable_account){
                        var receivable = $("<option selected></option>").val(data.receivable_account.id).text(data.receivable_account.name);
                        $("#accountReceivable").append(receivable).trigger('change');

                    }
                    $("#modalAdd").modal('show');
                });
            });

            $(".btn-detail").on('hidden.bs.modal', function(e){
                $("#item_picture").css('display', 'none')
                $(".info-detail").empty();
            })
            $("#modalAdd").on('hidden.bs.modal', function(e){
                $("#taxIdInput").val('');
                $("#taxAmountInput").val('');

            });
            $("#modalAdd").on('shown.bs.modal', function(e){
                alert('e')
                $("#taxIdInput").val('');
                $("#taxAmountInput").val('');
            });
		}
    })
    $(".selectAccount").select2({
        width: "100%",
        placeholder: "Choose Account",
        dropdownParent: $("#modalAdd"),
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/account') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        }
    });
});
</script>
@endsection

