@extends('dashboard.base')

@section('content')
    <coa-component lang="{{ Auth::user()->language }}"></coa-component>
@endsection
@section('javascript')
<script src="{{ asset('js/app.js') }}" defer></script>
@endsection

