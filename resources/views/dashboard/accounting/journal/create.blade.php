@extends('dashboard.base')

@section('content')
    <store-journal lang="{{Auth::user()->language}}" submit-url="/accounting/store-journal"></store-journal>
@endsection

@section('javascript')
<script src="/js/app.js" defer></script>
@endsection
