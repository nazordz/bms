@extends('dashboard.base')

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
            @if (session('status-success'))
                <div class="alert alert-success">
                    {{ session('status-success') }}
                </div>
            @endif
            @if (session('status-fail'))
                <div class="alert alert-danger">
                    {{ session('status-fail') }}
                </div>
            @endif
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <ul class="nav nav-pills">
                                <li class="nav-item">
                                    <a class="nav-link active filterStatus" data-status="0" href="#">@lang('dashboard.posted')</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link filterStatus" data-status="1" href="#">Draft</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link filterStatus" data-status="2" href="#">Approve</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link filterStatus" data-status="3" href="#">Voided</a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="col-lg-12 mb-5">
                                <a href="{{ url('/accounting/create-journal') }}" class="btn btn-primary pull-right" > {{ __('dashboard.addJournal') }}</a>
                            </div>
                            <table id="tbItem" class="table table-responsive-lg table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        {{-- <th>{{__('dashboard.code')}}</th> --}}
                                        <th>Ref</th>
                                        <th>{{__('dashboard.description')}}</th>
                                        <th>{{__('dashboard.clasification')}}</th>
                                        <th>Total</th>
                                        <th>{{ __('dashboard.transaction_at') }}</th>
                                        <th>{{ __('dashboard.created_at') }}</th>
                                        <th>Status</th>
                                        <th>{{ __('dashboard.action') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('javascript')
<script>
    function showDetail(id){
        $("#item_picture").css('display', 'none')
        $(".info-detail").empty();

        const item_id = id;
        $.ajax({
            type: "get",
            url: "{{url('item-list/show')}}/"+item_id,
            dataType: "json"
        })
        .done(function(data){
            console.log(data);
            $("#item_code").text(data.item_id);
            $("#item_name").text(data.name);
            $("#item_category").text(data.category.name);
            $("#item_description").text(data.description);
            $("#item_sell_price").text(data.sell_price);
            $("#item_buy_price").text(data.buy_price);
            $("#item_unit").text((data.unit == 0 ? "Pcs" : "Kg"));
            data.picture && $("#item_picture").css('display', 'block').prop('src', data.picture)
        });
        $("#modalDetail").modal('show');
    }

$(document).ready(function () {
    var status = 0;
    var myTable = $("#tbItem").DataTable({
        responsive: true,
        processing : true,
        serverSide : true,
        order      : [[6, "desc"]],
        autoWidth  : false,
        searchDelay: 500,
        ajax : {
            url : "{{ url('/accounting/table-journal') }}",
            data(data){
                data.status = status;
            }
        },
        columns:[
            {
                data: null,
                orderable  : false,
				searchable : false
            },
            // {
            //     data : 'code',
            //     name : 'journals.code',
            //     render(data, type, row){
            //         let date = moment(row.created_at);
            //         return `<a class="btn-link" href="/accounting/journal/${row.uuid}/show">${data}</a>`;
            //     }
            // },
            {
                data: 'serial', name: 'journals.serial',
                render(data, type, row) {
                    var serial = data.padStart(4, '0')
                    return `<a class="btn-link" href="/accounting/journal/${row.uuid}/show">BKK${serial}</a>`
                }
            },
            { data : 'description', name : 'journals.description' },
            // { data : 'reference', name : 'journals.reference' },
            { data : 'name', name : 'journal_clasifications.name' },
            { data : 'total', name : 'journal_details.credit',
                render(data, type, row){
                    let nominal = Number(data).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
                    // return data ? `${sessionStorage.getItem('currency')} ${nominal}` : '';
                    return nominal;
                }
            },
            {
                data: 'transaction_at', name: 'journals.transaction_at',
                render(data){
                    let date = moment(data).format('DD-MM-Y');
                    return date;
                }
            },
            {
                data: 'created_at', name: 'journals.created_at', visible: false
            },
            {
                data: 'status', name: 'journals.status',
                render(data){
                    var res = '';
                    switch (data) {
                        case "1":
                            res = '<div class="alert-tb alert-warning">Draft</div>';
                            break;
                        case "2":
                            res = '<div class="alert-tb alert-success">Approve</div>';
                            break;
                        case "3":
                            res = '<div class="alert-tb alert-danger">Voided</div>';
                            break;
                        default:
                            res = `<div class="alert-tb alert-warning">Draft</div>`;
                            break;
                    }
                    return res;
                }
            },
            {
                data: null,
                searchable:false,
                orderable : false,
                render(data, type, row){
                    var status = Number(row.status)
                    if(status == 2) return `<button class="btn btn-danger btn-delete btn-sm" data-journal-id="${row.id}" data-type="void"><i class="fa fa-trash"></i> Void</button>`
                    else return `<button class="btn btn-danger btn-delete btn-sm" data-journal-id="${row.id}" data-type="delete"><i class="fa fa-trash"></i> {{__('dashboard.delete')}}</button>`
                    // return `<button class="btn btn-danger btn-delete btn-sm" data-journal-id="${row.id}" data-type="void"><i class="fa fa-trash"></i> Void</button>
                    //         <button class="btn btn-danger btn-delete btn-sm" data-journal-id="${row.id}" data-type="delete"><i class="fa fa-trash"></i> {{__('dashboard.delete')}}</button>`
                }
            }
        ],
        fnCreatedRow(row, data, index) {
			if (myTable.page.info().start >= 10) {
				var panjang = myTable.page.info().length;
				var halaman = myTable.page.info().page;
				var i = 1;
				i = (halaman + 1) * panjang;
				i -= panjang - 1;
	        }else{
	        	var i = 1;
	        }
	        $('td', row).eq(0).html(index + i);
		},
        drawCallback(){
            $(".dataTables_length select").removeClass("form-control-sm");
            $(".btn-delete").click(function(){
                var journal_id = $(this).data('journal-id');
                var type = $(this).data('type');
                Swal.fire({
                    title: '{{__("dashboard.are_you_sure")}}',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: '{{ __("dashboard.cancel") }}',
                    confirmButtonText: '{{__("dashboard.confirm_delete")}}'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: "delete",
                            url: "/accounting/delete-journal",
                            data: { _token: "{{csrf_token()}}", _method:'delete',  journal_id, type},
                            dataType: "json"
                        }).done(function(data){
                            myTable.draw();
                            Swal.fire( '{{__("dashboard.deleted")}}', '', 'success' );
                        }).catch(function(err){
                            console.log(err);
                        });
                    }
                });
            });
		}
    })
    $(".filterStatus").click(function(e){
        e.preventDefault();
        status = $(this).data('status');
        $('.nav-link').removeClass('active')
        $(this).addClass('active');
        myTable.draw();
    });
});
</script>
@endsection

