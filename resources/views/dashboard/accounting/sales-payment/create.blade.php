@extends('dashboard.base')
@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
        @if (session('status-success'))
            <div class="alert alert-success">
                {{ session('status-success') }}
            </div>
        @endif
        @if (session('status-fail'))
            <div class="alert alert-danger">
                {{ session('status-fail') }}
            </div>
        @endif
            @if ($data)
                <sales-payment
                    :data="{{ $data }}"
                    lang="{{ Auth::user()->language }}">
                </sales-payment>
            @else
                <sales-payment
                    lang="{{ Auth::user()->language }}">
                </sales-payment>
            @endif

        </div>
    </div>

@endsection


@section('javascript')
<script src="{{ asset('js/app.js') }}" defer></script>

<script>
function handleTotal(event){
    if(event.detail){
        $("#inputTotal").val(event.detail.numberValue)
    }
}
function handleCharacter(e) {
    var key = e.which || e.keyCode;
    if ( key != 188 // Comma
        && key != 8 // Backspace
        && key != 9 // Tab
        && (key < 48 || key > 57) // Non digit
        && (key < 96 || key > 105) // Numpad Non digit
        )
    {
        e.preventDefault();
        return false;
    }
}
function formatMoney(el){
    var nominal = el.value.toString();
    var number_string = nominal.replace(/[^,\d]/g, '').toString(),
        split	= number_string.split(sessionStorage.getItem('thousand_separator')),
        sisa 	= split[0].length % 3,
        rupiah 	= split[0].substr(0, sisa),
        ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);

    if (ribuan) {
        var separator = sisa ? sessionStorage.getItem('thousand_separator') : '';
        rupiah += separator + ribuan.join(sessionStorage.getItem('thousand_separator'));
    }
    el.value = (split[1] != undefined ? rupiah + ',' + split[1] : rupiah);
}
function check(input){
    if (input.value == "") {
     input.setCustomValidity('Choose Sales');
   } else  {
     input.setCustomValidity('');
   }
}
</script>
@endsection
