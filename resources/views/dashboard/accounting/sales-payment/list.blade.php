@extends('dashboard.base')

@section('content')

    <div class="modal fade" id="modalVoid" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Void info</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-5">
                                @lang('dashboard.void_by')
                            </div>
                            <div class="col-lg-6" id="voidBy">

                            </div>
                            <div class="col-lg-5">
                                @lang('dashboard.void_at')
                            </div>
                            <div class="col-lg-6" id="voidAt">

                            </div>
                            <div class="col-lg-5">
                                @lang('dashboard.reason')
                            </div>
                            <div class="col-lg-6" id="voidNote">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('dashboard.close')</button>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="animated fadeIn">
            @if (session('status-success'))
                <div class="alert alert-success">
                    {{ session('status-success') }}
                </div>
            @endif
            @if (session('status-fail'))
                <div class="alert alert-danger">
                    {{ session('status-fail') }}
                </div>
            @endif
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                        <div class="card-header">

                        </div>
                        <div class="card-body">
                            <div class="col-lg-12 mb-5">
                                <a href="{{ url('/accounting/sales-create-payment') }}" class="btn btn-primary pull-right" >
                                    {{ __('dashboard.createPayment') }}
                                </a>
                            </div>
                            <table id="tbItem" class="table table-responsive-lg table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>{{__('dashboard.code')}}</th>
                                        <th>NO INV</th>
                                        <th>@lang('dashboard.rate')</th>
                                        <th>Total</th>
                                        <th>Remark</th>
                                        <th>PPh</th>
                                        <th>Status</th>
                                        <th>@lang('dashboard.payment_date')</th>
                                        <th>{{ __('dashboard.action') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('javascript')
<script>

$(document).ready(function () {
    var status = 0;
    var myTable = $("#tbItem").DataTable({
        responsive: true,
        processing : true,
        serverSide : true,
        order      : [[1,"desc"]],
        autoWidth  : false,
        searchDelay: 500,
        ajax : {
            url : "{{ url('/accounting/sales-table-payment') }}",
            data(data){
                data.status = status;
            }
        },
        columns:[
            {
                data: null,
                orderable  : false,
				searchable : false
            },
            {
                data : 'paySerial',
                name : 'payment_customers.serial',
                render(data, type, row){
                    // return `<a class="btn-link" href="/accounting/payment/${row.uuid}/show">PY${data}</a>`;
                    return `PR${data}`;
                }
            },
            { data : 'invSerial', name : 'invoice_customers.serial',
                render(data){
                    return 'INV'+data;
                }
            },
            {
                data: 'currency', name: 'invoice_customers.currency',
                render(data, type, row){
                    var curr = 'IDR';
                    switch (Number(data)) {
                        case 2:
                            curr = 'SGD';
                            break;
                        case 3:
                            curr = 'USD';
                            break;
                        case 4:
                            curr = 'RM';
                            break;
                    }
                    var str  = `${curr}`
                    if (Number(data) != 1) {
                        let rate = Number(row.rate).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
                        str += ` Rp${rate}`;
                    }
                    return str;
                }
            },
            { data : 'payment_amount', name : 'payment_customers.payment_amount',
                render(data){
                    data = Number(data);
                    var a = data.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
                    return 'IDR '+a;
                }
            },
            { data: 'remark' },
            { data: 'description', name: 'tax.description',
                render(data,type,row){
                    if(data){
                        return `${data} ${row.tax_amount}%`;
                    }
                    return ``;
                }
            },
            { data : 'status', name : 'payment_customers.status',
                render(data){
                    return data ? 'Publish': 'Void';
                }
            },
            {
                data: 'paymentDate',
                name: 'payment_customers.payment_date',
                render(data){
                    let date = data ? moment(data, 'YYYY-MM-DD hh:mm:ss').format('DD-MM-YYYY') : '';
                    return date;
                }
            },
            {
                data: null, orderable: false, searchable: false,
                render(data, type, row){
                    if(row.status){
                        return `<button class="btn btn-sm btn-danger btn-void" data-uuid="${row.uuid}" data-inv="${row.invoice_id}"><i class="fas fa-times"></i> Void</button>`;
                    }
                    return `<button class="btn btn-secondary btn-void-info btn-sm" data-uuid="${row.uuid}"><i class="fas fa-info"></i> {{__('dashboard.reason') }}</button>`;
                }
            }
        ],
        fnCreatedRow(row, data, index) {
			if (myTable.page.info().start >= 10) {
				var panjang = myTable.page.info().length;
				var halaman = myTable.page.info().page;
				var i = 1;
				i = (halaman + 1) * panjang;
				i -= panjang - 1;
	        }else{
	        	var i = 1;
	        }
	        $('td', row).eq(0).html(index + i);
		},
        drawCallback(){
            $(".dataTables_length select").removeClass("form-control-sm");
            $(".btn-void").click(function(){
                var uuid    = $(this).data('uuid');
                var invoice = $(this).data('inv');
                Swal.fire({
                    title: '{{__("dashboard.are_you_sure")}}',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    input: 'textarea',
                    inputPlaceholder: '{{__("dashboard.reason")}}',
                    inputAttributes: {
                        'aria-label': '{{__("dashboard.reason")}}...'
                    },
                    cancelButtonText: '{{ __("dashboard.cancel") }}',
                    confirmButtonText: 'Void'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: "post",
                            url: "/accounting/sales-void-payment",
                            data: { _token: "{{csrf_token()}}", _method:'patch',  uuid, invoice, reason: result.value},
                            dataType: "json"
                        }).done(function(data){
                            myTable.draw();
                            Swal.fire( 'Voided !', '', 'success' );
                        }).catch(function(err){
                            console.log(err);
                        });
                    }
                });
            });

            $(".btn-void-info").click(function(){
                var uuid = $(this).data('uuid');
                $.ajax({
                    type: "get",
                    dataType: "json",
                    url: "/accounting/sales-void-payment",
                    data: {uuid}
                }).done(data => {
                    let voidAt = moment(data.void_at).format('DD-MM-YYYY hh:mm:ss');
                    $("#voidAt").html(voidAt);
                    $("#voidBy").html(data.void_by.name);
                    $("#voidNote").html(data.void_note);
                    $("#modalVoid").modal('show');
                });
            });
		}
    })
    $(".filterStatus").click(function(e){
        e.preventDefault();
        status = $(this).data('status');
        $('.nav-link').removeClass('active')
        $(this).addClass('active');
        myTable.draw();
    });
});
</script>
@endsection

