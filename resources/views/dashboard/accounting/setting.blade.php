@extends('dashboard.base')
@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
        @if (session('status-success'))
            <div class="alert alert-success">
                {{ session('status-success') }}
            </div>
        @endif
        @if (session('status-fail'))
            <div class="alert alert-danger">
                {{ session('status-fail') }}
                @if ($errors->any())
                    <ul>
                        @foreach ($errors->all as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
            </div>
        @endif
            <form action="{{ url('/accounting/setting') }}" enctype="multipart/form-data" method="post">
                <div class="row">
                    <div class="col-lg-6 col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                {{__('dashboard.accounting_sales_setting')}}
                            </div>
                            <div class="card-body">
                                @csrf
                                <div class="col-lg-12">

                                    <div class="form-group row">
                                        <label for="">@lang('dashboard.sales_account')</label>
                                        <select type="text" name="sales_account_id" required class="form-control selectAccount">
                                            @if ($setting->sales)
                                                <option value="{{ $setting->sales->id  }}">{{ $setting->sales->id .' '.$setting->sales->name }}</option>
                                            @endif
                                        </select>
                                        @if ($errors->first('sales_account_id'))
                                            <small class="text-danger">{{ $errors->first('sales_account_id') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group row">
                                        <label for="">@lang('dashboard.receivable_sales_account')</label>
                                        <select type="text" name="receivable_account_id" required class="form-control selectAccount">
                                            @if ($setting->receivable)
                                                <option value="{{ $setting->receivable->id  }}">{{ $setting->receivable->id.' '.$setting->receivable->name }}</option>
                                            @endif
                                        </select>
                                        @if ($errors->first('receivable_account_id'))
                                            <small class="text-danger">{{ $errors->first('receivable_account_id') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group row">
                                        <label for="">@lang('dashboard.customer_sales_return_account')</label>
                                        <select type="text" name="sales_return_account_id" required class="form-control selectAccount">
                                            @if ($setting->sales_return)
                                                <option value="{{ $setting->sales_return->id  }}">{{ $setting->sales_return->id.' '.$setting->sales_return->name }}</option>
                                            @endif
                                        </select>
                                        @if ($errors->first('sales_return_account_id'))
                                            <small class="text-danger">{{ $errors->first('sales_return_account_id') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group row">
                                        <label for="">@lang('dashboard.sales_discount_account')</label>
                                        <select type="text" name="sales_discount_account_id" required class="form-control selectAccount">
                                            @if ($setting->sales_discount)
                                                <option value="{{ $setting->sales_discount->id  }}">{{ $setting->sales_discount->id.' '.$setting->sales_discount->name }}</option>
                                            @endif
                                        </select>
                                        @if ($errors->first('sales_return_account_id'))
                                            <small class="text-danger">{{ $errors->first('sales_return_account_id') }}</small>
                                        @endif
                                    </div>

                                    {{-- laba rugi --}}
                                    <div class="form-group row">
                                        <label for="">@lang('dashboard.profit_rate_account')</label>
                                        <select type="text" name="sales_profit_rate_account_id" required class="form-control selectAccount">
                                            @if ($setting->sales_profit_rate)
                                                <option value="{{ $setting->sales_profit_rate->id  }}">{{ $setting->sales_profit_rate->id.' '.$setting->sales_profit_rate->name }}</option>
                                            @endif
                                        </select>
                                        @if ($errors->first('sales_profit_rate_account_id'))
                                            <small class="text-danger">{{ $errors->first('sales_profit_rate_account_id') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group row">
                                        <label for="">@lang('dashboard.loss_rate_account')</label>
                                        <select type="text" name="sales_loss_rate_account_id" required class="form-control selectAccount">
                                            @if ($setting->sales_loss_rate)
                                                <option value="{{ $setting->sales_loss_rate->id  }}">{{ $setting->sales_loss_rate->id.' '.$setting->sales_loss_rate->name }}</option>
                                            @endif
                                        </select>
                                        @if ($errors->first('sales_loss_rate_account_id'))
                                            <small class="text-danger">{{ $errors->first('sales_loss_rate_account_id') }}</small>
                                        @endif
                                    </div>

                                    {{-- <div class="form-group row">
                                        <label for="">@lang('dashboard.customer_purchase_return_account')</label>
                                        <select type="text" name="customer_purchase_return_account_id" required class="form-control selectAccount">
                                            @if ($setting->customer_purchase_return)
                                                <option value="{{ $setting->customer_purchase_return->id  }}">{{ $setting->customer_purchase_return->id.' '.$setting->customer_purchase_return->name }}</option>
                                            @endif
                                        </select>
                                        @if ($errors->first('customer_purchase_return_account_id'))
                                            <small class="text-danger">{{ $errors->first('customer_purchase_return_account_id') }}</small>
                                        @endif
                                    </div> --}}

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-6 col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                {{__('dashboard.accounting_purchase_setting')}}
                            </div>
                            <div class="card-body">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label for="">@lang('dashboard.purchase_account')</label>
                                        <select type="text" name="purchase_account_id" required class="form-control selectAccount">
                                            @if ($setting->purchase)
                                                <option value="{{ $setting->purchase->id  }}">{{ $setting->purchase->id .' '.$setting->purchase->name }}</option>
                                            @endif
                                        </select>
                                        @if ($errors->first('purchase_account_id'))
                                            <small class="text-danger">{{ $errors->first('purchase_account_id') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group row">
                                        <label for="">@lang('dashboard.debt_account')</label>
                                        <select type="text" name="debt_account_id" required class="form-control selectAccount">
                                            @if ($setting->debt)
                                                <option value="{{ $setting->debt->id  }}">{{ $setting->debt->id.' '.$setting->debt->name }}</option>
                                            @endif
                                        </select>
                                        @if ($errors->first('debt_account_id'))
                                            <small class="text-danger">{{ $errors->first('debt_account_id') }}</small>
                                        @endif
                                    </div>

                                    {{-- <div class="form-group row">
                                        <label for="">@lang('dashboard.supplier_sales_return_account')</label>
                                        <select type="text" name="supplier_sales_return_account_id" required class="form-control selectAccount">
                                            @if ($setting->supplier_sales_return)
                                                <option value="{{ $setting->supplier_sales_return->id  }}">{{ $setting->supplier_sales_return->id.' '.$setting->supplier_sales_return->name }}</option>
                                            @endif
                                        </select>
                                        @if ($errors->first('suplier_sales_return_account_id'))
                                            <small class="text-danger">{{ $errors->first('suplier_sales_return_account_id') }}</small>
                                        @endif
                                    </div> --}}

                                    <div class="form-group row">
                                        <label for="">@lang('dashboard.supplier_purchase_return_account')</label>
                                        <select type="text" name="purchase_return_account_id" required class="form-control selectAccount">
                                            @if ($setting->purchase_return)
                                                <option value="{{ $setting->purchase_return->id  }}">{{ $setting->purchase_return->id.' '.$setting->purchase_return->name }}</option>
                                            @endif
                                        </select>
                                        @if ($errors->first('purchase_return_account_id'))
                                            <small class="text-danger">{{ $errors->first('purchase_return_account_id') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group row">
                                        <label for="">@lang('dashboard.purchase_discount_account')</label>
                                        <select type="text" name="purchase_discount_account_id" required class="form-control selectAccount">
                                            @if ($setting->purchase_discount)
                                                <option value="{{ $setting->purchase_discount->id  }}">{{ $setting->purchase_discount->id.' '.$setting->purchase_discount->name }}</option>
                                            @endif
                                        </select>
                                        @if ($errors->first('purchase_discount_account_id'))
                                            <small class="text-danger">{{ $errors->first('purchase_discount_account_id') }}</small>
                                        @endif
                                    </div>

                                    {{-- laba rugi --}}
                                    <div class="form-group row">
                                        <label for="">@lang('dashboard.profit_rate_account')</label>
                                        <select type="text" name="purchase_profit_rate_account_id" required class="form-control selectAccount">
                                            @if ($setting->purchase_profit_rate)
                                                <option value="{{ $setting->purchase_profit_rate->id  }}">{{ $setting->purchase_profit_rate->id.' '.$setting->purchase_profit_rate->name }}</option>
                                            @endif
                                        </select>
                                        @if ($errors->first('sales_profit_rate_account_id'))
                                            <small class="text-danger">{{ $errors->first('profit_rate_account_id') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group row">
                                        <label for="">@lang('dashboard.loss_rate_account')</label>
                                        <select type="text" name="purchase_loss_rate_account_id" required class="form-control selectAccount">
                                            @if ($setting->purchase_loss_rate)
                                                <option value="{{ $setting->purchase_loss_rate->id  }}">{{ $setting->purchase_loss_rate->id.' '.$setting->purchase_loss_rate->name }}</option>
                                            @endif
                                        </select>
                                        @if ($errors->first('sales_loss_rate_account_id'))
                                            <small class="text-danger">{{ $errors->first('sales_loss_rate_account_id') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="{{$setting->id}}">
                    <div class="col-lg-8 col-xs-12 mb-5">
                        <button class="btn btn-primary" type="submit">{{ __('dashboard.save') }}</button>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection


@section('javascript')
<script>
$(document).ready(function () {
    $(".selectAccount").select2({
        width: "100%",
        placeholder: "Choose Account",
        theme: 'bootstrap4',
        ajax: {
            url: " {{ url('/select/account') }}",
            dataType: "json",
            data(params) {
				return {
					term : params.term || '' ,
					page : params.page || 1,
					page_limit : 10
				};
            },

            cache: true,
        }
    });

});
</script>
@endsection
