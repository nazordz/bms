@extends('dashboard.base')
@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
        @if (session('status-success'))
            <div class="alert alert-success">
                {{ session('status-success') }}
            </div>
        @endif
        @if (session('status-fail'))
            <div class="alert alert-danger">
                {{ session('status-fail') }}
            </div>
        @endif
            @if ($data)
                <purchase-payment
                    :data="{{ $data }}"
                    lang="{{ Auth::user()->language }}">
                </purchase-payment>
            @else
                <purchase-payment
                    lang="{{ Auth::user()->language }}">
                </purchase-payment>
            @endif

        </div>
    </div>

@endsection


@section('javascript')
<script src="{{ asset('js/app.js') }}" defer></script>

@endsection
