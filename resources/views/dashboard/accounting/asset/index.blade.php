@extends('dashboard.base')

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
            @if (session('status-success'))
                <div class="alert alert-success">
                    {{ session('status-success') }}
                </div>
            @endif
            @if (session('status-fail'))
                <div class="alert alert-danger">
                    {{ session('status-fail') }}
                </div>
            @endif
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <ul class="nav nav-pills">
                                <li class="nav-item">
                                    <a class="nav-link filterStatus" data-status="1" href="#">Draft</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active filterStatus" data-status="2" href="#">@lang('dashboard.registered_asset')</a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="col-lg-12 mb-3">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <a href="{{ url('/accounting/asset/create') }}" class="btn btn-primary mx-1">
                                            {{ __('dashboard.add_assets') }}
                                        </a>
                                        <a href="{{ url('/accounting/asset/calculate-depreciation') }}" class="btn btn-success mx-1">
                                            {{ __('dashboard.calculate_depreciation') }}
                                        </a>
                                        <a href="{{ url('/accounting/asset/print/2') }}" target="_blank" id="printAsset" class="btn btn-warning mx-1 text-white">
                                            {{ __('dashboard.print') }}
                                        </a>
                                    </div>
                                    @if ($last_depre)
                                        <div class="col-lg-6">
                                            <div class="pull-right">
                                                @lang('dashboard.depreciation_until') <b>{{date_format(date_create($last_depre->to_date), "d-M-Y")}}</b>
                                            </div>
                                        </div>
                                    @endif

                                </div>
                            </div>
                            <table id="tbAsset" class="table table-responsive-lg table-striped">
                                <thead>
                                    <tr>
                                        <th>@lang('dashboard.asset_code')</th>
                                        <th>@lang('dashboard.asset_name')</th>
                                        <th>@lang('dashboard.clasification')</th>
                                        <th>@lang('dashboard.asset_type')</th>
                                        <th>@lang('dashboard.date_acquistion')</th>
                                        <th>@lang('dashboard.price_acquistion')</th>
                                        <th>@lang('dashboard.book_value')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('javascript')
<script>

$(document).ready(function () {
    var status = 2;
    var myTable = $("#tbAsset").DataTable({
        responsive: true,
        processing : true,
        serverSide : true,
        order      : [[1,"desc"]],
        autoWidth  : false,
        searchDelay: 500,
        ajax : {
            url : "{{ url('/accounting/asset/table') }}",
            data(data){
                data.status = status;
            }
        },
        columns:[
            { data: 'serial', name: 'fixed_assets.serial',
                render(data, type, row){
                    return `<a href="/accounting/asset/edit/${row.uuid}">${data}</a>`
                }
            },
            { data: 'name', name: 'fixed_assets.name' },
            { data: 'ClasName', name: 'journal_clasifications.name' },
            { data: 'typeName', name: 'setting_asset_types.name' },
            { data: 'date_acquistion', name: 'fixed_assets.date_acquistion' },
            { data: 'price_acquistion', name: 'fixed_assets.price_acquistion',
                render(data){
                    if(data) return Number(data).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
                    else return 0;
                }
            },
            { data: 'total_shrinkage', name: 'fixed_assets.total_shrinkage', searchable: false, orderable: false,
                render(data, type, row) {
                    if(data){
                        var itung = Number(row.price_acquistion) - Number(data)
                        return itung.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
                    }else return 0;
                },
            },
        ],
        drawCallback(){
            $(".dataTables_length select").removeClass("form-control-sm");
            $(".btn-void").click(function(){
                var uuid    = $(this).data('uuid');
                var invoice = $(this).data('inv');
                Swal.fire({
                    title: '{{__("dashboard.are_you_sure")}}',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    input: 'textarea',
                    inputPlaceholder: '{{__("dashboard.reason")}}',
                    inputAttributes: {
                        'aria-label': '{{__("dashboard.reason")}}...'
                    },
                    cancelButtonText: '{{ __("dashboard.cancel") }}',
                    confirmButtonText: 'Void'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: "post",
                            url: "/accounting/sales-void-payment",
                            data: { _token: "{{csrf_token()}}", _method:'patch',  uuid, invoice, reason: result.value},
                            dataType: "json"
                        }).done(function(data){
                            myTable.draw();
                            Swal.fire( 'Voided !', '', 'success' );
                        }).catch(function(err){
                            console.log(err);
                        });
                    }
                });
            });

            $(".btn-void-info").click(function(){
                var uuid = $(this).data('uuid');
                $.ajax({
                    type: "get",
                    dataType: "json",
                    url: "/accounting/sales-void-payment",
                    data: {uuid}
                }).done(data => {
                    let voidAt = moment(data.void_at).format('DD-MM-YYYY hh:mm:ss');
                    $("#voidAt").html(voidAt);
                    $("#voidBy").html(data.void_by.name);
                    $("#voidNote").html(data.void_note);
                    $("#modalVoid").modal('show');
                });
            });
		}
    })
    $(".filterStatus").click(function(e){
        e.preventDefault();
        status = $(this).data('status');
        $('.nav-link').removeClass('active')
        $(this).addClass('active');
        myTable.draw();

        $("#printAsset").prop('href', '/accounting/asset/print/'+status)
    });
});
</script>
@endsection

