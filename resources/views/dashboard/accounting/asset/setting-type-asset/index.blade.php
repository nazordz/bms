@extends('dashboard.base')

@section('content')

    <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('dashboard.add_asset_type') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="formAdd" action="/accounting/setting-type-asset/store">
                    <div class="modal-body">
                    @csrf
                        <div class="form-group">
                            <label for="">{{__('dashboard.name')}}</label>
                            <input type="text" name="name" id="inputName" required class="form-control" placeholder="{{__('dashboard.name')}}">
                        </div>
                        <div class="form-group">
                            <label for="">{{__('dashboard.shrinkage_method')}}</label>
                            <select name="shrinkage_method" required id="shrinkage_method" class="form-control">
                                <option value="Garis Lurus">@lang('dashboard.straight_line')</option>
                                {{-- <option value="Saldo Menurun">@lang('dashboard.decreased_balance')</option> --}}
                                {{-- <option value="Tidak Terdepresiasi">@lang('dashboard.not_depreciate')</option> --}}
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">{{__('dashboard.estimated_age')}}</label>
                            <input type="text" name="estimated_age" id="estimated_age" required class="form-control" placeholder="{{__('dashboard.estimated_age')}}">
                        </div>
                        <div class="form-group">
                            <label for="">{{__('dashboard.depreciation_rates')}}</label>
                            <div class="input-group">
                                <input type="text" name="depreciation_rates" id="depreciation_rates" readonly class="form-control" placeholder="">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="addon-wrapping">%</span>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="uuid" id="uuid" value="">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-submit btn-secondary" data-dismiss="modal">@lang('dashboard.close')</button>
                        <button type="submit" class="btn btn-submit btn-primary" id="btnSubmit">@lang('dashboard.save')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="animated fadeIn">
            @if (session('status-success'))
                <div class="alert alert-success">
                    {{ session('status-success') }}
                </div>
            @endif
            @if (session('status-fail'))
                <div class="alert alert-danger">
                    {{ session('status-fail') }}
                </div>
            @endif
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-edit"></i> @lang('dashboard.setting_type_asset')
                        </div>
                        <div class="card-body">
                            <div class="col-lg-12 mb-5">
                                <button class="btn btn-primary pull-right" onclick="openModal()"> {{ __('dashboard.add_asset_type') }}</button>
                            </div>
                            <table id="tbAsset" class="table table-responsive-lg table-responsive-sm table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>@lang('dashboard.code')</th>
                                        <th>{{__('dashboard.name')}}</th>
                                        <th>{{__('dashboard.shrinkage_method')}}</th>
                                        <th>{{__('dashboard.estimated_age')}}</th>
                                        <th>{{__('dashboard.depreciation_rates')}}</th>
                                        <th>{{__('dashboard.action') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('javascript')
<script>
function openModal() {
    $("#inputName, #estimated_age, #shrinkage_method, #depreciation_rates, #uuid").val('');
    $("#btnSubmit").prop('disabled', false);
    $("#modalAdd").modal('show');
}
$(document).ready(function () {
    $("#formAdd").submit(function(e){
        e.preventDefault();
        var formData = $(this).serialize();
        $("#btnSubmit").prop('disabled', true);
        $.ajax({
            type    : "post",
            url     : '/accounting/setting-type-asset/store',
            data    : formData,
            dataType: "json"
        })
        .done(function(data){
            $("#btnSubmit").prop('disabled', false);
            Swal.fire( '{{__("dashboard.success_saved")}}', '', 'success' );
            $("#modalAdd").modal('hide')
            $("#inputName, #estimated_age, #shrinkage_method, #depreciation_rates, #uuid").val('');
            myTable.draw();
        });
    })

    $('#shrinkage_method, #estimated_age').change(function(){
        var shrinkageMethod = $("#shrinkage_method").val();
        var estimatedAge    = $("#estimated_age").val();

        var rumus = 0;
        if(shrinkageMethod == 'Garis Lurus'){
            $("#estimated_age").prop('disabled', false).prop('required', true)
            rumus = 100 / estimatedAge;
        }else if(shrinkageMethod == 'Saldo Menurun'){
            $("#estimated_age").prop('disabled', false).prop('required', true)
            rumus = 2/estimatedAge*100;
        }else{
            $("#estimated_age").prop('disabled', true).prop('required', false).val(0);
        }
        $("#depreciation_rates").val(rumus);
    });

    var myTable = $("#tbAsset").DataTable({
        processing : true,
        serverSide : true,
        responsive : true,
        order      : [[1,"asc"]],
        autoWidth  : false,
        searchDelay: 500,
        ajax : {
            url : "{{ url('/accounting/setting-type-asset/table') }}"
        },
        columns:[
            {
                data: null,
                orderable  : false,
				searchable : false
            },
            { data : 'serial', name : 'serial' },
            { data : 'name', name : 'name' },
            { data : 'shrinkage_method', name : 'shrinkage_method' },
            { data : 'estimated_age', name : 'estimated_age' },
            { data : 'depreciation_rates', name : 'depreciation_rates',
                render(data){
                    return data+'%';
                }
            },
            {
                data      : null,
                searchable: false,
                orderable :  false,
                render(data, type, row){
                    return `<button data-uuid="${row.uuid}" class="btn btn-show btn-success btn-sm"><i class="fa fa-edit"></i> {{__('dashboard.edit')}}</button>
                            <button class="btn btn-danger btn-delete btn-sm" data-uuid="${row.uuid}"><i class="fa fa-trash"></i> {{__('dashboard.delete')}}</button> `
                }
            }
        ],
        fnCreatedRow(row, data, index) {
			if (myTable.page.info().start >= 10) {
				var panjang = myTable.page.info().length;
				var halaman = myTable.page.info().page;
				var i = 1;
				i = (halaman + 1) * panjang;
				i -= panjang - 1;
	        }else{
	        	var i = 1;
	        }
	        $('td', row).eq(0).html(index + i);
		},
        drawCallback(){
            $(".dataTables_length select").removeClass("form-control-sm");
            $(".btn-show").click(function(){
                var uuid = $(this).data('uuid');

                $("#inputName, #estimated_age, #shrinkage_method, #depreciation_rates").val('');
                $.ajax({
                    type: "get",
                    url: "/accounting/setting-type-asset/show/"+uuid,
                    dataType: "json"
                })
                .done(function(data){
                    console.log(data);
                    $('#inputName').val(data.name);
                    $('#estimated_age').val(data.estimated_age);
                    $('#shrinkage_method').val(data.shrinkage_method);
                    $('#depreciation_rates').val(data.depreciation_rates);
                    $('#uuid').val(data.uuid);

                    $("#modalAdd").modal('show')
                });
            });
            $(".btn-delete").click(function(){
                var uuid = $(this).data('uuid');
                Swal.fire({
                    title: '{{__("dashboard.are_you_sure")}}',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: '{{ __("dashboard.cancel") }}',
                    confirmButtonText: '{{__("dashboard.confirm_delete")}}'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: "post",
                            url: "/accounting/setting-type-asset/delete",
                            data: { _token: "{{csrf_token()}}", _method:'delete',  uuid},
                            dataType: "json"
                        }).done(function(data){
                            myTable.draw();
                            Swal.fire( '{{__("dashboard.deleted")}}', '', 'success' );
                        }).catch(function(err){
                            console.log(err);
                        });
                    }
                });
            });
		}
    })
    .on('responsive-display', function(e, datatable, row, showHide, update){
        $(".admin-active").bootstrapToggle();
        $(".admin-active").change(function(){
            var status = $(this).prop('checked') ? 1 : 0;
            var uuid     = $(this).data('uuid')

            $.ajax({
                type: "patch",
                url: "{{url('/admin/status')}}",
                data: { status, uuid, _token: '{{ csrf_token() }}' },
                dataType: "json"
            })
            .done(function(data){
                Swal.fire(
                    '{{ __("dashboard.success") }}',
                    '{{ __("dashboard.success_change") }}',
                    'success'
                );
                myTable.draw();
            })
            .catch(function(err){
                console.log(err);

            });
        })
    })
});
</script>
@endsection

