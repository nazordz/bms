@extends('dashboard.base')

@section('content')
    <asset-depreciation lang="{{Auth::user()->language}}"></asset-depreciation>
@endsection

@section('javascript')
<script src="/js/app.js" defer></script>
@endsection
