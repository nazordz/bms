@extends('dashboard.base')

@section('content')
    <asset-store lang="{{Auth::user()->language}}"></asset-store>
@endsection

@section('javascript')
<script src="/js/app.js" defer></script>
@endsection
