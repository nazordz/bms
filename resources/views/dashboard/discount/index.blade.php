@extends('dashboard.base')

@section('content')

    <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Discount</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="form-add">
                    @csrf
                    <input type="hidden" name="discount_id" id="discountIdInput" value="">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">@lang('dashboard.discount_amount')</label>
                            <div class="col-sm-8 input-group">
                                <input type="number" name="discount_amount" required min="0" id="discountAmountInput" class="form-control">
                                <div class="input-group-append">
                                    <div class="input-group-text">%</div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">@lang('dashboard.description')</label>
                            <div class="col-sm-8">
                                <input type="text" name="description" required min="0" id="discountDescriptionInput" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-submit btn-secondary" data-dismiss="modal">@lang('dashboard.close')</button>
                        <button type="submit" class="btn btn-submit btn-primary">@lang('dashboard.save')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-tag"></i> Discount
                        </div>
                        <div class="card-body">
                            <div class="col-lg-12 mb-5">
                                <button onclick="openModal()" class="btn btn-primary pull-right" > {{ __('dashboard.addDiscount') }}</button>
                            </div>
                            <table id="tbItem" class="table table-responsive-lg table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>@lang('dashboard.description')</th>
                                        <th>{{__('dashboard.discount_amount')}}</th>
                                        <th>{{ __('dashboard.action') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('javascript')
<script>
function openModal(){
    $("#modalAdd").modal('show')
    $("#discountIdInput").val('');
    $("#discountAmountInput").val('');
    $("#discountDescriptionInput").val('');
}
$(document).ready(function () {
    $("#form-add").submit(function(e){
        e.preventDefault();
        $(".btn-submit").prop('disabled', true);
        $.ajax({
            type: "post",
            url: "{{url('/discount/store')}}",
            data: $(this).serialize(),
            dataType: "json"
        })
        .done(function(response) {
            $("#modalAdd").modal('hide');
            Swal.fire( '{{__("dashboard.success_saved")}}', '', 'success' );
        })
        .fail((e) => {
            Swal.fire( '{{__("dashboard.fail")}}', '', 'warning' );
        })
        .always(function(){
            $(".btn-submit").prop('disabled', false);
            myTable.draw();
        });
    });


    var myTable = $("#tbItem").DataTable({
        responsive: true,
        processing : true,
        serverSide : true,
        order      : [[2,"ASC"]],
        autoWidth  : false,
        searchDelay: 500,
        ajax : {
            url : "{{ url('/discount/table') }}"
        },
        columns:[
            {
                data: null,
                orderable  : false,
				searchable : false
            },
            {
                data: 'description',
            },
            {
                data : 'discount_amount',
                name : 'discounts.discount_amount',
                render(data){
                    return `${data}%`;
                }
            },
            {
                data: null,
                searchable:false,
                orderable : false,
                render(data, type, row){
                    return `<button data-discount_id="${row.discount_id}" class="btn btn-success btn-edit btn-sm mr-2"><i class="fa fa-edit"></i> {{__('dashboard.edit')}}</a>
                            <button class="btn btn-danger btn-delete btn-sm" data-discount_id="${row.discount_id}"><i class="fa fa-trash"></i> {{__('dashboard.delete')}}</button>`
                }
            }
        ],
        fnCreatedRow(row, data, index) {
			if (myTable.page.info().start >= 10) {
				var panjang = myTable.page.info().length;
				var halaman = myTable.page.info().page;
				var i = 1;
				i = (halaman + 1) * panjang;
				i -= panjang - 1;
	        }else{
	        	var i = 1;
	        }
	        $('td', row).eq(0).html(index + i);
		},
        drawCallback(){
            $(".dataTables_length select").removeClass("form-control-sm");
            $(".btn-delete").click(function(){
                var discount_id = $(this).data('discount_id');
                Swal.fire({
                    title: '{{__("dashboard.are_you_sure")}}',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: '{{ __("dashboard.cancel") }}',
                    confirmButtonText: '{{__("dashboard.confirm_delete")}}'
                }).then(function(result) {
                    if (result.value) {
                        $.ajax({
                            type: "post",
                            url: "{{ route('discount.delete') }}",
                            data: { _token: "{{csrf_token()}}", _method:'delete',  discount_id},
                            dataType: "json"
                        }).done(function(data){
                            myTable.draw();
                            Swal.fire( '{{__("dashboard.deleted")}}', '', 'success' );
                        }).catch(function(err){
                            console.log(err);
                        });
                    }
                });
            });
            $(".btn-edit").click(function(){
                const discount_id = $(this).data('discount_id');
                $.ajax({
                    type: "get",
                    url: "{{url('discount/show')}}/"+discount_id,
                    dataType: "json"
                })
                .done(function(data){
                    $("#discountIdInput").val(data.discount_id);
                    $("#discountAmountInput").val(data.discount_amount);
                    $("#discountDescriptionInput").val(data.description);
                    $("#modalAdd").modal('show');
                });
            });

            $(".btn-detail").on('hidden.bs.modal', function(e){
                $("#item_picture").css('display', 'none')
                $(".info-detail").empty();
            })
            $("#modalAdd").on('hidden.bs.modal', function(e){
                $("#discountIdInput").val('');
                $("#discountAmountInput").val('');

            });
            $("#modalAdd").on('shown.bs.modal', function(e){
                alert('e')
                $("#discountIdInput").val('');
                $("#discountAmountInput").val('');
            });
		}
    })
});
</script>
@endsection

