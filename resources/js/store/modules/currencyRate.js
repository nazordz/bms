import { getField, updateField } from 'vuex-map-fields'

const state = {
    currency: 1,
    rate: 1,
    rateTax: 0
}

const getters = {
    getField,
    getCurrency: state => {
        var result = '';
        switch (Number(state.currency)) {
            case 2:
                result = 'SGD';
                break;
            case 3:
                result = 'USD';
                break;
            case 4:
                result = 'RM';
                break;
            default:
                result = 'IDR';
                break;
        }
        return result;
    }
}

const mutations = {
    updateField(state, {path, value}){

        if(path == 'currency' && value == 1){
            state.rate = 0;
            state.rateTax = 0;
        }
        state[path] = value;
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations
}
