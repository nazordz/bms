import Vue from 'vue';
import Vuex from 'vuex';
import currencyRate from './modules/currencyRate';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        currencyRate
    },
    strict: false
})
