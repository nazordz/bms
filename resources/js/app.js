/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import Vuelidate from 'vuelidate'
import commonLang from './locales/common';
import VueI18n from 'vue-i18n';
import store from './store/index';
import CurrencyInput from 'vue-currency-input';
Vue.use(CurrencyInput);
Vue.use(Vuelidate)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
Vue.component('b-button-group', require('bootstrap-vue').BButtonGroup);
Vue.component('b-button', require('bootstrap-vue').BButton);
Vue.component('b-popover', require('bootstrap-vue').BPopover);
Vue.component('select2', require('./components/Select2Component.vue').default );
Vue.component('v-datetimepicker', require('./components/DateTimePicker.vue').default );
Vue.component('v-currency', () => import(/* webpackChunkName: "group-currency-component"  */ './components/CurrencyComponent.vue'));
Vue.component('v-rate-curr', () => import(/* webpackChunkName: "group-currency-component" */ './components/CurrencyRateComponent.vue'));
Vue.component('v-print-quotation-supplier', () => import(/* webpackChunkName: "printing-component" */ './components/PrintQuotationSupplier.vue'));
Vue.component('v-print-po-supplier', () => import(/* webpackChunkName: "printing-component" */ './components/PrintPoSupplier.vue'));
Vue.component('v-print-quotation', () => import(/* webpackChunkName: "printing-component" */ './components/PrintQuotation.vue'));
Vue.component('v-print-credit', () => import(/* webpackChunkName: "printing-component" */ './components/PrintCreditNoteCustomer.vue'));
Vue.component('v-print-invoice-customer', () => import(/* webpackChunkName: "printing-component" */ './components/PrintInvoiceCustomer.vue'));
Vue.component('v-print-invoice-supplier', () => import(/* webpackChunkName: "printing-component" */ './components/PrintInvoiceSupplier.vue'));
Vue.component('v-print-equity', () => import(/* webpackChunkName: "printing-component" */ './components/report/equity/EquityPrint.vue'));
Vue.component('v-print-balance', () => import(/* webpackChunkName: "printing-component" */ './components/report/balance/BalancePrint.vue'));
Vue.component('v-print-cashflow', () => import(/* webpackChunkName: "printing-component" */ './components/report/cashflow/CashflowPrint.vue'));

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
// Vue.component('item-component', require('./components/ItemComponent.vue').default);
// Vue.component('booking-component', require('./components/BookingComponent.vue').default);
// Vue.component('create-journal', require('./components/CreateJournalComponent.vue').default);
// Vue.component('coa-component', require('./components/CoaComponent.vue').default);
// Vue.component('select2', require('./components/Select2Component.vue').default);
// Vue.component('account-list', require('./components/AccountLists.vue').default);

Vue.component(
    'item-component',
    () => import( /* webpackChunkName: "form-component" */ './components/ItemComponent.vue')
);
Vue.component(
    'booking-component',
    () => import( /* webpackChunkName: "form-component" */ './components/BookingComponent.vue')
);
Vue.component(
    'store-journal',
    () => import( /* webpackChunkName: "accounting" */ './components/StoreJournalComponent.vue')
);
Vue.component(
    'print-journal',
    () => import( /* webpackChunkName: "printing-component" */ './components/PrintJournal.vue')
);
Vue.component(
    'coa-component',
    () => import( /* webpackChunkName: "accounting" */ './components/CoaComponent.vue')
);
Vue.component(
    'note-component',
    () => import( /* webpackChunkName: "form-component" */ './components/NoteComponent.vue')
);
Vue.component(
    'allocate-credit',
    () => import( /* webpackChunkName: "form-component" */ './components/AllocateCredit.vue')
);
Vue.component(
    'account-list',
    () => import( /* webpackChunkName: "accounting" */ './components/AccountLists.vue')
);
Vue.component(
    'sales-payment',
    () => import( /* webpackChunkName: "accounting" */ './components/SalesPayment.vue')
);
Vue.component(
    'purchase-payment',
    () => import( /* webpackChunkName: "accounting" */ './components/PurchasePayment.vue')
);

// Report components
Vue.component(
    'sales-invoice',
    () => import( /* webpackChunkName: "report" */'./components/report/Sales/SalesInvoice.vue')
);
Vue.component(
    'sales-bc',
    () => import( /* webpackChunkName: "report" */'./components/report/Sales/SalesBc.vue')
);
Vue.component(
    'sales-receivable',
    () => import( /* webpackChunkName: "report" */'./components/report/Sales/SalesAgeOfReceivable.vue')
);
Vue.component(
    'purchase-invoice',
    () => import( /* webpackChunkName: "report" */'./components/report/Purchase/PurchaseInvoice.vue')
);
Vue.component(
    'purchase-po',
    () => import( /* webpackChunkName: "report" */'./components/report/Purchase/PurchasePo.vue')
);
Vue.component(
    'purchase-payable',
    () => import( /* webpackChunkName: "report" */'./components/report/Purchase/PurchaseAgeOfPayable.vue')
);
Vue.component(
    'ledger-list',
    () => import( /* webpackChunkName: "report" */'./components/report/ledger/LedgerList.vue')
);
Vue.component(
    'profit-loss-list',
    () => import( /* webpackChunkName: "report" */'./components/report/profit_loss/ProfitLossList.vue')
);
Vue.component(
    'equity-list',
    () => import( /* webpackChunkName: "report" */'./components/report/equity/EquityList.vue')
);
Vue.component(
    'balance-list',
    () => import( /* webpackChunkName: "report" */'./components/report/balance/BalanceList.vue')
);
Vue.component(
    'balance-account',
    () => import( /* webpackChunkName: "report" */'./components/report/balance/BalanceAccount.vue')
);
Vue.component(
    'cashflow-list',
    () => import( /* webpackChunkName: "report" */'./components/report/cashflow/CashflowList.vue')
);

Vue.component(
    'asset-store',
    () => import( /* webpackChunkName: "asset" */'./components/asset/StoreAsset.vue')
);

Vue.component(
    'asset-depreciation',
    () => import( /* webpackChunkName: "asset" */'./components/asset/CalculateDepreciation.vue')
);
Vue.config.productionTip = false;
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    store,
    i18n: new VueI18n({
        messages: {...commonLang}
    }),
});
