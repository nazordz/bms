<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['get.menu']], function () {
    Route::get('/', 'admin\DashboardController@index');
    Route::post('language', 'LanguageController@change');
    Route::get('profile', 'admin\UsersController@editForm');
    Route::put('profile/', 'admin\UsersController@edit');
    Route::get('item/{item_id}','SelectController@getItem');

    Auth::routes();
    Route::get('company', 'admin\DashboardController@company')->middleware('auth');
    Route::get('company/table', 'admin\DashboardController@tableCompany')->middleware('auth');
    Route::post('company/store', 'admin\DashboardController@storeCompany')->middleware('auth');
    Route::get('company/switch/{id}', 'admin\DashboardController@switchCompany')->middleware('auth');
    Route::get('company/show/{id}', 'admin\DashboardController@showCompany')->middleware('auth');
    Route::delete('company/delete', 'admin\DashboardController@deleteCompany')->name('company.delete')->middleware('auth');
    /*
    Route::group(['middleware' => ['role:user']], function () {
        Route::get('/colors', function () {     return view('dashboard.colors'); });
        Route::get('/typography', function () { return view('dashboard.typography'); });
        Route::get('/charts', function () {     return view('dashboard.charts'); });
        Route::get('/widgets', function () {    return view('dashboard.widgets'); });
        Route::get('/404', function () {        return view('dashboard.404'); });
        Route::get('/500', function () {        return view('dashboard.500'); });
        Route::prefix('base')->group(function () {
            Route::get('/breadcrumb', function(){   return view('dashboard.base.breadcrumb'); });
            Route::get('/cards', function(){        return view('dashboard.base.cards'); });
            Route::get('/carousel', function(){     return view('dashboard.base.carousel'); });
            Route::get('/collapse', function(){     return view('dashboard.base.collapse'); });

            Route::get('/forms', function(){        return view('dashboard.base.forms'); });
            Route::get('/jumbotron', function(){    return view('dashboard.base.jumbotron'); });
            Route::get('/list-group', function(){   return view('dashboard.base.list-group'); });
            Route::get('/navs', function(){         return view('dashboard.base.navs'); });

            Route::get('/pagination', function(){   return view('dashboard.base.pagination'); });
            Route::get('/popovers', function(){     return view('dashboard.base.popovers'); });
            Route::get('/progress', function(){     return view('dashboard.base.progress'); });
            Route::get('/scrollspy', function(){    return view('dashboard.base.scrollspy'); });

            Route::get('/switches', function(){     return view('dashboard.base.switches'); });
            Route::get('/tables', function () {     return view('dashboard.base.tables'); });
            Route::get('/tabs', function () {       return view('dashboard.base.tabs'); });
            Route::get('/tooltips', function () {   return view('dashboard.base.tooltips'); });
        });
        Route::prefix('buttons')->group(function () {
            Route::get('/buttons', function(){          return view('dashboard.buttons.buttons'); });
            Route::get('/button-group', function(){     return view('dashboard.buttons.button-group'); });
            Route::get('/dropdowns', function(){        return view('dashboard.buttons.dropdowns'); });
            Route::get('/brand-buttons', function(){    return view('dashboard.buttons.brand-buttons'); });
        });
        Route::prefix('icon')->group(function () {  // word: "icons" - not working as part of adress
            Route::get('/coreui-icons', function(){         return view('dashboard.icons.coreui-icons'); });
            Route::get('/flags', function(){                return view('dashboard.icons.flags'); });
            Route::get('/brands', function(){               return view('dashboard.icons.brands'); });
        });
        Route::prefix('notifications')->group(function () {
            Route::get('/alerts', function(){   return view('dashboard.notifications.alerts'); });
            Route::get('/badge', function(){    return view('dashboard.notifications.badge'); });
            Route::get('/modals', function(){   return view('dashboard.notifications.modals'); });
        });
        Route::resource('notes', 'NotesController');
    });
 */
    // select 2 👇
    Route::group(['prefix' => 'select', 'middleware' => 'auth'], function () {
        Route::get('category', 'SelectController@category');
        Route::get('customer', 'SelectController@customer');
        Route::get('sales', 'SelectController@sales');
        Route::get('supplier', 'SelectController@supplier');
        Route::get('po-supplier', 'SelectController@poSupplier');
        Route::get('item', 'SelectController@item');
        Route::get('tax/{type?}', 'SelectController@tax');
        Route::get('discount', 'SelectController@discount');
        Route::get('booking-confirmation', 'SelectController@bcCustomer');
        Route::get('group-account', 'SelectController@groupAccount');
        Route::get('account', 'SelectController@account');
        Route::get('contacts', 'SelectController@contacts');
        Route::get('clasifications', 'SelectController@clasifications');
        Route::get('qtt-customer', 'SelectController@qttCustomer');
        Route::get('qtt-supplier', 'SelectController@qttSupplier');
        Route::get('inv-customer', 'SelectController@invCustomer');
        Route::get('inv-customer-payment', 'SelectController@invCustomerPayment');

        Route::get('inv-supplier', 'SelectController@invSupplier');
        Route::get('inv-supplier-payment', 'SelectController@invSupplierPayment');
        Route::get('inv-supplier-debitnote', 'SelectController@invSupplierDebitNote');

        Route::get('province', 'SelectController@province');
        Route::get('regency', 'SelectController@regency');
        Route::get('district', 'SelectController@district');
        Route::get('village', 'SelectController@village');

        Route::get('customer-qtt-items/{id}', 'SelectController@customerQuotationItems');
        Route::get('supplier-po-items/{id}', 'SelectController@supplierPoItems');
        Route::get('customer-bc-detail/{id}', 'SelectController@customerBcDetails');
        Route::get('customer-qtt-detail/{id}', 'SelectController@customerQttDetails');
        Route::get('supplier-qtt-detail/{id}', 'SelectController@supplierQttDetails');
    });

    Route::group(['prefix' => 'customer', 'middleware' => ['company.valid', 'get.menu', 'role:superadmin|admin_customer']], function () {
        if (Auth::check()) {
            $lang = Auth::user()->language;
            App::setLocale($lang);
        }
        Route::get('list-customer', 'admin\CustomerController@listCustomer');
        Route::get('table-customer', 'admin\CustomerController@tableCustomer');
        Route::get('create-customer', 'admin\CustomerController@createCustomer');
        Route::get('edit-customer/{customer_id}', 'admin\CustomerController@editCustomer');
        Route::get('show-customer/{customer_id}', 'admin\CustomerController@showCustomer');
        Route::post('store-customer', 'admin\CustomerController@storeCustomer')->name('customer.store');
        Route::put('update-customer', 'admin\CustomerController@updateCustomer')->name('customer.update');
        Route::delete('delete-customer', 'admin\CustomerController@deleteCustomer')->name('customer.delete');

        Route::get('list-quotation', 'admin\CustomerController@listQuotation');
        Route::get('table-quotation', 'admin\CustomerController@tableQuotation');
        Route::get('create-quotation', 'admin\CustomerController@createQuotation');
        Route::post('store-quotation', 'admin\CustomerController@storeQuotation')->name('customer.qotation.store');
        Route::get('edit-quotation/{uuid}', 'admin\CustomerController@editQuotation');
        Route::get('show-quotation/{quotation_id}', 'admin\CustomerController@showQuotation');
        Route::put('update-quotation', 'admin\CustomerController@updateQuotation')->name('customer.quotation.update');
        Route::delete('delete-quotation', 'admin\CustomerController@deleteQuotation')->name('customer.quotation.delete');
        Route::get('export-quotation/{start}/{to}', 'admin\CustomerController@exportQutation');
        Route::get('print-quotation/{uuid}', 'admin\CustomerController@printQuotation');

        Route::get('purchase-order', 'admin\CustomerController@listPo');
        Route::get('table-purchase-order', 'admin\CustomerController@tablePo');
        Route::get('create-purchase-order', 'admin\CustomerController@createPo');
        Route::get('show-purchase-order/{uuid}', 'admin\CustomerController@showPo');
        Route::post('store-purchase-order', 'admin\CustomerController@storePo')->name('customer.po.store');
        Route::post('update-purchase-order', 'admin\CustomerController@updatePo')->name('customer.po.update');

        Route::get('booking-confirmation', 'admin\CustomerController@listBc');
        Route::get('table-booking-confirmation', 'admin\CustomerController@tableBc');
        Route::get('create-booking-confirmation', 'admin\CustomerController@createBc');
        Route::post('store-booking-confirmation', 'admin\CustomerController@storeBc')->name('customer.bc.store');
        Route::get('show-booking-confirmation/{uuid}', 'admin\CustomerController@showBc');
        Route::put('update-booking-confirmation', 'admin\CustomerController@updateBc')->name('customer.bc.update');
        Route::patch('cancel-booking-confirmation', 'admin\CustomerController@cancelBc')->name('customer.bc.cancel');
        Route::get('print-booking-confirmation/{id}', 'admin\CustomerController@printBc');

        Route::get('invoice', 'admin\CustomerController@listInvoice');
        Route::get('table-invoice', 'admin\CustomerController@tableInvoice');
        Route::get('create-invoice', 'admin\CustomerController@createInvoice');
        Route::get('show-invoice/{uuid}', 'admin\CustomerController@showInvoice');
        Route::post('store-invoice', 'admin\CustomerController@storeInvoice')->name('customer.invoice.store');
        Route::put('update-invoice', 'admin\CustomerController@updateInvoice')->name('customer.invoice.update');
        Route::patch('void-invoice', 'admin\CustomerController@voidInvoice')->name('customer.invoice.void');
        Route::get('print-invoice/{uuid}', 'admin\CustomerController@printInvoice');


        Route::get('credit-note', 'admin\CustomerController@listCreditNote');
        Route::get('table-credit-note', 'admin\CustomerController@tableCreditNote');
        Route::get('create-credit-note', 'admin\CustomerController@createCreditNote');
        Route::get('credit-note/{uuid}/show', 'admin\CustomerController@showCreditNote');
        Route::post('store-credit-note', 'admin\CustomerController@storeCreditNote')->name('customer.credit.note.store');
        Route::put('update-credit-note', 'admin\CustomerController@updateCreditNote')->name('customer.credit.note.update');
        Route::delete('delete-credit-note', 'admin\CustomerController@deleteCreditNote')->name('customer.credit-note.delete');
        Route::get('print-credit-note/{uuid}', 'admin\CustomerController@printCreditNote');

        Route::get('debit-note', 'admin\CustomerController@listDebitNote');
        Route::get('debit-note/{uuid}/show', 'admin\CustomerController@showDebitNote');
        Route::get('table-debit-note', 'admin\CustomerController@tableDebitNote');
        Route::get('create-debit-note', 'admin\CustomerController@createDebitNote');
        Route::post('store-debit-note', 'admin\CustomerController@storeDebitNote')->name('customer.debit.note.store');
        Route::get('print-debit-note/{id}', 'admin\CustomerController@printDebitNote');
        Route::delete('delete-debit-note', 'admin\CustomerController@deleteDebitNote')->name('customer.debit-note.delete');
        Route::post('update-debit-note', 'admin\CustomerController@updateDebitNote')->name('customer.debit.note.update');


        Route::get('seller', 'admin\CustomerController@seller');
        Route::get('table-seller', 'admin\CustomerController@tableSeller');
        Route::post('store-seller', 'admin\CustomerController@storeSeller');
        Route::get('show-seller/{id}', 'admin\CustomerController@showSeller');
        Route::delete('delete-seller', 'admin\CustomerController@deleteSeller');
    });

    Route::group(['prefix' => 'supplier', 'middleware' => ['company.valid', 'get.menu', 'role:superadmin|admin_supplier']], function () {
        if (Auth::check()) {
            $lang = Auth::user()->language;
            App::setLocale($lang);
        }
        Route::get('list-supplier', 'admin\SupplierController@listSupplier');
        Route::get('table-supplier', 'admin\SupplierController@tableSupplier');
        Route::get('create-supplier', 'admin\SupplierController@createSupplier');
        Route::get('edit-supplier/{supplier_id}', 'admin\SupplierController@editSupplier');
        Route::get('show-supplier/{supplier_id}', 'admin\SupplierController@showSupplier');
        Route::post('store-supplier', 'admin\SupplierController@storeSupplier')->name('supplier.store');
        Route::put('update-supplier', 'admin\SupplierController@updateSupplier')->name('supplier.update');
        Route::delete('delete-supplier', 'admin\SupplierController@deleteSupplier')->name('supplier.delete');

        Route::get('list-quotation', 'admin\SupplierController@listQuotation');
        Route::get('table-quotation', 'admin\SupplierController@tableQuotation');
        Route::get('create-quotation', 'admin\SupplierController@createQuotation');
        Route::post('store-quotation', 'admin\SupplierController@storeQuotation')->name('supplier.qotation.store');
        Route::get('edit-quotation/{quotation_id}', 'admin\SupplierController@editQuotation');
        Route::get('show-quotation/{supplier}', 'admin\SupplierController@showQuotation');
        Route::put('update-quotation', 'admin\SupplierController@updateQuotation')->name('supplier.quotation.update');
        Route::delete('delete-quotation', 'admin\SupplierController@deleteQuotation')->name('supplier.quotation.delete');
        Route::get('export-quotation/{start}/{to}', 'admin\SupplierController@exportQutation');
        Route::get('print-quotation/{uuid}', 'admin\SupplierController@printQuotation');

        Route::get('purchase-order', 'admin\SupplierController@listPo');
        Route::get('table-purchase-order', 'admin\SupplierController@tablePo');
        Route::get('create-purchase-order', 'admin\SupplierController@createPo');
        Route::get('show-purchase-order/{uuid}', 'admin\SupplierController@showPo');
        Route::delete('delete-purchase-order', 'admin\SupplierController@deletePo')->name('supplier.po.delete');
        Route::post('store-purchase-order', 'admin\SupplierController@storePo')->name('supplier.po.store');
        Route::post('update-purchase-order', 'admin\SupplierController@updatePo')->name('supplier.po.update');
        Route::get('print-purchase-order/{uuid}', 'admin\SupplierController@printPo');

        Route::get('invoice', 'admin\SupplierController@listInvoice');
        Route::get('table-invoice', 'admin\SupplierController@tableInvoice');
        Route::get('create-invoice', 'admin\SupplierController@createInvoice');
        Route::get('show-invoice/{uuid}', 'admin\SupplierController@showInvoice');
        Route::post('store-invoice', 'admin\SupplierController@storeInvoice')->name('supplier.invoice.store');
        Route::put('update-invoice', 'admin\SupplierController@updateInvoice')->name('supplier.invoice.update');
        Route::delete('delete-invoice', 'admin\SupplierController@deleteInvoice')->name('supplier.invoice.delete');
        Route::get('print-invoice/{id}', 'admin\SupplierController@printInvoice');


        Route::get('credit-note', 'admin\SupplierController@listCreditNote');
        Route::get('table-credit-note', 'admin\SupplierController@tableCreditNote');
        Route::get('create-credit-note', 'admin\SupplierController@createCreditNote');
        Route::get('print-credit-note/{id}', 'admin\SupplierController@printCreditNote');
        Route::post('store-credit-note', 'admin\SupplierController@storeCreditNote')->name('supplier.credit.note.store');
        Route::delete('delete-credit-note', 'admin\SupplierController@deleteCreditNote')->name('supplier.credit-note.delete');
        Route::get('credit-note/{uuid}/show', 'admin\SupplierController@showCreditNote');
        Route::put('update-credit-note', 'admin\SupplierController@updateCreditNote')->name('supplier.credit.note.update');


        Route::get('debit-note', 'admin\SupplierController@listDebitNote');
        Route::get('table-debit-note', 'admin\SupplierController@tableDebitNote');
        Route::get('create-debit-note', 'admin\SupplierController@createDebitNote');
        Route::get('debit-note/{uuid}/show', 'admin\SupplierController@showDebitNote');
        Route::get('allocate-credit/{uuid?}', 'admin\SupplierController@createAllocation');
        Route::get('list-allocate-credit/{uuid}', 'admin\SupplierController@listAllocation');
        Route::post('store-allocate-credit', 'admin\SupplierController@storeAllocation');
        Route::post('update-debit-note', 'admin\SupplierController@updateDebitNote')->name('supplier.debit.note.update');
        Route::post('store-debit-note', 'admin\SupplierController@storeDebitNote')->name('supplier.debit.note.store');
        Route::delete('delete-debit-note', 'admin\SupplierController@deleteDebitNote')->name('supplier.debit-note.delete');

    });

    Route::group(['prefix' => 'tax', 'middleware' => ['company.valid', 'get.menu', 'role:superadmin|admin_pajak']], function(){
        if (Auth::check()) {
            $lang = Auth::user()->language;
            App::setLocale($lang);
        }
        Route::get('/', 'admin\TaxController@index');
        Route::get('table', 'admin\TaxController@table');
        Route::get('show/{tax_id}', 'admin\TaxController@show');
        Route::post('store', 'admin\TaxController@store');
        Route::delete('delete', 'admin\TaxController@delete')->name('tax.delete');
    });

    Route::group(['prefix' => 'discount', 'middleware' => ['company.valid', 'get.menu', 'role:superadmin|admin_diskon']], function () {
        if (Auth::check()) {
            $lang = Auth::user()->language;
            App::setLocale($lang);
        }
        Route::get('/', 'admin\DiscountController@index');
        Route::get('table', 'admin\DiscountController@table');
        Route::get('show/{discount_id}', 'admin\DiscountController@show');
        Route::post('store', 'admin\DiscountController@store');
        Route::delete('delete', 'admin\DiscountController@delete')->name('discount.delete');
    });

    Route::group(['prefix' => 'setting', 'middleware' => ['company.valid', 'get.menu', 'role:superadmin|admin_setting']], function(){
        if (Auth::check()) {
            $lang = Auth::user()->language;
            App::setLocale($lang);
        }
        Route::get('/', 'admin\SettingController@index');
        Route::post('/save', 'admin\SettingController@edit')->name('setting.config');
    });

    Route::group(['prefix' => 'item-list', 'middleware' => ['company.valid', 'get.menu', 'role:superadmin|admin_item_list']], function(){
        if (Auth::check()) {
            $lang = Auth::user()->language;
            App::setLocale($lang);
        }

        Route::get('/', 'admin\ItemListController@index');
        Route::get('table', 'admin\ItemListController@table');
        Route::get('show/{uuid}', 'admin\ItemListController@show')->name('item-list.show');
        Route::get('add-form', 'admin\ItemListController@addForm');
        Route::get('edit/{uuid}', 'admin\ItemListController@editForm')->name('item-list.edit');
        Route::put('update', 'admin\ItemListController@update')->name('item-list.update');
        Route::post('create', 'admin\ItemListController@create')->name('item-list.create');
        Route::delete('delete', 'admin\ItemListController@delete')->name('item-list.delete');
    });

    Route::group(['prefix' => 'report', 'middleware' => ['company.valid', 'get.menu', 'role:superadmin|admin_laporan']], function(){

        Route::group(['prefix' => 'selling'], function(){
            Route::get('/', 'admin\Report\SellingController@index');
            Route::get('invoice', 'admin\Report\SellingController@listInvoice');
            Route::get('invoice/print', 'admin\Report\SellingController@printInvoice');
            Route::get('invoice/table', 'admin\Report\SellingController@tableInvoice');

            Route::get('booking-confirmation', 'admin\Report\SellingController@listBc');
            Route::get('booking-confirmation/table', 'admin\Report\SellingController@tableBc');
            Route::get('booking-confirmation/print', 'admin\Report\SellingController@printBc');

            Route::get('age-of-receivable', 'admin\Report\SellingController@listReceivable');
            Route::get('age-of-receivable/table', 'admin\Report\SellingController@tableReceivable');
            Route::get('age-of-receivable/print', 'admin\Report\SellingController@printReceivable');

        });

        Route::group(['prefix' => 'purchasing'], function () {
            Route::get('/', 'admin\Report\PurchasingController@index');
            Route::get('invoice', 'admin\Report\PurchasingController@listInvoice');
            Route::get('invoice/table', 'admin\Report\PurchasingController@tableInvoice');
            Route::get('invoice/print', 'admin\Report\PurchasingController@printInvoice');

            Route::get('purchase-order', 'admin\Report\PurchasingController@listPo');
            Route::get('purchase-order/table', 'admin\Report\PurchasingController@tablePo');
            Route::get('purchase-order/print', 'admin\Report\PurchasingController@printPo');

            Route::get('age-of-payable', 'admin\Report\PurchasingController@listPayable');
            Route::get('age-of-payable/table', 'admin\Report\PurchasingController@tablePayable');
            Route::get('age-of-payable/print', 'admin\Report\PurchasingController@printPayable');

        });

        Route::group(['prefix' => 'ledger'], function () {
            Route::get('/', 'admin\Report\LedgerController@index');
            Route::get('table/recap', 'admin\Report\LedgerController@tableRecap');
            Route::get('table/details', 'admin\Report\LedgerController@tableDetails');
            Route::get('print/recap', 'admin\Report\LedgerController@printRecap');
            Route::get('print/details', 'admin\Report\LedgerController@printDetails');
        });

        Route::group(['prefix' => 'profit-and-loss'], function () {
            Route::get('/', 'admin\Report\ProfitLossController@index');
            Route::get('lists', 'admin\Report\ProfitLossController@getProfitLoss');
        });

        Route::group(['prefix' => 'equity'], function () {
            Route::get('/', 'admin\Report\EquityController@index');
            Route::get('lists', 'admin\Report\EquityController@getEquity');
            Route::get('print', 'admin\Report\EquityController@printEquity');
        });

        Route::group(['prefix' => 'balance'], function () {
            Route::get('/', 'admin\Report\BalanceController@index');
            Route::get('lists', 'admin\Report\BalanceController@getBalance');
            Route::get('print', 'admin\Report\BalanceController@printBalance');
        });

        Route::group(['prefix' => 'cash-flow'], function () {
            Route::get('/', 'admin\Report\CashflowController@index');
            Route::get('lists', 'admin\Report\CashflowController@getCashflow');
            Route::get('print', 'admin\Report\CashflowController@printCashflow');
        });
    });

    Route::group(['prefix' => 'category', 'middleware' => ['company.valid', 'get.menu', 'role:superadmin|admin_item_list']], function(){
        if (Auth::check()) {
            $lang = Auth::user()->language;
            App::setLocale($lang);
        }
        Route::post('create', 'admin\CategoryController@create');
    });

    Route::group(['prefix' => 'accounting', 'middleware' => ['company.valid', 'get.menu', 'role:superadmin|admin_accounting']], function () {
        if (Auth::check()) {
            $lang = Auth::user()->language;
            App::setLocale($lang);
        }
        Route::get('coa', 'admin\AccountingController@coa');
        Route::get('coa/list', 'admin\AccountingController@getCoa');
        Route::post('coa/store', 'admin\AccountingController@storeCoa');
        Route::delete('coa/{id}/delete', 'admin\AccountingController@deleteCoa');
        Route::patch('coa/{id}/up/{serial}', 'admin\AccountingController@climbUpCoa');
        Route::patch('coa/{id}/down/{serial}', 'admin\AccountingController@climbDownCoa');

        Route::get('journal', 'admin\AccountingController@journal');
        Route::get('table-journal', 'admin\AccountingController@tableJournal');
        Route::get('create-journal', 'admin\AccountingController@createJournal');
        Route::post('store-journal', 'admin\AccountingController@storeJournal');
        Route::delete('delete-journal', 'admin\AccountingController@deleteJournal');
        Route::get('journal/{uuid}/show', 'admin\AccountingController@showJournal');
        Route::get('journal/{uuid}/print', 'admin\AccountingController@printJournal');
        Route::get('last-journal', 'admin\AccountingController@lastJournal');

        // sales payment
        Route::get('sales-payment', 'admin\AccountingController@salesPayment');
        Route::get('sales-payment/{uuid}/show', 'admin\AccountingController@salesShowPayment');
        Route::get('sales-table-payment', 'admin\AccountingController@salesTablePayment');
        Route::get('sales-create-payment/{uuid?}', 'admin\AccountingController@salesCreatePayment');
        Route::post('sales-store-payment', 'admin\AccountingController@salesStorePayment');
        Route::patch('sales-void-payment', 'admin\AccountingController@salesVoidPayment');
        Route::get('sales-void-payment', 'admin\AccountingController@salesVoidPaymentInfo');
        // purchase payment
        Route::get('purchase-payment', 'admin\AccountingController@purchasePayment');
        Route::get('purchase-payment/{uuid}/show', 'admin\AccountingController@purchaseShowPayment');
        Route::get('purchase-table-payment', 'admin\AccountingController@purchaseTablePayment');
        Route::get('purchase-create-payment/{uuid?}', 'admin\AccountingController@purchaseCreatePayment');
        Route::post('purchase-store-payment', 'admin\AccountingController@purchaseStorePayment');
        Route::patch('purchase-void-payment', 'admin\AccountingController@purchaseVoidPayment');
        Route::get('purchase-void-payment', 'admin\AccountingController@purchaseVoidPaymentInfo');

        Route::get('setting', 'admin\AccountingController@setting');
        Route::post('setting', 'admin\AccountingController@storeSetting');

        Route::group(['prefix' => 'setting-type-asset'], function () {
            Route::get('/', 'admin\AccountingAssetTypeController@settingAssetType');
            Route::get('table', 'admin\AccountingAssetTypeController@tableAssetType');
            Route::post('store', 'admin\AccountingAssetTypeController@tableAssetStore');
            Route::get('show/{uuid}', 'admin\AccountingAssetTypeController@showAssetType');
            Route::delete('delete', 'admin\AccountingAssetTypeController@deleteAssetType');
        });

        Route::group(['prefix' => 'asset'], function () {
            Route::get('/', 'admin\AccountingAssetController@index');
            Route::get('table', 'admin\AccountingAssetController@tableAsset');
            Route::get('create', 'admin\AccountingAssetController@createAsset');
            Route::post('store', 'admin\AccountingAssetController@storeAsset');
            Route::get('edit/{uuid}', 'admin\AccountingAssetController@editAsset');
            Route::get('print/{status}', 'admin\AccountingAssetController@printAsset');

            Route::get('asset-type', 'admin\AccountingAssetController@getAssetType');
            Route::get('clasification', 'admin\AccountingAssetController@getClasification');

            Route::get('calculate-depreciation', 'admin\AccountingAssetController@listDepreciation');
            Route::get('calculate-depreciation/table', 'admin\AccountingAssetController@tableDepreciation');
            Route::post('calculate-depreciation/store', 'admin\AccountingAssetController@storeDepreciation');
            Route::get('last-depreciation', 'admin\AccountingAssetController@lastDepreciation');
            Route::get('list-depreciation/{uuid}', 'admin\AccountingAssetController@itemsDepreciation');
        });
    });

    Route::group(['middleware' => ['company.valid', 'role:superadmin', 'get.menu']], function () {
        if (Auth::check()) {
            $lang = Auth::user()->language;
            App::setLocale($lang);
        }

        Route::group(['prefix' => 'admin', 'middleware' => 'get.menu', 'role:superadmin'], function () {
            Route::get('/', 'admin\AdminsController@index');
            Route::get('table', 'admin\AdminsController@table');
            Route::get('add-form', 'admin\AdminsController@addForm');
            Route::post('create', 'admin\AdminsController@create');
            Route::patch('status', 'admin\AdminsController@status');
            Route::get('show/{uuid}', 'admin\AdminsController@show');
            Route::put('update/{uuid}', 'admin\AdminsController@update');
            Route::delete('delete', 'admin\AdminsController@delete');
        });


        Route::resource('users',        'UsersController')->except(['create', 'store']);
        Route::resource('roles',        'RolesController');
        Route::get('/roles/move/move-up',      'RolesController@moveUp')->name('roles.up');
        Route::get('/roles/move/move-down',    'RolesController@moveDown')->name('roles.down');
        Route::prefix('menu/element')->group(function () {
            Route::get('/',             'MenuElementController@index')->name('menu.index');
            Route::get('/move-up',      'MenuElementController@moveUp')->name('menu.up');
            Route::get('/move-down',    'MenuElementController@moveDown')->name('menu.down');
            Route::get('/create',       'MenuElementController@create')->name('menu.create');
            Route::post('/store',       'MenuElementController@store')->name('menu.store');
            Route::get('/get-parents',  'MenuElementController@getParents');
            Route::get('/edit',         'MenuElementController@edit')->name('menu.edit');
            Route::post('/update',      'MenuElementController@update')->name('menu.update');
            Route::get('/show',         'MenuElementController@show')->name('menu.show');
            Route::get('/delete',       'MenuElementController@delete')->name('menu.delete');
        });
        Route::prefix('menu/menu')->group(function () {
            Route::get('/',         'MenuController@index')->name('menu.menu.index');
            Route::get('/create',   'MenuController@create')->name('menu.menu.create');
            Route::post('/store',   'MenuController@store')->name('menu.menu.store');
            Route::get('/edit',     'MenuController@edit')->name('menu.menu.edit');
            Route::post('/update',  'MenuController@update')->name('menu.menu.update');
            Route::get('/delete',   'MenuController@delete')->name('menu.menu.delete');
        });
        Route::prefix('media')->group(function () {
            Route::get('/',                 'MediaController@index')->name('media.folder.index');
            Route::get('/folder/store',     'MediaController@folderAdd')->name('media.folder.add');
            Route::post('/folder/update',   'MediaController@folderUpdate')->name('media.folder.update');
            Route::get('/folder',           'MediaController@folder')->name('media.folder');
            Route::post('/folder/move',     'MediaController@folderMove')->name('media.folder.move');
            Route::post('/folder/delete',   'MediaController@folderDelete')->name('media.folder.delete');;

            Route::post('/file/store',      'MediaController@fileAdd')->name('media.file.add');
            Route::get('/file',             'MediaController@file');
            Route::post('/file/delete',      'MediaController@fileDelete')->name('media.file.delete');
            Route::post('/file/update',     'MediaController@fileUpdate')->name('media.file.update');
            Route::post('/file/move',       'MediaController@fileMove')->name('media.file.move');
            Route::post('/file/cropp',      'MediaController@cropp');
            Route::get('/file/copy',        'MediaController@fileCopy')->name('media.file.copy');
        });
    });
});
