<?php

namespace App\Models;

use App\Scopes\CompanySupplierScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseOrderSupplier extends Model
{
    use SoftDeletes;
    protected $appends = ['total', 'serializing'];
    protected $primaryKey = 'po_id';
    protected $fillable = [
        'po_id',
        'supplier_id',
        'quotation_id',
        'serial',
        'uuid',
        'status',
        'term_and_condition',
        'description',
        'file',
        'currency',
        'rate',
        'rate_tax',
        'po_date',
        'shipping_date'
    ];
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new CompanySupplierScope((new self())->getTable()));
    }
    public function getSerializingAttribute()
    {
        return date('y', strtotime($this->po_date)) .str_pad($this->serial, 4, '0', STR_PAD_LEFT);
    }
    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id', 'supplier_id');
    }
    public function quotation()
    {
        return $this->belongsTo(QuotationSupplier::class, 'quotation_id', 'quotation_id');
    }
    public function invoice()
    {
        return $this->hasMany(InvoiceSupplier::class, 'po_id', 'po_id');
    }
    public function list_items()
    {
        return $this->hasMany(PoSupplierItem::class, 'po_id', 'po_id');
    }
    public function taxes()
    {
        return $this->hasManyThrough(Tax::class, PoTaxSupplier::class, 'po_id', 'tax_id', 'po_id', 'tax_id');
    }
    // public function tax()
    // {
    //     return $this->belongsTo(Tax::class, 'tax', 'tax_id');
    // }
    public function discount()
    {
        return $this->belongsTo(Discount::class, 'discount', 'discount_id');
    }
    public function getTotalAttribute()
    {
        $tax   = $this->taxes()->get()->sum('tax_amount');
        $items = $this->list_items()->get()->sum(function($query) {
            return $query->price*$query->quantity;
        });
        if($tax && $this->currency == 1){
            $kenaPajak = $items*($tax/100);
            $items+=$kenaPajak;
        }
        // yg kurs belum
        // elseif($tax && $this->currency > 1){
        //     $kenaPajak = $items*($tax/100);

        // }

        return $items;
    }
}
