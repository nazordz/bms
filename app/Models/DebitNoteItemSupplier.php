<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DebitNoteItemSupplier extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'debit_note_id', 'item_id', 'item_name', 'description', 'price', 'quantity', 'discount', 'subtotal'
    ];
}
