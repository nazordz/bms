<?php

namespace App\Models;

use App\Scopes\CompanyCustomerScope;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceCustomer extends Model
{
    use SoftDeletes;
    protected $primaryKey = 'invoice_id';
    protected $appends = ['paidAmount', 'total_invoice', 'serializing'];
    protected $fillable = [
        'invoice_id',
        'bc_id',
        'quotation_id',
        'customer_id',
        'serial',
        'uuid',
        'status',
        'term_and_condition',
        'term_of_payment',
        'payment',
        'currency',
        'rate',
        'rate_tax',
        'attn',
        'vessel',
        'voyage',
        'eta',
        'bl_no',
        'from',
        'to',
        'description',
        'tax',
        'discount',
        'due_date',
        'file',
        'void_note',
        'void_at',
        'created_time',
        'created_at',
        'payment_date',
        'payment_ref',
        'account_id',
        'invoice_date',
        'is_invoice'
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new CompanyCustomerScope((new self())->getTable()));
    }

    public function getSerializingAttribute()
    {
        return date('y', strtotime($this->invoice_date)) .str_pad($this->serial, 4, '0', STR_PAD_LEFT);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'customer_id');
    }
    public function paymentInvoice()
    {
        return $this->hasMany(PaymentCustomer::class, 'invoice_id', 'invoice_id');
    }
    public function getPaidAmountAttribute()
    {
        // $payments = $this->paymentInvoice()->get();
        $payment_amount = $this->paymentInvoice()->get()->sum(function($paid){
            if ($paid['status']) {
                if($paid->currency != 1){
                    return $paid['payment_amount']/$paid['rate'];
                }else{
                    return $paid['payment_amount'];
                }
            }else{
                return 0;
            }
        });
        return $payment_amount;
    }
    public function getTotalInvoiceAttribute()
    {
        $amount = $this->list_items()->get()->sum(function($query){
            return $query->price * $query->quantity;
        });
        $tax = $this->taxes()->get()->sum('tax_amount');
        if($tax){
            $amount += $amount*($tax/100);
        }
        return $amount;
    }
    public function booking_confirmation()
    {
        return $this->belongsTo(BcCustomer::class, 'bc_id', 'bc_id');
    }
    public function quotation()
    {
        return $this->belongsTo(QuotationCustomer::class, 'quotation_id', 'quotation_id');
    }
    public function list_items()
    {
        return $this->hasMany(InvoiceCustomerItem::class, 'invoice_id', 'invoice_id');
    }

    public function taxing()
    {
        return $this->hasMany(InvoiceTaxCustomer::class, 'id', 'invoice_id');
    }

    public function taxes()
    {
        return $this->hasManyThrough(Tax::class, InvoiceTaxCustomer::class,'invoice_id', 'tax_id', 'invoice_id', 'tax_id');
    }
    public function discount()
    {
        return $this->belongsTo(Discount::class, 'discount', 'discount_id');
    }

    public function void()
    {
        return $this->belongsTo(User::class, 'void_by', 'id');
    }

}
