<?php

namespace App\Models;

use App\Scopes\CompanyScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Tax extends Model
{
    use SoftDeletes;

    public $incrementing = false;
    protected $keyType = 'string';
    protected $table = 'tax';
    protected $primaryKey = 'tax_id';
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new CompanyScope);
        static::creating(function($tax){
            $tax->company_id = session()->get('company_id');
        });
    }
    protected $fillable = [
        'tax_id', 'tax_amount', 'type', 'description', 'payable_account_id', 'receivable_account_id'
    ];

    public function payable_account()
    {
        return $this->belongsTo(ChartAccount::class, "payable_account_id", 'id');
    }
    public function receivable_account()
    {
        return $this->belongsTo(ChartAccount::class, "receivable_account_id", 'id');
    }
}
