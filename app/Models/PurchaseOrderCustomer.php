<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrderCustomer extends Model
{
    protected $primaryKey = 'po_id';
    protected $keyType = 'string';
    protected $fillable = [
        'po_id', 'customer_id', 'uuid', 'status', 'term_and_condition', 'tax', 'discount', 'file', 'quotation_id'
    ];
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'customer_id');
    }

    public function quotation()
    {
        return $this->belongsTo(QuotationCustomer::class, 'quotation_id', 'quotation_id');
    }

    public function invoice()
    {
        return $this->hasMany(InvoiceCustomer::class, 'po_id', 'po_id');
    }

    public function list_items()
    {
        return $this->hasMany(PoCustomerItem::class, 'po_id', 'po_id');
    }
    public function tax()
    {
        return $this->belongsTo(Tax::class, 'tax', 'tax_id');
    }
    public function discount()
    {
        return $this->belongsTo(Discount::class, 'discount', 'discount_id');
    }

}
