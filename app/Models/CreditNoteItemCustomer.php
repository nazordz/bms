<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CreditNoteItemCustomer extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'credit_note_id', 'item_id', 'item_name', 'description', 'price', 'quantity', 'discount', 'subtotal'
    ];
}
