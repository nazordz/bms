<?php

namespace App\Models;

use App\Scopes\CompanyScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Seller extends Model
{
    protected $fillable = ['name', 'company_id'];
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new CompanyScope);
        static::creating(function($seller){
            $seller->company_id = session()->get('company_id');
        });
    }
}
