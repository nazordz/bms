<?php

namespace App\Models;

use App\Scopes\CompanySupplierScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreditNoteSupplier extends Model
{
    use SoftDeletes;
    protected $appends = ['serializing'];
    protected $fillable = [
        'serial',
        'uuid',
        'supplier_id',
        'invoice_id',
        'seller_id',
        'currency',
        'rate',
        'rate_tax',
        'total',
        'note',
        'proof_picture',
        'status',
        'paid_from_account'
    ];
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new CompanySupplierScope((new self())->getTable()));
    }
    public function getSerializingAttribute()
    {
        return date('y', strtotime($this->created_at)) .str_pad($this->serial, 4, '0', STR_PAD_LEFT);
    }
    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id', 'supplier_id');
    }

    public function invoice()
    {
        return $this->belongsTo(InvoiceSupplier::class, 'invoice_id', 'invoice_id');
    }

    public function list_items()
    {
        return $this->hasMany(CreditNoteItemSupplier::class, 'credit_note_id', 'id');
    }

    public function seller()
    {
        return $this->belongsTo(Seller::class, 'seller_id', 'id');
    }

    public function tax()
    {
        return $this->hasMany(CreditNoteTaxSupplier::class, 'credit_note_id');
    }
    public function taxing()
    {
        return $this->hasManyThrough(Tax::class, CreditNoteTaxSupplier::class,'credit_note_id', 'tax_id', 'id', 'tax_id');
    }
    public function paid_account()
    {
        return $this->belongsTo(ChartAccount::class, 'paid_from_account');
    }
}
