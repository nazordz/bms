<?php

namespace App\Models;

use App\Scopes\CompanyScope;
use App\User;
use Illuminate\Database\Eloquent\Model;

class PaymentSupplier extends Model
{
    protected $fillable = [
        'uuid',
        'serial',
        'invoice_id',
        'remark',
        'currency',
        'rate',
        'payment_amout',
        'discount_type',
        'discount_nominal',
        'status',
        'user_id',
        'company_id',
        'tax_id',
        'payment_date',
        'enter_to_account',
        'void_note',
        'void_by',
        'void_at'
    ];

    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new CompanyScope);
        self::creating(function($payment){
            $payment->company_id = session()->get('company_id');
        });
    }

    public function invoice()
    {
        return $this->belongsTo(InvoiceCustomer::class, 'invoice_id', 'invoice_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function void_by()
    {
        return $this->belongsTo(User::class, 'void_by', 'id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function tax()
    {
        return $this->belongsTo(Tax::class, 'tax_id', 'tax_id');
    }

}
