<?php

namespace App\Models;

use App\Scopes\CompanyCustomerScope;
use Illuminate\Database\Eloquent\Model;

class BcCustomer extends Model
{
    protected $primaryKey = 'bc_id';
    protected $appends = ['booking_reference', 'serializing'];
    protected $fillable = [
        'customer_id', 'quotation_id', 'serial', 'uuid', 'shipper', 'consignee', 'booking_ref' ,'voyage',  'port_of_landing',  'port_of_discharge',  'etd_port_of_loading',  'eta_port_of_discharge',  'sin_permit',  'batam_permit',   'total',  'special_remark', 'file', 'status'
    ];

    // Scope
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new CompanyCustomerScope((new self())->getTable()));
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'customer_id');
    }

    public function item()
    {
        return $this->hasMany(BcCustomerItem::class, 'bc_id', 'bc_id');
    }

    public function quotation()
    {
        return $this->belongsTo(QuotationCustomer::class, 'quotation_id', 'quotation_id');
    }

    // accessors
    public function getBookingReferenceAttribute(){
        switch ($this->booking_ref) {
            case 1:
                $ref = "BTMSIN";
                break;
            case 2:
                $ref = "SINBTM";
                break;
            case 3:
                $ref = "BTMIDN";
                break;
            case 4:
                $ref = "IDNBTM";
                break;
            case 5:
                $ref = "BTMTF";
                break;
            case 6:
                $ref = "TFBTM";
                break;

            default:
                $ref = "";
                break;
        }
        return $ref;
    }
    public function getSerializingAttribute()
    {
        return $this->booking_ref . date('y', strtotime($this->created_at)) .str_pad($this->serial, 4, '0', STR_PAD_LEFT);
    }
    // public function getShipperAttribute()
    // {
    //     switch ($this->booking_ref) {
    //         case 'BTMSIN':
    //             $ref = "BATAM";
    //             break;
    //         case 'SINBTM':
    //             $ref = "SINGAPORE";
    //             break;
    //         case 'BTMIDN':
    //             $ref = "BATAM";
    //             break;
    //         case 'IDNBTM':
    //             $ref = "INDONESIA";
    //             break;
    //         case 'BTMTF':
    //             $ref = "BATAM";
    //             break;
    //         case 'TFBTM':
    //             $ref = "INTERNATIONAL";
    //             break;

    //         default:
    //             $ref = "";
    //             break;
    //     }
    //     return $ref;
    // }

    // public function getConsigneeAttribute()
    // {
    //     switch ($this->booking_ref) {
    //         case 'BTMSIN':
    //             $ref = "SINGAPORE";
    //             break;
    //         case 'SINBTM':
    //             $ref = "BATAM";
    //             break;
    //         case 'BTMIDN':
    //             $ref = "INDONESIA";
    //             break;
    //         case 'IDNBTM':
    //             $ref = "BATAM";
    //             break;
    //         case 'BTMTF':
    //             $ref = "INTERNATIONAL";
    //             break;
    //         case 'TFBTM':
    //             $ref = "BATAM";
    //             break;

    //         default:
    //             $ref = "";
    //             break;
    //     }
    //     return $ref;
    // }
}
