<?php

namespace App\Models;

use App\Scopes\CompanyCustomerScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreditNoteCustomer extends Model
{
    use SoftDeletes;
    public $primaryKey = 'id';
    protected $appends = ['serializing'];
    protected $fillable = [
        'uuid',
        'serial',
        'invoice_id',
        'customer_id',
        'currency',
        'rate',
        'rate_tax',
        'note',
        'seller_id',
        'total',
        'proof_picture',
        'status',
        'paid_from_account'
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new CompanyCustomerScope((new self())->getTable()));
    }

    public function getSerializingAttribute()
    {
        return date('y', strtotime($this->created_at)) .str_pad($this->serial, 4, '0', STR_PAD_LEFT);
    }

    public function invoice()
    {
        return $this->belongsTo(InvoiceCustomer::class, 'invoice_id', 'invoice_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'customer_id');
    }

    public function list_items()
    {
        return $this->hasMany(CreditNoteItemCustomer::class, 'credit_note_id', 'id');
    }

    public function seller()
    {
        return $this->belongsTo(Seller::class, 'seller_id', 'id');
    }

    public function tax()
    {
        return $this->hasMany(CreditNoteTaxCustomer::class, 'credit_note_id');
    }
    public function taxing()
    {
        return $this->hasManyThrough(Tax::class, CreditNoteTaxCustomer::class,'credit_note_id', 'tax_id', 'id', 'tax_id');
    }
    public function paid_account()
    {
        return $this->belongsTo(ChartAccount::class, 'paid_from_account');
    }

}
