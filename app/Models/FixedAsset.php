<?php

namespace App\Models;

use App\Scopes\CompanyScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FixedAsset extends Model
{
    use SoftDeletes;
    protected $appends = ['total_shrinkage'];
    protected $fillable = [
        'company_id',
        'uuid',
        'serial',
        'name',
        'type_id',
        'clasification_id',
        'date_acquistion',
        'price_acquistion',
        'serial_number',
        'depreciation_start_date',
        'residue',
        'shrinkage_method',
        'estimated_age',
        'depreciation_rates',
        'asset_account',
        'expense_account',
        'depreciation_account',
        'accumulated_depreciation_account',
        'status'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function($asset){
            $asset->company_id = session()->get('company_id');
        });
    }

    public function shrinkage()
    {
        return $this->hasMany(Shrinkage::class, 'asset_id', 'id');
    }

    public function type()
    {
        return $this->belongsTo(SettingAssetType::class, 'type_id');
    }

    public function clasification()
    {
        return $this->belongsTo(JournalClasification::class, 'clasification_id');
    }

    public function getTotalShrinkageAttribute()
    {
        $shrinkage = $this->shrinkage()->get()->sum(function($shrinkage){
            return $shrinkage->status ? $shrinkage->depreciation_value:0;
        });
        return $shrinkage;
    }

    public function account_asset()
    {
        return $this->belongsTo(ChartAccount::class, 'asset_account');
    }
    public function account_expense()
    {
        return $this->belongsTo(ChartAccount::class, 'expense_account');
    }
    public function account_depreciation()
    {
        return $this->belongsTo(ChartAccount::class, 'depreciation_account');
    }
    public function account_accumulated()
    {
        return $this->belongsTo(ChartAccount::class, 'accumulated_depreciation_account');
    }
}
