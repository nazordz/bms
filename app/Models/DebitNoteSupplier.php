<?php

namespace App\Models;

use App\Scopes\CompanySupplierScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DebitNoteSupplier extends Model
{
    use SoftDeletes;
    protected $appends = ['credit_already_allocated', 'total_debit_note', 'serializing'];
    protected $fillable = [
        'uuid',
        'serial',
        'invoice_id',
        'supplier_id',
        'currency',
        'rate',
        'rate_tax',
        'note',
        'sales',
        'total',
        'proof_picture',
        'status',
        'payment',
        'account_id',
        'debit_note_date'
    ];
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new CompanySupplierScope((new self())->getTable()));
    }
    public function getSerializingAttribute()
    {
        return date('y', strtotime($this->debit_note_date)) .str_pad($this->serial, 4, '0', STR_PAD_LEFT);
    }
    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id', 'supplier_id');
    }

    public function list_items()
    {
        return $this->hasMany(DebitNoteItemSupplier::class, 'debit_note_id', 'id');
    }

    public function invoice()
    {
        return $this->belongsTo(InvoiceSupplier::class, 'invoice_id', 'invoice_id');
    }

    public function debit_note_tax()
    {
        return $this->hasMany(DebitNoteTaxSupplier::class, 'debit_note_id');
    }
    public function allocated_credit()
    {
        return $this->hasMany(DebitNoteAllocatedCreditSupplier::class, 'debit_note_id');
    }
    public function account()
    {
        return $this->belongsTo(ChartAccount::class, 'account_id');
    }
    public function getTotalDebitNoteAttribute()
    {
        // $amount = $this->list_items()->sum(function($query){
        //     return $query->price * $query->quantity;
        // });
        $amount = $this->list_items()->sum('subtotal');
        $tax = $this->taxing()->sum('tax_amount');
        if($tax){
            $amount += $amount*($tax/100);
        }
        return $amount;
    }
    public function getCreditAlreadyAllocatedAttribute()
    {
        return $this->allocated_credit()->where(['status' => 1])->sum('payment_amount');
    }
    public function taxing()
    {
        return $this->hasManyThrough(Tax::class, DebitNoteTaxSupplier::class,'debit_note_id', 'tax_id', 'id', 'tax_id');
    }
}
