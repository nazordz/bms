<?php

namespace App\Models;

use App\Scopes\CompanyScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SettingAssetType extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'uuid',
        'serial',
        'company_id',
        'name',
        'shrinkage_method',
        'estimated_age',
        'depreciation_rates'
    ];

    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new CompanyScope);
        static::creating(function($setting){
            $setting->company_id = session()->get('company_id');
        });
    }

    public function fixed_asset()
    {
        return $this->hasMany(FixedAsset::class, 'type_id');
    }
}
