<?php

namespace App\Models;

use App\Scopes\CompanyScope;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DebitNoteAllocatedCreditSupplier extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'debit_note_id',
        'serial',
        'uuid',
        'company_id',
        'allocated_date',
        'payment_amount',
        'user_id',
        'status'
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new CompanyScope);
        static::creating(function($allocate){
            $allocate->company_id = session()->get('company_id');
        });
    }

    public function debit_note()
    {
        return $this->belongsTo(DebitNoteSupplier::class, 'debit_note_id', 'id');
    }

    public function invoice()
    {
        return $this->hasOneThrough(InvoiceSupplier::class, DebitNoteSupplier::class, 'id', 'supplier_id', 'debit_note_id', 'invoice_id');
    }

    public function journal()
    {
        return $this->hasMany(Journal::class, 'supplier_allocated_credit_id');
    }
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
