<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JournalClasification extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';
    public $timestamps = false;
    protected $fillable = [ 'id', 'name' ];

}
