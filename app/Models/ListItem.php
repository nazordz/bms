<?php

namespace App\Models;

use App\Scopes\CompanyScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class ListItem extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'item_id', 'uuid', 'name', 'category_id', 'description', 'sell_price', 'buy_price', 'unit', 'picture'
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new CompanyScope);
        static::creating(function($listItem){
            $listItem->company_id = session()->get('company_id');
        });
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
}
