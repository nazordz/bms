<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoSupplierItem extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'po_id', 'item_id', 'item_name', 'description', 'price', 'quantity', 'discount_type', 'discount', 'subtotal'
    ];
}
