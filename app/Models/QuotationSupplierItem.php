<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuotationSupplierItem extends Model
{
    public $timestamps = false;
    protected $fillable =[
        'quotation_id', 'item_id', 'item_name', 'description', 'price', 'quantity', 'discount_type', 'discount', 'subtotal'
    ];

    public function list_item()
    {
        return $this->belongsTo(ListItem::class, 'item_id', 'id');
    }

    public function quotation_customer()
    {
        return $this->belongsTo(QuotationCustomer::class, 'quotation_id', 'quotation_id');
    }
}
