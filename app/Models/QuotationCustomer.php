<?php

namespace App\Models;

use App\Observers\SerialOnTable;
use App\Scopes\CompanyCustomerScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class QuotationCustomer extends Model
{
    use SoftDeletes;
    protected $primaryKey = 'quotation_id';
    protected $appends = ['serializing'];
    protected $fillable = [
        'quotation_id',
        'customer_id',
        'uuid',
        'serial',
        'term_and_condition',
        'term_of_payment',
        'description',
        'currency',
        'rate',
        'rate_tax',
        'attn',
        'vessel',
        'voyage',
        'eta',
        'bl_no',
        'from',
        'to',
        'sales',
        'seller_id',
        'valid_until',
        'status',
        'tax',
        'discount',
        'quantity',
        'created_time',
        'quotation_date',
        'status'
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new CompanyCustomerScope(with(new QuotationCustomer)->getTable()));
        // static::addGlobalScope(new CompanyCustomerScope((new self())->getTable()));
    }

    public function getSerializingAttribute()
    {
        return date('y', strtotime($this->quotation_date)) .str_pad($this->serial, 4, '0', STR_PAD_LEFT);
    }

    public function list_items()
    {
        return $this->hasMany(QuotationCustomerItem::class, 'quotation_id', 'quotation_id');
    }

    public function purchase_order()
    {
        return $this->hasOne(PurchaseOrderCustomer::class, 'quotation_id', 'quotation_id');
    }

    public function invoice()
    {
        return $this->hasMany(InvoiceCustomer::class, 'quotation_id', 'quotation_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'customer_id');
    }

    public function seller()
    {
        return $this->belongsTo(Seller::class, 'seller_id', 'id');
    }

    public function booking_confirmation()
    {
        return $this->hasMany(BcCustomer::class, 'quotation_id', 'quotation_id');
    }
    // public function taxes()
    // {
    //     return $this->hasMany(QuotationTaxCustomer::class, 'quotation_id', 'quotation_id');
    // }

    public function taxes()
    {
        return $this->hasManyThrough(Tax::class, QuotationTaxCustomer::class, 'quotation_id', 'tax_id', 'quotation_id', 'tax_id');
    }

    public function discount()
    {
        return $this->belongsTo(Discount::class, 'discount', 'discount_id');
    }


}
