<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CreditNoteTaxCustomer extends Model
{
    protected $fillable = ['id', 'credit_note_id', 'tax_id'];

    public function tax()
    {
        return $this->belongsTo(Tax::class, 'tax_id', 'tax_id');
    }

    public function credit_note()
    {
        return $this->belongsTo(CreditNoteCustomer::class, 'credit_note_id', 'id');
    }
}
