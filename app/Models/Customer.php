<?php

namespace App\Models;

use App\Events\CompanyCreating;
use App\Models\Regional\District;
use App\Models\Regional\Province;
use App\Models\Regional\Regency;
use App\Models\Regional\Village;
use App\Observers\CustomerObserver;
use App\Scopes\CompanyScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'customer_id';
    protected $fillable = [
        'customer_id',
        'company_id',
        'first_name',
        'last_name',
        'company',
        'nation',
        'province_id',
        'regency_id',
        'district_id',
        'village_id',
        'address',
        'email',
        'phone'
    ];

    protected static function boot()
    {
        parent::boot();
        self::observe(CustomerObserver::class);
        static::addGlobalScope(new CompanyScope);
    }

    public function quotation_customer()
    {
        return $this->hasMany(QuotationCustomer::class, 'customer_id', 'customer_id');
    }

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id', 'id');
    }
    public function regency()
    {
        return $this->belongsTo(Regency::class, 'regency_id', 'id');
    }
    public function district()
    {
        return $this->belongsTo(District::class, 'district_id', 'id');
    }
    public function village()
    {
        return $this->belongsTo(Village::class, 'village_id', 'id');
    }

    // accessors
    public function getCustomerNameAttribute()
    {
        return "{$this->company} - {$this->first_name} {$this->last_name}";
    }

}
