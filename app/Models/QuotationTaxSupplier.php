<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuotationTaxSupplier extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'quotation_id', 'tax_id'
    ];
}
