<?php

namespace App\Models;

use App\Scopes\CompanySupplierScope;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceSupplier extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'invoice_id';
    protected $appends = ['paidAmount', 'invoiceTitle', 'total_invoice', 'serializing'];
    protected $fillable = [
        'invoice_id',
        'serial',
        'uuid',
        'supplier_id',
        'quotation_id',
        'po_id',
        'payment',
        'currency',
        'rate',
        'rate_tax',
        'attn',
        'vessel',
        'voyage',
        'eta',
        'bl_no',
        'from',
        'to',
        'status',
        'term_and_condition',
        'term_of_payment',
        'description',
        'tax',
        'purchase_account',
        'discount',
        'due_date',
        'invoice_date',
        'file'
    ];
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new CompanySupplierScope((new self())->getTable()));
    }
    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id', 'supplier_id');
    }
    public function purchase_order()
    {
        return $this->belongsTo(PurchaseOrderSupplier::class, 'po_id', 'po_id');
    }
    public function list_items()
    {
        return $this->hasMany(InvoiceSupplierItem::class, 'invoice_id', 'invoice_id');
    }
    public function taxes()
    {
        return $this->hasManyThrough(Tax::class, InvoiceTaxSupplier::class, 'invoice_id', 'tax_id', 'invoice_id', 'tax_id');
    }
    // public function tax()
    // {
    //     return $this->belongsTo(Tax::class, 'tax', 'tax_id');
    // }
    public function discount()
    {
        return $this->belongsTo(Discount::class, 'discount', 'discount_id');
    }
    public function paymentInvoice()
    {
        return $this->hasMany(PaymentSupplier::class, 'invoice_id', 'invoice_id');
    }
    public function purchaseAccount()
    {
        return $this->belongsTo(ChartAccount::class, 'purchase_account');
    }
    public function getSerializingAttribute()
    {
        return date('y', strtotime($this->invoice_date)) .str_pad($this->serial, 4, '0', STR_PAD_LEFT);
    }
    public function getPaidAmountAttribute()
    {
        // $payments = $this->paymentInvoice()->get();
        $payment_amount = $this->paymentInvoice()->get()->sum(function($paid){
            // return $paid['status'] ? $paid['payment_amount'] : 0;
            if ($paid['status']) {
                if($paid->currency > 1){
                    return $paid['payment_amount']/$paid['rate'];
                }else{
                    return $paid['payment_amount'];
                }
            }else{
                return 0;
            }
        });
        return $payment_amount;
    }
    public function getTotalInvoiceAttribute()
    {
        $amount = $this->list_items()->get()->sum(function($query){
            return $query->price * $query->quantity;
        });
        $tax = $this->taxes()->get()->sum('tax_amount');
        if($tax){
            $amount += $amount*($tax/100);
        }
        return $amount;
    }
    public function getInvoiceTitleAttribute()
    {
        $supplier = $this->supplier()->first();
        $str = 'INV'.$this->serial.' - '.$supplier->company.' - '.$supplier->first_name;
        return $str;
    }

    public function void()
    {
        return $this->belongsTo(User::class, 'void_by', 'id');
    }

    public function debit_note()
    {
        return $this->hasMany(DebitNoteSupplier::class, 'invoice_id', 'invoice_id');
    }

}
