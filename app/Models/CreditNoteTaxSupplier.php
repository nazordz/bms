<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CreditNoteTaxSupplier extends Model
{
    protected $fillable = ['id', 'credit_note_id', 'tax_id'];

    public function tax()
    {
        return $this->belongsTo(Tax::class, 'tax_id', 'tax_id');
    }

    public function credit_note()
    {
        return $this->belongsTo(CreditNoteSupplier::class, 'credit_note_id', 'id');
    }
}
