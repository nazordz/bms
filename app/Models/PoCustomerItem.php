<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoCustomerItem extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'po_id', 'item_id', 'item_name', 'description', 'price', 'quantity', 'discount', 'discount_type', 'subtotal'
    ];
}
