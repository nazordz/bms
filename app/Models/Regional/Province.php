<?php

namespace App\Models\Regional;

use App\Models\Customer;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    public $timestamps = false;

    public function customer()
    {
        return $this->hasMany(Customer::class, 'province_id', 'id');
    }
}
