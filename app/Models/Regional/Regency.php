<?php

namespace App\Models\Regional;

use Illuminate\Database\Eloquent\Model;

class Regency extends Model
{
    public $timestamps = false;
    protected $table = 'regencies';
}
