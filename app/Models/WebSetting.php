<?php

namespace App\Models;

use App\Scopes\CompanyScope;
use Illuminate\Database\Eloquent\Model;

class WebSetting extends Model
{
    public $incrementing = false;
    public $timestamps = false;
    protected $keyType = 'string';
    protected $table = 'web_settings';
    protected $primaryKey = 'company_id';
    protected $fillable = [
        'company_id',
        'address',
        'phone',
        'email',
        'account_owner',
        'bank_account',
        'account',
        'bank_code',
        'swift_code',
        'currency',
        'thousand_separator',
        'decimal_separator',
        'nav_logo',
        'logo',
        'header',
        'footer',
        'behalf_of',
        'behalf_of_position',
        'bank_account_secondary',
        'account_secondary',
        'bank_code_secondary',
        'swift_code_secondary',
    ];

    protected static function boot()
    {
        parent::boot();
        self::addGlobalScope(new CompanyScope);
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
}
