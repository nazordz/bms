<?php

namespace App\Models;

use App\Scopes\CompanyScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class SettingAccount extends Model
{
    protected $fillable = [
        'company_id',

        'sales_account_id',
        'receivable_account_id',
        'sales_return_account_id',
        'sales_discount_account_id',
        'sales_profit_rate_account_id',
        'sales_loss_rate_account_id',

        'purchase_account_id',
        'debt_account_id',
        'purchase_return_account_id',
        'purchase_discount_account_id',
        'purchase_profit_rate_account_id',
        'purchase_loss_rate_account_id'

    ];

    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new CompanyScope);
        // static::creating(function($setting){
        //     $setting->company_id = session()->get('company_id');
        // });
    }

    public function sales()
    {
        return $this->belongsTo(ChartAccount::class, 'sales_account_id', 'id');
    }

    public function receivable()
    {
        return $this->belongsTo(ChartAccount::class, 'receivable_account_id', 'id');
    }
    public function sales_return()
    {
        return $this->belongsTo(ChartAccount::class, 'sales_return_account_id', 'id');
    }
    public function sales_discount()
    {
        return $this->belongsTo(ChartAccount::class, 'sales_discount_account_id', 'id');
    }
    public function sales_profit_rate()
    {
        return $this->belongsTo(ChartAccount::class, 'sales_profit_rate_account_id', 'id');
    }
    public function sales_loss_rate()
    {
        return $this->belongsTo(ChartAccount::class, 'sales_loss_rate_account_id', 'id');
    }

    public function purchase()
    {
        return $this->belongsTo(ChartAccount::class, 'purchase_account_id', 'id');
    }
    public function debt()
    {
        return $this->belongsTo(ChartAccount::class, 'debt_account_id', 'id');
    }
    public function purchase_return()
    {
        return $this->belongsTo(ChartAccount::class, 'purchase_return_account_id', 'id');
    }
    public function purchase_discount()
    {
        return $this->belongsTo(ChartAccount::class, 'purchase_discount_account_id', 'id');
    }


    public function purchase_profit_rate()
    {
        return $this->belongsTo(ChartAccount::class, 'purchase_profit_rate_account_id', 'id');
    }
    public function purchase_loss_rate()
    {
        return $this->belongsTo(ChartAccount::class, 'purchase_loss_rate_account_id', 'id');
    }

}
