<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;
    public $incrementing = false;
    protected $keyType = 'string';
    protected $table = 'companies';
    protected $fillable = [ 'id', 'name' ];

    public function web_setting()
    {
        return $this->hasMany(WebSetting::class, 'company_id', 'id');
    }

    public function accounting_setting()
    {
        return $this->hasMany(SettingAccount::class, 'company_id', 'id');
    }
}
