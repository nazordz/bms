<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceTaxSupplier extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'invoice_id', 'tax_id'
    ];
}
