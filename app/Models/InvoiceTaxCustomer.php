<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceTaxCustomer extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'invoice_id', 'tax_id'
    ];

    public function tax()
    {
        return $this->belongsTo(Tax::class, 'tax_id', 'tax_id');
    }
}
