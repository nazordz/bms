<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JournalDetail extends Model
{
    protected $fillable = ['id', 'journal_id', 'account_id', 'description', 'debet', 'credit', 'memo'];

    public static function boot()
    {
        parent::boot();
    }

    public function account()
    {
        return $this->belongsTo(ChartAccount::class, 'account_id', 'id', 'journal_details');
    }
    public function journal()
    {
        return $this->belongsTo(Journal::class, 'journal_id');
    }
}
