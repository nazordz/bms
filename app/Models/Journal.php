<?php

namespace App\Models;

use App\Scopes\CompanyScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Journal extends Model
{
    use SoftDeletes;
    protected $appends = ['total_credit', 'beginning_balance', 'serializing'];
    protected $fillable = [
        'id',
        'code',
        'uuid',
        'serial',
        'contact_id',
        'company_id',
        'account_id',
        'clasification_id',
        'description',
        'reference',
        'status',
        'transaction_at',
        'customer_invoice_id',
        'customer_payment_id',
        'customer_credit_note_id',
        'customer_debit_note_id',
        'supplier_invoice_id',
        'supplier_payment_id',
        'supplier_credit_note_id',
        'supplier_debit_note_id',
        'supplier_allocated_credit_id',
        'fixed_asset_id'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function($journal){
            $journal->company_id = session()->get('company_id');
        });
        self::addGlobalScope(new CompanyScope);
    }

    public function getSerializingAttribute()
    {
        return date('y', strtotime($this->transaction_at)) .str_pad($this->serial, 4, '0', STR_PAD_LEFT);
    }
    // public function parent()
    // {
    //     if ($this->customer_invoice_id) {
    //         return $this->belongsTo(InvoiceCustomer::class, 'invoice_id', 'customer_invoice_id');
    //     }
    //     elseif ($this->customer_payment_id) {
    //         return $this->belongsTo(PaymentCustomer::class, 'id', 'customer_payment_id');
    //     }
    //     elseif ($this->customer_credit_note_id) {
    //         return $this->belongsTo(CreditNoteCustomer::class, 'id', 'customer_credit_note_id');
    //     }
        // if ($this->customer_debit_note_id) {
        //     return $this->belongsTo();
        // }
        // if ($this->supplier_invoice_id) {
        //     return $this->belongsTo();
        // }
        // if ($this->supplier_payment_id) {
        //     return $this->belongsTo();
        // }
        // if ($this->supplier_credit_note_id) {
        //     return $this->belongsTo();
        // }
        // if ($this->supplier_debit_note_id) {
        //     return $this->belongsTo();
        // }
        // if ($this->supplier_allocated_credit_id) {
        //     return $this->belongsTo();
        // }
        // if ($this->fixed_asset_id) {
        //     return $this->belongsTo();
        // }
    //     else{
    //         return $this->belongsTo(InvoiceCustomer::class, 'invoice_id', 'customer_invoice_id');
    //     }
    // }
    public function clasification()
    {
        return $this->belongsTo(JournalClasification::class, 'clasification_id', 'id');
    }
    public function contact()
    {
        return $this->belongsTo(Contacts::class, 'contact_id', 'id');
    }
    public function details()
    {
        return $this->hasMany(JournalDetail::class, 'journal_id', 'id');
    }
    public function getBeginningBalanceAttribute()
    {
        // return (int) abs($this->getTotalDebitAttribute() - $this->getTotalCreditAttribute());
        return $this->details()->sum('credit');
    }

    public function getTotalCreditAttribute()
    {
        // return (int) $this->where('transaction_at', '<', $this->transaction_at)->where('status', 2)->details()->sum('credit');
        return $this->details()->sum('credit');
    }
    public function getTotalDebitAttribute()
    {
        return (int) $this->details()->sum('debet');
    }
}
