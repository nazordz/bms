<?php

namespace App\Models;

use App\Scopes\CompanySupplierScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuotationSupplier extends Model
{
    use SoftDeletes;
    protected $primaryKey = 'quotation_id';
    protected $appends = ['serializing'];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new CompanySupplierScope((new self())->getTable()));
    }

    protected $fillable = [
        'supplier_id',
        'uuid',
        'serial',
        'description',
        'term_and_condition',
        'term_of_payment',
        'valid_until',
        'currency',
        'rate',
        'rate_tax',
        'attn',
        'to',
        'from',
        'file',
        'quotation_date',
        'status',
    ];

    public function getSerializingAttribute()
    {
        return date('y', strtotime($this->quotation_date)) .str_pad($this->serial, 4, '0', STR_PAD_LEFT);
    }

    public function list_items()
    {
        return $this->hasMany(QuotationSupplierItem::class, 'quotation_id', 'quotation_id');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id', 'supplier_id');
    }
    public function taxes()
    {
        return $this->hasManyThrough(Tax::class, QuotationTaxSupplier::class, 'quotation_id', 'tax_id', 'quotation_id', 'tax_id');
    }
    // public function tax()
    // {
    //     return $this->belongsTo(Tax::class, 'tax', 'tax_id');
    // }
    public function discount()
    {
        return $this->belongsTo(Discount::class, 'discount', 'discount_id');
    }


}
