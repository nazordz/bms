<?php

namespace App\Models;

use App\Scopes\CompanyCustomerScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DebitNoteCustomer extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'serial',
        'uuid',
        'customer_id',
        'invoice_id',
        'currency',
        'rate',
        'rate_tax',
        'note',
        'status',
        'payment',
        'account_id',
        'debit_note_date',
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new CompanyCustomerScope((new self())->getTable()));
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'customer_id');
    }

    public function seller()
    {
        return $this->belongsTo(Seller::class, 'seller_id', 'id');
    }

    public function list_items()
    {
        return $this->hasMany(DebitNoteItemCustomer::class, 'debit_note_id', 'id');
    }

    public function debit_note_tax()
    {
        return $this->hasMany(DebitNoteTaxCustomer::class, 'debit_note_id');
    }
    public function taxing()
    {
        return $this->hasManyThrough(Tax::class, DebitNoteTaxCustomer::class,'debit_note_id', 'tax_id', 'id', 'tax_id');
    }


    public function invoice()
    {
        return $this->belongsTo(InvoiceCustomer::class, 'invoice_id', 'invoice_id');
    }

    public function account()
    {
        return $this->belongsTo(ChartAccount::class, 'account_id');
    }
}
