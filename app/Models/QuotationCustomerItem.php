<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuotationCustomerItem extends Model
{
    public $timestamps = false;
    protected $fillable =[
        'item_id', 'quotation_id'
    ];

    public function list_item()
    {
        return $this->belongsTo(ListItem::class, 'item_id', 'id');
    }

    public function quotation_customer()
    {
        return $this->belongsTo(QuotationCustomer::class, 'quotation_id', 'quotation_id');
    }
}
