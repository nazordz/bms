<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceSupplierItem extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'invoice_id', 'item_id', 'item_name', 'description', 'price', 'quantity', 'discount', 'discount_type', 'subtotal'
    ];
}
