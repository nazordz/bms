<?php

namespace App\Models;

use App\Scopes\CompanyScope;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'name', 'company_id'
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new CompanyScope);
        static::creating(function($category){
            $category->company_id = session()->get('company_id');
        });
    }


    public function listItems()
    {
        return $this->hasMany(ListItem::class, 'category_id', 'id');
    }

}
