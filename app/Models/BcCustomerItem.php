<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BcCustomerItem extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'bc_id', 'qty', 'forwarding_service', 'shipment_type', 'container_size', 'container_no'
    ];
}
