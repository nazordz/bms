<?php

namespace App\Models;

use App\Scopes\CompanyParentScope;
use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';
    protected $fillable = [
        'id', 'group_contact_id', 'name', 'company', 'phone', 'email'
    ];

    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new CompanyParentScope((new self())->getTable(), 'group_contacts', 'id', 'group_contact_id'));

    }
}
