<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoTaxSupplier extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'po_id', 'tax_id'
    ];
}
