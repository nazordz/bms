<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DebitNoteTaxCustomer extends Model
{
    public $timestamps = false;

    protected $fillable = ['debit_note_id', 'tax_id'];

    public function tax()
    {
        return $this->belongsTo(Tax::class, 'tax_id', 'tax_id');
    }

    public function debit_note()
    {
        return $this->belongsTo(DebitNoteCustomer::class, 'credit_note_id', 'id');
    }
}
