<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shrinkage extends Model
{
    protected $fillable = [
        'asset_id',
        'depreciation_value',
        'from_date',
        'to_date',
        'status'
    ];

    public static function boot()
    {
        parent::boot();

    }

}
