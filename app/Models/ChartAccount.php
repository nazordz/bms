<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ChartAccount extends Model
{
    public $incrementing = false;
    protected $cast = ['id' => 'string'];
    protected $keyType = 'string';
    // protected $appends = ['beginning_balance','total_debit', 'total_credit'];
    protected $appends = ['position'];
    protected $fillable = [
        'id',
        'account_id',
        'serial',
        'name',
        'description',
        'is_group',
        'default_position',
        'is_current_asset',
        'is_lability',
        'is_equity',
        'is_income',
        'is_expense',
        'is_non_business_income',
        'is_out_of_business_expense',
        'is_operating_expense',
        'is_depreciation_and_amortization',
        'is_equivalent_cash',
        'is_fixed_asset',
        'is_accumulation_of_fixed_asset',
        'is_prive',
        'company_id'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function($coa){
            $coa->company_id = session()->get('company_id');
        });
    }

    public function journal_details()
    {
        return $this->hasMany(JournalDetail::class, 'account_id', 'id');
    }
    public function journal()
    {
        return $this->hasManyThrough(Journal::class, JournalDetail::class, 'account_id', 'id', 'id', 'journal_id');
    }

    public function getBeginningBalanceAttribute()
    {
        // return (int) abs($this->getTotalDebitAttribute() - $this->getTotalCreditAttribute());
        // $query->where('transaction_at', '<', '2020-04-04');
        return $this->journal()->where('status', 2)->get()->sum(function($q){
            return $q->debet ;
        });// $this->journal()->where('status', 2)->sum('debet');
    }
    public function getPositionAttribute()
    {
        $position = 'credit';
        if($this->default_position == 2) $position = 'debit';
        return $position;
    }
    public function getTotalCreditAttribute()
    {
        if($this->account_id == null){
            return (int) $this->journal()->where('status', 2)->with('details')->sum('credit');
        } else 0;
    }
    public function getTotalDebitAttribute()
    {
        return (int) $this->journal()->where('status', 2)->with('details')->sum('debet');
    }
    public function totalCredit($condition)
    {
        return (int) $this->journal()->where('status', 2)
                    ->with('details')
                    ->whereRaw($condition)
                    ->sum('credit');
    }

    public function totalDebit($condition)
    {
        return (int) $this->journal()->where('status', 2)
                    ->with('details')
                    ->whereRaw($condition)
                    ->sum('debet');
    }



    public function parent()
    {
        return $this->belongsTo(ChartAccount::class, 'account_id', 'id');
    }

    public function children()
    {
        return $this->hasMany(ChartAccount::class, 'account_id', 'id');
    }
    public function recursiveChildren()
    {
        return $this->children()->with('recursiveChildren');
    }
}
