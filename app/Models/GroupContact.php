protected $keyType = 'string';<?php

namespace App\Models;

use App\Scopes\CompanyScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class GroupContact extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';
    protected $fillable = ['id', 'company_id', 'name'];

    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new CompanyScope);
        static::creating(function($supplier){
            $supplier->company_id = session()->get('company_id');
        });
    }
}
