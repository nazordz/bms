<?php

namespace App;

use App\Models\Company;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use HasRoles;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'menuroles', 'language', 'active', 'uuid', 'phone', 'company_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $dates = [
        'deleted_at'
    ];

    protected $attributes = [
        'menuroles' => 'user',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    // get menu
    public function getMenuRoles(){
        return explode(',', $this->menuroles);
    }

    public function isMenuAccessed($roles)
    {
        $menus = collect($this->getMenuRoles());
        if(is_array($roles)){
            foreach ($roles as $key => $val) {
                $check = $menus->contains(function($value, $key)use($val) {
                    return $value == $val;
                });
                if($check){
                    return true;
                }
            }
        }

        if(is_string($roles)){
            if(array_search($roles, $this->getMenuRoles())){
                return true;
            }
        }

        return false;
    }

}
