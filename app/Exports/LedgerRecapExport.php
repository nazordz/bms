<?php

namespace App\Exports;

use App\Models\ChartAccount;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Barryvdh\Debugbar\Facade as Debugbar;

class LedgerRecapExport implements FromView, ShouldAutoSize
{
    use Exportable;
    public function __construct($start_date, $to_date) {
        $this->start_date = $start_date;
        $this->to_date = $to_date;
    }

    public function view():view
    {
        $data = ChartAccount::where('is_group', 0)
                ->select(
                    'chart_accounts.id',
                    'chart_accounts.name',
                    DB::raw('(
                        select ABS(sum(journal_details.debet) - sum(journal_details.credit)) from journal_details
                        where journal_details.account_id = chart_accounts.id AND journal_details.journal_id = (
                            SELECT id from journals where id = journal_details.journal_id
                            and journals.transaction_at < "'.$this->start_date.'"
                            AND journals.status = 2
                        )
                    ) AS beginning'),
                    DB::raw('(
                        select ABS(sum(journal_details.debet)) from journal_details
                        where journal_details.account_id = chart_accounts.id AND journal_details.journal_id = (
                            SELECT id from journals where id = journal_details.journal_id
                            and DATE(journals.transaction_at) between "'.$this->start_date.'" AND "'.$this->to_date.'"
                            AND journals.status = 2
                        )
                    ) AS debit'),
                    DB::raw('(
                        select ABS(sum(journal_details.credit)) from journal_details
                        where journal_details.account_id = chart_accounts.id AND journal_details.journal_id = (
                            SELECT id from journals where id = journal_details.journal_id
                            and DATE(journals.transaction_at) between "'.$this->start_date.'" AND "'.$this->to_date.'"
                            AND journals.status = 2
                        )
                    ) AS credit'),
                    DB::raw('(
                        select ABS(sum(journal_details.debet)) from journal_details
                        where journal_details.account_id = chart_accounts.id AND journal_details.journal_id = (
                            SELECT id from journals where id = journal_details.journal_id
                            AND journals.status = 2
                        )
                    ) AS total_debit'),
                    DB::raw('(
                        select ABS(sum(journal_details.credit)) from journal_details
                        where journal_details.account_id = chart_accounts.id AND journal_details.journal_id = (
                            SELECT id from journals where id = journal_details.journal_id
                            AND journals.status = 2
                        )
                    ) AS total_credit')
                )
                ->get();
        $data = $data->map(function($ledger){
            // beginning
            $begin = $ledger->beginning;
            $total_debit = (int) $ledger->total_debit;
            $total_credit = (int) $ledger->total_credit;
            if($total_debit < $total_credit){
                $ledger->beginning = '('.$begin.')';
            }

            // ending
            $op1 = (int) $total_debit > $total_credit?$ledger->debit:$ledger->credit;
            $op2 = (int) $total_debit < $total_credit?$ledger->debit:$ledger->credit;
            if($total_debit > $total_credit){
                $ledger->ending = ($begin + $op1) - $op2;
            }elseif($total_debit < $total_credit){
                $ledger->ending = '('.(($begin + $op1) - $op2).')';
            }

            return $ledger;
        });
        Debugbar::info($data);
        return view('dashboard.report.ledger.print-recap', ['data' => $data]);
    }
}
