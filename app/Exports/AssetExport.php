<?php

namespace App\Exports;

use App\Models\FixedAsset;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AssetExport implements FromQuery, ShouldAutoSize, WithHeadings
{
    use Exportable;
    public function __construct($status) {
        $this->status = $status;
    }

    public function headings():array
    {
        return [
            Lang::get('dashboard.asset_code'),
            Lang::get('dashboard.asset_name'),
            Lang::get('dashboard.clasification'),
            Lang::get('dashboard.asset_type'),
            Lang::get('dashboard.date_acquistion'),
            Lang::get('dashboard.price_acquistion'),
            Lang::get('dashboard.book_value')
        ];
    }

    public function query()
    {
        $data = FixedAsset::query()
                ->join('setting_asset_types', 'fixed_assets.type_id', '=', 'setting_asset_types.id')
                ->leftJoin('journal_clasifications', 'fixed_assets.clasification_id', '=', 'journal_clasifications.id')
                ->select(
                    'fixed_assets.serial',
                    'fixed_assets.name',
                    'journal_clasifications.name as ClassfName',
                    'setting_asset_types.name as typeName',
                    'fixed_assets.date_acquistion',
                    'fixed_assets.price_acquistion',
                    DB::raw('(select SUM(depreciation_value) from shrinkages where shrinkages.asset_id = fixed_assets.id)')
                )
                ->where('fixed_assets.company_id', session()->get('company_id'))
                ->where('fixed_assets.status', $this->status);
        return $data;
    }
}
