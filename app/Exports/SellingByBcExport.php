<?php

namespace App\Exports;

use App\Models\BcCustomer;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SellingByBcExport implements FromView, ShouldAutoSize
{
    use Exportable;
    public function __construct($from = null, $to = null, $customer = null) {
        $this->from         = $from;
        $this->to           = $to;
        $this->customer     = $customer;
    }

    public function view():view
    {
        $data = BcCustomer::join('customers', 'bc_customers.customer_id', '=', 'customers.customer_id');
        if($this->customer){
            $data = $data->where('customers.first_name', $this->customer)
            ->orWhere('customers.company', $this->customer)
            ->orWhere('customers.last_name', $this->customer);
        }
        if($this->from && $this->to){
            $data = $data->whereBetween('bc_customers.created_at', [date_format(date_create($this->start_date), 'Y-m-d')." 00:00:00", date_format(date_create($this->to_date), 'Y-m-d')." 23:59:59"]);
        }
        return view('dashboard.report.sales.print-bc', ['items' => $data->get()]);
    }
}
