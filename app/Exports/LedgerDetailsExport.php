<?php

namespace App\Exports;

use App\Models\ChartAccount;
use App\Models\JournalDetail;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Barryvdh\Debugbar\Facade as Debugbar;

class LedgerDetailsExport implements FromView, ShouldAutoSize
{
    use Exportable;
    public function __construct($start_date, $to_date) {
        $this->start_date = $start_date;
        $this->to_date = $to_date;
    }
    public function view():View
    {

        $data = ChartAccount::with(['journal_details' => function($query){
            $query->whereHas('journal', function(Builder $query){
                $query->where('status', 2)->whereBetween('transaction_at', [$this->start_date, $this->to_date]);
            });
        }]);

        $data = $data->get()->map(function($query){
            if(count($query->journal_details)){
                $debit = JournalDetail::where('account_id', $query->id)->whereIn('journal_id', function($q){
                    $q->select('id')->from('journals')->where('status', 2)->where('transaction_at', '<', $this->start_date);
                })->sum('debet');
                $credit = JournalDetail::where('account_id', $query->id)->whereIn('journal_id', function($q){
                    $q->select('id')->from('journals')->where('status', 2)->where('transaction_at', '<', $this->start_date);
                })->sum('credit');
                $query->beginning = abs($debit - $credit);
                $query->beginning_balance_type = $debit > $credit ? 'debit':'credit';

                $total_debit = JournalDetail::where('account_id', $query->id)->whereIn('journal_id', function($q){
                    $q->select('id')->from('journals')->where('status', 2);
                })->sum('debet');
                $total_credit = JournalDetail::where('account_id', $query->id)->whereIn('journal_id', function($q){
                    $q->select('id')->from('journals')->where('status', 2);
                })->sum('credit');

                $op1 = $total_debit > $total_credit?$query->journal_details->sum('debet'):$query->journal_details->sum('credit');
                $op2 = $total_debit < $total_credit?$query->journal_details->sum('debet'):$query->journal_details->sum('credit');
                if($total_debit > $total_credit){
                    $query->ending = ($query->beginning + $op1) - $op2;
                    $query->ending_balance_type = $total_debit > $total_credit ? 'debit':'credit';
                }elseif($total_debit < $total_credit){
                    $query->ending = (($query->beginning + $op1) - $op2);
                    $query->ending_balance_type = $total_debit < $total_credit?'debit':'credit';
                }

            }else $query->beginning = 0;
            return $query;
        });
        Debugbar::info($data);
        return view('dashboard.report.ledger.print-details', ['data' => $data]);
    }
}
