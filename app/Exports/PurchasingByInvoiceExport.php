<?php

namespace App\Exports;

use App\Models\InvoiceSupplier;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Barryvdh\Debugbar\Facade AS Debugbar;

class PurchasingByInvoiceExport implements FromView, ShouldAutoSize
{
    use Exportable;
    public function __construct($invoice_date = null, $from = null, $to = null, $supplier = null) {
        $this->invoice_date = $invoice_date;
        $this->from         = $from;
        $this->to           = $to;
        $this->supplier     = $supplier;
    }

    public function view():view
    {
        $data = InvoiceSupplier::
            join('suppliers', 'invoice_suppliers.supplier_id', '=', 'suppliers.supplier_id')
            ->leftJoin('invoice_tax_suppliers', 'invoice_suppliers.invoice_id', '=', 'invoice_tax_suppliers.invoice_id')
            ->leftJoin('tax', 'invoice_tax_suppliers.tax_id', '=', 'tax.tax_id')
            ->select(
                DB::raw('concat("INV", invoice_suppliers.serial) AS serial'),
                'invoice_suppliers.invoice_date',
                DB::raw('concat(company, " ",first_name) as name'),
                'invoice_suppliers.due_date',
                DB::raw('(
                    SELECT SUM(price*quantity) FROM invoice_supplier_items WHERE invoice_suppliers.invoice_id = invoice_supplier_items.invoice_id
                ) AS total'),
                DB::raw('((
                    SELECT SUM(price*quantity) FROM invoice_supplier_items WHERE invoice_suppliers.invoice_id = invoice_supplier_items.invoice_id
                )* (tax.tax_amount / 100)) as nominal_tax'),
                DB::raw("((
                    SELECT SUM(price*quantity) FROM invoice_supplier_items WHERE invoice_suppliers.invoice_id = invoice_supplier_items.invoice_id
                )* (tax.tax_amount / 100)) + (
                    SELECT SUM(price*quantity) FROM invoice_supplier_items WHERE invoice_suppliers.invoice_id = invoice_supplier_items.invoice_id
                ) as total_bill"),
                DB::raw('(select SUM(payment_amount) from payment_suppliers where invoice_suppliers.invoice_id = payment_suppliers.invoice_id) as paid'),
                DB::raw('(
                    (
                        (
                            SELECT SUM(price*quantity) FROM invoice_supplier_items WHERE invoice_suppliers.invoice_id = invoice_supplier_items.invoice_id
                        ) * (tax.tax_amount / 100)
                    ) + (
                        SELECT SUM(price*quantity) FROM invoice_supplier_items WHERE invoice_suppliers.invoice_id = invoice_supplier_items.invoice_id
                    )
                ) - (select SUM(payment_amount) from payment_suppliers where invoice_suppliers.invoice_id = payment_suppliers.invoice_id) as balance'
                ),
                DB::raw("if(invoice_suppliers.payment = 2, 'Paid', 'Unpaid') as keterangan")
            )
            ->where('invoice_suppliers.status', 2);
            if($this->invoice_date){
                $data = $data->where('invoice_suppliers.invoice_date', $this->invoice_date);
            }
            if($this->from){
                $data = $data->whereBetween('invoice_suppliers.invoice_date', [$this->from, $this->to]);
            }
            if($this->supplier){
                $supplier = $this->supplier;
                $data = $data->where(function($query)use($supplier){
                    $query->where('suppliers.first_name', $supplier)
                            ->orWhere('suppliers.company', $supplier)
                            ->orWhere('suppliers.last_name', $supplier);
                });
            }
        Debugbar::info($data->get());
        // return $data;
        return view('dashboard.report.purchasing.print-invoice', ['invoices' => $data->get()]);

    }

}
