<?php

namespace App\Exports;

use App\Models\QuotationCustomer;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class QuotationCustomerExport implements FromQuery, WithHeadings, ShouldAutoSize
{
    use Exportable;
    public function __construct($from, $to) {
        $this->from = date_format(date_create($from), 'Y-m-d');
        $this->to   = date_format(date_create($to), 'Y-m-d');
    }

    public function headings(): array
    {
        return [
            'NO QTT',
            'Customer',
            'Company',
            'Term and Condition',
            'sales',
            'Valid until',
            'Created at'
        ];
    }

    public function query()
    {
        $data = QuotationCustomer::query()
                ->select(
                    DB::raw("CONCAT('QTT',quotation_customers.quotation_id)"),
                    DB::raw("CONCAT(customers.first_name, ' ', customers.last_name)"),
                    'customers.company',
                    'quotation_customers.term_and_condition',
                    'quotation_customers.sales',
                    DB::raw("DATE_FORMAT(quotation_customers.valid_until, '%d-%m-%Y')"),
                    DB::raw("DATE_FORMAT(quotation_customers.created_at, '%d-%m-%Y')")
                )
                ->join('customers', 'quotation_customers.customer_id', '=', 'customers.customer_id')
                ->whereNull('quotation_customers.deleted_at')
                ->whereBetween('quotation_customers.created_at', [$this->from." 00:00:00", $this->to." 23:59:59"])
                ->orderBy('quotation_customers.created_at', 'desc')
                ->limit(1000);
        return $data;
    }
}
