<?php

namespace App\Exports;

use App\Models\InvoiceCustomer;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Barryvdh\Debugbar\Facade AS Debugbar;
use Illuminate\Support\Carbon;

class SellingByReceivableExport implements FromView, ShouldAutoSize
{
    use Exportable;
    public function __construct($from = null, $to = null, $customer = null) {
        $this->from         = $from;
        $this->to           = $to;
        $this->customer     = $customer;
    }

    public function view():view
    {
        $data = InvoiceCustomer::with('customer')->where(['status' => 2, 'payment' => 1]);
            if($this->from && $this->to){
                $data = $data->whereBetween('invoice_customers.invoice_date', [$this->from, $this->to]);
            }
            if($this->customer){
                $customer = $this->customer;
                $data = $data->wherehas(function($query)use($customer){
                    $query->where('customers.first_name', $customer)
                            ->orWhere('customers.company', $customer)
                            ->orWhere('customers.last_name', $customer);
                });
            }
        Debugbar::info($data->get());
        $today = Carbon::createFromFormat('Y-m-d', $this->to);
        $receivables = $data->get()->map(function($invoice, $index)use($today){
            $thatDay = Carbon::createFromFormat('Y-m-d', $invoice->invoice_date);
            $diff = $thatDay->diffInDays($today);

            $invoice['today']   = $diff == 0 ?$invoice->total_invoice:0;
            $invoice['is1_15']  = $diff >= 1 && $diff <= 15 ?$invoice->total_invoice:0;
            $invoice['is16_30'] = $diff >= 16 && $diff <= 30 ?$invoice->total_invoice:0;
            $invoice['is31_45'] = $diff >= 31 && $diff <= 45 ?$invoice->total_invoice:0;
            $invoice['is46_60'] = $diff >= 46 && $diff <= 60 ?$invoice->total_invoice:0;
            $invoice['is61_90'] = $diff >= 61 && $diff <= 90 ?$invoice->total_invoice:0;
            $invoice['is90_above'] = $diff > 90 ?$invoice->total_invoice:0;
            return $invoice;
        });
        return view('dashboard.report.sales.print-receivable', ['invoices' => $receivables]);

    }

}
