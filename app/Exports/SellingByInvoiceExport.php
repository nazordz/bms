<?php

namespace App\Exports;

use App\Models\InvoiceCustomer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Barryvdh\Debugbar\Facade AS Debugbar;

// class SellingByInvoiceExport implements FromQuery, WithHeadings, ShouldAutoSize
class SellingByInvoiceExport implements FromView, ShouldAutoSize
{
    use Exportable;
    public function __construct($invoice_date = null, $from = null, $to = null, $customer = null) {
        $this->invoice_date = $invoice_date;
        $this->from         = $from;
        $this->to           = $to;
        $this->customer     = $customer;
    }

    /* public function headings(): array
    {
        return [
            'NO Invoice',
            Lang::get('dashboard.invoice_date'),
            'Customer',
            Lang::get('dashboard.due_date'),
            'Total',
            'Nominal '.Lang::get('dashboard.tax'),
            'Total '.Lang::get('dashboard.bill'),
            Lang::get('dashboard.paid'),
            Lang::get('dashboard.balance'),
            Lang::get('dashboard.description'),

        ];
    } */

    public function view():view
    {
        $data = InvoiceCustomer::
            join('customers', 'invoice_customers.customer_id', '=', 'customers.customer_id')
            ->leftJoin('invoice_tax_customers', 'invoice_customers.invoice_id', '=', 'invoice_tax_customers.invoice_id')
            ->leftJoin('tax', 'invoice_tax_customers.tax_id', '=', 'tax.tax_id')
            ->select(
                DB::raw('concat("INV", invoice_customers.serial) AS serial'),
                'invoice_customers.invoice_date',
                DB::raw('concat(company, " ",first_name) as name'),
                'invoice_customers.due_date',
                DB::raw('(
                    SELECT SUM(price*quantity) FROM invoice_customer_items WHERE invoice_customers.invoice_id = invoice_customer_items.invoice_id
                ) AS total'),
                DB::raw('((
                    SELECT SUM(price*quantity) FROM invoice_customer_items WHERE invoice_customers.invoice_id = invoice_customer_items.invoice_id
                )* (tax.tax_amount / 100)) as nominal_tax'),
                DB::raw("((
                    SELECT SUM(price*quantity) FROM invoice_customer_items WHERE invoice_customers.invoice_id = invoice_customer_items.invoice_id
                )* (tax.tax_amount / 100)) + (
                    SELECT SUM(price*quantity) FROM invoice_customer_items WHERE invoice_customers.invoice_id = invoice_customer_items.invoice_id
                ) as total_bill"),
                DB::raw('(select SUM(payment_amount) from payment_customers where invoice_customers.invoice_id = payment_customers.invoice_id) as paid'),
                DB::raw('(
                    (
                        (
                            SELECT SUM(price*quantity) FROM invoice_customer_items WHERE invoice_customers.invoice_id = invoice_customer_items.invoice_id
                        ) * (tax.tax_amount / 100)
                    ) + (
                        SELECT SUM(price*quantity) FROM invoice_customer_items WHERE invoice_customers.invoice_id = invoice_customer_items.invoice_id
                    )
                ) - (select SUM(payment_amount) from payment_customers where invoice_customers.invoice_id = payment_customers.invoice_id) as balance'
                ),
                DB::raw("if(invoice_customers.payment = 2, 'Paid', 'Unpaid') as keterangan")
            )
            ->where('invoice_customers.status', 2);
            if($this->invoice_date){
                $data = $data->where('invoice_customers.invoice_date', $this->invoice_date);
            }
            if($this->from){
                $data = $data->whereBetween('invoice_customers.invoice_date', [$this->from, $this->to]);
            }
            if($this->customer){
                $customer = $this->customer;
                $data = $data->where(function($query)use($customer){
                    $query->where('customers.first_name', $customer)
                            ->orWhere('customers.company', $customer)
                            ->orWhere('customers.last_name', $customer);
                });
            }
        Debugbar::info($data->get());
        // return $data;
        return view('dashboard.report.sales.print-invoice', ['invoices' => $data->get()]);

    }

}
