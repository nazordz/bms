<?php

namespace App\Exports;

use App\Models\PurchaseOrderSupplier;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Barryvdh\Debugbar\Facade AS Debugbar;

class PurchasingByPoExport implements FromView, ShouldAutoSize
{
    use Exportable;
    public function __construct($po_date = null, $start_date = null, $to_date = null, $supplier = null) {
        $this->po_date    = $po_date;
        $this->start_date = $start_date;
        $this->to_date    = $to_date;
        $this->supplier   = $supplier;
    }

    public function view():view
    {
        $data = PurchaseOrderSupplier::
        join('suppliers', 'purchase_order_suppliers.supplier_id', '=', 'suppliers.supplier_id')
        ->select(
            'purchase_order_suppliers.*',
            'suppliers.company',
            'suppliers.first_name',
            'suppliers.last_name'
        )
        ->where('purchase_order_suppliers.status', 2);
        if($this->po_date){
            $data = $data->where('purchase_order_suppliers.po_date', $this->po_date);
        }
        if($this->start_date && $this->to_date){
            $data = $data->whereBetween('purchase_order_suppliers.po_date', [$this->start_date, $this->to_date]);
        }
        if($this->supplier){
            $supplier = $this->supplier;
            $data = $data->where(function($query)use($supplier){
                $query->where('suppliers.first_name', 'like', '%'.$supplier.'%')
                        ->orWhere('suppliers.company', 'like', '%'.$supplier.'%')
                        ->orWhere('suppliers.last_name', 'like', '%'.$supplier.'%');
            });
        }
        Debugbar::info($data->get());
        // return $data;
        return view('dashboard.report.purchasing.print-po', ['purchases' => $data->get()]);

    }

}
