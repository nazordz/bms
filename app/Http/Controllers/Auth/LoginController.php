<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    // additional field 👇
    public function credentials(Request $request)
    {
        return [$this->username() => $request->{$this->username()}, 'password' => $request->password, 'active' => 1];
    }

    // modification error login
    public function sendFailedLoginResponse(Request $request)
    {
        $errors = [ $this->username() => [trans('auth.failed')]];
        $user = User::where($this->username(), $request->{$this->username()})->first();
        if($user && !Hash::check($request->password, $user->password) && $user->active){
            $errors['password'] = [trans('auth.wrong_password')];
        }
        if($user && !$user->active){
            $errors['email'] = [trans('auth.suspended')];
        }

        throw ValidationException::withMessages($errors);
    }

    public function logout(Request $request)
    {

        // $user_id = Auth::user()->id;
        // $user    = User::find($user_id);
        // $user->company_id = null;
        // $user->save();
        $request->session()->forget('company_id');
        $request->session()->forget('company_name');

        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return $this->loggedOut($request) ?: redirect('/');
    }

    public function authenticated()
    {
        if(Auth::user()->menuroles != 'superadmin'){
            $company = Company::find(Auth::user()->company_id);
            session(['company_id' => Auth::user()->company_id, 'company_name' => $company->name]);
        }
    }

    public function redirectTo()
    {
        if(Auth::user()->menuroles != 'superadmin'){
            return '/';
        }else{
            return '/company';
        }

    }
}
