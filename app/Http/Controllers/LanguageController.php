<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class LanguageController extends Controller
{
    public function change(Request $request)
    {

        $id = Auth::user()->id;
        $user = User::find($id);
        $user->language = $request->language;
        $user->save();
        return back();
    }
}
