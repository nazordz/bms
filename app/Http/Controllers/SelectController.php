<?php

namespace App\Http\Controllers;

use App\Models\BcCustomer;
use App\Models\Category;
use App\Models\ChartAccount;
use App\Models\Contacts;
use App\Models\Customer;
use App\Models\Discount;
use App\Models\InvoiceCustomer;
use App\Models\InvoiceSupplier;
use App\Models\JournalClasification;
use App\Models\ListItem;
use App\Models\PurchaseOrderSupplier;
use App\Models\QuotationCustomer;
use App\Models\QuotationSupplier;
use App\Models\Seller;
use App\Models\Supplier;
use App\Models\Tax;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SelectController extends Controller
{
    public function category(Request $request)
    {
        $offset = ($request->page - 1) * $request->page_limit;
        $result = [];

        $query = Category::select('id', "name AS text");
        if(!empty($request->term)){
            $query = $query->where('name', 'LIKE', '%'.$request->term.'%');
        }

        $count     = $query->count();
        $endCount  = $offset + $request->page_limit;
        $morePages =  $count > $endCount;
        $query     = $query->skip($offset)->take($request->page_limit)->get();

        $result = [
            'results'    => $query,
            'pagination' => [
                'more' => $morePages
            ]
        ];
        return response()->json($result);
    }


    public function province(Request $request)
    {
        $offset = ($request->page - 1) * $request->page_limit;
        $result = [];

        $query = DB::table('provinces')
                    ->select('id', "name AS text");
        if(!empty($request->term)){
            $query = $query->where('name', 'LIKE', '%'.$request->term.'%');
        }

        $count     = $query->count();
        $endCount  = $offset + $request->page_limit;
        $morePages =  $count > $endCount;
        $query     = $query->skip($offset)->take($request->page_limit)->get();

        $result = [
            'results'    => $query,
            'pagination' => [
                'more' => $morePages
            ]
        ];
        return response()->json($result);
    }
    public function regency(Request $request)
    {
        $offset = ($request->page - 1) * $request->page_limit;
        $result = [];

        $query = DB::table('regencies')
                    ->where('province_id', $request->province_id)
                    ->select('id', "name AS text");
        if(!empty($request->term)){
            $query = $query->where('name', 'LIKE', '%'.$request->term.'%');
        }

        $count     = $query->count();
        $endCount  = $offset + $request->page_limit;
        $morePages =  $count > $endCount;
        $query     = $query->skip($offset)->take($request->page_limit)->get();

        $result = [
            'results'    => $query,
            'pagination' => [
                'more' => $morePages
            ]
        ];
        return response()->json($result);
    }
    public function district(Request $request)
    {
        $offset = ($request->page - 1) * $request->page_limit;
        $result = [];

        $query = DB::table('districts')
                    ->where('regency_id', $request->regency_id)
                    ->select('id', "name AS text");
        if(!empty($request->term)){
            $query = $query->where('name', 'LIKE', '%'.$request->term.'%');
        }

        $count     = $query->count();
        $endCount  = $offset + $request->page_limit;
        $morePages =  $count > $endCount;
        $query     = $query->skip($offset)->take($request->page_limit)->get();

        $result = [
            'results'    => $query,
            'pagination' => [
                'more' => $morePages
            ]
        ];
        return response()->json($result);
    }
    public function village(Request $request)
    {
        $offset = ($request->page - 1) * $request->page_limit;
        $result = [];

        $query = DB::table('villages')
                    ->where('district_id', $request->district_id)
                    ->select('id', "name AS text");
        if(!empty($request->term)){
            $query = $query->where('name', 'LIKE', '%'.$request->term.'%');
        }

        $count     = $query->count();
        $endCount  = $offset + $request->page_limit;
        $morePages =  $count > $endCount;
        $query     = $query->skip($offset)->take($request->page_limit)->get();

        $result = [
            'results'    => $query,
            'pagination' => [
                'more' => $morePages
            ]
        ];
        return response()->json($result);
    }

    public function customer(Request $request)
    {
        $offset = ($request->page - 1) * $request->page_limit;
        $result = [];

        // $query = DB::table('customers')
        $query = Customer::select(
            'customer_id AS id',
            DB::raw("CONCAT(COALESCE(CONCAT(company, ' - '), ''), first_name, ' ', COALESCE(last_name, '')) AS text")
        );
        if(!empty($request->term)){
            $query = $query->where('first_name', 'LIKE', '%'.$request->term.'%')
                    ->orWhere('last_name', 'like', '%'.$request->term.'%')
                    ->orWhere('company', 'like', '%'.$request->term.'%');
        }

        $count     = $query->count();
        $endCount  = $offset + $request->page_limit;
        $morePages =  $count > $endCount;
        // $sql       = $query->skip($offset)->take($request->page_limit);
        $query     = $query->skip($offset)->take($request->page_limit)->get();

        $result = [
            'results'    => $query,
            'pagination' => [
                'more' => $morePages
            ],
            // 'toSql' => $sql->toSql()
        ];
        return response()->json($result);
    }
    public function supplier(Request $request)
    {
        $offset = ($request->page - 1) * $request->page_limit;
        $result = [];

        // $query = DB::table('suppliers')
        $query = Supplier::select('supplier_id AS id', DB::raw("CONCAT(company,' - ', first_name, ' ', COALESCE(last_name, '')) AS text"));
        if(!empty($request->term)){
            $query = $query->where('first_name', 'LIKE', '%'.$request->term.'%')
                    ->orWhere('last_name', 'like', '%'.$request->term.'%')
                    ->orWhere('company', 'like', '%'.$request->term.'%');
        }

        $count     = $query->count();
        $endCount  = $offset + $request->page_limit;
        $morePages =  $count > $endCount;
        $query     = $query->skip($offset)->take($request->page_limit)->get();

        $result = [
            'results'    => $query,
            'pagination' => [
                'more' => $morePages
            ]
        ];
        return response()->json($result);
    }

    public function item(Request $request)
    {
        $offset = ($request->page - 1) * $request->page_limit;
        $result = [];

        // $query = DB::table('list_items')
        $query = ListItem::whereNull('deleted_at')
                    ->select('id', "name as text", "description", 'sell_price');
        if(!empty($request->term)){
            $query = $query->where('name', 'LIKE', '%'.$request->term.'%');
        }

        $count     = $query->count();
        $endCount  = $offset + $request->page_limit;
        $morePages =  $count > $endCount;
        $query     = $query->skip($offset)->take($request->page_limit)->get();

        $result = [
            'results'    => $query,
            'pagination' => [
                'more' => $morePages
            ]
        ];
        return response()->json($result);
    }

    public function tax($type = null, Request $request)
    {
        $offset = ($request->page - 1) * $request->page_limit;
        $result = [];

        // $query = DB::table('tax')
        $query = Tax::select(
                'tax_id AS id',
                DB::raw("CONCAT(description, ' - ', tax_amount,'%') AS text"),
                "tax_amount",
                "type",
                'payable_account_id',
                'receivable_account_id'
            )
            ->whereNull('deleted_at');
        if(!empty($request->term)){
            $query = $query->where('description', 'LIKE', '%'.$request->term.'%')
                        ->where('tax_amount', 'LIKE', '%'.$request->term.'%');
        }
        if($type){
            $query = $query->where('type', $type);
        }

        $count     = $query->count();
        $endCount  = $offset + $request->page_limit;
        $morePages =  $count > $endCount;
        $query     = $query->skip($offset)->take($request->page_limit)->get();

        $result = [
            'results'    => $query,
            'pagination' => [
                'more' => $morePages
            ]
        ];
        return response()->json($result);
    }

    public function discount(Request $request)
    {
        $offset = ($request->page - 1) * $request->page_limit;
        $result = [];

        $query = Discount::select('discount_id AS id', DB::raw("CONCAT(discount_amount,'% - ',description) AS text"), 'discount_amount')
                    ->whereNull('deleted_at');
        if(!empty($request->term)){
            $query = $query->where('description', 'LIKE', '%'.$request->term.'%')
                        ->where('discount_amount', 'LIKE', '%'.$request->term.'%');
        }

        $count     = $query->count();
        $endCount  = $offset + $request->page_limit;
        $morePages =  $count > $endCount;
        $query     = $query->skip($offset)->take($request->page_limit)->get();

        $result = [
            'results'    => $query,
            'pagination' => [
                'more' => $morePages
            ]
        ];
        return response()->json($result);
    }

    public function getItem($item_id)
    {
        $data = ListItem::with('category')->first($item_id);
        return response()->json($data);
    }

    public function sales(Request $request)
    {
        $offset = ($request->page - 1) * $request->page_limit;
        $result = [];

        $query = Seller::select('id AS id', 'name AS text');
        if(!empty($request->term)){
            $query = $query->where('name', 'LIKE', '%'.$request->term.'%');
        }

        $count     = $query->count();
        $endCount  = $offset + $request->page_limit;
        $morePages =  $count > $endCount;
        $query     = $query->skip($offset)->take($request->page_limit)->get();

        $result = [
            'results'    => $query,
            'pagination' => [
                'more' => $morePages
            ]
        ];
        return response()->json($result);
    }

    public function poSupplier(Request $request)
    {
        $offset = ($request->page - 1) * $request->page_limit;
        $result = [];

        // $query = DB::table('purchase_order_suppliers')
        $query = PurchaseOrderSupplier::join('suppliers', 'purchase_order_suppliers.supplier_id', '=', 'suppliers.supplier_id')
                    ->select(
                        'po_id AS id',
                        DB::raw('concat(
                        "PO",
                        date_format(purchase_order_suppliers.po_date, "%y"),
                        LPAD(serial, 4, "0"),
                        " - ",
                        suppliers.company,
                        " - ",
                        suppliers.first_name) AS text'),
                        'suppliers.supplier_id',
                        DB::raw("CONCAT(suppliers.company,' - ', suppliers.first_name, ' ', suppliers.last_name) AS supplier")
                    );
        if(!empty($request->term)){
            $query = $query->where('po_id', 'LIKE', '%'.$request->term.'%');
        }
        if($request->filled('quotation')){
            $query = $query->where('purchase_order_suppliers.quotation_id', $request->quotation);
        }

        $count     = $query->count();
        $endCount  = $offset + $request->page_limit;
        $morePages =  $count > $endCount;
        $query     = $query->skip($offset)->take($request->page_limit)->get();

        $result = [
            'results'    => $query,
            'pagination' => [
                'more' => $morePages
            ]
        ];
        return response()->json($result);
    }

    public function qttCustomer(Request $request)
    {
        $offset = ($request->page - 1) * $request->page_limit;
        $result = [];
        $query = QuotationCustomer::join('customers', 'quotation_customers.customer_id', '=', 'customers.customer_id')
                    ->select(
                        'quotation_id AS id',
                        DB::raw('concat(
                            "QTT",
                            date_format(quotation_customers.quotation_date, "%y"),
                            LPAD(serial, 4, "0"),
                            coalesce(concat (" - ", customers.company, " - "), " - "),
                            customers.first_name
                        ) AS text'),
                        DB::raw("(SELECT SUM(price*quantity) FROM quotation_customer_items WHERE quotation_customer_items.quotation_id = quotation_customers.quotation_id ) AS price"),
                        DB::raw("(SELECT sum(tax.`tax_amount`) FROM quotation_tax_customers
                        INNER JOIN tax ON quotation_tax_customers.tax_id = tax.tax_id
                        WHERE quotation_tax_customers.quotation_id = quotation_customers.quotation_id) AS tax"),
                        // DB::raw("(
                        //     (SELECT
                        //       SUM(price * quantity)
                        //     FROM
                        //       quotation_customer_items
                        //     WHERE quotation_customer_items.quotation_id = quotation_customers.quotation_id) * (
                        //       CONVERT((SELECT
                        //         tax.`tax_amount`
                        //       FROM
                        //         quotation_tax_customers
                        //         INNER JOIN tax
                        //           ON quotation_tax_customers.tax_id = tax.tax_id
                        //       WHERE quotation_tax_customers.quotation_id = quotation_customers.quotation_id),
                        //         INT) / 10
                        //     )  ) AS total"),
                        // DB::raw("(SELECT sum(payment_amount) FROM payment_customers WHERE payment_customers.quotation_id = quotation_customers.quotation_id AND payment_customers.status = 1) AS bill"),
                        'quotation_customers.customer_id',
                        DB::raw("CONCAT(customers.company,' - ', customers.first_name, ' ', customers.last_name) AS customer")
                    )
                    ->where('quotation_customers.status', 2);
        if(!empty($request->term)){
            $query = $query->where(function($q)use($request){{
                $q->where('quotation_customers.serial', 'LIKE', '%'.$request->term.'%')
                ->orWhere('customers.first_name', 'LIKE', "%{$request->term}%")
                ->orWhere('customers.company', 'LIKE', "%{$request->term}%");
            }});
        }

        if($request->has('customer_id')){
            $query = $query->where('quotation_customers.customer_id', $request->customer_id);
        }
        $query = $query->orderByDesc('serial');
        $count     = $query->count();
        $endCount  = $offset + $request->page_limit;
        $morePages =  $count > $endCount;
        $query     = $query->skip($offset)->take($request->page_limit)->get();

        $result = [
            'results'    => $query,
            'pagination' => [
                'more' => $morePages
            ]
        ];
        return response()->json($result);
    }

    public function qttSupplier(Request $request)
    {
        $offset = ($request->page - 1) * $request->page_limit;
        $result = [];
        $query = QuotationSupplier::join('suppliers', 'quotation_suppliers.supplier_id', '=', 'suppliers.supplier_id')
                    ->select(
                        'quotation_id AS id',
                        DB::raw('concat(
                            "QTT",
                            date_format(quotation_suppliers.quotation_date, "%y"),
                            LPAD(serial, 4, "0"),
                            coalesce(concat (" - ", suppliers.company, " - "), " - "),
                            suppliers.first_name
                        ) AS text'),
                        DB::raw("(SELECT SUM(price*quantity) FROM quotation_supplier_items WHERE quotation_supplier_items.quotation_id = quotation_suppliers.quotation_id ) AS price"),
                        DB::raw("(SELECT sum(tax.`tax_amount`) FROM quotation_tax_suppliers
                        INNER JOIN tax ON quotation_tax_suppliers.tax_id = tax.tax_id
                        WHERE quotation_tax_suppliers.quotation_id = quotation_suppliers.quotation_id) AS tax"),
                        'quotation_suppliers.supplier_id',
                        DB::raw("CONCAT(suppliers.company,' - ', suppliers.first_name, ' ', suppliers.last_name) AS supplier")
                    )
                    ->where('quotation_suppliers.status', 2);
        if(!empty($request->term)){
            $query = $query->where(function($q)use($request){{
                $q->where('quotation_suppliers.serial', 'LIKE', '%'.$request->term.'%')
                ->orWhere('suppliers.first_name', 'LIKE', "%{$request->term}%")
                ->orWhere('suppliers.company', 'LIKE', "%{$request->term}%");
            }});
        }

        if($request->filled('supplier')){
            $query = $query->where('quotation_suppliers.supplier_id', $request->supplier);
        }
        $query = $query->orderByDesc('serial');
        $count     = $query->count();
        $endCount  = $offset + $request->page_limit;
        $morePages =  $count > $endCount;
        $query     = $query->skip($offset)->take($request->page_limit)->get();

        $result = [
            'results'    => $query,
            'pagination' => [
                'more' => $morePages
            ]
        ];
        return response()->json($result);
    }

    public function invCustomer(Request $request)
    {
        $offset = ($request->page - 1) * $request->page_limit;
        $result = [];
        $query = InvoiceCustomer::join('customers', 'invoice_customers.customer_id', '=', 'customers.customer_id')
                    ->select(
                        'invoice_id AS id',
                        DB::raw('concat(
                            "INV",
                            date_format(invoice_customers.invoice_date, "%y"),
                            LPAD(serial, 4, "0"),
                            coalesce(concat (" - ", customers.company, " - "), " - "),
                            customers.first_name
                        ) AS text'),
                        'invoice_customers.customer_id',
                        'invoice_customers.payment',
                        DB::raw("CONCAT(customers.company,' - ', customers.first_name, ' ', customers.last_name) AS customer")
                    )
                    // ->whereIn('status', [1, 2]);
                    ->where('status', 2);
        if(!empty($request->term)){
            $query = $query->where(function($q) use($request){
                $q->where('serial', 'LIKE', '%'.$request->term.'%')
                        ->orWhere('customers.first_name', 'LIKE', "%{$request->term}%")
                        ->orWhere('customers.company', 'LIKE', "%{$request->term}%");
            });
            // $query = $query->where('invoice_id', 'LIKE', '%'.$request->term.'%')
            //                 ->where('customers.first_name', 'LIKE', "%{$request->term}%")
            //                 ->where('customers.company', 'LIKE', "%{$request->term}%");
        }
        if($request->filled('customer')){
            $query = $query->where('customers.customer_id', $request->customer);
        }
        $query = $query->orderByDesc('invoice_customers.created_at');

        $count     = $query->count();
        $endCount  = $offset + $request->page_limit;
        $morePages =  $count > $endCount;
        // $sql = $query->skip($offset)->take($request->page_limit);
        $query     = $query->skip($offset)->take($request->page_limit)->get();

        $result = [
            'results'    => $query,
            'pagination' => [
                'more' => $morePages
            ],
            // 'sql' => $sql->toSql()
        ];
        return response()->json($result);
    }
    public function invCustomerPayment(Request $request)
    {
        $offset = ($request->page - 1) * $request->page_limit;
        $result = [];
        $query = InvoiceCustomer::join('customers', 'invoice_customers.customer_id', '=', 'customers.customer_id')
                    ->select(
                        'invoice_id AS id',
                        'serial',
                        DB::raw('concat(
                            "INV",
                            date_format(invoice_customers.invoice_date, "%y"),
                            LPAD(serial, 4, "0"),
                            coalesce(concat (" - ", customers.company, " - "), " - "),
                            customers.first_name
                        ) AS text'),
                        'invoice_customers.customer_id',
                        DB::raw("CONCAT(customers.company,' - ', customers.first_name, ' ', customers.last_name) AS customer"),
                        DB::raw("(SELECT SUM(price*quantity) FROM invoice_customer_items WHERE invoice_customer_items.invoice_id = invoice_customers.invoice_id ) AS price"),
                        DB::raw("(SELECT tax.`tax_amount` FROM invoice_tax_customers
                        INNER JOIN tax ON invoice_tax_customers.tax_id = tax.tax_id
                        WHERE invoice_tax_customers.invoice_id = invoice_customers.invoice_id) AS tax"),
                        DB::raw("(SELECT IF(currency > 1, sum(payment_amount)/payment_customers.rate, sum(payment_amount)) FROM payment_customers WHERE payment_customers.invoice_id = invoice_customers.invoice_id AND payment_customers.status = 1) AS bill"),
                        "invoice_customers.currency",
                        "invoice_customers.rate",
                        "invoice_customers.serial",
                        "invoice_customers.invoice_date AS paymentDate"

                    )
                    ->where(['invoice_customers.payment' => 1, 'invoice_customers.status' => 2]);
        if(!empty($request->term)){
            $query = $query->where(function($q) use($request){
                $q->where('invoice_customers.serial', 'LIKE', "%{$request->term}%")
                ->orWhere('customers.first_name', 'LIKE', "%{$request->term}%")
                ->orWhere('customers.company', 'LIKE', "%{$request->term}%");
            });

            // $query = $query->orWhere('invoice_customers.serial', 'LIKE', "%{$request->term}%")
            //                 ->orWhere('customers.first_name', 'LIKE', "%{$request->term}%")
            //                 ->orWhere('customers.company', 'LIKE', "%{$request->term}%");
        }
        $query = $query->orderByDesc('invoice_customers.created_at');
        $count     = $query->count();
        $endCount  = $offset + $request->page_limit;
        $morePages =  $count > $endCount;
        $sql = $query->skip($offset)->take($request->page_limit);
        $query     = $query->skip($offset)->take($request->page_limit)->get();

        $result = [
            'results'    => $query,
            'pagination' => [
                'more' => $morePages
            ],
            // 'sql' => $sql->toSql()
        ];
        return response()->json($result);
    }

    public function invSupplier(Request $request)
    {
        $offset = ($request->page - 1) * $request->page_limit;
        $result = [];
        $query = InvoiceSupplier::join('suppliers', 'invoice_suppliers.supplier_id', '=', 'suppliers.supplier_id')
                    ->select(
                        'invoice_id AS id',
                        DB::raw('concat(
                            "INV",
                            date_format(invoice_suppliers.invoice_date, "%y"),
                            LPAD(serial, 4, "0"),
                            coalesce(concat (" - ", suppliers.company, " - "), " - "),
                            suppliers.first_name
                        ) AS text'),
                        'invoice_suppliers.supplier_id',
                        'invoice_suppliers.payment',
                        DB::raw("CONCAT(suppliers.company,' - ', suppliers.first_name, ' ', suppliers.last_name) AS supplier")
                    )
                    ->where('status', 2);
        if(!empty($request->term)){
            $query = $query->where(function($q) use($request){
                $q->where('serial', 'LIKE', '%'.$request->term.'%')
                        ->orWhere('suppliers.first_name', 'LIKE', "%{$request->term}%")
                        ->orWhere('suppliers.company', 'LIKE', "%{$request->term}%");
            });
        }

        if($request->filled('supplier')){
            $query = $query->where('invoice_suppliers.supplier_id', $request->supplier);
        }
        $query = $query->orderByDesc('invoice_suppliers.created_at');

        $count     = $query->count();
        $endCount  = $offset + $request->page_limit;
        $morePages =  $count > $endCount;
        // $sql = $query->skip($offset)->take($request->page_limit);
        $query     = $query->skip($offset)->take($request->page_limit)->get();

        $result = [
            'results'    => $query,
            'pagination' => [
                'more' => $morePages
            ],
            // 'sql' => $sql->toSql()
        ];
        return response()->json($result);
    }
    public function invSupplierPayment(Request $request)
    {
        $offset = ($request->page - 1) * $request->page_limit;
        $result = [];
        $query = InvoiceSupplier::join('suppliers', 'invoice_suppliers.supplier_id', '=', 'suppliers.supplier_id')
                    ->leftJoin('debit_note_suppliers', 'invoice_suppliers.invoice_id', '=', 'debit_note_suppliers.invoice_id')
                    ->select(
                        'invoice_suppliers.invoice_id AS id',
                        'invoice_suppliers.serial',
                        DB::raw('concat(
                            "INV",
                            date_format(invoice_suppliers.invoice_date, "%y"),
                            LPAD(invoice_suppliers.serial, 4, "0),
                            coalesce(concat (" - ", suppliers.company, " - "), " - "),
                            suppliers.first_name
                        ) AS text'),
                        'invoice_suppliers.supplier_id',
                        DB::raw("CONCAT(suppliers.company,' - ', suppliers.first_name, ' ', suppliers.last_name) AS supplier"),
                        DB::raw("(SELECT SUM(price*quantity) FROM invoice_supplier_items WHERE invoice_supplier_items.invoice_id = invoice_suppliers.invoice_id ) AS price"),
                        DB::raw("(SELECT SUM(payment_amount) FROM debit_note_allocated_credit_suppliers dnac where dnac.debit_note_id = debit_note_suppliers.id AND dnac.status = 1) AS debit_note_allocated"),
                        DB::raw("(SELECT tax.`tax_amount` FROM invoice_tax_suppliers
                        INNER JOIN tax ON invoice_tax_suppliers.tax_id = tax.tax_id
                        WHERE invoice_tax_suppliers.invoice_id = invoice_suppliers.invoice_id) AS tax"),
                        DB::raw("(SELECT IF(currency > 1, sum(payment_amount)/payment_suppliers.rate, sum(payment_amount)) FROM payment_suppliers WHERE payment_suppliers.invoice_id = invoice_suppliers.invoice_id AND payment_suppliers.status = 1) AS bill"),
                        "invoice_suppliers.currency",
                        "invoice_suppliers.rate",
                        "invoice_suppliers.serial",
                        "invoice_suppliers.invoice_date AS paymentDate"

                    )
                    ->where(['invoice_suppliers.payment' => 1, 'invoice_suppliers.status' => 2]);
        if(!empty($request->term)){
            $query = $query->where(function($q) use($request){
                $q->where('invoice_suppliers.serial', 'LIKE', "%{$request->term}%")
                ->orWhere('suppliers.first_name', 'LIKE', "%{$request->term}%")
                ->orWhere('suppliers.company', 'LIKE', "%{$request->term}%");
            });
        }
        $query = $query->orderByDesc('invoice_suppliers.created_at');
        $count     = $query->count();
        $endCount  = $offset + $request->page_limit;
        $morePages =  $count > $endCount;
        $sql = $query->skip($offset)->take($request->page_limit);
        $query     = $query->skip($offset)->take($request->page_limit)->get();

        $result = [
            'results'    => $query,
            'pagination' => [
                'more' => $morePages
            ],
            // 'sql' => $sql->toSql()
        ];
        return response()->json($result);
    }
    public function invSupplierDebitNote(Request $request)
    {
        $offset = ($request->page - 1) * $request->page_limit;
        $result = [];
        $query = InvoiceSupplier::join('suppliers', 'invoice_suppliers.supplier_id', '=', 'suppliers.supplier_id')
                    ->select(
                        'invoice_id AS id',
                        'serial',
                        DB::raw('concat(
                            "INV",
                            date_format(invoice_suppliers.invoice_date, "%y"),
                            LPAD(serial, 4, "0"),
                            coalesce(concat (" - ", suppliers.company, " - "), " - "),
                            suppliers.first_name
                        ) AS text'),
                        'invoice_suppliers.supplier_id',
                        DB::raw("CONCAT(suppliers.company,' - ', suppliers.first_name, ' ', suppliers.last_name) AS supplier"),
                        DB::raw("(SELECT SUM(price*quantity) FROM invoice_supplier_items WHERE invoice_supplier_items.invoice_id = invoice_suppliers.invoice_id ) AS price"),
                        DB::raw("(SELECT tax.`tax_amount` FROM invoice_tax_suppliers
                        INNER JOIN tax ON invoice_tax_suppliers.tax_id = tax.tax_id
                        WHERE invoice_tax_suppliers.invoice_id = invoice_suppliers.invoice_id) AS tax"),
                        DB::raw("(SELECT IF(currency > 1, sum(payment_amount)/payment_suppliers.rate, sum(payment_amount)) FROM payment_suppliers WHERE payment_suppliers.invoice_id = invoice_suppliers.invoice_id AND payment_suppliers.status = 1) AS bill"),
                        "invoice_suppliers.currency",
                        "invoice_suppliers.rate",
                        "invoice_suppliers.serial",
                        "invoice_suppliers.invoice_date AS paymentDate"

                    )
                    ->where(['invoice_suppliers.status' => 2]);
        if(!empty($request->term)){
            $query = $query->where(function($q) use($request){
                $q->where('invoice_suppliers.serial', 'LIKE', "%{$request->term}%")
                ->orWhere('suppliers.first_name', 'LIKE', "%{$request->term}%")
                ->orWhere('suppliers.company', 'LIKE', "%{$request->term}%");
            });
        }
        $query = $query->orderByDesc('invoice_suppliers.created_at');
        $count     = $query->count();
        $endCount  = $offset + $request->page_limit;
        $morePages =  $count > $endCount;
        // $sql = $query->skip($offset)->take($request->page_limit);
        $query     = $query->skip($offset)->take($request->page_limit)->get();

        $result = [
            'results'    => $query,
            'pagination' => [
                'more' => $morePages
            ],
            // 'sql' => $sql->toSql()
        ];
        return response()->json($result);
    }

    public function customerQuotationItems($id)
    {
        $query = QuotationCustomer::with('list_items')->find($id);
        return $query;
    }
    public function supplierPoItems($id)
    {
        $query = PurchaseOrderSupplier::with('list_items')->find($id);
        return $query;
    }

    public function customerBcDetails($id)
    {
        $query = BcCustomer::find($id);
        return $query;
    }

    public function customerQttDetails($id)
    {
        $query = QuotationCustomer::find($id);
        return $query;
    }

    public function supplierQttDetails($id)
    {
        $query = QuotationSupplier::find($id);
        return $query;
    }

    public function bcCustomer(Request $request)
    {
        $offset = ($request->page - 1) * $request->page_limit;
        $query = BcCustomer::join('customers', 'bc_customers.customer_id', '=', 'customers.customer_id')
                    ->select(
                        'bc_id AS id',
                        DB::raw('concat(
                            booking_ref,
                            date_format(bc_customers.created_at, "%y"),
                            LPAD(bc_customers.serial, 4, "0"),
                            coalesce(concat (" - ", customers.company, " - "), " - "),
                            customers.first_name
                        ) AS text'),
                        'bc_customers.customer_id',
                        DB::raw("CONCAT(customers.company,' - ', customers.first_name, ' ', customers.last_name) AS customer")
                    );
        if(!empty($request->term)){
            $query = $query->where(function($q)use($request){
                $q->where('bc_customers.serial', 'LIKE', '%'.$request->term.'%')
                    ->orWhere('bc_customers.booking_ref', 'LIKE', '%'.$request->term.'%')
                    ->orWhere('customers.first_name', 'LIKE', "%{$request->term}%")
                    ->orWhere('customers.company', 'LIKE', "%{$request->term}%");
            });
        }

        if($request->filled('quotation_id')){
            $query = $query->where('quotation_id', $request->quotation_id);
        }

        $count     = $query->count();
        $endCount  = $offset + $request->page_limit;
        $morePages =  $count > $endCount;
        $query     = $query->skip($offset)->take($request->page_limit)->get();

        $result = [
            'results'    => $query,
            'pagination' => [
                'more' => $morePages
            ]
        ];
        return response()->json($result);
    }

    public function groupAccount(Request $request)
    {
        $offset = ($request->page - 1) * $request->page_limit;
        $result = [];

        $query = ChartAccount::select('id AS id', DB::raw('CONCAT(id," ",name) AS text'))->where('is_group', 1);
        if(!empty($request->term)){
            $query = $query->where('name', 'LIKE', '%'.$request->term.'%');
        }

        $count     = $query->count();
        $endCount  = $offset + $request->page_limit;
        $morePages =  $count > $endCount;
        $query     = $query->skip($offset)->take($request->page_limit)->get();

        $result = [
            'results'    => $query,
            'pagination' => [
                'more' => $morePages
            ]
        ];
        return response()->json($result);
    }
    public function account(Request $request)
    {
        $offset = ($request->page - 1) * $request->page_limit;
        $result = [];

        $query = ChartAccount::select('id AS id', DB::raw('CONCAT(id," ",name) AS text'))
                ->where('is_group', 0);
        if(!empty($request->term)){
            $query = $query->where('name', 'LIKE', '%'.$request->term.'%')
                    ->orWhere('id', 'like', '%'.$request->term.'%');
        }
        if($request->has('lock')){
                $query = $query->whereIn('account_id', $request->lock);
        }

        $count     = $query->count();
        $endCount  = $offset + $request->page_limit;
        $morePages =  $count > $endCount;
        $query     = $query->skip($offset)->take($request->page_limit)->get();

        $result = [
            'results'    => $query,
            'pagination' => [
                'more' => $morePages
            ]
        ];
        return response()->json($result);
    }

    public function contacts(Request $request)
    {
        $offset = ($request->page - 1) * $request->page_limit;
        $result = [];

        $query = Contacts::select('id AS id', DB::raw('CONCAT(company," - ",name) AS text'));
        if(!empty($request->term)){
            $query = $query->where('company', 'LIKE', '%'.$request->term.'%')
                    ->where('name', 'LIKE', '%'.$request->term.'%');
        }
        $count     = $query->count();
        $endCount  = $offset + $request->page_limit;
        $morePages =  $count > $endCount;
        $query     = $query->skip($offset)->take($request->page_limit)->get();

        $result = [
            'results'    => $query,
            'pagination' => [
                'more' => $morePages
            ]
        ];
        return response()->json($result);
    }

    public function clasifications(Request $request)
    {
        $offset = ($request->page - 1) * $request->page_limit;
        $result = [];

        $query = JournalClasification::select('id AS id', 'name AS text');
        if(!empty($request->term)){
            $query = $query->where('name', 'LIKE', '%'.$request->term.'%');
        }
        $count     = $query->count();
        $endCount  = $offset + $request->page_limit;
        $morePages =  $count > $endCount;
        $query     = $query->skip($offset)->take($request->page_limit)->get();

        $result = [
            'results'    => $query,
            'pagination' => [
                'more' => $morePages
            ]
        ];
        return response()->json($result);
    }
}
