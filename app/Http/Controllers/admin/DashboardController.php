<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Customer;
use App\Models\InvoiceCustomer;
use App\Models\InvoiceSupplier;
use App\Models\SettingAccount;
use App\Models\Supplier;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class DashboardController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
            $lang = Auth::user()->language;
            setlocale(LC_ALL, $lang);
        }

        if (Auth::check()) {
            $countCustomers = Customer::whereNull('deleted_at')->count();
            $countSuppliers = Supplier::whereNull('deleted_at')->count();
            $countUnpaidInvoiceSuppliers = InvoiceSupplier::where('status', 1)->count();
            $countUnpaidInvoiceCustomers = InvoiceCustomer::where('status', 1)->count();

            return view('dashboard.homepage', [
                'countCustomers' => $countCustomers,
                'countSuppliers' => $countSuppliers,
                'countUnpaidInvoiceSuppliers' => $countUnpaidInvoiceSuppliers,
                'countUnpaidInvoiceCustomers' => $countUnpaidInvoiceCustomers
            ]);
        } else {
            return redirect('/login');
        }
    }

    public function company()
    {
        if(Auth::user()->menuroles == 'superadmin'){
            return view('dashboard.company');
        }else{
            return redirect('/');
        }
    }

    public function tableCompany()
    {
        $query = Company::select('id', 'name', 'created_at')->whereNull('deleted_at');
        return datatables()->of($query)->make(true);
    }

    public function storeCompany(Request $request)
    {
        $id = null;
        if($request->filled('id')){
            $company = Company::find($request->id);
        }else{
            $company = new Company;
            $id = Str::uuid();
            $company->id   = $id;
        }
        $company->name = $request->name;
        $company->save();

        if($id){
            $company->web_setting()->create([
                'currency' => 'IDR',
                'thousand_separator' => '.',
                'decimal_separator' => ',',
            ]);
            SettingAccount::create([
                'company_id' => $id,
                'sales_account_id'          => '4.1.00',
                'receivable_account_id'     => '1.1.03.01',
                'sales_return_account_id'   => '4.3.00',
                'sales_discount_account_id' => '4.2.00',
                'ppn_in_account_id'         => '1.1.06.01',

                'purchase_account_id'        => '5.2.01',
                'debt_account_id'            => '2.1.01.01',
                'sales_return_account_id'    => '5.2.03',
                'purchase_return_account_id' => '5.2.02',
                'purchase_profit_rate_account_id' => '7.2.00',
                'purchase_loss_rate_account_id' => '8.4.00'
            ]);
        }

        return response()->json(['status' => true, 'statusText' => 'success']);
    }

    public function switchCompany(Request $request, $id)
    {
        $company = Company::find($id);

        // $user_id = Auth::user()->id;
        // $user    = User::find($user_id);
        // $user->company_id = $company->id;
        // $user->save();
        $request->session()->put('company_id', $company->id);
        $request->session()->put('company_name', $company->name);


        return redirect('/');
    }
    public function showCompany($id)
    {
        $company = Company::find($id);
        return response()->json($company);
    }

    public function deleteCompany(Request $request)
    {
        Company::find($request->id)->delete();
        return response()->json(['status' => true, 'statusText' => 'success']);
    }
}
