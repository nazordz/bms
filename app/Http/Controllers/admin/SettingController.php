<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\WebSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class SettingController extends Controller
{
    public function index()
    {
        $setting = WebSetting::first();
        return view('dashboard.settings.index', ['setting' => $setting]);
    }
    public function edit(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'email'     => 'nullable|email',
            'phone'     => 'nullable|numeric',
            'currency'  => 'in:IDR,USD',
            'logo'      => 'image|max:6000',
            'header'    => 'image|max:6000',
            'footer'    => 'image|max:6000'
        ]);
        if($validator->fails()){
            return redirect('/setting')->withErrors($validator)->with('status-fail', Lang::get('dashboard.setting_failed'))->withInput();
        }
        $updateData = [
            'address'                => $request->address,
            'email'                  => $request->email,
            'phone'                  => $request->phone,
            'account_owner'          => $request->account_owner,
            'bank_account'           => $request->bank_account,
            'account'                => $request->account,
            'bank_code'              => $request->bank_code,
            'swift_code'             => $request->swift_code,
            'bank_account_secondary' => $request->bank_account_secondary,
            'account_secondary'      => $request->account_secondary,
            'bank_code_secondary'    => $request->bank_code_secondary,
            'swift_code_secondary'   => $request->swift_code_secondary,
            'currency'               => $request->currency,
            'behalf_of'              => $request->behalf_of,
            'behalf_of_position'     => $request->behalf_of_position,
            'thousand_separator'     => $request->thousand_separator,
            'decimal_separator'      => $request->decimal_separator
        ];
        if($request->hasFile('nav_logo')){
            $path = $request->nav_logo->storeAs("public/settingWeb", Str::random(5).'_'.$request->nav_logo->getClientOriginalName());
            $locationFile = Storage::url($path);
            $updateData['nav_logo'] = $locationFile;
        }
        if($request->hasFile('logo')){
            $path = $request->logo->storeAs("public/settingWeb", Str::random(5).'_'.$request->logo->getClientOriginalName());
            $locationFile = Storage::url($path);
            $updateData['logo'] = $locationFile;
        }
        if($request->hasFile('header')){
            $path = $request->header->storeAs("public/settingWeb", Str::random(5).'_'.$request->header->getClientOriginalName());
            $locationFile = Storage::url($path);
            $updateData['header'] = $locationFile;
        }
        if($request->hasFile('footer')){
            $path = $request->footer->storeAs("public/settingWeb", Str::random(5).'_'.$request->footer->getClientOriginalName());
            $locationFile = Storage::url($path);
            $updateData['footer'] = $locationFile;
        }
        WebSetting::where('company_id', session()->get('company_id'))->update($updateData);
        activity()->causedBy(Auth::id())->log('Web Setting updated!');
        return redirect('/setting')->with('status-success', Lang::get('dashboard.setting_updated'));
    }
}
