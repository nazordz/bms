<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\SettingAssetType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class AccountingAssetTypeController extends Controller
{
    public function settingAssetType()
    {
        return view('dashboard.accounting.asset.setting-type-asset.index');
    }

    public function tableAssetType()
    {
        $data = SettingAssetType::select(
            'uuid',
            'serial',
            'name',
            'shrinkage_method',
            'estimated_age',
            'depreciation_rates'
        );
        return datatables()->of($data)->make(true);
    }

    public function tableAssetStore(Request $request)
    {
        $request->validate([
            'name'               => 'required',
            'shrinkage_method'   => 'required',
            'estimated_age'      => 'required_if:shrinkage_method,Garis Lurus,Saldo Menurun',
            'depreciation_rates' => 'required_if:shrinkage_method,Garis Lurus,Saldo Menurun',
        ]);
        $serial = SettingAssetType::withTrashed()->count()+1;
        // $serial = '1';
        // for($i = 0; $i < 4; $i++){
        //     if($i == strlen($serial)){
        //         $res = abs($i-4)+strlen($serial);
        //         break;
        //     }
        // }
        // echo str_pad($serial, $res, '0', STR_PAD_LEFT);

        if($request->filled('uuid')){
            $asset = SettingAssetType::whereUuid($request->uuid)->first();
        }else{
            $asset = new SettingAssetType;
            $asset->uuid = Str::uuid();
            $asset->serial             = $serial;
        }
        $asset->name               = $request->name;
        $asset->shrinkage_method   = $request->shrinkage_method;
        $asset->estimated_age      = $request->estimated_age;
        $asset->depreciation_rates = $request->depreciation_rates;
        $asset->save();
        activity()->causedBy(Auth::id())->log('Asset type Created!');
        return ['status' => 'oke'];
    }

    public function showAssetType($uuid)
    {
        $data = SettingAssetType::select(
            'uuid',
            'serial',
            'name',
            'shrinkage_method',
            'estimated_age',
            'depreciation_rates'
        )
        ->where('uuid', $uuid)
        ->first();
        return $data;
    }

    public function deleteAssetType(Request $request)
    {
        SettingAssetType::whereUuid($request->uuid)->delete();
        activity()->causedBy(Auth::id())->log('Asset type deleted!');
        return ['status' => 'oke'];
    }
}
