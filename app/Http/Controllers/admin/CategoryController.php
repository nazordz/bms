<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function create(Request $request)
    {
        // $request->validate([
        //     'name' => 'required|max:150|unique:categories,name'
        // ]);
        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                'max:150',
                Rule::unique('categories')->where(function($query) use($request) {
                    return $query->where('name', $request->name)
                            ->where('company_id', session()->get('company_id'));
                })
            ]
        ]);

        $category = new Category;
        $category->name = $request->name;
        $category->save();
        activity()->causedBy(Auth::id())->log('Category created!');

        return response()->json([
            'status' => true,
            'statusText' => 'success',
            'data' => ['id' => $category->id, 'name' => $request->name]
        ]);
    }
}
