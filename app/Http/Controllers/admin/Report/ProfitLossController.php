<?php

namespace App\Http\Controllers\admin\Report;

use App\Http\Controllers\Controller;
use App\Models\ChartAccount;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class ProfitLossController extends Controller
{

    public function index()
    {
        return view('dashboard.report.profit-loss.index');
    }

    public function getProfitLoss(Request $request)
    {
        // pendapatan
        $rowPendapatan = ChartAccount::where('is_income', 1)->first();
        $coaPendapatan = $this->findGrandChildren($rowPendapatan->id);

        $pendapatan =  ChartAccount::with(['journal_details' => function($query)use($request){
            $query->whereHas('journal', function(Builder $query)use($request){
                $query->where('status', 2)->whereBetween('transaction_at', [$request->start_date, $request->to_date]);
            });
        }])
        ->whereIn('chart_accounts.id', $coaPendapatan)->get();
        $pendapatan = $pendapatan->map(function($list){
            $credit = $list->journal_details->sum('credit');
            $debit  = $list->journal_details->sum('debet');
            // $list->balance = ($list->default_position == 1) ? '('.abs($credit-$debit).')':abs($credit-$debit);
            $list->balance = abs($credit-$debit);
            return $list;
        });
        $total_debit_pendapatan = $pendapatan->sum(function($list){
            return $list->default_position == 2 ? $list->balance: 0;
        });
        $total_credit_pendapatan = $pendapatan->sum(function($list){
            return $list->default_position == 1 ? $list->balance: 0;
        });
        $total_pendapatan = 0;
        if($total_debit_pendapatan > $total_credit_pendapatan){
            $total_pendapatan = '('.number_format(abs($total_debit_pendapatan-$total_credit_pendapatan), 2, ',', '.').')';
        }elseif($total_debit_pendapatan < $total_credit_pendapatan) {
            $total_pendapatan = number_format(abs($total_debit_pendapatan-$total_credit_pendapatan), 2, ',', '.');
        }
        $data['pendapatan']['total_raw'] = abs($total_debit_pendapatan-$total_credit_pendapatan);
        $data['pendapatan']['total'] = $total_pendapatan;
        $data['pendapatan']['lists'] = $pendapatan;

        // beban
        $rowBeban = ChartAccount::where('is_expense', 1)->first();
        $coaBeban = $this->findGrandChildren($rowBeban->id);

        $beban_usaha = ChartAccount::with(['journal_details' => function($query)use($request){
            $query->whereHas('journal', function(Builder $query)use($request){
                $query->where('status', 2)->whereBetween('transaction_at', [$request->start_date, $request->to_date]);
            });
        }])
        ->whereIn('chart_accounts.id', $coaBeban)->get();
        $beban_usaha = $beban_usaha->map(function($list){
            $credit = $list->journal_details->sum('credit');
            $debit  = $list->journal_details->sum('debet');
            $list->balance = abs($credit-$debit);
            return $list;
        });
        $total_debit_beban_usaha = $beban_usaha->sum(function($list){
            return $list->default_position == 2 ? $list->balance: 0;
        });
        $total_credit_beban_usaha = $beban_usaha->sum(function($list){
            return $list->default_position == 1 ? $list->balance: 0;
        });
        $total_beban_usaha = 0;
        if($total_debit_beban_usaha > $total_credit_beban_usaha){
            $total_beban_usaha = number_format(abs($total_debit_beban_usaha-$total_credit_beban_usaha), 2, ',', '.');
        }elseif($total_debit_beban_usaha < $total_credit_beban_usaha) {
            $total_beban_usaha = '('.number_format(abs($total_debit_beban_usaha-$total_credit_beban_usaha), 2, ',', '.').')';
        }
        $data['beban_usaha']['total_raw'] = abs($total_debit_beban_usaha-$total_credit_beban_usaha);
        $data['beban_usaha']['total'] = $total_beban_usaha;
        $data['beban_usaha']['lists'] = $beban_usaha;

        // pendapatan diluar usaha
        $rowPendapatanDiluar = ChartAccount::where('is_non_business_income', 1)->first();
        $coaPendapatanDiluar = $this->findGrandChildren($rowPendapatanDiluar->id);
        $pendapatan_diluar_usaha = ChartAccount::with(['journal_details' => function($query)use($request){
            $query->whereHas('journal', function(Builder $query)use($request){
                $query->where('status', 2)->whereBetween('transaction_at', [$request->start_date, $request->to_date]);
            });
        }])
        // ->where('chart_accounts.account_id', '7.0.00')->get();
        ->whereIn('chart_accounts.id', $coaPendapatanDiluar)->get();
        $pendapatan_diluar_usaha = $pendapatan_diluar_usaha->map(function($list){
            $credit = $list->journal_details->sum('credit');
            $debit  = $list->journal_details->sum('debet');
            $list->balance = abs($credit-$debit);
            return $list;
        });
        $total_debit_pendapatan_diluar_usaha = $pendapatan_diluar_usaha->sum(function($list){
            return $list->default_position == 2 ? $list->balance: 0;
        });
        $total_credit_pendapatan_diluar_usaha = $pendapatan_diluar_usaha->sum(function($list){
            return $list->default_position == 1 ? $list->balance: 0;
        });
        $total_pendapatan_diluar_usaha = 0;
        if($total_debit_pendapatan_diluar_usaha > $total_credit_pendapatan_diluar_usaha){
            $total_pendapatan_diluar_usaha = '('.number_format(abs($total_debit_pendapatan_diluar_usaha-$total_credit_pendapatan_diluar_usaha), 2, ',', '.').')';
        }elseif($total_debit_pendapatan_diluar_usaha < $total_credit_pendapatan_diluar_usaha) {
            $total_pendapatan_diluar_usaha = number_format(abs($total_debit_pendapatan_diluar_usaha-$total_credit_pendapatan_diluar_usaha), 2, ',', '.');
        }
        $data['pendapatan_diluar_usaha']['total_raw'] = abs($total_debit_pendapatan_diluar_usaha-$total_credit_pendapatan_diluar_usaha);
        $data['pendapatan_diluar_usaha']['total'] = $total_pendapatan_diluar_usaha;
        $data['pendapatan_diluar_usaha']['lists'] = $pendapatan_diluar_usaha;

        // beban diluar usaha
        $rowBebanDiluar = ChartAccount::where('is_out_of_business_expense', 1)->first();
        $coaBebanDiluar = $this->findGrandChildren($rowBebanDiluar->id);
        $beban_diluar_usaha = ChartAccount::with(['journal_details' => function($query)use($request){
            $query->whereHas('journal', function(Builder $query)use($request){
                $query->where('status', 2)->whereBetween('transaction_at', [$request->start_date, $request->to_date]);
            });
        }])
        // ->where('chart_accounts.account_id', '8.0.00')->get();
        ->whereIn('chart_accounts.id', $coaBebanDiluar)->get();
        $beban_diluar_usaha = $beban_diluar_usaha->map(function($list){
            $credit = $list->journal_details->sum('credit');
            $debit  = $list->journal_details->sum('debet');
            $list->balance = abs($credit-$debit);
            return $list;
        });
        $total_debit_beban_diluar_usaha = $beban_diluar_usaha->sum(function($list){
            return $list->default_position == 2 ? $list->balance: 0;
        });
        $total_credit_beban_diluar_usaha = $beban_diluar_usaha->sum(function($list){
            return $list->default_position == 1 ? $list->balance: 0;
        });
        $total_beban_diluar_usaha = 0;
        if($total_debit_beban_diluar_usaha > $total_credit_beban_diluar_usaha){
            $total_beban_diluar_usaha = number_format(abs($total_debit_beban_diluar_usaha-$total_credit_beban_diluar_usaha), 2, ',', '.');
        }elseif($total_debit_beban_diluar_usaha < $total_credit_beban_diluar_usaha) {
            $total_beban_diluar_usaha = '('.number_format(abs($total_debit_beban_diluar_usaha-$total_credit_beban_diluar_usaha), 2, ',', '.').')';
        }
        $data['beban_diluar_usaha']['total_raw'] = abs($total_debit_beban_diluar_usaha-$total_credit_beban_diluar_usaha);
        $data['beban_diluar_usaha']['total'] = $total_beban_diluar_usaha;
        $data['beban_diluar_usaha']['lists'] = $beban_diluar_usaha;
        return $data;
    }

    public function findGrandChildren($account_id)
    {
        $coa = $this->findGrandChildrenRecursively($account_id);
        $coa = collect($coa)->flatten();
        return $coa->values()->all();
    }

    public function findGrandChildrenRecursively($account_id)
    {
        $data = [];
        $coa = ChartAccount::where('account_id', $account_id)->get();
        if(count($coa)){
            foreach ($coa as $key => $value) {
                $data[] = $this->findGrandChildrenRecursively($value->id);
            }
        }else{
            $data = $account_id;
        }
        return $data;
    }
}
