<?php

namespace App\Http\Controllers\admin\Report;

use App\Http\Controllers\Controller;
use App\Models\ChartAccount;
use App\Models\Journal;
use App\Models\WebSetting;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class CashflowController extends Controller
{
    public function index()
    {
        return view('dashboard.report.cashflow.index');
    }
    public function printCashflow(Request $request)
    {
        $web    = WebSetting::with('company')->first();
        return view('dashboard.report.cashflow.print', ['web' => $web, 'start_date' => $request->start_date, 'to_date' => $request->to_date]);
    }

    public function getCashflow(Request $request)
    {

        $data['labarugi']             = $this->getProfitLoss($request->start_date, $request->to_date);
        $data['akumulasi_penyusutan'] = $this->depreciationAccumulated($request->start_date, $request->to_date);
        $data['aset_lancar']          = $this->getCurrentAsset($request->start_date, $request->to_date);
        $data['labilitas']            = $this->getLability($request->start_date, $request->to_date);
        $data['aset_tetap']           = $this->getFixedAsset($request->start_date, $request->to_date);
        $data['kas_awal']             = $this->initialCash($request->start_date);
        $data['laba_ditahan']         = $this->retainedEarning($request->start_date);

        // total operasi
        if($data['labarugi']['is_normal'] === $data['akumulasi_penyusutan']['is_normal']){
            $data['total_operation'] = $data['labarugi']['total'] + $data['akumulasi_penyusutan']['net_change'];
        }else {
            $data['total_operation'] = $data['labarugi']['total'] - $data['akumulasi_penyusutan']['net_change'];
        }

        // kas bersih
        $kas_bersih  = $data['total_operation'];
        $perhitungan = [
            [
                'nominal'   => $data['aset_lancar']['nominal'],
                'is_normal' => $data['aset_lancar']['is_normal'],
            ],
            [
                'nominal'   => $data['labilitas']['nominal'],
                'is_normal' => $data['labilitas']['is_normal'],
            ],
            [
                'nominal'   => $data['aset_tetap']['nominal'],
                'is_normal' => $data['aset_tetap']['is_normal'],
            ],
            [
                'nominal'   => $data['laba_ditahan']['total'],
                'is_normal' => $data['laba_ditahan']['is_normal'],
            ]
        ];
        foreach ($perhitungan as $key => $value) {
            if($value['is_normal']){
                $kas_bersih += $value['nominal'];
            }else $kas_bersih -= $value['nominal'];
        }
        $data['kas_bersih'] = [
            'is_normal' => $kas_bersih >= 0,
            'nominal'   => abs($kas_bersih)
        ];

        // kas akhir
        $kas_akhir = 0;
        if($data['kas_bersih']['is_normal']){
            $kas_akhir += $data['kas_bersih']['nominal'];
        }else{
            $kas_akhir -= $data['kas_bersih']['nominal'];
        }
        if($data['kas_awal']['is_normal']){
            $kas_akhir += $data['kas_awal']['nominal'];
        }else {
            $kas_akhir -= $data['kas_awal']['nominal'];
        }
        $data['kas_akhir'] = [
            'is_normal'=> $kas_akhir >= 0,
            'nominal'  => abs($kas_akhir),
        ];


        return $data;
    }

    // laba rugi
    public function getProfitLoss($start_date, $to_date)
    {
        // pendapatan
        $rowPendapatan = ChartAccount::where('is_income', 1)->first();
        $coaPendapatan = $this->findGrandChildren($rowPendapatan->id);

        $pendapatan =  ChartAccount::with(['journal_details' => function($query)use($start_date, $to_date){
            $query->whereHas('journal', function(Builder $query)use($start_date, $to_date){
                $query->where('status', 2)->whereBetween('transaction_at', [$start_date, $to_date]);
            });
        }])
        ->whereIn('chart_accounts.id', $coaPendapatan)->get();
        $pendapatan = $pendapatan->map(function($list){
            $credit = $list->journal_details->sum('credit');
            $debit  = $list->journal_details->sum('debet');
            $list->balance = abs($credit-$debit);
            return $list;
        });
        $total_debit_pendapatan = $pendapatan->sum(function($list){
            return $list->default_position == 2 ? $list->balance: 0;
        });
        $total_credit_pendapatan = $pendapatan->sum(function($list){
            return $list->default_position == 1 ? $list->balance: 0;
        });
        $data['pendapatan']['total_raw'] = abs($total_debit_pendapatan-$total_credit_pendapatan);

        // beban
        $rowBeban = ChartAccount::where('is_expense', 1)->first();
        $coaBeban = $this->findGrandChildren($rowBeban->id);

        $beban_usaha = ChartAccount::with(['journal_details' => function($query)use($start_date, $to_date){
            $query->whereHas('journal', function(Builder $query)use($start_date, $to_date){
                $query->where('status', 2)->whereBetween('transaction_at', [$start_date, $to_date]);
            });
        }])
        ->whereIn('chart_accounts.id', $coaBeban)->get();
        $beban_usaha = $beban_usaha->map(function($list){
            $credit = $list->journal_details->sum('credit');
            $debit  = $list->journal_details->sum('debet');
            $list->balance = abs($credit-$debit);
            return $list;
        });
        $total_debit_beban_usaha = $beban_usaha->sum(function($list){
            return $list->default_position == 2 ? $list->balance: 0;
        });
        $total_credit_beban_usaha = $beban_usaha->sum(function($list){
            return $list->default_position == 1 ? $list->balance: 0;
        });
        $data['beban_usaha']['total_raw'] = abs($total_debit_beban_usaha-$total_credit_beban_usaha);

        // pendapatan diluar usaha
        $rowPendapatanDiluar = ChartAccount::where('is_non_business_income', 1)->first();
        $coaPendapatanDiluar = $this->findGrandChildren($rowPendapatanDiluar->id);
        $pendapatan_diluar_usaha = ChartAccount::with(['journal_details' => function($query)use($start_date, $to_date){
            $query->whereHas('journal', function(Builder $query)use($start_date, $to_date){
                $query->where('status', 2)->whereBetween('transaction_at', [$start_date, $to_date]);
            });
        }])
        ->whereIn('chart_accounts.id', $coaPendapatanDiluar)->get();
        $pendapatan_diluar_usaha = $pendapatan_diluar_usaha->map(function($list){
            $credit = $list->journal_details->sum('credit');
            $debit  = $list->journal_details->sum('debet');
            $list->balance = abs($credit-$debit);
            return $list;
        });
        $total_debit_pendapatan_diluar_usaha = $pendapatan_diluar_usaha->sum(function($list){
            return $list->default_position == 2 ? $list->balance: 0;
        });
        $total_credit_pendapatan_diluar_usaha = $pendapatan_diluar_usaha->sum(function($list){
            return $list->default_position == 1 ? $list->balance: 0;
        });
        $data['pendapatan_diluar_usaha']['total_raw'] = abs($total_debit_pendapatan_diluar_usaha-$total_credit_pendapatan_diluar_usaha);

        // beban diluar usaha
        $rowBebanDiluar = ChartAccount::where('is_out_of_business_expense', 1)->first();
        $coaBebanDiluar = $this->findGrandChildren($rowBebanDiluar->id);
        $beban_diluar_usaha = ChartAccount::with(['journal_details' => function($query)use($start_date, $to_date){
            $query->whereHas('journal', function(Builder $query)use($start_date, $to_date){
                $query->where('status', 2)->whereBetween('transaction_at', [$start_date, $to_date]);
            });
        }])
        ->whereIn('chart_accounts.id', $coaBebanDiluar)->get();
        $beban_diluar_usaha = $beban_diluar_usaha->map(function($list){
            $credit = $list->journal_details->sum('credit');
            $debit  = $list->journal_details->sum('debet');
            $list->balance = abs($credit-$debit);
            return $list;
        });
        $total_debit_beban_diluar_usaha = $beban_diluar_usaha->sum(function($list){
            return $list->default_position == 2 ? $list->balance: 0;
        });
        $total_credit_beban_diluar_usaha = $beban_diluar_usaha->sum(function($list){
            return $list->default_position == 1 ? $list->balance: 0;
        });

        $data['beban_diluar_usaha']['total_raw'] = abs($total_debit_beban_diluar_usaha - $total_credit_beban_diluar_usaha);

        $laba_usaha = abs($data['pendapatan']['total_raw'] - $data['beban_usaha']['total_raw']);
        $type_pendapatan = $rowPendapatan->default_position == 2 ? 'debit':'credit';

        $data['total_bersih']['raw'] = $laba_usaha;
        if($data['pendapatan']['total_raw'] < $data['beban_diluar_usaha']['total_raw']){
            $data['total_bersih']['nominal'] = '('.$data['total_bersih']['raw'].')';
            $type_pendapatan = $type_pendapatan == 'debit'?'credit':'debit';
        }else{
            $data['total_bersih']['nominal'] = $data['total_bersih']['raw'];
        }
        //diluar
        $hasil_diluar = $data['pendapatan_diluar_usaha']['total_raw'] - $data['beban_diluar_usaha']['total_raw'];
        $type_diluar  = $rowPendapatanDiluar->position;

        $hasil_akhir = [
            'type' => '',
            'total' => 0,
            'is_normal' => 1
        ];
        if($type_pendapatan == $type_diluar){
            $hasil_akhir['total'] = $laba_usaha + $hasil_diluar;
            $hasil_akhir['type']  = $type_pendapatan;
        }else{
            $hasil_akhir['total'] =  $laba_usaha - $hasil_diluar;
            $hasil_akhir['type'] = $type_pendapatan == 'debit'?'credit': 'debit';
        }

        if($hasil_akhir['type'] == 'credit') $hasil_akhir['is_normal'] = false;
        $data['hasil_akhir'] = $hasil_akhir;

        return $hasil_akhir;
    }

    // akumulasi penyusutan
    public function depreciationAccumulated($start_date, $to_date)
    {
        $rowDepreciationAndAmorization = ChartAccount::where('is_depreciation_and_amortization', 1)->first();

        $accumulated_account = $this->getNetChange($start_date, $to_date, $rowDepreciationAndAmorization->id);

        return $accumulated_account;
    }
    // aset lancar
    public function getCurrentAsset($start_date, $to_date)
    {
        $rowAset = ChartAccount::where('is_current_asset', 1)->first();
        $aset = $this->findGrandChildren($rowAset->id);
        $assets = ChartAccount::whereIn('id', $aset)->where('is_equivalent_cash', 0)->where('is_accumulation_of_fixed_asset', 0)->get();
        $asetLancar['lists'] = $assets->map(function($aset)use($start_date, $to_date){
            $aset['balance'] = $this->getNetChange($start_date, $to_date, $aset->id);
            return $aset;
        });

        $totalNormal = collect($asetLancar['lists'])->sum(function($list){
            return $list->balance['is_normal'] ? $list->balance['net_change'] : 0;
        });
        $totalNotNormal = collect($asetLancar['lists'])->sum(function($list){
            return !$list->balance['is_normal'] ? $list->balance['net_change'] : 0;
        });
        $asetLancar['nominal']   = abs($totalNormal-$totalNotNormal);
        $asetLancar['is_normal'] = $totalNormal >= $totalNotNormal;
        return $asetLancar;
    }

    // aset tetap
    public function getFixedAsset($start_date, $to_date)
    {
        $rowFixed = ChartAccount::where('is_fixed_asset', 1)->first();
        $fixed = $this->findGrandChildren($rowFixed->id);
        $fixed = ChartAccount::whereIn('id', $fixed)->where('is_accumulation_of_fixed_asset', 0)->get();
        $fixed_assets['lists'] = $fixed->map(function($fixed)use($start_date, $to_date){
            $fixed['balance'] = $this->getNetChange($start_date, $to_date, $fixed->id);
            return $fixed;
        });
        $totalNormal = collect($fixed_assets['lists'])->sum(function($list){
            return $list->balance['is_normal'] ? $list->balance['net_change'] : 0;
        });
        $totalNotNormal = collect($fixed_assets['lists'])->sum(function($list){
            return !$list->balance['is_normal'] ? $list->balance['net_change'] : 0;
        });
        $fixed_assets['nominal']   = abs($totalNormal-$totalNotNormal);
        $fixed_assets['is_normal'] = $totalNormal >= $totalNotNormal;
        return $fixed_assets;
    }

    // labilitas
    public function getLability($start_date, $to_date)
    {
        $rowLability = ChartAccount::where('is_lability', 1)->first();
        $labil = $this->findGrandChildren($rowLability->id);
        $labil = ChartAccount::whereIn('id', $labil)->get();
        $labilitas['lists'] = $labil->map(function($liability)use($start_date, $to_date){
            $liability['balance'] = $this->getNetChange($start_date, $to_date, $liability->id, 'kanan');
            return $liability;
        });
        $totalNormal = collect($labilitas['lists'])->sum(function($list){
            return $list->balance['is_normal'] ? $list->balance['net_change'] : 0;
        });
        $totalNotNormal = collect($labilitas['lists'])->sum(function($list){
            return !$list->balance['is_normal'] ? $list->balance['net_change'] : 0;
        });
        $labilitas['nominal']   = abs($totalNormal-$totalNotNormal);
        $labilitas['is_normal'] = $totalNormal >= $totalNotNormal;
        return $labilitas;
    }

    // kas awal
    public function initialCash($start_date)
    {
        $rowInitialCash = ChartAccount::select('id')->where('is_equivalent_cash', 1)->get()
                            ->map(function($coa){
                                return $coa['id'];
                            });
        $condition = "journals.transaction_at < '{$start_date}'";
        $akun = ChartAccount::whereIn('id', $rowInitialCash)->get();

        $initialCash['lists'] = $akun->map(function($cash)use($condition){
            $cash['balance'] = $this->getBalance($cash->id, $condition, $cash->position);
            return $cash;
        });

        $cashTotal['total'] = 0;
        foreach ($initialCash['lists'] as $key => $value) {
            $balance = $value->balance;
            if($balance['is_normal']){
                $cashTotal['total'] += $balance['total'];
            }else{
                $cashTotal['total'] -= $balance['total'];
            }
        }
        $cashTotal['is_normal'] = $cashTotal['total'] >= 0;
        $cashTotal['nominal']   = abs($cashTotal['total']);

        return $cashTotal;
    }

    // laba ditahan
    public function retainedEarning($start_date)
    {
        // pendapatan
        $rowPendapatan = ChartAccount::where('is_income', 1)->first();
        $coaPendapatan = $this->findGrandChildren($rowPendapatan->id);

        $pendapatan =  ChartAccount::with(['journal_details' => function($query)use($start_date){
            $query->whereHas('journal', function(Builder $query)use($start_date){
                $query->where('status', 2)->where('transaction_at', '<', $start_date);
            });
        }])
        ->whereIn('chart_accounts.id', $coaPendapatan)->get();
        $pendapatan = $pendapatan->map(function($list){
            $credit = $list->journal_details->sum('credit');
            $debit  = $list->journal_details->sum('debet');
            $list->balance = abs($credit-$debit);
            return $list;
        });
        $total_debit_pendapatan = $pendapatan->sum(function($list){
            return $list->default_position == 2 ? $list->balance: 0;
        });
        $total_credit_pendapatan = $pendapatan->sum(function($list){
            return $list->default_position == 1 ? $list->balance: 0;
        });
        $data['pendapatan']['total_raw'] = abs($total_debit_pendapatan-$total_credit_pendapatan);

        // beban
        $rowBeban = ChartAccount::where('is_expense', 1)->first();
        $coaBeban = $this->findGrandChildren($rowBeban->id);

        $beban_usaha = ChartAccount::with(['journal_details' => function($query)use($start_date){
            $query->whereHas('journal', function(Builder $query)use($start_date){
                $query->where('status', 2)->where('transaction_at', '<', $start_date);
            });
        }])
        ->whereIn('chart_accounts.id', $coaBeban)->get();
        $beban_usaha = $beban_usaha->map(function($list){
            $credit = $list->journal_details->sum('credit');
            $debit  = $list->journal_details->sum('debet');
            $list->balance = abs($credit-$debit);
            return $list;
        });
        $total_debit_beban_usaha = $beban_usaha->sum(function($list){
            return $list->default_position == 2 ? $list->balance: 0;
        });
        $total_credit_beban_usaha = $beban_usaha->sum(function($list){
            return $list->default_position == 1 ? $list->balance: 0;
        });
        $data['beban_usaha']['total_raw'] = abs($total_debit_beban_usaha-$total_credit_beban_usaha);

        // pendapatan diluar usaha
        $rowPendapatanDiluar = ChartAccount::where('is_non_business_income', 1)->first();
        $coaPendapatanDiluar = $this->findGrandChildren($rowPendapatanDiluar->id);
        $pendapatan_diluar_usaha = ChartAccount::with(['journal_details' => function($query)use($start_date){
            $query->whereHas('journal', function(Builder $query)use($start_date){
                $query->where('status', 2)->where('transaction_at', '<', $start_date);
            });
        }])
        ->whereIn('chart_accounts.id', $coaPendapatanDiluar)->get();
        $pendapatan_diluar_usaha = $pendapatan_diluar_usaha->map(function($list){
            $credit = $list->journal_details->sum('credit');
            $debit  = $list->journal_details->sum('debet');
            $list->balance = abs($credit-$debit);
            return $list;
        });
        $total_debit_pendapatan_diluar_usaha = $pendapatan_diluar_usaha->sum(function($list){
            return $list->default_position == 2 ? $list->balance: 0;
        });
        $total_credit_pendapatan_diluar_usaha = $pendapatan_diluar_usaha->sum(function($list){
            return $list->default_position == 1 ? $list->balance: 0;
        });
        $data['pendapatan_diluar_usaha']['total_raw'] = abs($total_debit_pendapatan_diluar_usaha-$total_credit_pendapatan_diluar_usaha);

        // beban diluar usaha
        $rowBebanDiluar = ChartAccount::where('is_out_of_business_expense', 1)->first();
        $coaBebanDiluar = $this->findGrandChildren($rowBebanDiluar->id);
        $beban_diluar_usaha = ChartAccount::with(['journal_details' => function($query)use($start_date){
            $query->whereHas('journal', function(Builder $query)use($start_date){
                $query->where('status', 2)->where('transaction_at', '<', $start_date);
            });
        }])
        ->whereIn('chart_accounts.id', $coaBebanDiluar)->get();
        $beban_diluar_usaha = $beban_diluar_usaha->map(function($list){
            $credit = $list->journal_details->sum('credit');
            $debit  = $list->journal_details->sum('debet');
            $list->balance = abs($credit-$debit);
            return $list;
        });
        $total_debit_beban_diluar_usaha = $beban_diluar_usaha->sum(function($list){
            return $list->default_position == 2 ? $list->balance: 0;
        });
        $total_credit_beban_diluar_usaha = $beban_diluar_usaha->sum(function($list){
            return $list->default_position == 1 ? $list->balance: 0;
        });

        $data['beban_diluar_usaha']['total_raw'] = abs($total_debit_beban_diluar_usaha - $total_credit_beban_diluar_usaha);

        $laba_usaha = abs($data['pendapatan']['total_raw'] - $data['beban_usaha']['total_raw']);
        $type_pendapatan = $rowPendapatan->default_position == 2 ? 'debit':'credit';

        $data['total_bersih']['raw'] = $laba_usaha;
        if($data['pendapatan']['total_raw'] < $data['beban_diluar_usaha']['total_raw']){
            $data['total_bersih']['nominal'] = '('.$data['total_bersih']['raw'].')';
            $type_pendapatan = $type_pendapatan == 'debit'?'credit':'debit';
        }else{
            $data['total_bersih']['nominal'] = $data['total_bersih']['raw'];
        }
        //diluar
        $hasil_diluar = $data['pendapatan_diluar_usaha']['total_raw'] - $data['beban_diluar_usaha']['total_raw'];
        $type_diluar  = $rowPendapatanDiluar->default_position == 2 ? 'debit':'credit';

        $hasil_akhir = [
            'type' => '',
            'total' => 0
        ];
        if($type_pendapatan == $type_diluar){
            $hasil_akhir['total'] = $laba_usaha + $hasil_diluar;
            $hasil_akhir['type']  = $type_pendapatan;
        }else{
            $hasil_akhir['total'] =  $laba_usaha - $hasil_diluar;
            $hasil_akhir['type'] = $type_pendapatan == 'debit'?'credit': 'debit';
        }
        $data['hasil_akhir'] = $hasil_akhir;

        // prive
        $akun_prive = ChartAccount::where('is_prive', 1)->first();
        $prive_id = $this->findGrandChildren($akun_prive->id);

        $akun = ChartAccount::whereIn('id', $prive_id)->get();
        $condition = "journals.transaction_at < '{$start_date}'";

        $retained['lists'] = $akun->map(function($cash)use($condition){
            $cash['balance'] = $this->getBalance($cash->id, $condition, $cash->position);
            return $cash;
        });

        $profit['total'] = 0;
        foreach ($retained['lists'] as $key => $value) {
            $balance = $value->balance;
            if($balance['is_normal']){
                $profit['total'] += $balance['total'];
            }else{
                $profit['total'] -= $balance['total'];
            }
        }
        $profit['is_normal'] = $profit['total'] >= 0;
        $profit['total']     = abs($profit['total']);

        $laba_ditahan['total'] = $hasil_akhir['total'] - $profit['total'];
        $laba_ditahan['is_normal'] = $hasil_akhir['total'] >= $profit['total'];
        return $laba_ditahan;
    }

    // get net change
    public function getNetChange($start_date, $to_date, $account_id, $setting = 'kiri')
    {
        $condition_before = "date_format(journals.transaction_at, '%Y-%m-%d') < '{$start_date}'";
        $condition_now    = "date_format(journals.transaction_at, '%Y-%m-%d') BETWEEN '{$start_date}' and '{$to_date}'";
        $akun = ChartAccount::find($account_id);
        $before = $this->getBalance($account_id, $condition_before, $akun->position);
        $now    = $this->getBalance($account_id, $condition_now, $akun->position);

        $is_normal  = $setting == 'kiri'? $before['total'] >= $now['total']: $now['total'] >= $before['total'];
        $net_change = abs($before['total'] - $now['total']);
        if(($before['is_normal'] && $now['is_normal']) || ($before['is_normal'] === false && $now['is_normal'] === false)){
            $net_change = abs($before['total'] + $now['total']);
        }

        $res = [
            'position'   => $akun->position,
            'before'     => $before,
            'now'        => $now,
            'net_change' => $net_change,
            'is_normal'  => $is_normal
        ];
        return $res;
    }

    // get balance
    public function getBalance($account_id, $condition, $position)
    {
        $accounts = $this->findGrandChildren($account_id);
        $result = [
            'total'     => 0,
            'is_normal' => 1,
        ];
        $account_balances = Journal::withoutGlobalScopes()
                            ->where('journals.status', 2)
                            ->selectRaw('SUM(credit) AS credit, SUM(debet) AS debit')
                            ->join('journal_details', 'journals.id', '=', 'journal_details.journal_id')
                            ->whereIn('journal_details.account_id', $accounts)
                            ->whereRaw($condition)
                            ->where('journals.company_id', session()->get('company_id'))
                            ->first();
        if($account_balances){
            $result['total'] = abs($account_balances->debit - $account_balances->credit);
            if($position == 'debit' && $account_balances->credit > $account_balances->debit){
                $result['is_normal'] = 0;
            }
            elseif($position == 'credit' && $account_balances->debit > $account_balances->credit){
                $result['is_normal'] = 0;
            }
        }

        return $result;
    }

    public function findGrandChildren($account_id)
    {
        $coa = $this->findGrandChildrenRecursively($account_id);
        $coa = collect($coa)->flatten();
        return $coa->values()->all();
    }

    public function findGrandChildrenRecursively($account_id)
    {
        $data = [];
        $coa = ChartAccount::where('account_id', $account_id)->get();
        if(count($coa)){
            foreach ($coa as $key => $value) {
                $data[] = $this->findGrandChildrenRecursively($value->id);
            }
        }else{
            $data = $account_id;
        }
        return $data;
    }

}
