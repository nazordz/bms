<?php

namespace App\Http\Controllers\admin\Report;

use App\Exports\SellingByBcExport;
use App\Exports\SellingByInvoiceExport;
use App\Exports\SellingByReceivableExport;
use App\Http\Controllers\Controller;
use App\Models\BcCustomer;
use App\Models\InvoiceCustomer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SellingController extends Controller
{
    public function index()
    {
        return view('dashboard.report.sales.index');
    }

    public function listInvoice()
    {
        return view('dashboard.report.sales.list-invoice');
    }

    public function tableInvoice(Request $request)
    {
        $data = InvoiceCustomer::join('customers', 'invoice_customers.customer_id', '=', 'customers.customer_id')
        ->leftJoin('invoice_tax_customers', 'invoice_customers.invoice_id', '=', 'invoice_tax_customers.invoice_id')
        ->leftJoin('tax', 'invoice_tax_customers.tax_id', '=', 'tax.tax_id')
        ->select('invoice_customers.*',
            'customers.first_name',
            'customers.last_name',
            'customers.company',
            'invoice_customers.payment',
            'tax.*',
            // DB::raw('(select IF(payment_customers.currency > 1, SUM(payment_amount*rate), SUM(payment_amount)) from payment_customers where invoice_customers.invoice_id = payment_customers.invoice_id) as paid'),
            DB::raw('(select SUM(payment_amount) from payment_customers where invoice_customers.invoice_id = payment_customers.invoice_id) as paid'),
            DB::raw('(
                SELECT SUM(price*quantity) FROM invoice_customer_items WHERE invoice_customers.invoice_id = invoice_customer_items.invoice_id
            ) AS subtotal')
        )
        ->where('invoice_customers.status', 2);

        if($request->filled('invoice_date')){
            $data = $data->where('invoice_customers.invoice_date', $request->invoice_date);
        }
        if($request->filled(['start_date', 'to_date'])){
            $data = $data->whereBetween('invoice_customers.invoice_date', [$request->start_date, $request->to_date]);
        }
        if($request->filled('customer')){
            $data = $data->where(function($query)use($request){
                $query->where('customers.first_name', 'like', '%'.$request->customer.'%')
                        ->orWhere('customers.company', 'like', '%'.$request->customer.'%')
                        ->orWhere('customers.last_name', 'like', '%'.$request->customer.'%');
            });
        }
        return $data->get();
    }
    public function printInvoice(Request $request)
    {
        $exportInvoice = new SellingByInvoiceExport(
            $request->invoice_date,
            $request->start_date,
            $request->to_date,
            $request->customer
        );
        return $exportInvoice->download('sales-invoice.xlsx');
    }

    public function listBc()
    {
        return view('dashboard.report.sales.list-bc');
    }

    public function tableBc(Request $request)
    {
        $data = BcCustomer::join('customers', 'bc_customers.customer_id', '=', 'customers.customer_id');
        if($request->filled('customer')){
            $data = $data->where('customers.first_name', $request->customer)
            ->orWhere('customers.company', $request->customer)
            ->orWhere('customers.last_name', $request->customer);
        }
        if($request->filled(['start_date', 'to_date'])){
            $data = $data->whereBetween('bc_customers.created_at', [date_format(date_create($request->start_date), 'Y-m-d')." 00:00:00", date_format(date_create($request->to_date), 'Y-m-d')." 23:59:59"]);
        }

        return $data->get();
    }
    public function printBc(Request $request)
    {
        $data = BcCustomer::join('customers', 'bc_customers.customer_id', '=', 'customers.customer_id');
        if($request->filled('customer')){
            $data = $data->where('customers.first_name', $request->customer)
            ->orWhere('customers.company', $request->customer)
            ->orWhere('customers.last_name', $request->customer);
        }
        if($request->filled(['start_date', 'to_date'])){
            $data = $data->whereBetween('bc_customers.created_at', [date_format(date_create($request->start_date), 'Y-m-d')." 00:00:00", date_format(date_create($request->to_date), 'Y-m-d')." 23:59:59"]);
        }

        // return $data->get();
        return (new SellingByBcExport($request->start_date, $request->to_date, $request->customer))->download('sales-booking-confirmation.xlsx');
    }

    public function listReceivable()
    {
        return view('dashboard.report.sales.list-receivable');
    }

    public function tableReceivable(Request $request)
    {
        $data = InvoiceCustomer::with('customer')->where(['status' => 2, 'payment' => 1]);

        if($request->filled(['start_date', 'to_date'])){
            $data = $data->whereBetween('invoice_customers.invoice_date', [$request->start_date, $request->to_date]);
        }
        if($request->filled('customer')){
            $data = $data->whereHas('customer',function($query)use($request){
                $query->where('customers.first_name', 'like', '%'.$request->customer.'%')
                        ->orWhere('customers.company', 'like', '%'.$request->customer.'%')
                        ->orWhere('customers.last_name', 'like', '%'.$request->customer.'%');
            });
        }
        return $data->get();
    }

    public function printReceivable(Request $request)
    {
        $receivable = new SellingByReceivableExport($request->start_date, $request->to_date, $request->customer);
        return $receivable->download('sales-receivable.xlsx');
    }
}
