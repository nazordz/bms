<?php

namespace App\Http\Controllers\admin\Report;

use App\Exports\LedgerDetailsExport;
use App\Exports\LedgerRecapExport;
use App\Http\Controllers\Controller;
use App\Models\ChartAccount;
use App\Models\Journal;
use App\Models\JournalDetail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LedgerController extends Controller
{
    public function index()
    {
        return view('dashboard.report.ledger.index');
    }
    public function tableRecap(Request $request)
    {
        $data = ChartAccount::where('is_group', 0)
                ->select(
                    'chart_accounts.id',
                    'chart_accounts.name',
                    DB::raw('(
                        select ABS(sum(journal_details.debet) - sum(journal_details.credit)) from journal_details
                        where journal_details.account_id = chart_accounts.id AND journal_details.journal_id = (
                            SELECT id from journals where id = journal_details.journal_id
                            and journals.transaction_at < "'.$request->start_date.'"
                            AND journals.status = 2
                        )
                    ) AS beginning'),
                    DB::raw('(
                        select ABS(sum(journal_details.debet)) from journal_details
                        where journal_details.account_id = chart_accounts.id AND journal_details.journal_id = (
                            SELECT id from journals where id = journal_details.journal_id
                            and DATE(journals.transaction_at) between "'.$request->start_date.'" AND "'.$request->to_date.'"
                            AND journals.status = 2
                        )
                    ) AS debit'),
                    DB::raw('(
                        select ABS(sum(journal_details.credit)) from journal_details
                        where journal_details.account_id = chart_accounts.id AND journal_details.journal_id = (
                            SELECT id from journals where id = journal_details.journal_id
                            and DATE(journals.transaction_at) between "'.$request->start_date.'" AND "'.$request->to_date.'"
                            AND journals.status = 2
                        )
                    ) AS credit'),
                    DB::raw('(
                        select ABS(sum(journal_details.debet)) from journal_details
                        where journal_details.account_id = chart_accounts.id AND journal_details.journal_id = (
                            SELECT id from journals where id = journal_details.journal_id
                            AND journals.status = 2
                        )
                    ) AS total_debit'),
                    DB::raw('(
                        select ABS(sum(journal_details.credit)) from journal_details
                        where journal_details.account_id = chart_accounts.id AND journal_details.journal_id = (
                            SELECT id from journals where id = journal_details.journal_id
                            AND journals.status = 2
                        )
                    ) AS total_credit')
                )
                ->get();
        return $data;
    }
    public function tableDetails(Request $request)
    {
        $data = ChartAccount::with(['journal_details' => function($query)use($request){
            $query->whereHas('journal', function(Builder $query)use($request){
                $query->where('status', 2)->whereBetween('transaction_at', [$request->start_date, $request->to_date]);
            });
        }]);

        $data = $data->get()->map(function($query)use($request){
            if(count($query->journal_details)){
                $debit = JournalDetail::where('account_id', $query->id)->whereIn('journal_id', function($q)use($request){
                    $q->select('id')->from('journals')->where('status', 2)->where('transaction_at', '<', $request->start_date);
                })->sum('debet');
                $credit = JournalDetail::where('account_id', $query->id)->whereIn('journal_id', function($q)use($request){
                    $q->select('id')->from('journals')->where('status', 2)->where('transaction_at', '<', $request->start_date);
                })->sum('credit');
                $query->beginning = abs($debit - $credit);
                $query->beginning_balance_type = $debit > $credit ? 'debit':'credit';

                $total_debit = JournalDetail::where('account_id', $query->id)->whereIn('journal_id', function($q)use($request){
                    $q->select('id')->from('journals')->where('status', 2);
                })->sum('debet');
                $total_credit = JournalDetail::where('account_id', $query->id)->whereIn('journal_id', function($q)use($request){
                    $q->select('id')->from('journals')->where('status', 2);
                })->sum('credit');

                $op1 = $total_debit > $total_credit?$query->journal_details->sum('debet'):$query->journal_details->sum('credit');
                $op2 = $total_debit < $total_credit?$query->journal_details->sum('debet'):$query->journal_details->sum('credit');
                if($total_debit > $total_credit){
                    $query->ending = ($query->beginning + $op1) - $op2;
                    $query->ending_balance_type = $total_debit > $total_credit ? 'debit':'credit';
                }elseif($total_debit < $total_credit){
                    // $query->ending = '('.(($query->beginning + $op1) - $op2).')';
                    $query->ending = (($query->beginning + $op1) - $op2);
                    $query->ending_balance_type = $total_debit < $total_credit?'debit':'credit';
                }

            }else $query->beginning = 0;
            return $query;
        });
        return $data;
    }

    public function printRecap(Request $request)
    {
        $ledger = new LedgerRecapExport($request->start_date, $request->to_date);
        return $ledger->download('buku-besar-rekap-'.$request->start_date.'-'.$request->to_date.'.xlsx');
    }

    public function printDetails(Request $request)
    {
        $ledger = new LedgerDetailsExport($request->start_date, $request->to_date);
        return $ledger->download('buku-besar-rincian'.$request->start_date.'-'.$request->to_date.'.xlsx');
    }
}
