<?php

namespace App\Http\Controllers\admin\Report;

use App\Http\Controllers\Controller;
use App\Models\InvoiceSupplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Exports\PurchasingByInvoiceExport;
use App\Exports\PurchasingByPayableExport;
use App\Exports\PurchasingByPoExport;
use App\Models\PurchaseOrderSupplier;

class PurchasingController extends Controller
{
    public function index()
    {
        return view('dashboard.report.purchasing.index');
    }

    public function listInvoice()
    {
        return view('dashboard.report.purchasing.list-invoice');
    }
    public function tableInvoice(Request $request)
    {
        $data = InvoiceSupplier::join('suppliers', 'invoice_suppliers.supplier_id', '=', 'suppliers.supplier_id')
        ->leftJoin('invoice_tax_suppliers', 'invoice_suppliers.invoice_id', '=', 'invoice_tax_suppliers.invoice_id')
        ->leftJoin('tax', 'invoice_tax_suppliers.tax_id', '=', 'tax.tax_id')
        ->select('invoice_suppliers.*',
            'suppliers.first_name',
            'suppliers.last_name',
            'suppliers.company',
            'invoice_suppliers.payment',
            'tax.*',
            // DB::raw('(select IF(payment_suppliers.currency > 1, SUM(payment_amount*rate), SUM(payment_amount)) from payment_suppliers where invoice_suppliers.invoice_id = payment_suppliers.invoice_id) as paid'),
            DB::raw('(select SUM(payment_amount) from payment_suppliers where invoice_suppliers.invoice_id = payment_suppliers.invoice_id) as paid'),
            DB::raw('(
                SELECT SUM(price*quantity) FROM invoice_supplier_items WHERE invoice_suppliers.invoice_id = invoice_supplier_items.invoice_id
            ) AS subtotal')
        )
        ->where('invoice_suppliers.status', 2);

        if($request->filled('invoice_date')){
            $data = $data->where('invoice_suppliers.invoice_date', $request->invoice_date);
        }
        if($request->filled(['start_date', 'to_date'])){
            $data = $data->whereBetween('invoice_suppliers.invoice_date', [$request->start_date, $request->to_date]);
        }
        if($request->filled('supplier')){
            $data = $data->where(function($query)use($request){
                $query->where('suppliers.first_name', 'like', '%'.$request->supplier.'%')
                        ->orWhere('suppliers.company', 'like', '%'.$request->supplier.'%')
                        ->orWhere('suppliers.last_name', 'like', '%'.$request->supplier.'%');
            });
        }
        return $data->get();
    }
    public function printInvoice(Request $request)
    {
        $invoice = new PurchasingByInvoiceExport(
            $request->invoice_date,
            $request->start_date,
            $request->to_date,
            $request->supplier
        );
        return $invoice->download('purchase-invoice.xlsx');
    }

    public function listPo()
    {
        return view('dashboard.report.purchasing.list-po');
    }

    public function tablePo(Request $request)
    {
        $data = PurchaseOrderSupplier::join('suppliers', 'purchase_order_suppliers.supplier_id', '=', 'suppliers.supplier_id')
                ->select(
                    'purchase_order_suppliers.*',
                    'suppliers.company',
                    'suppliers.first_name',
                    'suppliers.last_name'
                )
                ->where('purchase_order_suppliers.status', 2);
        if($request->filled('po_date')){
            $data = $data->where('purchase_order_suppliers.po_date', $request->po_date);
        }
        if($request->filled(['start_date', 'to_date'])){
            $data = $data->whereBetween('purchase_order_suppliers.po_date', [$request->start_date, $request->to_date]);
        }
        if($request->filled('supplier')){
            $data = $data->where(function($query)use($request){
                $query->where('suppliers.first_name', 'like', '%'.$request->supplier.'%')
                        ->orWhere('suppliers.company', 'like', '%'.$request->supplier.'%')
                        ->orWhere('suppliers.last_name', 'like', '%'.$request->supplier.'%');
            });
        }
        // $data = PurchaseOrderSupplier::all();
        return $data->get();
    }

    public function printPo(Request $request)
    {
        $po = new PurchasingByPoExport(
            $request->po_date,
            $request->start_date,
            $request->to_date,
            $request->supplier
        );
        return $po->download('purchase-po.xlsx');
    }

    public function listPayable()
    {
        return view('dashboard.report.purchasing.list-payable');
    }
    public function tablePayable(Request $request)
    {
        $data = InvoiceSupplier::with('supplier')->where(['status' => 2, 'payment' => 1]);

        if($request->filled(['start_date', 'to_date'])){
            $data = $data->whereBetween('invoice_suppliers.invoice_date', [$request->start_date, $request->to_date]);
        }
        if($request->filled('supplier')){
            $data = $data->whereHas('supplier',function($query)use($request){
                $query->where('suppliers.first_name', 'like', '%'.$request->supplier.'%')
                        ->orWhere('suppliers.company', 'like', '%'.$request->supplier.'%')
                        ->orWhere('suppliers.last_name', 'like', '%'.$request->supplier.'%');
            });
        }
        return $data->get();
    }

    public function printPayable(Request $request)
    {
        $payable = new PurchasingByPayableExport($request->start_date, $request->to_date, $request->supplier);
        return $payable->download('purchase-payable.xlsx');
    }
}
