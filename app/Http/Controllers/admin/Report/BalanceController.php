<?php

namespace App\Http\Controllers\admin\Report;

use App\Http\Controllers\Controller;
use App\Models\ChartAccount;
use App\Models\Journal;
use App\Models\JournalDetail;
use App\Models\WebSetting;
use Illuminate\Http\Request;

class BalanceController extends Controller
{

    public function index()
    {
        return view('dashboard.report.balance.index');
    }

    public function getBalance(Request $request)
    {
        $condition   = "date_format(journals.transaction_at, '%Y-%m') < '{$request->to_date}'";
        $assets      = ChartAccount::where('is_asset', 1)->with('recursiveChildren')->get();
        $assets      = $this->recursive_total($assets, $condition);
        $liabilities = ChartAccount::where('is_lability', 1)->with('recursiveChildren')->get();
        $liabilities = $this->recursive_total($liabilities, $condition);
        $equities    = ChartAccount::where('is_equity', 1)->with('recursiveChildren')->get();
        $equities    = $this->recursive_total($equities, $condition);

        return ['assets'=>$assets, 'liabilities'=>$liabilities,'equities'=>$equities];
    }

    public function recursive_total($accounts, $condition)
    {
        $data = [];
        try {
            foreach ($accounts as $key => $account) {
                $data[$key]['id']               = $account->id;
                $data[$key]['account_id']       = $account->account_id;
                $data[$key]['name']             = $account->name;
                $data[$key]['default_position'] = $account->default_position == 2?'debit':'credit';
                $data[$key]['total_credit']     = $account->totalCredit($condition);
                $data[$key]['total_debit']      = $account->totalDebit($condition);

                $data[$key]['total_result']['nominal'] = abs($data[$key]['total_credit'] - $data[$key]['total_debit']);
                $data[$key]['total_result']['is_normal'] = true;
                if($data[$key]['default_position'] == 'credit' && $data[$key]['total_debit'] > $data[$key]['total_credit']){
                    $data[$key]['total_result']['is_normal'] = false;
                }
                elseif($data[$key]['default_position'] == 'debit' && $data[$key]['total_credit'] > $data[$key]['total_debit']){
                    $data[$key]['total_result']['is_normal'] = false;
                }

                if($account->recursiveChildren){
                    $data[$key]['recursive_children'] = $this->recursive_total(collect($account->recursiveChildren), $condition);
                }
            }
            return $data;
        } catch (\Throwable $th) {
            if(env('APP_DEBUG') == false){
                abort(500);
            }else dd($th);
        }

    }

    public function listAccount(Array $accounts, $parent_id = null)
    {
        $data = [];
        foreach ($accounts as $key => $account) {
            try {
                $data[$key] = [
                    'id'   => $account['id'],
                    'name' => $account['name']
                ];
                if($parent_id == $account['account_id'] && $account['is_group'] == 1){
                    $data[$key]['child'] = $this->listAccount($accounts, $account['id']);

                }else{
                    $data[$key]['child'] = null;
                }
            } catch (\Throwable $th) {
                dd([$account, $parent_id]);
            }
        }
        return $data;
    }

    public function printBalance(Request $request)
    {
        $web    = WebSetting::with('company')->first();

        return view('dashboard.report.balance.print', ['to_date' => $request->to_date, 'web' => $web]); //, [ 'equity' => $equity]
    }
}
