<?php

namespace App\Http\Controllers\admin\Report;

use App\Http\Controllers\Controller;
use App\Models\ChartAccount;
use App\Models\Journal;
use App\Models\WebSetting;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EquityController extends Controller
{
    public function index()
    {
        return view('dashboard.report.equity.index');
    }

    public function getEquity(Request $request)
    {
        $data['ekuitas_awal'] = DB::table('journals')
                        ->join('journal_details', 'journals.id', '=', 'journal_details.journal_id')
                        ->selectRaw(
                            'abs(sum(journal_details.credit) - sum(journal_details.debet)) as total,
                            if(sum(journal_details.credit) < sum(journal_details.debet), "debit", "credit") AS type
                        ')
                        ->whereRaw("status = 2
                            and date_format(transaction_at, '%Y-%m') < '{$request->to_date}'
                            and journal_details.account_id = '3.1.01'
                        ")->first();
        $data['tambahan_modal_disetor'] = DB::table('journals')
                                        ->join('journal_details', 'journals.id', '=', 'journal_details.journal_id')
                                        ->selectRaw(
                                            'abs(sum(journal_details.credit) - sum(journal_details.debet)) as total,
                                            if(sum(journal_details.credit) < sum(journal_details.debet), "debit", "credit") AS type
                                        ')
                                        ->whereRaw("status = 2
                                            and date_format(transaction_at, '%Y-%m') < '{$request->to_date}'
                                            and journal_details.account_id = '3.1.02'
                                        ")->first();
        $data['laba_ditahan'] = $this->getProfit($request->to_date);

        $start_year =  Carbon::createFromFormat('Y-m', $request->to_date)->year;
        $data['laba_tahun_berjalan'] = $this->getProfit($request->to_date, $start_year.'-01');
        $data['penarikan'] = DB::table('journals')
                                ->join('journal_details', 'journals.id', '=', 'journal_details.journal_id')
                                ->selectRaw(
                                    'abs(sum(journal_details.credit) - sum(journal_details.debet)) as total,
                                    if(sum(journal_details.credit) > sum(journal_details.debet), "credit", "debit") AS type
                                ')
                                ->whereRaw("status = 2
                                    and date_format(transaction_at, '%Y-%m') < '{$request->to_date}'
                                    and journal_details.account_id = '3.3.01'
                                ")->first();
        return $data;
    }

    public function getProfit($to_date, $start_date = null)
    {

        // pendapatan
        $pendapatan =  ChartAccount::with(['journal_details' => function($query)use($start_date, $to_date){
            $query->whereHas('journal', function(Builder $query)use($start_date, $to_date){
                if($start_date){
                    $query->where('status', 2)->whereRaw("(date_format(transaction_at, '%Y-%m') between '{$start_date}' and '{$to_date}')");
                }else $query->where('status', 2)->whereRaw("date_format(transaction_at, '%Y-%m') < '{$to_date}'");
            });
        }])
        ->where('chart_accounts.account_id', '4.0.00')->get();
        $pendapatan = $pendapatan->map(function($list){
            $credit = $list->journal_details->sum('credit');
            $debit  = $list->journal_details->sum('debet');
            // $list->balance = ($list->default_position == 1) ? '('.abs($credit-$debit).')':abs($credit-$debit);
            $list->balance = abs($credit-$debit);
            return $list;
        });
        $total_debit_pendapatan = $pendapatan->sum(function($list){
            return $list->default_position == 2 ? $list->balance: 0;
        });
        $total_credit_pendapatan = $pendapatan->sum(function($list){
            return $list->default_position == 1 ? $list->balance: 0;
        });
        $total_pendapatan = 0;
        if($total_debit_pendapatan > $total_credit_pendapatan){
            $total_pendapatan = '('.number_format(abs($total_debit_pendapatan-$total_credit_pendapatan), 2, ',', '.').')';
        }elseif($total_debit_pendapatan < $total_credit_pendapatan) {
            $total_pendapatan = number_format(abs($total_debit_pendapatan-$total_credit_pendapatan), 2, ',', '.');
        }
        $data['pendapatan']['total_raw'] = abs($total_debit_pendapatan-$total_credit_pendapatan);
        $data['pendapatan']['total'] = $total_pendapatan;
        // $data['pendapatan']['lists'] = $pendapatan;

        // beban
        $beban_usaha = ChartAccount::with(['journal_details' => function($query)use($start_date, $to_date){
            $query->whereHas('journal', function(Builder $query)use($start_date, $to_date){
                if($start_date){
                    $query->where('status', 2)->whereRaw("(date_format(transaction_at, '%Y-%m') between '{$start_date}' and '{$to_date}')");
                }else $query->where('status', 2)->whereRaw("date_format(transaction_at, '%Y-%m') < '{$to_date}'");            });
        }])
        ->whereIn('chart_accounts.account_id', ['6.1.00', '6.2.00', '6.3.00'])->get();
        $beban_usaha = $beban_usaha->map(function($list){
            $credit = $list->journal_details->sum('credit');
            $debit  = $list->journal_details->sum('debet');
            $list->balance = abs($credit-$debit);
            return $list;
        });
        $total_debit_beban_usaha = $beban_usaha->sum(function($list){
            return $list->default_position == 2 ? $list->balance: 0;
        });
        $total_credit_beban_usaha = $beban_usaha->sum(function($list){
            return $list->default_position == 1 ? $list->balance: 0;
        });
        $total_beban_usaha = 0;
        if($total_debit_beban_usaha > $total_credit_beban_usaha){
            $total_beban_usaha = number_format(abs($total_debit_beban_usaha-$total_credit_beban_usaha), 2, ',', '.');
        }elseif($total_debit_beban_usaha < $total_credit_beban_usaha) {
            $total_beban_usaha = '('.number_format(abs($total_debit_beban_usaha-$total_credit_beban_usaha), 2, ',', '.').')';
        }
        $data['beban_usaha']['total_raw'] = abs($total_debit_beban_usaha-$total_credit_beban_usaha);
        $data['beban_usaha']['total'] = $total_beban_usaha;
        // $data['beban_usaha']['lists'] = $beban_usaha;

        // pendapatan diluar usaha
        $pendapatan_diluar_usaha = ChartAccount::with(['journal_details' => function($query)use($start_date, $to_date){
            $query->whereHas('journal', function(Builder $query)use($start_date, $to_date){
                if($start_date){
                    $query->where('status', 2)->whereRaw("(date_format(transaction_at, '%Y-%m') between '{$start_date}' and '{$to_date}')");
                }else $query->where('status', 2)->whereRaw("date_format(transaction_at, '%Y-%m') < '{$to_date}'");
            });
        }])
        ->where('chart_accounts.account_id', '7.0.00')->get();
        $pendapatan_diluar_usaha = $pendapatan_diluar_usaha->map(function($list){
            $credit = $list->journal_details->sum('credit');
            $debit  = $list->journal_details->sum('debet');
            $list->balance = abs($credit-$debit);
            return $list;
        });
        $total_debit_pendapatan_diluar_usaha = $pendapatan_diluar_usaha->sum(function($list){
            return $list->default_position == 2 ? $list->balance: 0;
        });
        $total_credit_pendapatan_diluar_usaha = $pendapatan_diluar_usaha->sum(function($list){
            return $list->default_position == 1 ? $list->balance: 0;
        });
        $total_pendapatan_diluar_usaha = 0;
        if($total_debit_pendapatan_diluar_usaha > $total_credit_pendapatan_diluar_usaha){
            $total_pendapatan_diluar_usaha = '('.number_format(abs($total_debit_pendapatan_diluar_usaha-$total_credit_pendapatan_diluar_usaha), 2, ',', '.').')';
        }elseif($total_debit_pendapatan_diluar_usaha < $total_credit_pendapatan_diluar_usaha) {
            $total_pendapatan_diluar_usaha = number_format(abs($total_debit_pendapatan_diluar_usaha-$total_credit_pendapatan_diluar_usaha), 2, ',', '.');
        }
        $data['pendapatan_diluar_usaha']['total_raw'] = abs($total_debit_pendapatan_diluar_usaha-$total_credit_pendapatan_diluar_usaha);
        $data['pendapatan_diluar_usaha']['total'] = $total_pendapatan_diluar_usaha;
        // $data['pendapatan_diluar_usaha']['lists'] = $pendapatan_diluar_usaha;

        // beban diluar usaha
        $beban_diluar_usaha = ChartAccount::with(['journal_details' => function($query)use($start_date, $to_date){
            $query->whereHas('journal', function(Builder $query)use($start_date, $to_date){
                if($start_date){
                    $query->where('status', 2)->whereRaw("(date_format(transaction_at, '%Y-%m') between '{$start_date}' and '{$to_date}')");
                }else $query->where('status', 2)->whereRaw("date_format(transaction_at, '%Y-%m') < '{$to_date}'");
            });
        }])
        ->where('chart_accounts.account_id', '8.0.00')->get();
        $beban_diluar_usaha = $beban_diluar_usaha->map(function($list){
            $credit = $list->journal_details->sum('credit');
            $debit  = $list->journal_details->sum('debet');
            $list->balance = abs($credit-$debit);
            return $list;
        });
        $total_debit_beban_diluar_usaha = $beban_diluar_usaha->sum(function($list){
            return $list->default_position == 2 ? $list->balance: 0;
        });
        $total_credit_beban_diluar_usaha = $beban_diluar_usaha->sum(function($list){
            return $list->default_position == 1 ? $list->balance: 0;
        });
        $total_beban_diluar_usaha = 0;
        if($total_debit_beban_diluar_usaha > $total_credit_beban_diluar_usaha){
            $total_beban_diluar_usaha = number_format(abs($total_debit_beban_diluar_usaha-$total_credit_beban_diluar_usaha), 2, ',', '.');
        }elseif($total_debit_beban_diluar_usaha < $total_credit_beban_diluar_usaha) {
            $total_beban_diluar_usaha = '('.number_format(abs($total_debit_beban_diluar_usaha-$total_credit_beban_diluar_usaha), 2, ',', '.').')';
        }
        $data['beban_diluar_usaha']['total_raw'] = abs($total_debit_beban_diluar_usaha-$total_credit_beban_diluar_usaha);
        $data['beban_diluar_usaha']['total'] = $total_beban_diluar_usaha;
        // $data['beban_diluar_usaha']['lists'] = $beban_diluar_usaha;

        $data['laba_ditahan']['type'] = $data['pendapatan']['total_raw'] > $data['beban_diluar_usaha']['total_raw']?'credit':'debit';
        $data['laba_ditahan']['total'] = $data['laba_ditahan']['type'] != 'credit'
            ? $data['pendapatan']['total_raw'] - $data['beban_diluar_usaha']['total_raw']
            : $data['pendapatan']['total_raw'] + $data['beban_diluar_usaha']['total_raw'];
        return $data;
    }

    public function printEquity(Request $request)
    {
        // $equity = $this->getEquity($request);
        // $equity = collect($equity);
        $web    = WebSetting::with('company')->first();
        return view('dashboard.report.equity.print', ['to_date' => $request->to_date, 'web' => $web]); //, [ 'equity' => $equity]
    }
}
