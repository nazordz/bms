<?php

namespace App\Http\Controllers\admin;

use App\Exports\QuotationCustomerExport;
use App\Http\Controllers\Controller;
use App\Models\BcCustomer;
use App\Models\BcCustomerItem;
use App\Models\CreditNoteCustomer;
use App\Models\CreditNoteItemCustomer;
use App\Models\CreditNoteTaxCustomer;
use App\Models\Customer;
use App\Models\DebitNoteCustomer;
use App\Models\DebitNoteItemCustomer;
use App\Models\DebitNoteTaxCustomer;
use App\Models\InvoiceCustomer;
use App\Models\InvoiceCustomerItem;
use App\Models\InvoiceTaxCustomer;
use App\Models\Journal;
use App\Models\JournalDetail;
use App\Models\PaymentCustomer;
use App\Models\PoCustomerItem;
use App\Models\PurchaseOrderCustomer;
use App\Models\QuotationCustomer;
use App\Models\QuotationCustomerItem;
use App\Models\QuotationTaxCustomer;
use App\Models\Seller;
use App\Models\SettingAccount;
use App\Models\WebSetting;
use Barryvdh\DomPDF\Facade as PDF;
use Barryvdh\Debugbar\Facade as Debugbar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CustomerController extends Controller
{
    public function listCustomer()
    {
        return view('dashboard.customer.list-customer');
    }
    public function tableCustomer()
    {
        $data = Customer::whereNull('deleted_at');
        return datatables()->of($data)->make(true);
    }

    public function createCustomer(){
        return view('dashboard.customer.create-customer');
    }

    public function editCustomer($customer_id){
        $data = Customer::with(['province', 'regency', 'district', 'village'])->find($customer_id);
        return view('dashboard.customer.edit-customer', ['data' => $data]);
    }

    public function storeCustomer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name'  => 'required',
            'last_name'   => 'nullable',
            'company'     => 'required',
            'province_id' => 'nullable|exists:provinces,id',
            'regency_id'  => 'nullable|exists:regencies,id',
            'district_id' => 'nullable|exists:districts,id',
            'village_id'  => 'nullable|exists:villages,id',
            'address'     => 'nullable',
            'email'       => 'nullable|email',
            'phone'       => 'nullable|numeric',
        ]);
        if($validator->fails()){
            return redirect('/customer/create-customer')->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_created'));
        }
        Customer::create([
            'customer_id' => Str::uuid(),
            'first_name'  => $request->first_name,
            'last_name'   => $request->last_name,
            'company'     => $request->company,
            'province_id' => $request->province_id,
            'regency_id'  => $request->regency_id,
            'district_id' => $request->district_id,
            'village_id'  => $request->village_id,
            'address'     => $request->address,
            'email'       => $request->email,
            'phone'       => $request->phone,
        ]);
        activity()->causedBy(Auth::id())->log('Customer Created!');
        return redirect('/customer/list-customer')->with('status-success', Lang::get('dashboard.customer_created'));
    }

    public function updateCustomer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|exists:customers,customer_id',
            'first_name'  => 'required',
            'last_name'   => 'nullable',
            'company'     => 'required',
            'province_id' => 'nullable|exists:provinces,id',
            'regency_id'  => 'nullable|exists:regencies,id',
            'district_id' => 'nullable|exists:districts,id',
            'village_id'  => 'nullable|exists:villages,id',
            'address'     => 'nullable',
            'email'       => 'nullable|email',
            'phone'       => 'nullable|numeric',
        ]);
        if($validator->fails()){
            return redirect('/customer/edit-customer/'.$request->customer_id)->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_saved'));
        }
        Customer::where('customer_id', $request->customer_id)
                ->update([
                    'first_name'  => $request->first_name,
                    'last_name'   => $request->last_name,
                    'company'     => $request->company,
                    'nation'      => $request->nation,
                    'province_id' => $request->province_id,
                    'regency_id'  => $request->regency_id,
                    'district_id' => $request->district_id,
                    'village_id'  => $request->village_id,
                    'address'     => $request->address,
                    'email'       => $request->email,
                    'phone'       => $request->phone,
                ]);
        activity()->causedBy(Auth::id())->log('Customer updated!');
        return redirect('/customer/list-customer')->with('status-success', Lang::get('dashboard.customer_updated'));
    }

    public function showCustomer($customer_id)
    {
        $data = Customer::with(['province', 'regency', 'district', 'village'])->find($customer_id);
        return response()->json($data);
    }

    public function deleteCustomer(Request $request)
    {
        Customer::where('customer_id', $request->customer_id)->delete();
        activity()->causedBy(Auth::id())->log('Customer deleted!');
        return response()->json(['status' => true, 'statustext' => 'Customer Deleted!']);
    }

    public function listQuotation()
    {
        return view('dashboard.customer.list-quotation');
    }

    public function tableQuotation(Request $request)
    {
        $data = QuotationCustomer::select('customers.first_name', 'customers.last_name', 'customers.company', 'quotation_customers.*', 'sellers.name AS sellerName') // 'quotation_customers.quotation_id', 'term_and_condition', 'valid_until', 'sales'
                ->join('customers', 'quotation_customers.customer_id', '=', 'customers.customer_id')
                ->leftJoin('sellers', 'quotation_customers.seller_id', '=', 'sellers.id');
        if($request->filled(['start_date', 'to_date'])){
            $start_date = date_format(date_create($request->start_date), "Y-m-d");
            $to_date = date_format(date_create($request->to_date), "Y-m-d");
            $data = $data->whereBetween('quotation_customers.created_at', ["{$start_date} 00:00:00", "{$to_date} 23:59:59"]);
        }
        return datatables()->of($data)->make(true);
    }

    public function createQuotation(Request $request)
    {
        $data = null;
        $customer = null;
        if($request->has('customer_id') || $request->has('copy_from')){
            $data = collect($request->all());
            $customer = collect([
                'customer_id' => $request->customer_id,
                'customer_name' => $request->customer_name
            ]);
        }
        return view('dashboard.customer.create-quotation', ['data' => $data, 'customer' => $customer]);
    }

    public function storeQuotation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id'        => 'required|exists:customers,customer_id',
            'seller_id'          => 'nullable|exists:sellers,id',
            'term_and_condition' => 'nullable',
            'valid_until'        => 'required|date|date_format:d-m-Y',
        ]);
        if($validator->fails()){
            return redirect('/customer/create-quotation')->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_saved'));
        }
        $serial = QuotationCustomer::withTrashed()->count() + 1;
        $quotation = new QuotationCustomer();
        $quotation->customer_id        = $request->customer_id;
        $quotation->serial             = $serial;
        $quotation->uuid               = Str::uuid();
        // $quotation->seller_id          = $request->seller_id;
        $quotation->term_and_condition = $request->term_and_condition;
        $quotation->term_of_payment    = $request->term_of_payment;
        $quotation->description        = $request->description;
        $quotation->currency           = $request->currency;
        $quotation->rate               = $request->rate;
        $quotation->rate_tax           = $request->rate_tax;
        $quotation->attn               = $request->attn;
        $quotation->vessel             = $request->vessel;
        $quotation->voyage             = $request->voyage;
        $quotation->eta                = $request->eta ? date_format(date_create($request->eta), "Y-m-d"):null;
        $quotation->quotation_date     = date_format(date_create($request->quotation_date), "Y-m-d");
        $quotation->bl_no              = $request->bl_no;
        $quotation->from               = $request->from;
        $quotation->to                 = $request->to;
        $quotation->status             = $request->status;
        $quotation->valid_until        = date_format(date_create($request->valid_until), "Y-m-d");
        $quotation->eta                = date_format(date_create($request->eta), "Y-m-d");
        $quotation->created_time       = $request->created_time;

        $quotation->save();
        $id = $quotation->quotation_id;

        $row = collect($request->row)->map(function($item) use($request, $id){
            $item['quotation_id'] = $id;
            // $item['price'] =  preg_replace('/\.||\,/','',$item['price']);
            // $item['subtotal'] =  preg_replace('/\.||\,/','',$item['subtotal']);
            // $item['price'] =  $item['price'];
            // $item['subtotal'] =  $item['subtotal'];
            return $item;
        });

        QuotationCustomerItem::insert($row->toArray());
        if($request->tax[0]['id']){
            $taxes = collect($request->tax);
            $taxes = $taxes->filter(function($value, $key) {
                return $value['id'];
            });
            $taxes = $taxes->map(function($taxes)use($id){
                return [
                    'tax_id' => $taxes['id'],
                    'quotation_id' => $id
                ];
            });

            QuotationTaxCustomer::insert($taxes->toArray());
        }

        activity()->causedBy(Auth::id())->log('Customer Quotation created!');
        return redirect('customer/list-quotation')->with('status-success', Lang::get('dashboard.success_saved'));
    }

    public function showQuotation($uuid)
    {
        $data = QuotationCustomer::with(['customer', 'invoice', 'list_items', 'taxes', 'discount', 'booking_confirmation', 'seller'])->whereUuid($uuid)->first();
        abort_if(!$data, 404, 'Not found');
        return view('dashboard.customer.edit-quotation', ['data' => $data]);
    }

    public function updateQuotation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'quotation_id'       => 'required|exists:quotation_customers,quotation_id',
            'customer_id'        => 'required|exists:customers,customer_id',
            // 'seller_id'          => 'nullable|exists:sellers,id',
            'term_and_condition' => 'nullable',
            'valid_until'        => 'required|date|date_format:d-m-Y'
        ]);

        if($validator->fails()){
            return redirect('/customer/create-quotation')->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_saved'));
        }

        $quotation = QuotationCustomer::find($request->quotation_id);
        $quotation->customer_id        = $request->customer_id;
        // $quotation->seller_id          = $request->seller_id;
        $quotation->term_and_condition = $request->term_and_condition;
        $quotation->term_of_payment    = $request->term_of_payment;
        $quotation->description        = $request->description;
        $quotation->currency           = $request->currency;
        $quotation->rate               = $request->rate;
        $quotation->rate_tax           = $request->rate_tax;
        $quotation->attn               = $request->attn;
        $quotation->vessel             = $request->vessel;
        $quotation->voyage             = $request->voyage;
        $quotation->eta                = $request->eta ? date_format(date_create($request->eta), "Y-m-d"):null;
        $quotation->bl_no              = $request->bl_no;
        $quotation->from               = $request->from;
        $quotation->to                 = $request->to;
        $quotation->status             = $request->status;
        $quotation->valid_until        = date_format(date_create($request->valid_until), "Y-m-d");
        $quotation->quotation_date     = date_format(date_create($request->quotation_date), "Y-m-d");
        $quotation->eta                = date_format(date_create($request->eta), "Y-m-d");
        $quotation->created_time       = $request->created_time;

        $quotation->save();

        QuotationCustomerItem::where('quotation_id', $request->quotation_id)->delete();
        $row = collect($request->row)->map(function($item) use($request){
            $item['quotation_id'] = $request->quotation_id;
            $item['price']        =  $item['price'];
            $item['subtotal']     =  $item['subtotal'];
            return $item;

        });
        QuotationCustomerItem::insert($row->toArray());

        if($request->tax[0]['id']){
            QuotationTaxCustomer::where('quotation_id', $request->quotation_id)->delete();
            $taxes = collect($request->tax);

            $taxes = $taxes->filter(function($value, $key) {
                return $value['id'];
            });

            $taxes = $taxes->map(function($taxes)use($request){
                return [
                    'tax_id' => $taxes['id'],
                    'quotation_id' => $request->quotation_id
                ];
            });
            QuotationTaxCustomer::insert($taxes->toArray());
        }else{
            QuotationTaxCustomer::where('quotation_id', $request->quotation_id)->delete();

        }

        activity()->causedBy(Auth::id())->log('Customer Quotation updated!');
        return redirect('customer/list-quotation')->with('status-success', Lang::get('dashboard.success_saved'));
    }

    public function deleteQuotation(Request $request)
    {
        $qtt = QuotationCustomer::find($request->quotation_id);
        if($request->type == 'delete'){
            $qtt->delete();
        }else{
            $qtt->status = 0;
            $qtt->save();
        }

        activity()->causedBy(Auth::id())->log('Customer Quotation void!');
        return response()->json(['status' => true, 'statustext' => 'Customer Quotation Void!']);
    }

    public function exportQutation($start, $to)
    {
        activity()->causedBy(Auth::id())->log('Customer Quotation exported!');
        return (new QuotationCustomerExport($start, $to))->download("data-quotation-customer-{$start}-{$to}.xlsx");
    }

    public function printQuotation($uuid)
    {
        $web  = WebSetting::with('company')->find(session()->get('company_id'));
        $quotation = QuotationCustomer::whereUuid($uuid)->with(['list_items', 'seller', 'taxes', 'customer', 'customer.province', 'customer.regency', 'customer.district', 'customer.village' ])->first();
        abort_if(!$quotation, 404, 'Not found');

        return view('dashboard.customer.cetak-penawaran', ['data' => $quotation, 'web' => $web]);
    }


    public function listBc(){
        return view('dashboard.customer.list-bc');
    }

    public function tableBc(Request $request)
    {
        $data = BcCustomer::join('customers', 'bc_customers.customer_id', '=', 'customers.customer_id')
                ->select(
                    'booking_ref', 'customers.company', 'status', 'bc_customers.created_at', 'bc_id', 'bc_customers.serial', 'customers.company',
                    'customers.first_name', 'customers.last_name', 'bc_customers.uuid'
                )
                ->where('bc_customers.status', '>', 0);
        if($request->filled(['start_date', 'to_date'])){
            $data = $data->whereBetween('bc_customers.created_at', [date_format(date_create($request->start_date), 'Y-m-d')." 00:00:00", date_format(date_create($request->to_date), 'Y-m-d')." 23:59:59"]);
        }
        return datatables()->of($data)->make(true);
    }

    public function createBc(Request $request)
    {
        $data = null;
        $customer = null;
        if($request->has('quotation_id') || $request->has('customer_id')){
            $data = $request->all();
            $customer = collect([
                'customer_id' => $request->customer_id,
                'customer_name' => $request->customer_name
            ]);
        }
        return view('dashboard.customer.create-bc', ['data' => $data, 'customer' => $customer]);
    }

    public function storeBc(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id'    => 'required|exists:customers,customer_id',
            'status'         => 'required',
            'booking_ref'    => 'required',
            "sin_permit"     => 'required|numeric|min:0|max:99',
            "batam_permit"   => 'required|numeric|min:0|max:99',
            "file"           => 'nullable|file|max:6000',
        ]);
        if($validator->fails()){
            return redirect('/customer/create-booking-confirmation')->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_saved'));
        }
        $serial = BcCustomer::where('booking_ref', $request->booking_ref)->count()+1;
        $bc = new BcCustomer;
        $bc->customer_id           = $request->customer_id;
        $bc->uuid                  = Str::uuid();
        $bc->serial                = $serial;
        $bc->booking_ref           = $request->booking_ref;
        $bc->consignee             = $request->consignee;
        $bc->shipper               = $request->shipper;
        $bc->vessel                = $request->vessel;
        $bc->voyage                = $request->voyage;
        $bc->port_of_loading       = $request->port_of_loading;
        $bc->port_of_discharge     = $request->port_of_discharge;
        $bc->etd_port_of_loading   = date_format(date_create($request->etd_port_of_loading), 'Y-m-d');
        $bc->eta_port_of_discharge = date_format(date_create($request->eta_port_of_discharge), 'Y-m-d');
        $bc->sin_permit            = $request->sin_permit;
        $bc->batam_permit          = $request->batam_permit;
        $bc->total                 = $request->total;
        $bc->special_remark        = $request->special_remark;
        $bc->status                = $request->status;

        if($request->hasFile('file')){
            $path = $request->file->storeAs("public/bc-customer", Str::random(5).'_'.$request->file->getClientOriginalName());
            $locationFile = Storage::url($path);
            $bc->file = $locationFile;
        }
        if($request->has('quotation_id')){
            $bc->quotation_id = $request->quotation_id;
        }

        $bc->save();

        $items = collect($request->items)->map(function($data){
            $newData['qty']                = $data['qty'];
            $newData['forwarding_service'] = $data['forwardingService'];
            $newData['shipment_type']      = $data['shipmentType'] ?? null;
            $newData['container_size']     = $data['containerSize'];
            $newData['container_no']       = $data['containerNo'];
            return $newData;
        })->toArray();
        $bc->item()->createMany($items);
        activity()->causedBy(Auth::id())->log('Customer Booking Confirmation created!');
        return redirect('customer/booking-confirmation')->with('status-success', Lang::get('dashboard.success_saved'));

    }

    public function showBc($uuid)
    {
        $data = BcCustomer::with(['customer', 'item', 'quotation'])->whereUuid($uuid)->first();
        abort_if(!$data, 404, 'Not found');
        return view('dashboard.customer.edit-bc', ['data' => $data]);
    }

    public function updateBc(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id'    => 'required|exists:customers,customer_id',
            'status'         => 'required',
            'booking_ref'    => 'required',
            "sin_permit"     => 'required|numeric|min:0|max:99',
            "batam_permit"   => 'required|numeric|min:0|max:99',
        ]);
        if($validator->fails()){
            return back()->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_saved'));
        }
        $bc = BcCustomer::find($request->bc_id);
        $bc->customer_id           = $request->customer_id;
        $bc->vessel                = $request->vessel;
        $bc->voyage                = $request->voyage;
        $bc->consignee             = $request->consignee;
        $bc->shipper               = $request->shipper;
        $bc->port_of_loading       = $request->port_of_loading;
        $bc->port_of_discharge     = $request->port_of_discharge;
        $bc->etd_port_of_loading   = date_format(date_create($request->etd_port_of_loading), 'Y-m-d');
        $bc->eta_port_of_discharge = date_format(date_create($request->eta_port_of_discharge), 'Y-m-d');
        $bc->sin_permit            = $request->sin_permit;
        $bc->batam_permit          = $request->batam_permit;
        $bc->total                 = $request->total;
        $bc->special_remark        = $request->special_remark;
        $bc->status                = $request->status;
        $bc->quotation_id          = $request->quotation_id;
        if($request->hasFile('file')){
            $path = $request->file->storeAs("public/bc-customer", Str::random(5).'_'.$request->file->getClientOriginalName());
            $locationFile = Storage::url($path);
            $bc->file = $locationFile;
        }
        if($bc->booking_ref != $request->booking_ref){
            $serial = BcCustomer::where('booking_ref', $request->booking_ref)->count()+1;
            $bc->serial      = $serial;
            $bc->booking_ref = $request->booking_ref;
        }
        $bc->save();
        BcCustomerItem::where('bc_id', $request->bc_id)->delete();
        $items = collect($request->items)->map(function($data){
            $newData['qty']                = $data['qty'];
            $newData['forwarding_service'] = $data['forwardingService'];
            $newData['shipment_type']      = $data['shipmentType'] ?? null;
            $newData['container_size']     = $data['containerSize'];
            $newData['container_no']       = $data['containerNo'];
            return $newData;
        })->toArray();
        $bc->item()->createMany($items);
        activity()->causedBy(Auth::id())->log('Customer Booking Confirmation updated!');
        return redirect('customer/booking-confirmation')->with('status-success', Lang::get('dashboard.success_saved'));

    }

    public function cancelBc(Request $request)
    {
        BcCustomer::where('bc_id', $request->bc_id)
        ->update(['status' => 0]);
        activity()->causedBy(Auth::id())->log('Customer Booking Confirmation canceled!');

        return response()->json(['status' => true, 'statusText' => 'Canceled']);
    }

    public function printBc($id)
    {
        $data = BcCustomer::with(['customer', 'item', 'customer.province', 'customer.regency','customer.district','customer.village'])->find($id);
        $web  = WebSetting::with('company')->find(session()->get('company_id'));
        return view('dashboard.customer.print-bc', ['data' => $data, 'web' => $web]);

        $pdf = PDF::loadView('dashboard.customer.print-bc', ['data' => $data, 'web' => $web])->setPaper('a4', 'potrait');
        return $pdf->download('bc-customer-'.$data->booking_ref.$id.'.pdf');
    }


    public function listPo()
    {
        return view('dashboard.customer.list-po');
    }

    public function tablePo(Request $request)
    {
        $data = DB::table('purchase_order_customers')
                    ->join('po_customer_items', 'purchase_order_customers.po_id', '=', 'po_customer_items.po_id')
                    ->join('customers', 'purchase_order_customers.customer_id', '=', 'customers.customer_id');
    if($request->filled(['start_date', 'to_date'])){
        $data = $data->whereBetween('purchase_order_customers.created_at', [date_format(date_create($request->start_date), 'Y-m-d')." 00:00:00", date_format(date_create($request->to_date), 'Y-m-d')." 23:59:59"]);
    }
        return datatables()->of($data)->make(true);
    }

    public function createPo(Request $request)
    {
        $data = null;
        $customer = null;
        if($request->has('quotation')){
            $data = collect($request->all())->toJson();
            $customer = collect([
                'customer_id' => $request->customer_id,
                'customer_name' => $request->customer_name
            ]);
        }
        return view('dashboard.customer.create-po', ['data' => $data, 'customer' => $customer]);
    }

    public function storePo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|exists:customers,customer_id',
            'file'        => 'nullable|max:600'
        ]);
        if($validator->fails()){
            return redirect('/customer/create-quotation')->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_saved'));
        }
        $insertPo = [
                'customer_id'        => $request->customer_id,
                'status'             => $request->status,
                'tax'                => $request->tax,
                'discount'           => $request->discount,
                'term_and_condition' => $request->term_and_condition,
                'uuid'               => Str::uuid(),
        ];
        if($request->hasFile('file')){
            $path = $request->file->storeAs("public/po-customer", Str::random(5).'_'.$request->file->getClientOriginalName());
            $locationFile = Storage::url($path);
            $insertPo['file'] = $locationFile;
        }
        if($request->has('quotation_id')){
            $insertPo['quotation_id'] = $request->quotation_id;
        }
        $id_po = DB::table('purchase_order_customers')->insertGetId($insertPo);
        $row = collect($request->row)->map(function($item) use($request, $id_po){
            $item['po_id']    = $id_po;
            $item['price']    =  preg_replace('/\.||\,/','',$item['price']);
            $item['subtotal'] =  preg_replace('/\.||\,/','',$item['subtotal']);
            return $item;

        });
        PoCustomerItem::insert($row->toArray());
        activity()->causedBy(Auth::id())->log('Customer Po created!');
        return redirect('customer/purchase-order')->with('status-success', Lang::get('dashboard.success_saved'));

    }

    public function showPo($uuid)
    {
        $data = PurchaseOrderCustomer::with(['quotation', 'invoice', 'list_items', 'tax', 'discount', 'customer'])->where('uuid', $uuid)->first();

        return view('dashboard.customer.edit-po', ['data' => $data]);
    }

    public function updatePo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|exists:customers,customer_id',
            'file'        => 'nullable|max:6000'
        ]);
        if($validator->fails()){
            return redirect('/customer/create-quotation')->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_saved'));
        }
        $updatePo = [
                'customer_id'        => $request->customer_id,
                'status'             => $request->status,
                'tax'                => $request->tax,
                'discount'           => $request->discount,
                'term_and_condition' => $request->term_and_condition
        ];
        if($request->hasFile('file')){
            $path = $request->file->storeAs("public/po-customer", Str::random(5).'_'.$request->file->getClientOriginalName());
            $locationFile = Storage::url($path);
            $updatePo['file'] = $locationFile;
        }
        DB::table('purchase_order_customers')->where('po_id', $request->po_id)
        ->update($updatePo);

        $row = collect($request->row)->map(function($item) use($request){
            $item['po_id']    = $request->po_id;
            $item['price']    =  preg_replace('/\.||\,/','',$item['price']);
            $item['subtotal'] =  preg_replace('/\.||\,/','',$item['subtotal']);
            return $item;

        });
        DB::table('po_customer_items')->where('po_id', $request->po_id)->delete();
        DB::table('po_customer_items')->insert($row->toArray());
        activity()->causedBy(Auth::id())->log('Customer Po updated!');
        return redirect('customer/purchase-order')->with('status-success', Lang::get('dashboard.success_saved'));
    }


    public function invoiceToJournalSales($id, $serial, $request, $transaction_at)
    {
        $subTotal         = $request->subTotal;
        $subTotalDiscount = $request->subTotalDiscount;
        $taxes            = $request->tax;
        $rate             = $request->rate;
        $rate_tax         = $request->rate_tax;
        $total            = $request->grandTotal;


        $seri = Journal::withTrashed()->orderByDesc('serial')->first()->serial + 1;

        $journal = new Journal;
        $journal->code             = $seri.'/'.date_format(date_create(now()), "m/Y");
        $journal->serial           = $seri;
        $journal->uuid             = Str::uuid();
        $journal->description      = 'Penjualan Dari INV'.$serial;
        $journal->status           = 2;  // Status utk dimasukan ke dalam jurnal
        $journal->transaction_at   = $transaction_at;
        $journal->customer_invoice_id = $id;
        $journal->save();

        $account           = SettingAccount::first();
        $accountReceivable = $account->receivable_account_id;
        $accountSales      = $account->sales_account_id;
        $accountProfitRate = $account->sales_profit_rate_account_id;
        $accountLossRate   = $account->sales_loss_rate_account_id;
        // $accountDiscount   = $account->sales_discount_account_id;

        $journalDetails = [
            [
                'journal_id' => $journal->id,
                'account_id' => $accountReceivable,
                'description'=> "Piutang Usaha dari INV{$serial}",
                'debet'      => $total,
                'credit'     => 0
            ]
        ];

        // selisih kurs
        if($request->currency > 1 && $rate_tax > 0){
            $konversi   = ($subTotal/$rate);
            $totalPajak = collect($request->tax)->sum('amount');
            $persentase = $konversi*($totalPajak/100);
            $selisih    = abs(($persentase*$rate) - ($persentase*$rate_tax));

            if($rate < $rate_tax && $selisih){
                //rugi
                $journalDetails[] = [
                    'journal_id'  => $journal->id,
                    'account_id'  => $accountLossRate,
                    'description' => 'Rugi selish kurs',
                    'debet'       => $selisih,
                    'credit'      => 0
                ];
            }elseif($rate > $rate_tax && $selisih){
                //laba
                $journalDetails[] = [
                    'journal_id'  => $journal->id,
                    'account_id'  => $accountProfitRate,
                    'description' => 'Laba selish kurs',
                    'debet'       => $selisih,
                    'credit'      => 0
                ];
            }
        }
        $journalDetails[] =             [
            'journal_id'  => $journal->id,
            'account_id'  => $accountSales,
            'description' => "Penjualan dari INV{$serial}",
            'debet'       => 0,
            'credit'      => $subTotal
        ];
        if($taxes[0]['id']){
            foreach ($taxes as $key => $value) {

                $tax_out = $subTotal * ($value['amount']/100);

                if($rate_tax > 0 && $request->currency > 1){
                    $tax_out = ($subTotal/$rate) * ($value['amount']/100) * $rate_tax;
                }

                $journalDetails[] = [
                    'journal_id'  => $journal->id,
                    'account_id'  => $value['receivable_account_id'],
                    'description' => 'Pajak',
                    'debet'       => 0,
                    'credit'      => $tax_out
                ];
            }

        }

        activity()->causedBy(Auth::id())->log('Journal invoice sales created!');

        JournalDetail::insert($journalDetails);
    }

    // public function invoiceToJournalPayment($id, $account_id, $subTotalDiscount, $grandTotal)
    // {
    //     $statement = DB::select("SHOW TABLE STATUS LIKE 'journals'");
    //     $nextId = $statement[0]->Auto_increment;

    //     $journal = new Journal;
    //     $journal->code             = $nextId.'/'.date_format(date_create(now()), "m/Y");
    //     $journal->uuid             = Str::uuid();
    //     $journal->description      = 'Pembayaran Dari Penjualan INV'.$id;
    //     $journal->status           = 2;  // Status utk dimasukan ke dalam jurnal
    //     $journal->transaction_at   = now();
    //     $journal->customer_invoice_id = $id;
    //     $journal->save();

    //     $account = SettingAccount::first();
    //     $accountSales  = $account->sales_account_id;
    //     $accountDiscount  = $account->sales_discount_account_id;
    //     $journalDetails = [
    //         [
    //             'journal_id'  => $journal->id,
    //             'account_id'  => $account_id,
    //             'description' => "Bayar penjualan dari INV{$id}",
    //             'debet'       => $grandTotal,
    //             'credit'      => 0
    //         ],
    //         [
    //             'journal_id' => $journal->id,
    //             'account_id' => $accountSales,
    //             'description'=> "Penjualan dari INV{$id}",
    //             'debet'      => 0,
    //             'credit'     => ($subTotalDiscount) ? $grandTotal+$subTotalDiscount : $grandTotal
    //         ]
    //     ];
    //     if($subTotalDiscount){
    //         $journalDiscount[] = [
    //             'journal_id'  => $journal->id,
    //             'account_id'  => $accountDiscount,
    //             'description' => "Diskon Penjualan dari INV{$id}",
    //             'debet'       => $subTotalDiscount,
    //             'credit'      => 0
    //         ];
    //         array_splice($journalDetails, 1, 0, $journalDiscount);
    //     }
    //     JournalDetail::insert($journalDetails);
    // }

    private static function creditNoteToJournal($id, $serial, $request)
    {
        $invoice_id  = $request->invoice_id;
        $taxes       = $request->tax;
        $accountPaid = $request->paid_from_account;
        $total       = $request->grandTotal;
        $invoice = InvoiceCustomer::find($invoice_id);

        $nextId = Journal::withTrashed()->orderByDesc('serial')->first()->serial + 1;
        $journal = new Journal;
        $journal->serial           = $nextId;
        $journal->code             = $nextId.'/'.date_format(date_create(now()), "m/Y");
        $journal->uuid             = Str::uuid();
        $journal->description      = 'Nota kredit Dari CN'.$serial;
        $journal->status           = 2;  // Status utk dimasukan ke dalam jurnal
        $journal->transaction_at   = now();//$request->transaction_at;
        $journal->customer_credit_note_id = $id;
        $journal->save();

        $account = SettingAccount::first();
        $accountSalesReturn  = $account->sales_return_account_id;
        $accountReceivable  = $account->receivable_account_id;

        $journalDetails = [
            [
                'journal_id'  => $journal->id,
                'account_id'  => $accountSalesReturn,
                'description' => "Retur Penjualan",
                'debet'       => $total,
                'credit'      => 0
            ]
        ];

        $credit_total = $total;
        // ini unpaid
        if($invoice->payment == 1){
            // ada ppn
            if($taxes[0]['id']){
                foreach ($taxes as $key => $value) {
                    $tax_amount = $value['amount'];
                    $tax_out    = $total * ($tax_amount/100);
                    $credit_total += $tax_out;

                    $journalDetails[] = [
                        'journal_id'  => $journal->id,
                        'account_id'  => $value['receivable_account_id'],
                        'description' => "PPN",
                        'debet'       => $tax_out,
                        'credit'      => 0
                    ];
                }
            }

            $journalDetails[] = [
                'journal_id'  => $journal->id,
                'account_id'  => $accountReceivable,
                'description' => "Piutang",
                'debet'       => 0,
                'credit'      => $credit_total
            ];

        }elseif($invoice->payment == 2){ // ini paid
            // ada ppn
            if($taxes[0]['id']){
                foreach ($taxes as $key => $value) {
                    $tax_amount = $value['amount'];
                    $tax_out    = $total * ($tax_amount/100);
                    $credit_total += $tax_out;

                    $journalDetails[] = [
                        'journal_id'  => $journal->id,
                        'account_id'  => $value['receivable_account_id'],
                        'description' => "PPN",
                        'debet'       => $tax_out,
                        'credit'      => 0
                    ];
                }
            }
            $journalDetails[] = [
                'journal_id'  => $journal->id,
                'account_id'  => $accountPaid,
                'description' => "Akun Kas/Bank",
                'debet'       => 0,
                'credit'      => $credit_total
            ];

        }


        JournalDetail::insert($journalDetails);
    }

    public function debetNoteToJournal($id, $serial, $request)
    {

        $total    = $request->grandTotal;
        $subTotal = $request->subTotal;
        $taxes    = $request->tax;
        $invoice = InvoiceCustomer::find($request->invoice_id);
        $nextId = Journal::withTrashed()->orderByDesc('serial')->first()->serial + 1;
        $journal = new Journal;
        $journal->code             = $nextId.'/'.date_format(date_create(now()), "m/Y");
        $journal->serial           = $nextId;
        $journal->uuid             = Str::uuid();
        $journal->description      = 'Nota Debit Dari DN'.$id;
        $journal->status           = 2;  // Status utk dimasukan ke dalam jurnal
        $journal->transaction_at   = now();
        $journal->customer_debit_note_id = $id;
        $journal->save();

        $account = SettingAccount::first();
        $accountPurchaseReturn  = $account->purchase_return_account_id;
        $accountSales = $account->sales_account_id;

        $accountProfitRate = $account->sales_profit_rate_account_id;
        $accountLossRate   = $account->sales_loss_rate_account_id;
        if($request->currency > 1){
            $subTotal *= $request->rate;
        }

        $sum_debit = 0;
        $sum_credit = 0;
        // jika alokasi credit
        if($invoice->payment == 1){
            // utang
            $journalDetails[] = [
                'journal_id'  => $journal->id,
                'account_id'  => $accountSales,
                'description' => "utang",
                'debet'       => $total,
                'credit'      => 0
            ];
            $sum_debit += $total;
            // ada ppn
            if($taxes[0]['id']){
                foreach ($taxes as $key => $value) {
                    $tax_amount = $value['amount'];
                    $tax_out    = $subTotal * ($tax_amount/100);
                    if($request->currency > 1){
                        $tax_out = (($request->subTotal* $request->rate_tax) * ($tax_amount/100));
                    }
                    $journalDetails[] = [
                        'journal_id'  => $journal->id,
                        'account_id'  => $value['payable_account_id'],
                        'description' => "PPN",
                        'debet'       => 0,
                        'credit'      => $tax_out
                    ];
                    $sum_credit += $tax_out;

                }
            }

            $journalDetails[] = [
                'journal_id'  => $journal->id,
                'account_id'  => $request->account_id,
                'description' => "Retur",
                'debet'       => 0,
                'credit'      => $subTotal
            ];
            $sum_credit += $subTotal;
        }elseif($invoice->payment == 2){
            $journalDetails[] = [
                'journal_id'  => $journal->id,
                'account_id'  => $invoice->purchase_account,
                'description' => "Kas/Bank",
                'debet'       => $total,
                'credit'      => 0
            ];
            $sum_debit += $total;
            // ada ppn
            if($taxes[0]['id']){
                foreach ($taxes as $key => $value) {
                    $tax_amount = $value['amount'];
                    $tax_out    = $subTotal * ($tax_amount/100);

                    if($request->currency > 1){
                        $tax_out = (($request->subTotal* $request->rate_tax) * ($tax_amount/100));
                    }

                    $journalDetails[] = [
                        'journal_id'  => $journal->id,
                        'account_id'  => $value['payable_account_id'],
                        'description' => "PPN",
                        'debet'       => 0,
                        'credit'      => $tax_out
                    ];
                    $sum_credit += $tax_out;

                }
            }
            $journalDetails[] = [
                'journal_id'  => $journal->id,
                'account_id'  => $accountPurchaseReturn,
                'description' => "utang",
                'debet'       => 0,
                'credit'      => $subTotal
            ];
            $sum_credit += $subTotal;
        }
        // laba rugi atas selisih kurs
        if($request->currency > 1){
            $selisih = abs($sum_debit - $sum_credit);
            if($sum_debit < $sum_credit){
                //masuk ke laba
                $journalProfitLoss[] = [
                    'journal_id'  => $journal->id,
                    'account_id'  => $accountLossRate,
                    'description' => "Rugi selisih kurs",
                    'debet'       => $selisih,
                    'credit'      => 0
                ];
            }elseif($sum_credit < $sum_debit){
                // masuk ke piutang
                $journalProfitLoss[] = [
                    'journal_id'  => $journal->id,
                    'account_id'  => $accountProfitRate,
                    'description' => "Laba selisih kurs",
                    'debet'       => 0,
                    'credit'      => $selisih
                ];
            }
            array_splice($journalDetails, 1, 0, $journalProfitLoss);
        }

        JournalDetail::insert($journalDetails);
    }

    public function listInvoice()
    {
        return view('dashboard.customer.list-invoice');
    }

    public function tableInvoice(Request $request)
    {
        $data = InvoiceCustomer::join('customers', 'invoice_customers.customer_id', '=', 'customers.customer_id')
                ->select('invoice_customers.*', 'customers.first_name', 'customers.last_name', 'customers.company');
        if($request->status < 4){
            $data = $data->where('status', $request->status);
        }
        if($request->filled(['start_date', 'to_date'])){
            $data = $data->whereBetween('invoice_customers.created_at', [date_format(date_create($request->start_date), 'Y-m-d')." 00:00:00", date_format(date_create($request->to_date), 'Y-m-d')." 23:59:59"]);
        }
        return datatables()->of($data)->make(true);
    }

    public function createInvoice(Request $request)
    {
        $data = null;
        $customer = null;
        if($request->has('quotation_id') || $request->has('bc_id') || $request->has('customer_id')){
            $data = collect($request->all());//->toJson();
            $customer = collect([
                'customer_id' => $request->customer_id,
                'customer_name' => $request->customer_name
            ]);
        }
        return view('dashboard.customer.create-invoice', ['data' => $data, 'customer' => $customer]);
    }

    public function storeInvoice(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|exists:customers,customer_id',
            'quotation_id'=> 'nullable|exists:quotation_customers,quotation_id',
            'bc_id'       => 'nullable|exists:bc_customers,bc_id',
            'payment'     => 'nullable|in:0,1,2',
            'due_date'    => 'required|date|date_format:d-m-Y',
        ]);
        if($validator->fails()){
            return redirect('/customer/create-invoice')->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_saved'));
        }
        $invoice_date = date_format(date_create($request->invoice_date), "Y-m-d");
        $serial = InvoiceCustomer::withTrashed()->count()+1;
        $inv = new InvoiceCustomer;
        $inv->invoice_id         = $request->invoice_id;
        $inv->customer_id        = $request->customer_id;
        $inv->bc_id              = $request->bc_id;
        $inv->quotation_id       = $request->quotation_id;
        $inv->serial             = $serial;
        $inv->uuid               = Str::uuid();
        $inv->term_and_condition = $request->term_and_condition;
        $inv->term_of_payment    = $request->term_of_payment;
        $inv->currency           = $request->currency;
        $inv->rate               = $request->rate;
        $inv->rate_tax           = $request->rate_tax;
        $inv->attn               = $request->attn;
        $inv->vessel             = $request->vessel;
        $inv->voyage             = $request->voyage;
        $inv->eta                = $request->eta ? date_format(date_create($request->eta), "Y-m-d"):null;
        $inv->bl_no              = $request->bl_no;
        $inv->from               = $request->from;
        $inv->to                 = $request->to;
        $inv->description        = $request->description;
        $inv->status             = $request->status;
        $inv->discount           = $request->discount;
        // $inv->payment            = $request->payment;
        $inv->created_time       = $request->created_time;
        $inv->invoice_date       = $invoice_date;
        $inv->due_date           = date_format(date_create($request->due_date), "Y-m-d");
        $inv->is_invoice         = $request->is_invoice;

        // if invoice paid cash(dibayar dimuka)
        if($request->status == 2 && $request->payment == 2){
            $inv->payment_date = date_format(date_create($request->payment_date), "Y-m-d");
            $inv->payment_ref  = $request->payment_ref;
            $inv->account_id   = $request->account_id;
        }
        $inv->save();
        $id = $inv->invoice_id;

        // if invoice set to publish
        if($request->status == 2){
            $this->invoiceToJournalSales(
                $id,
                $inv->serializing,
                $request,
                $invoice_date
            );
        }

        // invoice items
        if($request->filled('row')){
            $row = collect($request->row)->map(function($item) use($id){
                $item['invoice_id'] = $id;
                return $item;
            });
            InvoiceCustomerItem::insert($row->toArray());
        }

        // invoice taxes
        if($request->tax[0]['id']){
            $taxes = collect($request->tax);

            $taxes = $taxes->filter(function($value, $key) {
                return $value['id'];
            });

            $taxes = $taxes->map(function($taxes)use($id){
                return [
                    'tax_id' => $taxes['id'],
                    'invoice_id' => $id
                ];
            });
            InvoiceTaxCustomer::insert($taxes->toArray());
        }

        activity()->causedBy(Auth::id())->log('Customer invoice created!');
        return redirect('customer/invoice')->with('status-success', Lang::get('dashboard.success_saved'));
    }

    public function showInvoice($uuid)
    {
        $data = InvoiceCustomer::with(['booking_confirmation.customer', 'paymentInvoice', 'quotation.customer','list_items', 'taxes', 'discount', 'customer', 'void'])->where('uuid', $uuid)->first();
        abort_if(!$data, 404, 'Not found');
        $isFormDisabled = ($data->paidAmount > 0 || $data->status == 0) ? "disabled":"";
        $paymentAmount = $data->paymentInvoice->sum(function($payment){
                return $payment['status'] ? $payment['payment_amount']:0;
        });
        Debugbar::info($data);
        return view('dashboard.customer.edit-invoice', ['data' => $data, 'isFormDisabled' => $isFormDisabled, 'paymentAmount' => $paymentAmount]);
    }

    public function updateInvoice(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'customer_id' => 'nullable|exists:customers,customer_id',
            'bc_id'       => 'nullable|exists:bc_customers,bc_id',
            'payment'     => 'nullable|in:0,1,2',
            'due_date'    => 'nullable|date|date_format:d-m-Y',
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_saved'));
        }
        $invoice_date = date_format(date_create($request->invoice_date), "Y-m-d");
        $updateInvoice = [
            'customer_id'        => $request->customer_id,
            // 'payment'            => $request->payment,
            'bc_id'              => $request->bc_id,
            'due_date'           => date_format(date_create($request->due_date), "Y-m-d"),
            'invoice_date'       => $invoice_date,
            'term_of_payment'    => $request->term_of_payment,
            'currency'           => $request->currency,
            'rate'               => $request->rate,
            'rate_tax'           => $request->rate_tax,
            'attn'               => $request->attn,
            'vessel'             => $request->vessel,
            'voyage'             => $request->voyage,
            'eta'                => date_format(date_create($request->eta), "Y-m-d"),
            'bl_no'              => $request->bl_no,
            'from'               => $request->from,
            'to'                 => $request->to,
            'description'        => $request->description,
            'discount'           => $request->discount,
            'term_and_condition' => $request->term_and_condition,
            'is_invoice'         => $request->is_invoice

        ];
        // if it saved with (status publish & payment paid)
        if($request->filled('status')){
            if ($request->status == 2) {
                $updateInvoice['status'] = $request->status;
                // if invoice set to publish
                $this->invoiceToJournalSales($request->invoice_id, $request->serial, $request, $invoice_date);
            }
        }

        InvoiceCustomer::where('invoice_id', $request->invoice_id)
        ->update($updateInvoice);

        $row = collect($request->row)->map(function($item) use($request){
            $item['invoice_id']     = $request->invoice_id;
            return $item;

        });
        InvoiceCustomerItem::where('invoice_id', $request->invoice_id)->delete();
        InvoiceCustomerItem::insert($row->toArray());

        if($request->tax[0]['id']){
            InvoiceTaxCustomer::where('invoice_id', $request->invoice_id)->delete();
            $taxes = collect($request->tax);
            $taxes = $taxes->filter(function($value, $key) {
                return $value['id'];
            });
            $taxes = $taxes->map(function($taxes)use($request){
                return [
                    'tax_id' => $taxes['id'],
                    'invoice_id' => $request->invoice_id
                ];
            });
            InvoiceTaxCustomer::insert($taxes->toArray());
        }else{
            InvoiceTaxCustomer::where('invoice_id', $request->invoice_id)->delete();
        }

        activity()->causedBy(Auth::id())->log('Customer invoice updated!');
        return redirect('customer/invoice')->with('status-success', Lang::get('dashboard.success_saved'));
    }

    public function voidInvoice(Request $request)
    {
        InvoiceCustomer::where('invoice_id',$request->invoice_id)
                        ->update(['status' => 0, 'payment' => 0, 'void_at' => now(), 'void_by' => Auth::id(), 'void_note' => $request->reason]);
        if($request->type == 'void'){
            Journal::where('customer_invoice_id', $request->invoice_id)->update(['status' => 3]);
            PaymentCustomer::where('invoice_id', $request->invoice_id)->update(['status' => 0]);
            activity()->causedBy(Auth::id())->log('Customer invoice Voided!');

        }elseif($request->type == 'delete'){
            InvoiceCustomer::destroy($request->invoice_id);
            activity()->causedBy(Auth::id())->log('Customer invoice delete!');

        }
        if($request->ajax()){
            return response()->json(['status' => true, 'statusText' => 'Invoice Voided']);
        }
        return redirect('customer/invoice')->with('status-success', Lang::get('dashboard.success_saved'));
    }

    public function printInvoice($uuid)
    {
        $web  = WebSetting::with('company')->find(session()->get('company_id'));
        $invoice = InvoiceCustomer::whereUuid($uuid)->with(['list_items', 'taxes', 'customer', 'customer.province', 'paymentInvoice', 'customer.regency', 'customer.district', 'customer.village' ])->first();
        abort_if(!$invoice, 404, 'Not found');

        // $pdf = PDF::loadView('dashboard.customer.print-invoice', ['data' => $data, 'web' => $web, 'items' => $item, 'taxes' => $taxes])->setPaper('a4', 'potrait');
        // return $pdf->stream('invoice-customer-INV'.$id.'.pdf');
        return view('dashboard.customer.cetak-invoice', ['data' => $invoice, 'web' => $web]);

    }

    public function listCreditNote()
    {
        return view('dashboard.customer.list-credit-note');
    }

    public function tableCreditNote(Request $request)
    {
        $data = CreditNoteCustomer::select(
            'customers.first_name',
            'customers.last_name',
            'invoice_customers.serial as serialInvoice',
            'invoice_customers.uuid as uuidInvoice',
            'invoice_customers.invoice_date as dateInvoice',
            'customers.company',
            'sellers.name',
            'credit_note_customers.*'
        )
                ->join('invoice_customers', 'credit_note_customers.invoice_id', '=', 'invoice_customers.invoice_id')
                ->join('customers', 'credit_note_customers.customer_id', '=', 'customers.customer_id')
                ->leftJoin('sellers', 'credit_note_customers.seller_id', '=', 'sellers.id');
        if($request->filled(['start_date', 'to_date'])){
            $data = $data->whereBetween('credit_note_customers.created_at', [date_format(date_create($request->start_date), 'Y-m-d')." 00:00:00", date_format(date_create($request->to_date), 'Y-m-d')." 23:59:59"]);
        }
        return datatables()->of($data)->make(true);
    }

    public function createCreditNote(Request $request)
    {
        $data = null;
        if($request->has('items')){
            // $data = collect($request->all())->toJson();
            $data = collect($request->all());
        }
        return view('dashboard.customer.create-credit-note', ['data' => $data]);
    }

    public function storeCreditNote(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id'   => 'required|exists:customers,customer_id',
            // 'seller_id'     => 'required|exists:sellers,id',
            'invoice_id'    => 'required|exists:invoice_customers,invoice_id',
            'proof_picture' => 'nullable|file|max:6000',
            // 'sales'         => 'nullable',
            'note'          => 'nullable'
        ]);
        if($validator->fails()){
            return redirect('/customer/create-credit-note')->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_saved'));
        }

        $serial = CreditNoteCustomer::withTrashed()->count()+1;
        $creditNote = new CreditNoteCustomer;
        $creditNote->uuid              = Str::uuid();
        $creditNote->serial            = $serial;
        $creditNote->customer_id       = $request->customer_id;
        $creditNote->invoice_id        = $request->invoice_id;
        $creditNote->paid_from_account = $request->paid_from_account;
        $creditNote->note              = $request->note;
        $creditNote->currency          = $request->currency;
        $creditNote->rate              = $request->rate;
        $creditNote->rate_tax          = $request->rate_tax;
        // $creditNote->seller_id         = $request->seller_id;
        $creditNote->status            = $request->status;
        if($request->hasFile('proof_picture')){
            $path = $request->proof_picture->storeAs("public/credit-note-customer", Str::random(5).'_'.$request->proof_picture->getClientOriginalName());
            $locationFile = Storage::url($path);
            $creditNote->proof_picture = $locationFile;
        }
        $creditNote->save();
        $id = $creditNote->id;
        // Credit note items
        $row = collect($request->row)->map(function($item) use($id){
            $item['credit_note_id'] = $id;
            $item['price']      =  $item['price'];
            $item['subtotal']   =  $item['subtotal'];
            return $item;
        });
        CreditNoteItemCustomer::insert($row->toArray());

        // tax
        if($request->tax[0]['id']){
            $taxes = collect($request->tax)->map(function($taxes)use($id){
                return [
                    'tax_id' => $taxes['id'],
                    'credit_note_id' => $id
                ];
            });
            CreditNoteTaxCustomer::insert($taxes->toArray());
        }
        if($request->status == 2){
            static::creditNoteToJournal(
                $id,
                $creditNote->serializing,
                $request
            );
        }

        activity()->causedBy(Auth::id())->log('Customer Credit Note Created!');
        return redirect('/customer/credit-note')->with('status-success', Lang::get('dashboard.success_saved'));;
    }

    public function showCreditNote($uuid)
    {
        $data = CreditNoteCustomer::with(['customer', 'invoice', 'list_items', 'paid_account', 'seller', 'taxing'])->whereUuid($uuid)->first();
        abort_if(!$data, 404, 'Not found');
        return view('dashboard.customer.show-credit-note', ['data' => $data]);
    }

    public function updateCreditNote(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'proof_picture' => 'nullable|file|max:6000',
            // 'sales'         => 'nullable',
            'note'          => 'nullable'
        ]);
        if($validator->fails()){
            return redirect('/customer/create-credit-note')->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_saved'));
        }
        $creditNote = CreditNoteCustomer::find($request->id);
        $creditNote->paid_from_account = $request->paid_from_account;
        $creditNote->note              = $request->note;

        if($request->hasFile('proof_picture')){
            $path = $request->proof_picture->storeAs("public/credit-note-customer", Str::random(5).'_'.$request->proof_picture->getClientOriginalName());
            $locationFile = Storage::url($path);
            $update['proof_picture'] = $locationFile;
        }
        if($request->filled('status')){
            $creditNote->status = $request->status;
        }
        // $id = CreditNoteCustomer::update($update);
        $creditNote->save();
        $id = $request->id;

        // Credit note items
        CreditNoteItemCustomer::where('credit_note_id', $request->id)->delete();
        $row = collect($request->row)->map(function($item) use($id){
            $item['credit_note_id'] = $id;
            $item['price']          = $item['price'];
            $item['subtotal']       = $item['subtotal'];
            return $item;
        });
        CreditNoteItemCustomer::insert($row->toArray());

        // tax
        CreditNoteTaxCustomer::where('credit_note_id', $request->id)->delete();
        if($request->tax[0]['id']){
            $taxes = collect($request->tax)->map(function($taxes)use($id){
                return [
                    'tax_id' => $taxes['id'],
                    'credit_note_id' => $id
                ];
            });
            CreditNoteTaxCustomer::insert($taxes->toArray());
        }

        if($request->status == 2){
            static::creditNoteToJournal(
                $id,
                $creditNote->serializing,
                $request
            );
        }

        activity()->causedBy(Auth::id())->log('Customer Credit Note Updated!');
        return redirect('/customer/credit-note')->with('status-success', Lang::get('dashboard.success_saved'));
    }

    public function printCreditNote($uuid)
    {
        $web  = WebSetting::with(['company'])->find(session()->get('company_id'));
        $debitNote = CreditNoteCustomer::whereUuid($uuid)->with(['customer', 'list_items'])->first();
        abort_if(!$web, 404, 'Not found');

        return view('dashboard.customer.cetak-credit', ['data' => $debitNote, 'web' => $web]);
    }

    public function deleteCreditNote(Request $request)
    {
        if($request->type == 'cancel'){
            $cn = CreditNoteCustomer::find($request->id);
            $cn->status = 0;
            $cn->save();
            Journal::where('customer_credit_note_id', $request->id)->update(['status' => 3]);
            activity()->causedBy(Auth::id())->log('Customer Credit Note voided!');

        }else{
            CreditNoteCustomer::destroy($request->id);
            activity()->causedBy(Auth::id())->log('Customer Credit Note deleted!');
        }
        return response()->json(['status' => true, 'statusText' => 'Customer Credit Note voided']);
    }


    public function listDebitNote()
    {
        return view('dashboard.customer.list-debit-note');
    }

    public function tableDebitNote(Request $request)
    {
        $data = DebitNoteCustomer::select('customers.first_name', 'debit_note_customers.uuid', 'debit_note_customers.status', 'debit_note_customers.payment', 'customers.last_name', 'customers.company', 'debit_note_customers.*')
                ->join('customers', 'debit_note_customers.customer_id', '=', 'customers.customer_id');
        if($request->filled(['start_date', 'to_date'])){
            $data = $data->whereBetween('debit_note_customers.created_at', [date_format(date_create($request->start_date)." 00:00:00", 'Y-m-d'), date_format(date_create($request->to_date), 'Y-m-d')." 23:59:59"]);
        }
        return datatables()->of($data)->make(true);
    }

    public function showDebitNote($uuid)
    {
        $data = DebitNoteCustomer::with(['customer', 'invoice', 'list_items', 'taxing', 'account'])->whereUuid($uuid)->first();
        $isFormDisabled = $data->status == 2 ? 'disabled':'';
        Debugbar::info($data);
        return view('dashboard.customer.edit-debit-note', ['data' => $data, 'isFormDisabled' => $isFormDisabled]);
    }

    public function createDebitNote(Request $request)
    {
        $data = null;
        if($request->has('items')){
            $data = collect($request->all());
        }
        return view('dashboard.customer.create-debit-note', ['data' => $data]);
    }

    public function updateDebitNote(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|exists:customers,customer_id',
            'invoice_id'  => 'required|exists:invoice_customers,invoice_id',
            'note'        => 'nullable'
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_saved'));
        }

        $debitNote = DebitNoteCustomer::find($request->id);
        $debitNote->invoice_id      = $request->invoice_id;
        $debitNote->customer_id     = $request->customer_id;
        $debitNote->currency        = $request->currency;
        $debitNote->rate            = $request->rate;
        $debitNote->rate_tax        = $request->rate_tax;
        $debitNote->note            = $request->note;
        $debitNote->status          = $request->status;
        $debitNote->account_id      = $request->account_id;
        $debitNote->debit_note_date = date_format(date_create($request->debit_note_date), "Y-m-d");
        $debitNote->save();
        $id = $request->id;
        DebitNoteItemCustomer::where('debit_note_id', $request->id)->delete();
        $row = collect($request->row)->map(function($item) use($id){
            $item['debit_note_id'] = $id;
            $item['price']          = $item['price'];
            $item['subtotal']       = $item['subtotal'];
            return $item;
        });
        DebitNoteItemCustomer::insert($row->toArray());

        // tax
        DebitNoteTaxCustomer::where('debit_note_id', $request->id)->delete();
        if($request->tax[0]['id']){
            $taxes = collect($request->tax)->map(function($taxes)use($id){
                return [
                    'tax_id' => $taxes['id'],
                    'debit_note_id' => $id
                ];
            });
            DebitNoteTaxCustomer::insert($taxes->toArray());
        }

        // if($request->status == 2){
        //     $this->debetNoteToJournal(
        //         $id,
        //         $request->serial,
        //         $request
        //     );
        // }

        activity()->causedBy(Auth::id())->log('Customer Debit Note Updated!');
        return redirect('/customer/debit-note')->with('status-success', Lang::get('dashboard.success_saved'));
    }

    public function storeDebitNote(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|exists:customers,customer_id',
        ]);
        if($validator->fails()){
            return redirect('/customer/create-debit-note')->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_saved'));
        }
        $uuid = Str::uuid();
        $serial = DebitNoteCustomer::withTrashed()->count() + 1;
        $debitNote                  = new DebitNoteCustomer;
        $debitNote->uuid            = $uuid;
        $debitNote->serial          = $serial;
        $debitNote->invoice_id      = $request->invoice_id;
        $debitNote->customer_id     = $request->customer_id;
        $debitNote->currency        = $request->currency;
        $debitNote->rate            = $request->rate;
        $debitNote->rate_tax        = $request->rate_tax;
        $debitNote->note            = $request->note;
        $debitNote->account_id      = $request->account_id;
        $debitNote->status          = $request->status;
        $debitNote->debit_note_date = date_format(date_create($request->debit_note_date), "Y-m-d");
        $debitNote->save();
        $id = $debitNote->id;
        // item
        $row = collect($request->row)->map(function($item) use($id){
            $item['debit_note_id'] = $id;
            $item['price']          = $item['price'];
            $item['subtotal']       = $item['subtotal'];
            return $item;
        });
        $debitNote->list_items()->createMany($row->toArray());
        // tax
        if($request->tax[0]['id']){
            $taxes = collect($request->tax)->map(function($taxes)use($id){
                return [
                    'tax_id' => $taxes['id'],
                    'debit_note_id' => $id
                ];
            });
            $debitNote->debit_note_tax()->createMany($taxes->toArray());
        }
        // if ($request->status == 2) {
        //     $this->debetNoteToJournal($id, $serial, $request);
        // }

        activity()->causedBy(Auth::id())->log('Customer Debit Note Created!');
        return redirect('/customer/debit-note')->with('status-success', Lang::get('dashboard.success_saved'));;
    }

    public function printDebitNote($id)
    {
        $debitNote = DebitNoteCustomer::with(['customer', 'seller'])->find($id);
        $web  = WebSetting::find(session()->get('company_id'));

        // return view('dashboard.customer.print-debit-note', ['data' => $debitNote, 'web' => $web]);
        $pdf = PDF::loadView('dashboard.customer.print-debit-note', ['data' => $debitNote, 'web' => $web])->setPaper('a4', 'potrait');
        return $pdf->stream("customer-debit-note-DN{$id}.pdf");
    }

    public function deleteDebitNote(Request $request)
    {
        $dn = DebitNoteCustomer::find($request->id);
        $dn->status = 0;
        $dn->save();
        Journal::where('customer_debit_note_id', $request->id)->update(['status' => 3]);
        activity()->causedBy(Auth::id())->log('Customer Credit Note deleted!');
        return response()->json(['status' => true, 'statusText' => 'Customer Credit Note deleted']);
    }

    public function seller()
    {
        return view('dashboard.customer.seller');
    }

    public function tableSeller()
    {
        $data = Seller::select('name', 'id', 'created_at');

        return datatables()->of($data)->make(true);
    }

    public function storeSeller(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);
        if ($request->filled('seller_id')) {
            $seller = Seller::find($request->seller_id);
        }else{
            $seller = new Seller;
        }
        $seller->name = $request->name;
        $seller->save();
        return response()->json(['status' => true, 'statusText' => 'success']);
    }

    public function showSeller($id)
    {
        return Seller::find($id);
    }
    public function deleteSeller(Request $request)
    {
        Seller::find($request->id)->delete();
        return response()->json(['status' => true, 'statusText' => 'success']);
    }
}
