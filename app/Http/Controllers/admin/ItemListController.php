<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\ListItem;
use App\Models\WebSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ItemListController extends Controller
{
    public function index()
    {
        return view('dashboard.item-list.showItem');
    }

    public function table()
    {
        $data = ListItem::whereNull('deleted_at');
        return datatables()->of($data)->make(true);
    }

    public function addForm()
    {
        return view('dashboard.item-list.addItem');
    }

    public function show($uuid)
    {
        $data = ListItem::with(['category'])->where('uuid', $uuid)->first();
        return response()->json($data);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // 'code'        => 'required|max:150',
            'name'        => 'required|max:150',
            // 'category_id' => 'required|exists:categories,id',
            'description' => 'max:250',
            // 'sell_price'  => 'required|min:0',
            // 'buy_price'   => 'required|min:0',
            // 'unit'        => 'in:0,1,2,3,4,5,6,7,8,9',
            'picture'     => 'image|max:6000',
        ]);
        if($validator->fails()){
            return redirect('/item-list/edit/'.$request->id)->withErrors($validator)->with('status-fail', Lang::get('dashboard.item_failed'))->withInput();
        }
        $updateData = [
            // 'item_id'     => $request->code,
            'name'        => $request->name,
            // 'category_id' => $request->category_id,
            'description' => $request->description,
            // 'sell_price'  => preg_replace('/\.||\,/', '', $request->sell_price),
            // 'buy_price'   => preg_replace('/\.||\,/', '', $request->buy_price),
            // 'unit'        => $request->unit
        ];
        if($request->hasFile('picture')){
            $path = $request->picture->storeAs("public/item", Str::random(5).'_'.$request->picture->getClientOriginalName());
            $locationFile = Storage::url($path);
            $updateData['picture'] = $locationFile;
        }
        ListItem::where('uuid', $request->uuid)->update($updateData);
        activity()->causedBy(Auth::id())->log('Item List updated!');
        return redirect('/item-list')->with('status-success', Lang::get('dashboard.item_updated'));

    }

    public function editForm($uuid)
    {
        // $item = ListItem::find($uuid)->with(['category'])->first();
        $item = ListItem::where('uuid', $uuid)->with(['category'])->first();
        return view('dashboard.item-list.editItem', ['item' => $item]);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // 'code'        => 'required|unique:list_items,item_id|max:150',
            'name'        => 'required|max:150',
            // 'category_id' => 'required|exists:categories,id',
            'description' => 'max:250',
            // 'sell_price'  => 'required|min:0',
            // 'buy_price'   => 'required|min:0',
            // 'unit'        => 'in:0,1,2,3,4,5,6,7,8,9',
            'picture'     => 'image|max:6000',
        ]);
        if($validator->fails()){
            return redirect('/item-list/add-form')->withErrors($validator)->with('status-fail', 'dashboard.item_failed')->withInput();
        }
        $insertData = [
            // 'item_id'     => $request->code,
            'uuid'        => Str::uuid(),
            'name'        => $request->name,
            // 'category_id' => $request->category_id,
            'description' => $request->description,
            // 'sell_price'  => preg_replace('/\.||\,/', '', $request->sell_price),
            // 'buy_price'   => preg_replace('/\.||\,/', '', $request->buy_price),
            // 'unit'        => $request->unit
        ];

        if($request->hasFile('picture')){
            $path = $request->picture->storeAs("public/item", Str::random(5).'_'.$request->picture->getClientOriginalName());
            $locationFile = Storage::url($path);
            $insertData['picture'] = $locationFile;
        }

        ListItem::create($insertData);
        activity()->causedBy(Auth::id())->log('Item List created!');
        return redirect('/item-list')->with('status-success', Lang::get(('dashboard.item_created')));
    }

    public function delete(Request $request)
    {
        ListItem::where('id', $request->id)->delete();
        activity()->causedBy(Auth::id())->log('Item deleted!');
        return response()->json(['status' => true, 'statustext' => 'Item Deleted!']);
    }
}
