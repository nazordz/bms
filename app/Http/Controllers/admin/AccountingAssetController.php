<?php

namespace App\Http\Controllers\admin;

use App\Exports\AssetExport;
use App\Http\Controllers\Controller;
use App\Models\FixedAsset;
use App\Models\Journal;
use App\Models\JournalClasification;
use App\Models\SettingAssetType;
use App\Models\Shrinkage;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class AccountingAssetController extends Controller
{
    public function index()
    {
        $last_depre = FixedAsset::join('shrinkages', 'fixed_assets.id', '=', 'shrinkages.asset_id')
                        ->where('fixed_assets.company_id', session()->get('company_id'))
                        ->select('shrinkages.to_date')
                        ->orderBy('shrinkages.to_date', 'desc')
                        ->first();
        return view('dashboard.accounting.asset.index', ['last_depre' => $last_depre]);
    }

    public function tableAsset(Request $request)
    {
        $data = FixedAsset::join('setting_asset_types', 'fixed_assets.type_id', '=', 'setting_asset_types.id')
                ->leftJoin('journal_clasifications', 'fixed_assets.clasification_id', '=', 'journal_clasifications.id')
                ->select(
                    'fixed_assets.*',
                    'setting_asset_types.name as typeName',
                    'journal_clasifications.name as ClasName'
                )
                ->where('fixed_assets.company_id', session()->get('company_id'))
                ->where('fixed_assets.status', $request->status);
        return datatables()->eloquent($data)->make(true);
    }

    public function createAsset()
    {
        return view('dashboard.accounting.asset.create');
    }
    public function printAsset($status)
    {
        $asset = new AssetExport($status);
        return $asset->download('asset-'.session()->get('company_name').'.xlsx');
    }
    public function storeAsset(Request $request)
    {
        $request->validate([
            'asset_code'                       => 'nullable',
            'asset_name'                       => 'required',
            'asset_type'                       => 'required',
            'clasification'                    => 'nullable',
            'date_acquistion'                  => 'required',
            'price_acquistion'                 => 'required|min:1',
            'serial_number'                    => 'nullable',
            'depreciation_start_date'          => 'required',
            'residue_value'                    => 'nullable',
            'depreciation_rates'               => 'required',
            'shrinkage_method'                 => 'required',
            'status'                           => 'required',
            'account_asset'                    => 'required|exists:chart_accounts,id',
            'account_expense'                  => 'required|exists:chart_accounts,id',
            'account_depreciation'             => 'required|exists:chart_accounts,id',
            'accumulated_depreciation_account' => 'required|exists:chart_accounts,id',
        ]);

        $serial = FixedAsset::where('company_id', session()->get('company_id'))->withTrashed()->count()+1;
        if($request->filled('uuid')){
            $asset = FixedAsset::whereUuid($request->uuid)->first();
            if($request->filled('status')){
                $asset->status = $request->status;
            }
        }else{
            $asset = new FixedAsset;
            $asset->uuid   = Str::uuid();
            $asset->status = $request->status;
        }
        $asset->serial                           = $request->filled('asset_code') ? $request->asset_code : date('Y-m').'-'.$serial;
        $asset->name                             = $request->asset_name;
        $asset->type_id                          = $request->asset_type;
        $asset->clasification_id                 = $request->clasification;
        $asset->date_acquistion                  = $request->date_acquistion;
        $asset->price_acquistion                 = $request->price_acquistion;
        $asset->serial_number                    = $request->serial_number;
        $asset->depreciation_start_date          = $request->depreciation_start_date;
        $asset->residue                          = $request->residue_value;
        $asset->shrinkage_method                 = $request->shrinkage_method;
        $asset->estimated_age                    = $request->estimated_age;
        $asset->depreciation_rates               = $request->depreciation_rates;
        $asset->asset_account                    = $request->account_asset;
        $asset->expense_account                  = $request->account_expense;
        $asset->depreciation_account             = $request->account_depreciation;
        $asset->accumulated_depreciation_account = $request->accumulated_depreciation_account;
        $asset->save();
        if($request->status == 2){
            $fixed_asset = [
                'id'     => $asset->id,
                'serial' => $request->filled('asset_code') ? $request->asset_code : date('Y-m').'-'.$serial,
                'transaction_at' => $request->date_acquistion
            ];
            $this->storeAssetJournal($request->account_asset, $request->account_expense, $request->price_acquistion, $fixed_asset);
        }
        return ['status' => 'oke'];
    }
    public function storeAssetJournal($asset_account, $expense_account, $nominal, $fixed_asset)
    {
        $nextId = Journal::count() + 1;

        $journal = new Journal;
        $journal->code           = $nextId.'/'.date_format(date_create(now()), "m/Y");
        $journal->serial         = $nextId;
        $journal->uuid           = Str::uuid();
        $journal->description    = 'Aset tetap dari Kode aset '.$fixed_asset['serial'];
        $journal->status         = 2;
        $journal->transaction_at = $fixed_asset['transaction_at'];
        $journal->fixed_asset_id = $fixed_asset['id'];
        $journal->save();

        $journalDetails = [
            [
                'journal_id'  => $journal->id,
                'account_id'  => $asset_account,
                'description' => "Akun Aset ",
                'debet'       => $nominal,
                'credit'      => 0
            ],
            [
                'journal_id'  => $journal->id,
                'account_id'  => $expense_account,
                'description' => "Akun pengeluaran",
                'debet'       => 0,
                'credit'      => $nominal
            ],
        ];
        $journal->details()->createMany($journalDetails);
    }
    public function getAssetType()
    {
        $data = SettingAssetType::all();
        return $data;
    }
    public function getClasification()
    {
        $data = JournalClasification::all();
        return $data;
    }

    public function lastDepreciation()
    {
        $data = Shrinkage::where('status', 1)
                ->whereIn('asset_id', function($query){
                    $query->select('id')->from('fixed_assets')
                    ->where('fixed_assets.company_id', session()->get('company_id'));
                })
                ->orderBy('to_date', 'desc')->first();
        $from_date = null;
        if($data){
            $from_date = $data->to_date;
            $already = true;
        }else{
            $asset = FixedAsset::where('company_id', session()->get('company_id'))
                    ->orderBy('depreciation_start_date', 'desc')
                    ->first();
            // $startDate = Carbon::now()->startOfMonth();
            // $from_date = $startDate->format('Y-m-d');
            $from_date = $asset->depreciation_start_date ?? null;
            $already = false;

        }
        return ['from_date' => $from_date, 'already_depreciated' => $already];
    }

    public function listDepreciation()
    {
        return view('dashboard.accounting.asset.calculate-depreciation');
    }

    public function tableDepreciation(Request $request)
    {
        // $data = FixedAsset::with(['shrinkage', 'type'])->where('company_id', session()->get('company_id'))->get();
        $data = SettingAssetType::with(['fixed_asset.shrinkage'])->get();
        return ['lists' => $data];

    }
    public function storeDepreciation(Request $request)
    {
        $request->validate([
            'depreciation.*.asset_id'          => 'required|exists:chart_accounts,id',
            'depreciation.*.depreciation_value'=> 'required',
            'depreciation.*.from_date'         => 'required',
            'depreciation.*.to_date'           => 'required',
        ]);
        $depreciation = collect($request->depreciation);
        Shrinkage::insert($depreciation->toArray());
        return ['status' => 'oke', 'req' => $depreciation->toArray()];
    }

    public function editAsset($uuid)
    {
        $asset = FixedAsset::with([
            'type',
            'clasification',
            'shrinkage',
            'account_asset',
            'account_expense',
            'account_depreciation',
            'account_accumulated'
        ])->whereUuid($uuid)->first();
        return view('dashboard.accounting.asset.edit', ['payload' => $asset]);
    }

    public function itemsDepreciation($uuid)
    {
        $asset = FixedAsset::with('shrinkage')->whereUuid($uuid)->first();
        return response()->json($asset->shrinkage);
    }
}
