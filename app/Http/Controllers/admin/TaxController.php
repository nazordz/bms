<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Tax;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;

class TaxController extends Controller
{
    public function index()
    {
        return view('dashboard.tax.index');
    }

    public function table()
    {
        $data = Tax::whereNull('deleted_at');
        return datatables()->of($data)->make(true);
    }
    public function store(Request $request)
    {
        $response = ['status' => 'true'];
        $request->validate([
            'tax_amount' => 'required'
        ]);
        if($request->filled('tax_id')){
            Tax::where('tax_id', $request->tax_id)
            ->update([
                'tax_amount'            => $request->tax_amount,
                'description'           => $request->description,
                'payable_account_id'    => $request->account_payable,
                'receivable_account_id' => $request->account_receivable,
                'type'                  => $request->type
            ]);
            $response['statusText'] = Lang::get('dashboard.tax_updated');
            activity()->causedBy(Auth::id())->log('Tax Updated!');
        }else{

            Tax::create([
                'tax_id'                => Str::uuid(),
                'tax_amount'            => $request->tax_amount,
                'description'           => $request->description,
                'payable_account_id'    => $request->account_payable,
                'receivable_account_id' => $request->account_receivable,
                'type'                  => $request->type
            ]);
            $response['statusText'] = Lang::get('dashboard.tax_created');
            activity()->causedBy(Auth::id())->log('Tax Created!');
        }
        return response()->json($response);
    }

    public function show($tax_id)
    {
        $data = Tax::with(['payable_account', 'receivable_account'])->find($tax_id);
        return response()->json($data);
    }

    public function delete(Request $request)
    {
        Tax::where('tax_id', $request->tax_id)->delete();
        activity()->causedBy(Auth::id())->log('Tax deleted!');
        return response()->json(['status' => true, 'statusText' => 'Tax Deleted!']);
    }
}
