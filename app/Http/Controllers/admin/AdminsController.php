<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;
use Spatie\Activitylog\Contracts\Activity;

class AdminsController extends Controller
{
    public function __construct() {
        $this->middleware('role:superadmin');
    }
    public function index()
    {
        return view('dashboard.admin.adminList');
    }

    public function table(Request $request)
    {
        $data = User::select('name', 'phone', 'uuid', 'email', 'menuroles', 'created_at', 'active')
                    ->where('menuroles', 'NOT LIKE', '%superadmin%')
                    ->where('company_id', $request->session()->get('company_id'));
        return datatables()->of($data)->make(true);
    }

    public function status(Request $request)
    {
        User::where('uuid', $request->uuid)
        ->update(['active' => $request->status]);

        return response()->json(['status' => true]);
    }

    public function addForm()
    {
        return view('dashboard.admin.adminAddForm');
    }

    public function show()
    {
        $user = User::where('uuid', Auth::user()->uuid)->first();
        return view('dashboard.admin.adminShow', ['user' => $user]);
    }

    public function update($uuid, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required|max:191',
            'email' => 'required|email|max:191',
            'phone' => 'numeric|digits_between:0,20',
            'password' => 'confirmed|max:191',
            'role'  => 'required',
        ]);
        if($validator->fails()){
            return redirect('/admin/show/'.$uuid)->withErrors($validator)->withInput();
        }
        $changed = [
            'name'      => $request->name,
            'email'     => $request->email,
            'phone'     => $request->phone,
            'menuroles' => 'user,'.implode(',', $request->role)
        ];
        if($request->insert_password == 1){
            $changed['password'] = bcrypt($request->password);
        }
        User::where('uuid', $uuid)->update($changed);
        activity()->causedBy(Auth::user())->log('admin updated!');
        return redirect('/admin/show/'.$uuid)->with('status-success', Lang::get(('dashboard.admin_changed')));

    }

    public function delete(Request $request)
    {
        User::where('uuid', $request->uuid)->forceDelete();
        return response()->json(['status' => true]);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required|unique:users,name|max:191',
            'email' => 'required|email|unique:users,email|max:191',
            'phone' => 'numeric|digits_between:0,20',
            'password' => 'required|confirmed|max:191',
            'role'  => 'required',
        ]);
        if($validator->fails()){
            return redirect('/admin/add-form')->withErrors($validator)->withInput();
        }
        User::create([
            'uuid'      => Str::uuid(),
            'name'      => $request->name,
            'email'     => $request->email,
            'phone'     => $request->phone,
            'password'  => bcrypt($request->password),
            'menuroles' => 'user,'.implode(',', $request->role),
            'company_id'=> $request->session()->get('company_id')
        ]);
        activity()->causedBy(Auth::user())->log('admin created!');
        return redirect('/admin')->with('status-success', Lang::get(('dashboard.admin_created')));
    }
}
