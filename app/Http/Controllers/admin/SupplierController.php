<?php

namespace App\Http\Controllers\admin;

// use App\Exports\QuotationSupplierExport;
use App\Http\Controllers\Controller;
use App\Models\CreditNoteItemSupplier;
use App\Models\CreditNoteSupplier;
use App\Models\CreditNoteTaxSupplier;
use App\Models\DebitNoteAllocatedCreditSupplier;
use App\Models\DebitNoteItemSupplier;
use App\Models\Supplier;
use App\Models\DebitNoteSupplier;
use App\Models\DebitNoteTaxSupplier;
use App\Models\InvoiceSupplier;
use App\Models\InvoiceSupplierItem;
use App\Models\InvoiceTaxSupplier;
use App\Models\Journal;
use App\Models\JournalDetail;
use App\Models\PaymentSupplier;
use App\Models\PoSupplierItem;
use App\Models\PoTaxSupplier;
use App\Models\PurchaseOrderSupplier;
use App\Models\QuotationSupplier;
use App\Models\QuotationSupplierItem;
use App\Models\QuotationTaxSupplier;
use App\Models\SettingAccount;
use App\Models\WebSetting;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Barryvdh\Debugbar\Facade as Debugbar;
use Illuminate\Database\Eloquent\Builder;

class SupplierController extends Controller
{
    public function listSupplier()
    {
        return view('dashboard.supplier.list-supplier');
    }
    public function tableSupplier()
    {
        $data = Supplier::whereNull('deleted_at');
        return datatables()->of($data)->make(true);
    }

    public function createSupplier(){
        return view('dashboard.supplier.create-supplier');
    }

    public function editSupplier($supplier_id){
        $data = Supplier::with(['province', 'regency', 'district', 'village'])->find($supplier_id);
        return view('dashboard.supplier.edit-supplier', ['data' => $data]);
    }

    public function invoiceToJournalPurchase($id, $serial, $request, $transaction_at)
    {
        $subTotal         = $request->subTotal;
        $subTotalDiscount = $request->subTotalDiscount;
        $taxes            = $request->tax;
        $rate             = $request->rate;
        $rate_tax         = $request->rate_tax;
        $total            = $request->grandTotal;
        // if invoice set to publish
        // $statement = DB::select("SHOW TABLE STATUS LIKE 'journals'");
        // $nextId = $statement[0]->Auto_increment;
        $nextId = Journal::withTrashed()->orderByDesc('serial')->first()->serial + 1;

        $journal = new Journal;
        $journal->code             = $nextId.'/'.date_format(date_create(now()), "m/Y");
        $journal->serial           = $nextId;
        $journal->uuid             = Str::uuid();
        $journal->description      = 'Pembelian Dari INV'.$serial;
        $journal->status           = 2;  // Status utk dimasukan ke dalam jurnal
        $journal->transaction_at   = $transaction_at;
        $journal->supplier_invoice_id = $id;
        $journal->save();

        $account = SettingAccount::first();
        $accountPurchase   = $account->purchase_account_id;
        $accountDebt       = $account->debt_account_id;
        $accountProfitRate = $account->purchase_profit_rate_account_id;
        $accountLossRate   = $account->purchase_loss_rate_account_id;

        $sum_debit = 0;
        $sum_credit = 0;

        $journalDetails[] = [
            'journal_id'  => $journal->id,
            'account_id'  => $request->purchase_account,
            'description' => "Pembelian dari INV{$serial}",
            'debet'       => $subTotal,
            'credit'      => 0
        ];
        $sum_debit += $subTotal;
        // tax
        if($taxes[0]['id']){
            foreach ($taxes as $key => $value) {

                $tax_out = $subTotal * ($value['amount']/100);

                if($rate_tax > 0 && $request->currency > 1){
                    $tax_out = ($subTotal/$rate) * ($value['amount']/100) * $rate_tax;
                }

                $journalDetails[] = [
                    'journal_id'  => $journal->id,
                    'account_id'  => $value['payable_account_id'],
                    'description' => 'Pajak',
                    'debet'       => $tax_out,
                    'credit'      => 0
                ];
                $sum_debit += $tax_out;
            }

        }

        $sum_credit += $total;

        // selisih kurs
        if($request->currency > 1 && $rate_tax > 0){
            $konversi   = ($subTotal/$rate);
            $totalPajak = collect($request->tax)->sum('amount');
            $persentase = $konversi*($totalPajak/100);
            $selisih    = abs(($persentase*$rate) - ($persentase*$rate_tax));

            // if($rate < $rate_tax && $selisih){
            if($sum_debit < $sum_credit && $selisih){
                //rugi
                $journalDetails[] = [
                    'journal_id'  => $journal->id,
                    'account_id'  => $accountLossRate,
                    'description' => 'Rugi selish kurs',
                    'debet'       => $selisih,
                    'credit'      => 0
                ];
            }
            // elseif($rate > $rate_tax && $selisih){
            elseif($sum_credit < $sum_debit && $selisih){
                //laba
                $journalDetails[] = [
                    'journal_id'  => $journal->id,
                    'account_id'  => $accountProfitRate,
                    'description' => 'Laba selish kurs',
                    'debet'       => 0,
                    'credit'      => $selisih
                ];
            }
        }

        $journalDetails[] = [
            'journal_id' => $journal->id,
            'account_id' => $accountDebt,
            'description'=> "Utang dari INV{$serial}",
            'debet'      => 0,
            'credit'     => $total
        ];
        activity()->causedBy(Auth::id())->log('Journal invoice purchasing created!');

        JournalDetail::insert($journalDetails);
    }

    public function invoiceToJournalPayment($id, $account_id, $grandTotal)
    {
        // if invoice set to paid
        $statement = DB::select("SHOW TABLE STATUS LIKE 'journals'");
        $nextId = $statement[0]->Auto_increment;

        $journal = new Journal;
        $journal->code             = $nextId.'/'.date_format(date_create(now()), "m/Y");
        $journal->uuid             = Str::uuid();
        $journal->description      = 'Pembayaran Dari Pembelian INV'.$id;
        $journal->status           = 2;  // Status utk dimasukan ke dalam jurnal
        $journal->transaction_at   = now();
        $journal->customer_invoice_id = $id;
        $journal->save();

        $debtAccount  = SettingAccount::first()->debt_account_id;
        $journalDetails = [
            [
                'journal_id'  => $journal->id,
                'account_id'  => $account_id,
                'description' => "Pembelian dari INV{$id}",
                'debet'       => $grandTotal,
                'credit'      => 0
            ],
            [
                'journal_id' => $journal->id,
                'account_id' => $debtAccount,
                'description'=> "Utang Usaha dari INV{$id}",
                'debet'      => 0,
                'credit'     => $grandTotal
            ]
        ];
        JournalDetail::insert($journalDetails);
    }

    public function creditNoteToJournal($id, $total)
    {
        $statement = DB::select("SHOW TABLE STATUS LIKE 'journals'");
        $nextId = $statement[0]->Auto_increment;

        $journal = new Journal;
        $journal->code             = $nextId.'/'.date_format(date_create(now()), "m/Y");
        $journal->uuid             = Str::uuid();
        $journal->description      = 'Nota kredit Pembelian Dari CN'.$id;
        $journal->status           = 2;  // Status utk dimasukan ke dalam jurnal
        $journal->transaction_at   = now();
        $journal->supplier_credit_note_id = $id;
        $journal->save();

        $account = SettingAccount::first();
        $accountPurchaseSupplier  = $account->supplier_purchase_return_account_id;
        $accountPurchases = $account->purchase_account_id;

        $journalDetails = [
            [
                'journal_id'  => $journal->id,
                'account_id'  => $accountPurchaseSupplier,
                'description' => "Retur Pembelian",
                'debet'       => preg_replace('/\.||\,/','',$total),
                'credit'      => 0
            ],
            [
                'journal_id' => $journal->id,
                'account_id' => $accountPurchases,
                'description'=> "Pembelian",
                'debet'      => 0,
                'credit'     => preg_replace('/\.||\,/','',$total)
            ]
        ];
        JournalDetail::insert($journalDetails);
    }

    public function debitNoteToJournal($id, $serial, $request)
    {
        $total    = $request->grandTotal;
        $subTotal = $request->subTotal;
        $taxes    = $request->tax;

        $invoice = InvoiceSupplier::find($request->invoice_id);
        $nextId = Journal::withTrashed()->orderByDesc('serial')->first()->serial + 1;
        $journal = new Journal;
        $journal->serial           = $nextId;
        $journal->code             = $nextId.'/'.date_format(date_create(now()), "m/Y");
        $journal->uuid             = Str::uuid();
        $journal->description      = 'Nota Debit Pembelian Dari DN'.$serial;
        $journal->status           = 2;  // Status utk dimasukan ke dalam jurnal
        $journal->transaction_at   = now();
        $journal->supplier_debit_note_id = $id;
        $journal->save();

        $account               = SettingAccount::first();
        $accountDebt           = $account->debt_account_id;
        $accountProfitRate = $account->sales_profit_rate_account_id;
        $accountLossRate   = $account->sales_loss_rate_account_id;
        if($request->currency > 1){
            $subTotal *= $request->rate;
        }

        $sum_debit = 0;
        $sum_credit = 0;
        // jika alokasi credit
        if($invoice->payment == 1){
            // utang
            $journalDetails[] = [
                'journal_id'  => $journal->id,
                'account_id'  => $accountDebt,
                'description' => "utang",
                'debet'       => $total,
                'credit'      => 0
            ];
            $sum_debit += $total;
            // ada ppn
            if($taxes[0]['id']){
                foreach ($taxes as $key => $value) {
                    $tax_amount = $value['amount'];
                    $tax_out    = $subTotal * ($tax_amount/100);
                    if($request->currency > 1){
                        $tax_out = (($request->subTotal* $request->rate_tax) * ($tax_amount/100));
                    }
                    $journalDetails[] = [
                        'journal_id'  => $journal->id,
                        'account_id'  => $value['payable_account_id'],
                        'description' => "PPN",
                        'debet'       => 0,
                        'credit'      => $tax_out
                    ];
                    $sum_credit += $tax_out;

                }
            }

            $journalDetails[] = [
                'journal_id'  => $journal->id,
                'account_id'  => $request->account_id,
                'description' => "Retur",
                'debet'       => 0,
                'credit'      => $subTotal
            ];
            $sum_credit += $subTotal;
        }elseif($invoice->payment == 2){
            $journalDetails[] = [
                'journal_id'  => $journal->id,
                'account_id'  => $invoice->purchase_account,
                'description' => "Kas/Bank",
                'debet'       => $total,
                'credit'      => 0
            ];
            $sum_debit += $total;
            // ada ppn
            if($taxes[0]['id']){
                foreach ($taxes as $key => $value) {
                    $tax_amount = $value['amount'];
                    $tax_out    = $subTotal * ($tax_amount/100);

                    if($request->currency > 1){
                        $tax_out = (($request->subTotal* $request->rate_tax) * ($tax_amount/100));
                    }

                    $journalDetails[] = [
                        'journal_id'  => $journal->id,
                        'account_id'  => $value['payable_account_id'],
                        'description' => "PPN",
                        'debet'       => 0,
                        'credit'      => $tax_out
                    ];
                    $sum_credit += $tax_out;

                }
            }
            $journalDetails[] = [
                'journal_id'  => $journal->id,
                'account_id'  => $accountDebt,
                'description' => "utang",
                'debet'       => 0,
                'credit'      => $subTotal
            ];
            $sum_credit += $subTotal;
        }
        // laba rugi atas selisih kurs
        if($request->currency > 1){
            $selisih = abs($sum_debit - $sum_credit);
            if($sum_debit < $sum_credit){
                //masuk ke laba
                $journalProfitLoss[] = [
                    'journal_id'  => $journal->id,
                    'account_id'  => $accountLossRate,
                    'description' => "Rugi selisih kurs",
                    'debet'       => $selisih,
                    'credit'      => 0
                ];
            }elseif($sum_credit < $sum_debit){
                // masuk ke piutang
                $journalProfitLoss[] = [
                    'journal_id'  => $journal->id,
                    'account_id'  => $accountProfitRate,
                    'description' => "Laba selisih kurs",
                    'debet'       => 0,
                    'credit'      => $selisih
                ];
            }
            array_splice($journalDetails, 1, 0, $journalProfitLoss);
        }

        JournalDetail::insert($journalDetails);
    }


    public function storeSupplier(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name'  => 'required',
            'last_name'   => 'nullable',
            'company'     => 'required',
            'province_id' => 'nullable|exists:provinces,id',
            'regency_id'  => 'nullable|exists:regencies,id',
            'district_id' => 'nullable|exists:districts,id',
            'village_id'  => 'nullable|exists:villages,id',
            'address'     => 'nullable',
            'email'       => 'nullable|email',
            'phone'       => 'nullable|numeric',
        ]);
        if($validator->fails()){
            return redirect('/supplier/create-supplier')->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_created'));
        }
        Supplier::create([
            'supplier_id' => Str::uuid(),
            'first_name'  => $request->first_name,
            'last_name'   => $request->last_name,
            'company'     => $request->company,
            'province_id' => $request->province_id,
            'regency_id'  => $request->regency_id,
            'district_id' => $request->district_id,
            'village_id'  => $request->village_id,
            'address'     => $request->address,
            'email'       => $request->email,
            'phone'       => $request->phone,
        ]);
        activity()->causedBy(Auth::id())->log('Supplier Created!');
        return redirect('/supplier/list-supplier')->with('status-success', Lang::get('dashboard.supplier_created'));
    }

    public function updateSupplier(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'supplier_id' => 'required|exists:suppliers,supplier_id',
            'first_name'  => 'required',
            'last_name'   => 'nullable',
            'company'     => 'required',
            'province_id' => 'nullable|exists:provinces,id',
            'regency_id'  => 'nullable|exists:regencies,id',
            'district_id' => 'nullable|exists:districts,id',
            'village_id'  => 'nullable|exists:villages,id',
            'address'     => 'required',
            'email'       => 'nullable|email',
            'phone'       => 'nullable|numeric',
        ]);
        if($validator->fails()){
            return redirect('/supplier/edit-supplier/'.$request->supplier_id)->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_saved'));
        }
        Supplier::where('supplier_id', $request->supplier_id)
                ->update([
                    'first_name'  => $request->first_name,
                    'last_name'   => $request->last_name,
                    'company'     => $request->company,
                    'province_id' => $request->province_id,
                    'regency_id'  => $request->regency_id,
                    'district_id' => $request->district_id,
                    'village_id'  => $request->village_id,
                    'address'     => $request->address,
                    'email'       => $request->email,
                    'phone'       => $request->phone,
                ]);
        activity()->causedBy(Auth::id())->log('Supplier updated!');
        return redirect('/supplier/list-supplier')->with('status-success', Lang::get('dashboard.supplier_updated'));
    }

    public function showSupplier($supplier_id)
    {
        $data = Supplier::with(['province', 'regency', 'district', 'village'])->find($supplier_id);
        return response()->json($data);
    }

    public function deleteSupplier(Request $request)
    {
        Supplier::where('supplier_id', $request->supplier_id)->delete();
        activity()->causedBy(Auth::id())->log('Supplier deleted!');
        return response()->json(['status' => true, 'statustext' => 'Supplier Deleted!']);
    }

    public function listQuotation()
    {
        return view('dashboard.supplier.list-quotation');
    }

    public function tableQuotation(Request $request)
    {
        $data = QuotationSupplier::select('suppliers.first_name', 'suppliers.last_name', 'suppliers.company', 'quotation_suppliers.*') // 'quotation_suppliers.quotation_id', 'term_and_condition', 'valid_until', 'sales'
                ->join('suppliers', 'quotation_suppliers.supplier_id', '=', 'suppliers.supplier_id');
        if($request->filled(['start_date', 'to_date'])){
            $start_date = date_format(date_create($request->start_date), "Y-m-d");
            $to_date = date_format(date_create($request->to_date), "Y-m-d");
            $data = $data->whereBetween('quotation_suppliers.created_at', [$start_date." 00:00:00", $to_date." 23:59:59"]);
        }
        return datatables()->of($data)->make(true);
    }

    public function createQuotation(Request $request)
    {
        $data = null;
        $supplier = null;
        if($request->has('supplier_id') || $request->has('copy_from')){
            $data = collect($request->all());
            $supplier = collect([
                'supplier_id' => $request->supplier_id,
                'supplier_name' => $request->supplier_name
            ]);
        }
        return view('dashboard.supplier.create-quotation', ['data' => $data, 'supplier' => $supplier]);
    }

    public function storeQuotation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'supplier_id'        => 'required|exists:suppliers,supplier_id',
            'sales'              => 'nullable',
            'term_and_condition' => 'nullable',
            'valid_until'        => 'required|date|date_format:d-m-Y',
            'file'               => 'nullable|file|max:6000'
        ]);
        if($validator->fails()){
            return redirect('/supplier/create-quotation')->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_saved'));
        }
        $serial = QuotationSupplier::withTrashed()->count()+1;
        $qtt = new QuotationSupplier;
        $qtt->supplier_id        = $request->supplier_id;
        $qtt->serial             = $serial;
        $qtt->uuid               = Str::uuid();
        $qtt->term_and_condition = $request->term_and_condition;
        $qtt->term_of_payment    = $request->term_of_payment;
        $qtt->description        = $request->description;
        $qtt->status             = $request->status;
        $qtt->currency           = $request->currency;
        $qtt->rate               = $request->rate;
        $qtt->rate_tax           = $request->rate_tax;
        $qtt->from               = $request->from;
        $qtt->to                 = $request->to;
        $qtt->attn               = $request->attn;
        $qtt->quotation_date     = date_format(date_create($request->quotation_date), "Y-m-d");
        $qtt->valid_until        = date_format(date_create($request->valid_until), "Y-m-d");

        if($request->hasFile('file')){
            $path = $request->file->storeAs("public/quotation-supplier", Str::random(5).'_'.$request->file->getClientOriginalName());
            $locationFile = Storage::url($path);
            $qtt->file = $locationFile;
        }
        $qtt->save();
        $id = $qtt->quotation_id;
        $row = collect($request->row)->map(function($item) use($request, $id){
            $item['quotation_id'] = $id;
            $item['price']        = $item['price'];
            $item['subtotal']     = $item['subtotal'];
            return $item;

        });
        QuotationSupplierItem::insert($row->toArray());
        // kalo diisi lebih dari satu pajaknya itu masih error jika yg keduanya kosong
        if($request->tax[0]['id']){
            $taxes = collect($request->tax)->map(function($taxes)use($id){
                return [
                    'tax_id'       => $taxes['id'],
                    'quotation_id' => $id
                ];
            });
            QuotationTaxSupplier::insert($taxes->toArray());
        }
        activity()->causedBy(Auth::id())->log('Supplier Quotation created!');
        return redirect('supplier/list-quotation')->with('status-success', Lang::get('dashboard.success_saved'));
    }



    public function showQuotation($uuid)
    {
        $data = QuotationSupplier::with(['supplier', 'list_items', 'taxes'])->whereUuid($uuid)->first();
        Debugbar::info($data);
        return view('dashboard.supplier.edit-quotation', ['data' => $data]);
    }

    public function updateQuotation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'quotation_id'       => 'required|exists:quotation_suppliers,quotation_id',
            'supplier_id'        => 'required|exists:suppliers,supplier_id',
            'sales'              => 'nullable',
            'term_and_condition' => 'nullable',
            'file'               => 'nullable|file|max:6000',
            'valid_until'        => 'required|date|date_format:d-m-Y'
        ]);

        if($validator->fails()){
            return redirect('/supplier/create-quotation')->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_saved'));
        }

        $quotation = QuotationSupplier::find($request->quotation_id);
        $quotation->supplier_id        = $request->supplier_id;
        $quotation->term_and_condition = $request->term_and_condition;
        $quotation->term_of_payment    = $request->term_of_payment;
        $quotation->description        = $request->description;
        $quotation->status             = $request->status;
        $quotation->currency           = $request->currency;
        $quotation->rate               = $request->rate;
        $quotation->rate_tax           = $request->rate_tax;
        $quotation->from               = $request->from;
        $quotation->to                 = $request->to;
        $quotation->attn               = $request->attn;
        $quotation->quotation_date     = date_format(date_create($request->quotation_date), "Y-m-d");
        $quotation->valid_until        = date_format(date_create($request->valid_until), "Y-m-d");

        // $updateQuo = [
        //     'supplier_id'        => $request->supplier_id,
        //     'sales'              => $request->sales,
        //     'term_and_condition' => $request->term_and_condition,
        //     'discount'           => $request->discount,
        //     'status'             => $request->status,
        //     'valid_until'        => date_format(date_create($request->valid_until), "Y-m-d")
        // ];

        if($request->hasFile('file')){
            $path = $request->file->storeAs("public/quotation-supplier", Str::random(5).'_'.$request->file->getClientOriginalName());
            $locationFile = Storage::url($path);
            $quotation->file = $locationFile;
        }
        // QuotationSupplier::where('quotation_id', $request->quotation_id)
        //                 ->update($updateQuo);
        $quotation->save();
        QuotationSupplierItem::where('quotation_id', $request->quotation_id)->delete();

        $row = collect($request->row)->map(function($item) use($request){
            $item['quotation_id'] = $request->quotation_id;
            $item['price']        =  $item['price'];
            $item['subtotal']     =  $item['subtotal'];
            return $item;

        });
        QuotationSupplierItem::insert($row->toArray());
        QuotationTaxSupplier::where('quotation_id', $request->quotation_id)->delete();
        if($request->tax[0]['id']){
            $taxes = collect($request->tax)->map(function($taxes)use($request){
                return [
                    'tax_id' => $taxes['id'],
                    'quotation_id' => $request->quotation_id
                ];
            });
            QuotationTaxSupplier::insert($taxes->toArray());
        }

        activity()->causedBy(Auth::id())->log('Supplier Quotation updated!');
        return redirect('supplier/list-quotation')->with('status-success', Lang::get('dashboard.success_saved'));
    }

    public function deleteQuotation(Request $request)
    {
        if($request->type == 'void'){
            QuotationSupplier::where('quotation_id', $request->quotation_id)
            ->update(['status' => 0]);
            activity()->causedBy(Auth::id())->log('Supplier Quotation void!');
        }elseif($request->type == 'cancel'){
            QuotationSupplier::destroy($request->quotation_id);
            activity()->causedBy(Auth::id())->log('Supplier Quotation Canceled!');
        }
        return response()->json(['status' => true, 'statustext' => 'Supplier Quotation Void!']);
    }

    public function printQuotation($uuid)
    {
        $web  = WebSetting::with('company')->find(session()->get('company_id'));
        $quotation = QuotationSupplier::whereUuid($uuid)->with(['list_items', 'taxes', 'supplier', 'supplier.province', 'supplier.regency', 'supplier.district', 'supplier.village' ])->first();
        // $pdf = PDF::loadView('dashboard.supplier.print-quotation', ['data' => $data, 'web' => $web, 'items' => $item])->setPaper('a4', 'potrait');
        // return $pdf->stream('invoice-supplier.pdf');

        return view('dashboard.supplier.cetak-penawaran', ['data' => $quotation, 'web' => $web]);

    }


    public function listPo()
    {
        return view('dashboard.supplier.list-po');
    }

    public function tablePo()
    {
        $data = PurchaseOrderSupplier::join('po_supplier_items', 'purchase_order_suppliers.po_id', '=', 'po_supplier_items.po_id')
                    ->join('suppliers', 'purchase_order_suppliers.supplier_id', '=', 'suppliers.supplier_id');
        return datatables()->of($data)->make(true);
    }

    public function createPo(Request $request)
    {
        $data = null;
        $supplier = null;
        if($request->has('quotation_id') || $request->has('supplier_id')){
            $data = collect($request->all());
            // dd($data['attn']);
            $supplier = collect([
                'supplier_id' => $request->supplier_id,
                'supplier_name' => $request->supplier_name
            ]);
        }
        return view('dashboard.supplier.create-po', ['data' => $data, 'supplier' => $supplier]);
    }

    public function deletePo(Request $request)
    {
        if($request->type == 'void'){
            $po = PurchaseOrderSupplier::find($request->po_id);
            $po->status = 0;
            $po->save();
            $res = 'voided';
            activity()->causedBy(Auth::id())->log('Supplier PO Voided!');
        }elseif($request->type == 'cancel'){
            PurchaseOrderSupplier::destroy($request->po_id);
            $res = 'canceled';
            activity()->causedBy(Auth::id())->log('Supplier PO canceled!');
        }
        if($request->ajax()){
            return response()->json(['status' => true, 'statusText' => 'Po '.$request->type]);
        }
        return redirect('/supplier/purchase-order')->with('status-success', 'Data '.$res);
    }

    public function storePo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'supplier_id' => 'required|exists:suppliers,supplier_id',
        ]);
        if($validator->fails()){
            return redirect('/supplier/create-quotation')->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_saved'));
        }
        $serial = PurchaseOrderSupplier::withTrashed()->count() + 1;
        $po = new PurchaseOrderSupplier;
        $po->uuid               = Str::uuid();
        $po->supplier_id        = $request->supplier_id;
        $po->serial             = $serial;
        $po->status             = $request->status;
        $po->term_and_condition = $request->term_and_condition;
        $po->description        = $request->description;
        $po->currency           = $request->currency;
        $po->rate               = $request->rate;
        $po->rate_tax           = $request->rate_tax;
        $po->po_date            = date_format(date_create($request->po_date), 'Y-m-d');
        $po->shipping_date      = date_format(date_create($request->shipping_date), 'Y-m-d');

        if($request->has('quotation_id')){
            $po->quotation_id = $request->quotation_id;
        }
        $po->save();
        $po_id = $po->po_id;
        $row = collect($request->row)->map(function($item) use($request, $po_id){
            $item['po_id']    = $po_id;
            $item['price']    =  $item['price'];
            $item['subtotal'] =  $item['subtotal'];
            return $item;

        });
        PoSupplierItem::insert($row->toArray());
        PoTaxSupplier::where('po_id', $request->po_id)->delete();
        if($request->tax[0]['id']){
            $taxes = collect($request->tax)->map(function($taxes)use($po_id){
                return [
                    'tax_id' => $taxes['id'],
                    'po_id'  => $po_id
                ];
            });
            PoTaxSupplier::insert($taxes->toArray());
        }


        activity()->causedBy(Auth::id())->log('Supplier Po created!');
        return redirect('supplier/purchase-order')->with('status-success', Lang::get('dashboard.success_saved'));

    }

    public function showPo($uuid)
    {
        $data = PurchaseOrderSupplier::with(['quotation', 'invoice', 'list_items', 'taxes', 'supplier'])->where('uuid', $uuid)->first();
        $data->isPo = true;
        $isFormDisabled = $data->status == 2 ? 'disabled':'';
        // dd($data->status);
        return view('dashboard.supplier.edit-po', ['data' => $data, 'isFormDisabled' => $isFormDisabled]);
    }

    public function updatePo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'supplier_id' => 'required|exists:suppliers,supplier_id',
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_saved'));
        }
        $po = PurchaseOrderSupplier::find($request->po_id);
        $po->supplier_id  = $request->supplier_id;
        $po->quotation_id = $request->quotation_id;
        if($request->has('status')){
            $po->status = $request->status;
        }
        $po->term_and_condition = $request->term_and_condition;
        $po->description        = $request->description;
        $po->currency           = $request->currency;
        $po->rate               = $request->rate;
        $po->rate_tax           = $request->rate_tax;
        $po->po_date            = date_format(date_create($request->po_date), 'Y-m-d');
        $po->shipping_date      = date_format(date_create($request->shipping_date), 'Y-m-d');
        // $updatePo = [
        //         'supplier_id'        => $request->supplier_id,
        //         'status'             => $request->status,
        //         'term_and_condition' => $request->term_and_condition
        // ];
        // PurchaseOrderSupplier::where('po_id', $request->po_id)
        // ->update($updatePo);
        $po->save();
        $row = collect($request->row)->map(function($item) use($request){
            $item['po_id']    = $request->po_id;
            $item['price']    =  $item['price'];
            $item['subtotal'] =  $item['subtotal'];
            return $item;
        });

        PoSupplierItem::where('po_id', $request->po_id)->delete();
        PoSupplierItem::insert($row->toArray());

        PoTaxSupplier::where('po_id', $request->po_id)->delete();
        if($request->tax[0]['id']){
            $taxes = collect($request->tax)->map(function($taxes)use($request){
                return [
                    'tax_id' => $taxes['id'],
                    'po_id'  => $request->po_id
                ];
            });
            PoTaxSupplier::insert($taxes->toArray());
        }

        activity()->causedBy(Auth::id())->log('Supplier Po updated!');
        return redirect('supplier/purchase-order')->with('status-success', Lang::get('dashboard.success_saved'));
    }

    public function printPo($uuid)
    {
        $web  = WebSetting::with('company')->find(session()->get('company_id'));
        $po = PurchaseOrderSupplier::whereUuid($uuid)->with(['list_items', 'taxes', 'supplier', 'supplier.province', 'supplier.regency', 'supplier.district', 'supplier.village' ])->first();
        // $pdf = PDF::loadView('dashboard.supplier.print-po', ['data' => $data, 'web' => $web, 'items' => $item, 'taxes' => $taxes])->setPaper('a4', 'potrait');
        // return $pdf->stream('purchase-order-supplier-PO'.$id.'.pdf');
        return view('dashboard.supplier.cetak-po', ['data' => $po, 'web' => $web]);

    }

    public function listInvoice()
    {
        return view('dashboard.supplier.list-invoice');
    }

    public function tableInvoice(Request $request)
    {
        $data = InvoiceSupplier::join('suppliers', 'invoice_suppliers.supplier_id', '=', 'suppliers.supplier_id')
                ->select('invoice_suppliers.*', 'suppliers.first_name', 'suppliers.last_name', 'suppliers.company');
        if($request->filled(['start_date', 'to_date'])){
            $data = $data->whereBetween('invoice_suppliers.created_at', [date_format(date_create($request->start_date), 'Y-m-d')." 00:00:00", date_format(date_create($request->to_date), 'Y-m-d')." 23:59:59"]);
        }
        return datatables()->of($data)->make(true);
    }

    public function createInvoice(Request $request)
    {
        $data = null;
        $supplier = null;
        if($request->has('purchase_order') || $request->has('quotation_id') || $request->has('supplier_id')){
            $data = collect($request->all());
            $supplier = collect([
                'supplier_id' => $request->supplier_id,
                'supplier_name' => $request->supplier_name
            ]);
        }
        return view('dashboard.supplier.create-invoice', ['data' => $data, 'supplier' => $supplier]);
    }
    public function storeInvoice(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'supplier_id' => 'required|exists:suppliers,supplier_id',
            'due_date'    => 'required|date|date_format:d-m-Y',
            'purchase_account' => 'required|exists:chart_accounts,id'
        ]);
        if($validator->fails()){
            return redirect('/supplier/create-invoice')->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_saved'));
        }
        $serial = InvoiceSupplier::withTrashed()->count() + 1;
        $invoice_date = date_format(date_create($request->invoice_date), "Y-m-d");
        $inv = new InvoiceSupplier;
        $inv->serial             = $serial;
        $inv->invoice_id         = $request->invoice_id;
        $inv->supplier_id        = $request->supplier_id;
        $inv->quotation_id       = $request->quotation_id;
        $inv->uuid               = Str::uuid();
        $inv->term_and_condition = $request->term_and_condition;
        $inv->term_of_condition  = $request->term_of_condition;
        $inv->attn               = $request->attn;
        $inv->vessel             = $request->vessel;
        $inv->voyage             = $request->voyage;
        $inv->eta                = $request->eta ? date_format(date_create($request->eta), "Y-m-d"):null;
        $inv->bl_no              = $request->bl_no;
        $inv->from               = $request->from;
        $inv->to                 = $request->to;
        $inv->description        = $request->description;
        $inv->currency           = $request->currency;
        $inv->rate               = $request->rate;
        $inv->rate_tax           = $request->rate_tax;
        $inv->purchase_account   = $request->purchase_account;
        $inv->status             = $request->status;
        $inv->invoice_date       = $invoice_date;
        $inv->due_date           = date_format(date_create($request->due_date), "Y-m-d");

        if($request->has('po_id')){
            $inv->po_id = $request->po_id;
        }

        if($request->hasFile('file')){
            $path = $request->file->storeAs("public/invoice-supplier", Str::random(5).'_'.$request->file->getClientOriginalName());
            $locationFile = Storage::url($path);
            $inv->file = $locationFile;
        }

        $inv->save();
        $id = $inv->invoice_id;

        // if invoice set to publish
        if($request->status == 2){
            // $this->invoiceToJournalPurchase($id, $request->grandTotal);
            $this->invoiceToJournalPurchase(
                $id,
                $inv->serializing,
                $request,
                $invoice_date
            );
        }

        $row = collect($request->row)->map(function($item) use($id){
            $item['invoice_id'] = $id;
            $item['price']      = $item['price'];
            $item['subtotal']   = $item['subtotal'];
            return $item;

        });
        InvoiceSupplierItem::insert($row->toArray());

        if($request->tax[0]['id']){
            $taxes = collect($request->tax)->map(function($taxes)use($id){
                return [
                    'tax_id' => $taxes['id'],
                    'invoice_id' => $id
                ];
            });
            InvoiceTaxSupplier::insert($taxes->toArray());
        }

        activity()->causedBy(Auth::id())->log('Supplier invoice created!');
        return redirect('supplier/invoice')->with('status-success', Lang::get('dashboard.success_saved'));
    }

    public function showInvoice($uuid)
    {
        $data = InvoiceSupplier::with([
            'purchase_order',
            'paymentInvoice',
            'purchaseAccount',
            'debit_note',
            'list_items',
            'taxes',
            'discount',
            'supplier'
        ])->where('uuid', $uuid)->first();
        abort_if(!$data, 404, 'Not found');

        Debugbar::info($data);
        $isFormDisabled = ($data->paidAmount > 0 || $data->status == 0) ? "disabled":"";
        $paymentAmount = $data->paymentInvoice->sum(function($payment){
            return $payment['status'] ? $payment['payment_amount']:0;
        });
        return view('dashboard.supplier.edit-invoice', ['data' => $data, 'isFormDisabled' => $isFormDisabled, 'paymentAmount' => $paymentAmount]);
    }

    public function updateInvoice(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'supplier_id' => 'required|exists:suppliers,supplier_id',
            'due_date'    => 'required|date|date_format:d-m-Y',
            'purchase_account' => 'required|exists:chart_accounts,id'
        ]);
        if($validator->fails()){
            return back()->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_saved'));
        }
        $invoice_date = date_format(date_create($request->invoice_date), "Y-m-d");
        $updateInvoice = [
            'supplier_id'        => $request->supplier_id,
            'quotation_id'       => $request->quotation_id,
            'po_id'              => $request->po_id,
            'due_date'           => date_format(date_create($request->due_date), "Y-m-d"),
            'invoice_date'       => $invoice_date,
            'term_and_condition' => $request->term_and_condition,
            'term_of_payment'    => $request->term_of_payment,
            'attn'               => $request->attn,
            'vessel'             => $request->vessel,
            'voyage'             => $request->voyage,
            'eta'                => date_format(date_create($request->eta), "Y-m-d"),
            'bl_no'              => $request->bl_no,
            'from'               => $request->from,
            'to'                 => $request->to,
            'description'        => $request->description,
            'purchase_account'   => $request->purchase_account,
        ];
        if($request->filled('currency')){
            $updateInvoice['currency'] = $request->currency;
            $updateInvoice['rate']     = $request->rate;
            $updateInvoice['rate_tax'] = $request->rate_tax;
        }
        if($request->hasFile('file')){
            $path = $request->file->storeAs("public/invoice-supplier", Str::random(5).'_'.$request->file->getClientOriginalName());
            $locationFile = Storage::url($path);
            $updateInvoice['file'] = $locationFile;
        }
        // if it saved with (status publish & payment paid)
        if($request->filled('status')){
            $updateInvoice['status'] = $request->status;

            // if invoice set to publish
            if($request->status == 2){
                Journal::where('supplier_invoice_id', $request->invoice_id)->delete();
                $this->invoiceToJournalPurchase($request->invoice_id, $request->serial, $request, $invoice_date);
            }
        }
        // if paid with condition status is publish
        // if($request->payment == 2 && ($request->status == null || $request->status == 2) && $request->filled(['payment_date', 'payment_ref', 'account_id'])){
        //     $updateInvoice['payment_date'] = date_format(date_create($request->payment_date), "Y-m-d");
        //     $updateInvoice['payment_ref']  = $request->payment_ref;
        //     $updateInvoice['account_id']   = $request->account_id;
        //     $this->invoiceToJournalPayment($request->invoice_id, $request->account_id, $request->grandTotal);
        // }
        InvoiceSupplier::where('invoice_id', $request->invoice_id)
        ->update($updateInvoice);

        $row = collect($request->row)->map(function($item) use($request){
            $item['invoice_id']     = $request->invoice_id;
            $item['price']          =  $item['price'];
            $item['subtotal']       =  $item['subtotal'];
            return $item;

        });
        InvoiceSupplierItem::where('invoice_id', $request->invoice_id)->delete();
        InvoiceSupplierItem::insert($row->toArray());

        InvoiceTaxSupplier::where('invoice_id', $request->invoice_id)->delete();
        if($request->tax[0]['id']){
            $taxes = collect($request->tax)->map(function($taxes)use($request){
                return [
                    'tax_id' => $taxes['id'],
                    'invoice_id' => $request->invoice_id
                ];
            });
            InvoiceTaxSupplier::insert($taxes->toArray());
        }

        activity()->causedBy(Auth::id())->log('Supplier invoice updated!');
        return redirect('supplier/invoice')->with('status-success', Lang::get('dashboard.success_saved'));
    }

    public function deleteInvoice(Request $request)
    {
        InvoiceSupplier::where('invoice_id',$request->invoice_id)
                        ->update(['status' => 0, 'payment' => 0, 'void_at' => now(), 'void_by' => Auth::id(), 'void_note' => $request->reason]);
        if($request->type == 'void'){
            Journal::where('supplier_invoice_id', $request->invoice_id)->update(['status' => 3]);
            PaymentSupplier::where('invoice_id', $request->invoice_id)->update(['status' => 0]);
            activity()->causedBy(Auth::id())->log('Supplier invoice Voided!');
        }elseif($request->type == 'cancel'){
            InvoiceSupplier::destroy($request->invoice_id);
            activity()->causedBy(Auth::id())->log('Supplier invoice cancel!');
        }
        if($request->ajax()){
            return response()->json(['status' => true, 'statusText' => 'Invoice Voided/canceled']);
        }
        return redirect('supplier/invoice')->with('status-success', Lang::get('dashboard.success_deleted'));
    }

    public function printInvoice($uuid)
    {
        // $data = DB::table('invoice_suppliers')
        //         ->select(
        //             'invoice_suppliers.*',
        //             'suppliers.first_name',
        //             'suppliers.address',
        //             'suppliers.phone',
        //             'suppliers.email',
        //             'suppliers.last_name',
        //             'provinces.name AS provinces',
        //             'regencies.name AS regencies',
        //             'districts.name AS districts',
        //             'villages.name AS villages',
        //             'tax.description AS taxDesc',
        //             'tax.tax_amount',
        //             'discounts.description AS disDesc',
        //             'discounts.discount_amount'

        //         )
        //         ->join('suppliers', 'invoice_suppliers.supplier_id', '=', 'suppliers.supplier_id')
        //         ->join('tax', 'invoice_suppliers.tax', '=', 'tax.tax_id')
        //         ->leftJoin('discounts', 'invoice_suppliers.discount', '=', 'discounts.discount_id')
        //         ->leftJoin('provinces', 'suppliers.province_id', '=', 'provinces.id')
        //         ->leftJoin('regencies', 'suppliers.regency_id', '=', 'regencies.id')
        //         ->leftJoin('districts', 'suppliers.district_id', '=', 'districts.id')
        //         ->leftJoin('villages', 'suppliers.village_id', '=', 'villages.id')
        //         ->where('invoice_id', $id)
        //         ->first();
        // $item = DB::table('invoice_supplier_items')
        //         ->select('invoice_supplier_items.*', 'list_items.name')
        //         ->join('list_items', 'invoice_supplier_items.item_id', '=', 'list_items.id')
        //         ->where('invoice_id', $id)->get();
        $web  = WebSetting::with('company')->find(session()->get('company_id'));
        $invoice = InvoiceSupplier::whereUuid($uuid)->with(['list_items', 'taxes', 'supplier', 'supplier.province', 'paymentInvoice', 'supplier.regency', 'supplier.district', 'supplier.village' ])->first();
        abort_if(!$invoice, 404, 'Not found');
        return view('dashboard.supplier.cetak-invoice', ['data' => $invoice, 'web' => $web]);
        // // $pdf = PDF::loadView('dashboard.supplier.print-invoice', ['data' => $data, 'web' => $web, 'items' => $item])->setPaper('a4', 'potrait');
        // return $pdf->stream('quotation-supplier.pdf');
    }

    public function listCreditNote()
    {
        return view('dashboard.supplier.list-credit-note');
    }

    public function tableCreditNote(Request $request)
    {
        $data = CreditNoteSupplier::join('suppliers', 'credit_note_suppliers.supplier_id', '=', 'suppliers.supplier_id')
                ->leftJoin('sellers', 'seller_id', '=', 'sellers.id')
                ->select(
                    'suppliers.company',
                    'suppliers.first_name',
                    'suppliers.last_name',
                    'credit_note_suppliers.*',
                    'sellers.name as sales',
                );
        if($request->filled(['start_date', 'to_date'])){
            $data = $data->whereBetween('credit_note_suppliers.created_at', [$request->start_date." 00:00:00", $request->to_date." 23:59:59"]);
        }
        return datatables()->of($data)->make(true);
    }

    public function createCreditNote(Request $request)
    {
        $data = null;
        if($request->has('items')){
            // $data = collect($request->all())->toJson();
            $data = collect($request->all());
        }
        return view('dashboard.supplier.create-credit-note', ['data' => $data]);
    }

    public function storeCreditNote(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'supplier_id' => 'required|exists:suppliers,supplier_id',
            'seller_id'     => 'required|exists:sellers,id',
            'invoice_id'    => 'required|exists:invoice_suppliers,invoice_id',
            'proof_picture' => 'nullable|file|max:6000',
            'sales'         => 'nullable',
            'note'          => 'nullable'
        ]);
        if($validator->fails()){
            return redirect('/supplier/create-credit-note')->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_saved'));
        }
        $serial = CreditNoteSupplier::withTrashed()->count() + 1;
        $creditNote = new CreditNoteSupplier;
        $creditNote->serial        = $serial;
        $creditNote->uuid          = Str::uuid();
        $creditNote->supplier_id   = $request->supplier_id;
        $creditNote->invoice_id    = $request->invoice_id;
        $creditNote->seller_id     = $request->seller_id;
        $creditNote->currency      = $request->currency;
        $creditNote->rate          = $request->rate;
        $creditNote->rate_tax      = $request->rate_tax;
        $creditNote->total         = $request->total;
        $creditNote->note          = $request->note;
        $creditNote->proof_picture = $request->proof_picture;
        $creditNote->status        = $request->status;
        $creditNote->paid_from_account = $request->paid_from_account;
        if($request->hasFile('proof_picture')){
            $path = $request->proof_picture->storeAs("public/credit-note-supplier", Str::random(5).'_'.$request->proof_picture->getClientOriginalName());
            $locationFile = Storage::url($path);
            $creditNote->proof_picture = $locationFile;
        }
        $creditNote->save();
        $id = $creditNote->id;
        // Credit note items
        $row = collect($request->row)->map(function($item) use($id){
            $item['credit_note_id'] = $id;
            $item['price']      =  $item['price'];
            $item['subtotal']   =  $item['subtotal'];
            return $item;
        });
        CreditNoteItemSupplier::insert($row->toArray());

        // tax
        if($request->tax[0]['id']){
            $taxes = collect($request->tax)->map(function($taxes)use($id){
                return [
                    'tax_id' => $taxes['id'],
                    'credit_note_id' => $id
                ];
            });
            CreditNoteTaxSupplier::insert($taxes->toArray());
        }

        activity()->causedBy(Auth::id())->log('Supplier Credit Note Created!');
        return redirect('/supplier/credit-note')->with('status-success', Lang::get('dashboard.success_saved'));;
    }

    public function showCreditNote($uuid)
    {
        $data = CreditNoteSupplier::with(['supplier', 'invoice', 'list_items', 'paid_account', 'seller', 'taxing'])->whereUuid($uuid)->first();
        abort_if(!$data, 404, 'Not found');
        return view('dashboard.supplier.show-credit-note', ['data' => $data]);
    }

    public function deleteCreditNote(Request $request)
    {
        $cn = CreditNoteSupplier::find($request->id);
        $cn->status = 0;
        $cn->save();
        Journal::where('supplier_credit_note_id', $request->id)->update(['status' => 3]);
        activity()->causedBy(Auth::id())->log('Supplier Credit Note Voided!');
        return response()->json(['status' => true, 'statusText' => 'Supplier Credit Note voided']);
    }

    public function updateCreditNote(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'proof_picture' => 'nullable|file|max:6000',
        ]);
        if($validator->fails()){
            return redirect('/customer/create-credit-note')->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_saved'));
        }
        $creditNote = CreditNoteSupplier::find($request->id);
        $creditNote->paid_from_account = $request->paid_from_account;
        $creditNote->note              = $request->note;

        if($request->hasFile('proof_picture')){
            $path = $request->proof_picture->storeAs("public/credit-note-customer", Str::random(5).'_'.$request->proof_picture->getClientOriginalName());
            $locationFile = Storage::url($path);
            $update['proof_picture'] = $locationFile;
        }
        if($request->filled('status')){
            $creditNote->status = $request->status;
        }
        // $id = CreditNoteCustomer::update($update);
        $creditNote->save();
        $id = $request->id;

        // Credit note items
        CreditNoteItemSupplier::where('credit_note_id', $request->id)->delete();
        $row = collect($request->row)->map(function($item) use($id){
            $item['credit_note_id'] = $id;
            $item['price']          = $item['price'];
            $item['subtotal']       = $item['subtotal'];
            return $item;
        });
        CreditNoteItemSupplier::insert($row->toArray());

        // tax
        CreditNoteTaxSupplier::where('credit_note_id', $request->id)->delete();
        if($request->tax[0]['id']){
            $taxes = collect($request->tax)->map(function($taxes)use($id){
                return [
                    'tax_id' => $taxes['id'],
                    'credit_note_id' => $id
                ];
            });
            CreditNoteTaxSupplier::insert($taxes->toArray());
        }

        // if($request->status == 2){
            // static::creditNoteToJournal(
            //     $id,
            //     $request->serial,
            //     $request
            // );
        // }

        activity()->causedBy(Auth::id())->log('Supplier Credit Note Updated!');
        return redirect('/supplier/credit-note')->with('status-success', Lang::get('dashboard.success_saved'));
    }

    public function listDebitNote()
    {
        return view('dashboard.supplier.list-debit-note');
    }

    public function tableDebitNote(Request $request)
    {
        $data = DebitNoteSupplier::join('suppliers', 'debit_note_suppliers.supplier_id', '=', 'suppliers.supplier_id')
                ->whereNotNull('id');
        if($request->filled(['start_date', 'to_date'])){
            $data = $data->whereBetween('debit_note_suppliers.created_at', [$request->start_date." 00:00:00", $request->to_date." 23:59:59"]);
        }
        return datatables()->of($data)->make(true);
    }

    public function createDebitNote(Request $request)
    {
        $data = null;
        if($request->has('items')){
            $data = collect($request->all());
        }
        return view('dashboard.supplier.create-debit-note', ['data' => $data]);
    }

    public function storeDebitNote(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'supplier_id'   => 'required|exists:suppliers,supplier_id',
            'proof_picture' => 'nullable|file|max: 6000',
            'note'          => 'nullable'
        ]);
        if($validator->fails()){
            return redirect('/supplier/create-debit-note')->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_saved'));
        }
        $serial = DebitNoteSupplier::withTrashed()->count() + 1;
        $debitnote = new DebitNoteSupplier;
        $debitnote->uuid            = Str::uuid();
        $debitnote->serial          = $serial;
        $debitnote->invoice_id      = $request->invoice_id;
        $debitnote->supplier_id     = $request->supplier_id;
        $debitnote->currency        = $request->currency;
        $debitnote->rate            = $request->rate;
        $debitnote->rate_tax        = $request->rate_tax;
        $debitnote->note            = $request->note;
        $debitnote->account_id      = $request->account_id;
        $debitnote->status          = $request->status;
        $debitnote->debit_note_date = date_format(date_create($request->debit_note_date), "Y-m-d");
        if($request->hasFile('proof_picture')){
            $path = $request->proof_picture->storeAs("public/credit-note-supplier", Str::random(5).'_'.$request->proof_picture->getClientOriginalName());
            $locationFile = Storage::url($path);
            $debitnote->proof_picture = $locationFile;
        }
        $debitnote->save();
        $id = $debitnote->id;

        // item
        $row = collect($request->row)->map(function($item) use($id){
            $item['debit_note_id'] = $id;
            $item['price']          = $item['price'];
            $item['subtotal']       = $item['subtotal'];
            return $item;
        });
        $debitnote->list_items()->createMany($row->toArray());

        // tax
        if($request->tax[0]['id']){
            $taxes = collect($request->tax)->map(function($taxes)use($id){
                return [
                    'tax_id' => $taxes['id'],
                    'debit_note_id' => $id
                ];
            });
            $debitnote->debit_note_tax()->createMany($taxes->toArray());
        }
        if($request->status == 2){
            $this->debitNoteToJournal($id, $serial, $request);
        }
        activity()->causedBy(Auth::id())->log('Supplier Credit Note Created!');
        return redirect('/supplier/debit-note')->with('status-success', Lang::get('dashboard.success_saved'));;
    }

    public function showDebitNote($uuid)
    {
        $data = DebitNoteSupplier::with(['supplier', 'invoice', 'list_items', 'taxing', 'account'])->whereUuid($uuid)->first();
        $isFormDisabled = $data->status == 2 ? 'disabled':'';
        Debugbar::info($data);
        return view('dashboard.supplier.edit-debit-note', ['data' => $data, 'isFormDisabled' => $isFormDisabled]);
    }

    public function updateDebitNote(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'proof_picture' => 'nullable|file|max:6000',
            'note'          => 'nullable'
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_saved'));
        }
        $debitNote = DebitNoteSupplier::find($request->id);
        $debitNote->invoice_id      = $request->invoice_id;
        $debitNote->supplier_id     = $request->supplier_id;
        $debitNote->currency        = $request->currency;
        $debitNote->rate            = $request->rate;
        $debitNote->rate_tax        = $request->rate_tax;
        $debitNote->account_id      = $request->account_id;
        $debitNote->note            = $request->note;
        $debitNote->debit_note_date = date_format(date_create($request->debit_note_date), "Y-m-d");
        if($request->hasFile('proof_picture')){
            $path = $request->proof_picture->storeAs("public/debit-note-supplier", Str::random(5).'_'.$request->proof_picture->getClientOriginalName());
            $locationFile = Storage::url($path);
            $update['proof_picture'] = $locationFile;
        }
        if($request->filled('status')){
            $debitNote->status = $request->status;
        }
        $debitNote->save();
        $id = $request->id;

        // Credit note items
        DebitNoteItemSupplier::where('debit_note_id', $request->id)->delete();
        $row = collect($request->row)->map(function($item) use($id){
            $item['debit_note_id'] = $id;
            $item['price']          = $item['price'];
            $item['subtotal']       = $item['subtotal'];
            return $item;
        });
        DebitNoteItemSupplier::insert($row->toArray());

        // tax
        DebitNoteTaxSupplier::where('debit_note_id', $request->id)->delete();
        if($request->tax[0]['id']){
            $taxes = collect($request->tax)->map(function($taxes)use($id){
                return [
                    'tax_id' => $taxes['id'],
                    'debit_note_id' => $id
                ];
            });
            DebitNoteTaxSupplier::insert($taxes->toArray());
        }

        if($request->status == 2){
            $this->debitNoteToJournal(
                $id,
                $request->serial,
                $request
            );
        }

        activity()->causedBy(Auth::id())->log('Supplier Debit Note Updated!');
        return redirect('/supplier/debit-note')->with('status-success', Lang::get('dashboard.success_saved'));
    }

    public function deleteDebitNote(Request $request)
    {
        $dn = DebitNoteSupplier::find($request->id);
        $dn->status = 0;
        $dn->save();
        Journal::where('supplier_debit_note_id', $request->id)->update(['status' => 3]);
        activity()->causedBy(Auth::id())->log('Supplier Credit Note Void!');
        return response()->json(['status' => true, 'statusText' => 'Supplier Credit Note Void']);
    }

    public function printCreditNote($id)
    {
        $creditNote = CreditNoteSupplier::with('supplier')->find($id);
        $web  = WebSetting::find(session()->get('company_id'));

        $pdf = PDF::loadView('dashboard.supplier.print-credit-note', ['data' => $creditNote, 'web' => $web])->setPaper('a4', 'potrait');
        return $pdf->stream("supplier-credit-note-CN{$id}.pdf");
    }

    public function createAllocation($uuid = null)
    {
        $data = [];
        if($uuid){
            $debit = DebitNoteSupplier::with([
                'invoice.supplier',
                'allocated_credit',
                'debit_note_tax'
            ])->whereUuid($uuid)->first();
            $data = $debit;
            Debugbar::info($debit);
        }
        return view('dashboard.supplier.allocate-credit', ['data' => $data]);
    }

    public function listAllocation($uuid)
    {
        $debit = DebitNoteSupplier::with([
            'allocated_credit.users',
        ])->whereUuid($uuid)->first();
        return ['allocated_credit' => $debit->allocated_credit];
    }

    public function storeAllocation(Request $request)
    {
        $serial = DebitNoteAllocatedCreditSupplier::count()+1;
        $allocation = new DebitNoteAllocatedCreditSupplier;
        $allocation->serial         = 'DNA'.$serial;
        $allocation->debit_note_id  = $request->debit_note_id;
        $allocation->company_id     = session()->get('company_id');
        $allocation->uuid           = Str::uuid();
        $allocation->allocated_date = $request->allocated_date;
        $allocation->payment_amount = $request->payment_amount;
        $allocation->user_id        = Auth::id();
        $allocation->status         = 1;
        $allocation->save();

        if($request->is_paid_off){
            $debit = DebitNoteSupplier::find($request->debit_note_id);
            $debit->payment = 2;
            $debit->save();
        }

        $nextId = Journal::withTrashed()->orderByDesc('serial')->first()->serial + 1;
        $journal = new Journal;
        $journal->code           = $nextId.'/'.date_format(date_create(now()), "m/Y");
        $journal->serial         = $nextId;
        $journal->uuid           = Str::uuid();
        $journal->description    = 'Pengalokasian Nota Debit Dari '.$request->invoice;
        $journal->status         = 2;  // Status utk dimasukan ke dalam jurnal
        $journal->supplier_allocated_credit_id = $allocation->id;
        $journal->transaction_at = $request->allocated_date;
        $journal->save();

        $debtAccount  = SettingAccount::first()->debt_account_id;
        if($request->invoice_payment == 1){
            $journalDetails = [
                [
                    'account_id'  => $debtAccount,
                    'description' => "Hutang usaha dari {$request->invoice}",
                    'debet'       => $request->payment_amount,
                    'credit'      => 0
                ],
                [
                    'account_id'  => $debtAccount,
                    'description' => "Nota debit dari ".$nextId.'/'.date_format(date_create(now()), "m/Y"),
                    'debet'       => 0,
                    'credit'      => $request->payment_amount
                ]
            ];
        }elseif($request->invoice_payment == 2){
            $journalDetails = [
                [
                    'account_id'  => $request->invoice_account,
                    'description' => "Kas/Bank {$request->invoice}",
                    'debet'       => $request->payment_amount,
                    'credit'      => 0
                ],
                [
                    'account_id'  => $debtAccount,
                    'description' => "Nota debit dari ".$nextId.'/'.date_format(date_create(now()), "m/Y"),
                    'debet'       => 0,
                    'credit'      => $request->payment_amount
                ]
            ];

        }
        $journal->details()->createMany($journalDetails);

        return response()->json(['staus' => true]);
    }
}
