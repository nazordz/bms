<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Discount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class DiscountController extends Controller
{
    public function index()
    {
        return view('dashboard.discount.index');
    }

    public function table()
    {
        $data = Discount::whereNull('deleted_at');
        return datatables()->of($data)->make(true);
    }

    public function show($discount_id)
    {
        $data = Discount::find($discount_id);
        return response()->json($data);
    }

    public function store(Request $request)
    {
        $response = ['status' => 'true'];
        $request->validate([
            'discount_amount' => 'required'
        ]);
        if($request->filled('discount_id')){
            Discount::where('discount_id', $request->discount_id)
            ->update(['discount_amount' => $request->discount_amount, 'description' => $request->description]);
            $response['statusText'] = Lang::get('dashboard.discount_updated');
            activity()->causedBy(Auth::id())->log('Discount Updated!');
        }else{
            // $validate = Discount::where('discount_amount', $request->discount_amount)
            // ->whereNull('deleted_at')->first();
            // if($validate){
            //     return response()->json(['status' => false], 422);
            // }

            Discount::create([
                'discount_id'     => Str::uuid(),
                'discount_amount' => $request->discount_amount,
                'description'     => $request->description
            ]);
            $response['statusText'] = Lang::get('dashboard.discount_created');
            activity()->causedBy(Auth::id())->log('Discount Created!');
        }
        return response()->json($response);
    }

    public function delete(Request $request)
    {
        Discount::where('discount_id', $request->discount_id)->delete();
        activity()->causedBy(Auth::id())->log('Discount deleted!');
        return response()->json(['status' => true, 'statusText' => 'Discount Deleted!']);
    }
}
