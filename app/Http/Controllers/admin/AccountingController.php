<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\ChartAccount;
use App\Models\CreditNoteCustomer;
use App\Models\CreditNoteSupplier;
use App\Models\DebitNoteAllocatedCreditSupplier;
use App\Models\DebitNoteCustomer;
use App\Models\DebitNoteSupplier;
use App\Models\FixedAsset;
use App\Models\InvoiceCustomer;
use App\Models\InvoiceCustomerItem;
use App\Models\InvoiceSupplier;
use App\Models\InvoiceSupplierItem;
use App\Models\Journal;
use App\Models\JournalDetail;
use App\Models\PaymentCustomer;
use App\Models\PaymentSupplier;
use App\Models\SettingAccount;
use App\Models\WebSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Barryvdh\Debugbar\Facade as Debugbar;


class AccountingController extends Controller
{
    public function coa()
    {
        return view('dashboard.accounting.coa.list');
    }

    public function getCoa()
    {
        $coa = ChartAccount::orderBy('id', 'ASC')->get();
        return response()->json($coa);
    }

    public function makeAccountList($coa){
        $accounts = [];
        foreach ($coa as $key => $value) {
            if($value->account_id){

                // find nested array

                // $accounts[$value->account_id]['child'] = [
                //     'id'               => $value->id,
                //     'account_id'       => $value->account_id,
                //     'serial_number'    => $value->serial_number,
                //     'name'             => $value->name,
                //     'description'      => $value->description,
                //     'is_group'         => $value->is_group,
                //     'default_position' => $value->default_position,
                //     'child'            => []
                // ];

            }else{
                $accounts[$value->id] = [
                    'id'               => $value->id,
                    'account_id'       => $value->account_id,
                    'serial'           => $value->serial,
                    'name'             => $value->name,
                    'description'      => $value->description,
                    'is_group'         => $value->is_group,
                    'default_position' => $value->default_position,
                    'child'            => []
                ];
            }
        }
        return $accounts;
    }

    public function storeCoa(Request $request)
    {
        $request->validate([
            'name'            => "required",
            'description'     => "nullable",
            'groupAccount'    => "nullable",
            'setAsGroup'      => "nullable",
            'defaultPosition' => "required",
        ]);

        if($request->filled('serial')){
            $coa = ChartAccount::where('serial', $request->serial)->first();
        }else{
            $coa = new ChartAccount;
        }
        $coa->id               = $request->id;
        $coa->account_id       = $request->groupAccount;
        $coa->name             = $request->name;
        $coa->description      = $request->description;
        $coa->is_group         = $request->setAsGroup;
        $coa->default_position = $request->defaultPosition;
        $coa->save();
        return response()->json(['status' => true, 'statusText' => 'success']);
    }

    public function deleteCoa($id)
    {
        ChartAccount::find($id)->delete();
        return response()->json(['status' => true, 'statusText' => 'success']);
    }

    public function climbDownCoa($id, $serial)
    {
        $coa = ChartAccount::find($id);
        $coa->serial_number = $serial + 1;
        $coa->save();
        return response()->json(['status' => true, 'statusText' => 'success']);
    }
    public function climbUpCoa($id, $serial)
    {
        $coa = ChartAccount::find($id);
        $coa->serial_number = $serial - 1;
        $coa->save();
        return response()->json(['status' => true, 'statusText' => 'success']);
    }

    public function journal()
    {
        return view('dashboard.accounting.journal.list');
    }

    public function tableJournal(Request $request)
    {
        $journals = Journal::leftJoin('journal_clasifications', 'journals.clasification_id', '=', 'journal_clasifications.id')
                    ->groupBy('journals.id')
                    ->select(
                        'journals.id',
                        'journals.serial',
                        'journals.code',
                        'journals.uuid',
                        'journals.description',
                        'journals.reference',
                        'journals.status',
                        'journal_clasifications.name',
                        'journal_details.debet',
                        'journal_details.credit',
                        'journals.transaction_at',
                        'journals.created_at',
                        DB::raw('(SELECT SUM(credit) FROM journal_details AS jd WHERE jd.journal_id = journals.id) AS total')
                    )
                    ->join('journal_details', 'journals.id', '=', 'journal_details.journal_id');
        if($request->filled(['start_date', 'to_date'])){
            $start_date = date_format(date_create($request->start_date), "Y-m-d");
            $to_date    = date_format(date_create($request->to_date), "Y-m-d");
            $journals   = $journals->whereBetween('journals.transaction_at', [$start_date." 00:00:00", $to_date." 23:59:59"]);
        }
        if($request->status > 0){
            $journals = $journals->where('status', $request->status);
        }

        return datatables()->of($journals)->make(true);
    }

    public function createJournal()
    {
        return view('dashboard.accounting.journal.create');
    }

    public function lastJournal()
    {
        $journal = Journal::withTrashed()->orderByDesc('serial')->first();
        return response()->json($journal);
    }

    public function storeJournal(Request $request)
    {
        $request->validate([
            'clasification' => 'nullable|exists:journal_clasifications,id',
            'contact'       => 'nullable|exists:contacts,id',
            'journals.*.code_account' => 'required|exists:chart_accounts,id'
        ]);

        // $statement = DB::select("SHOW TABLE STATUS LIKE 'journals'");
        // $nextId = $statement[0]->Auto_increment;
        if($request->filled('serial')) {
            $seri = $request->serial;
        } else {
            $seri = Journal::withTrashed()->count() + 1;
        }
        if ($request->filled('id')) {
            $journal = Journal::find($request->id);
        }else{
            $journal = new Journal;
            $journal->code             = $seri.'/'.date_format(date_create(now()), "m/Y");
            $journal->uuid             = Str::uuid();
        }

        $journal->serial           = $seri;
        $journal->contact_id       = $request->contact;
        $journal->clasification_id = $request->clasification;
        $journal->reference        = $request->ref ?? '';
        $journal->description      = $request->description;
        $journal->status           = $request->status;
        $journal->transaction_at   = $request->date_transaction;
        $journal->save();
        $journalDetails = [];
        JournalDetail::where('journal_id', $journal->id)->delete();
        foreach ($request->journals as $key => $value) {
            $journalDetails[] = [
                'journal_id'       => $journal->id,
                'account_id'       => $value['code_account'],
                'description'      => $value['description'],
                'debet'            => $value['debet'] ? preg_replace('/\.||\,/', '', $value['debet']) : 0,
                'credit'           => $value['credit'] ? preg_replace('/\.||\,/', '', $value['credit']) : 0,
                'memo'             => $value['memo'],
            ];
        };
        JournalDetail::insert($journalDetails);
        activity()->causedBy(Auth::id())->log('Journal Saved!');
        return redirect('accounting/journal')->with('status-success', Lang::get('dashboard.success_saved'));
    }

    public function showJournal($uuid)
    {
        $journal = Journal::where('uuid', $uuid)->with(['clasification', 'contact', 'details.account'])->first();
        return view('dashboard.accounting.journal.show', ['journal' => $journal]);
    }
    public function printJournal($uuid)
    {
        $jurnal = Journal::where('uuid', $uuid)
                    ->with(['clasification',
                    'contact',
                    'details.account'])
                    ->first();
        $web  = WebSetting::with('company')->find(session()->get('company_id'));
        $journal = collect($jurnal->toArray());
        if ($journal['customer_invoice_id']) {
            $journal->put('parent', InvoiceCustomer::with('taxes')->find($journal['customer_invoice_id']));
        }
        elseif ($journal['customer_payment_id']) {
            $journal->put('parent', PaymentCustomer::find($journal['customer_payment_id']));
        }
        elseif ($journal['customer_credit_note_id']) {
            $journal->put('parent', CreditNoteCustomer::find($journal['customer_credit_note_id']));
        }
        elseif ($journal['customer_debit_note_id']) {
            $journal->put('parent', DebitNoteCustomer::find($journal['customer_debit_note_id']));
        }
        elseif ($journal['supplier_invoice_id']) {
            $journal->put('parent', InvoiceSupplier::with('taxes')->find($journal['supplier_invoice_id']));
        }
        elseif ($journal['supplier_payment_id']) {
            $journal->put('parent', PaymentSupplier::find($journal['supplier_payment_id']));
        }
        elseif ($journal['supplier_credit_note_id']) {
            $journal->put('parent', CreditNoteSupplier::find($journal['supplier_credit_note_id']));
        }
        elseif ($journal['supplier_debit_note_id']) {
            $journal->put('parent', DebitNoteSupplier::find($journal['supplier_debit_note_id']));
        }
        elseif ($journal['supplier_allocated_credit_id']) {
            $journal->put('parent', DebitNoteAllocatedCreditSupplier::find($journal['supplier_allocated_credit_id']));
        }
        elseif ($journal['fixed_asset_id']) {
            $journal->put('parent', FixedAsset::find($journal['fixed_asset_id']));
        }
        else {
            $journal->put('parent', null);
        }
            // dd($journal);
        return view('dashboard.accounting.journal.print', ['journal' => $journal, 'web' => $web]);
    }

    public function salesPayment(Request $request)
    {
        return view('dashboard.accounting.sales-payment.list');
    }
    public function purchasePayment(Request $request)
    {
        return view('dashboard.accounting.purchase-payment.list');
    }
    public function purchaseVoidPayment(Request $request)
    {
        PaymentSupplier::where('uuid', $request->uuid)
        ->update([
            'status'      => 0,
            'void_note' => $request->reason,
            'void_by'     => Auth::id(),
            'void_at'     => now(),
        ]);
        $payment_id = PaymentSupplier::where('uuid', $request->uuid)->first();
        Journal::where('supplier_payment_id', $payment_id->id)->update(['status' => 3]);

        $inv = InvoiceSupplier::with(['list_items', 'taxes'])->find($request->invoice);
        $taxing = null;
        $price = collect($inv->list_items)->sum(function($items){
            return $items->price * $items->quantity;
        });
        if($inv->taxes){
            $taxing = collect($inv->taxes)->sum('tax_amount');
            $payTax = $price * ($taxing/100);
            $price += $payTax;
        }

        $paidInvoice = PaymentSupplier::where(['invoice_id' => $request->invoice, 'status' => 1])->sum('payment_amount');
        // $paidInvoice = collect($paidInvoice)->sum(function($inv){
        //     return $inv->payment_amount;
        // });

        if ($paidInvoice < $price) {
            $invoice = InvoiceSupplier::find($request->invoice);
            $invoice->payment = 1;
            $invoice->Save();
        }

        return ['status' => true, 'price' => $price, 'paidInvoice' => $paidInvoice];
    }

    public function purchaseVoidPaymentInfo(Request $request)
    {
        $payment = PaymentSupplier::with('void_by')->whereUuid($request->uuid)->first();
        return $payment;
    }
    public function salesTablePayment()
    {
        $data = DB::table('payment_customers')
                                ->select('payment_customers.*',
                                        DB::raw('payment_customers.payment_date as paymentDate'),
                                        DB::raw('payment_customers.serial as paySerial'),
                                        // 'invoice_customers.*',
                                        DB::raw('invoice_customers.serial AS invSerial'),
                                        'tax.*',
                                        'customers.first_name',
                                        'customers.last_name',
                                        'customers.company')
                                ->join('invoice_customers', 'payment_customers.invoice_id', '=', 'invoice_customers.invoice_id')
                                ->join('customers', 'invoice_customers.customer_id', '=', 'customers.customer_id')
                                ->leftJoin('tax', 'payment_customers.tax_id', '=', 'tax.tax_id')
                                ->where('payment_customers.company_id', session()->get('company_id'));

        return datatables()->of($data)->make(true);
    }
    public function purchaseTablePayment()
    {
        $data = DB::table('payment_suppliers')
                                ->select('payment_suppliers.*',
                                        DB::raw('payment_suppliers.payment_date as paymentDate'),
                                        DB::raw('payment_suppliers.serial as paySerial'),
                                        // 'invoice_customers.*',
                                        DB::raw('invoice_suppliers.serial AS invSerial'),
                                        'tax.*',
                                        'suppliers.first_name',
                                        'suppliers.last_name',
                                        'suppliers.company')
                                ->join('invoice_suppliers', 'payment_suppliers.invoice_id', '=', 'invoice_suppliers.invoice_id')
                                ->join('suppliers', 'invoice_suppliers.supplier_id', '=', 'suppliers.supplier_id')
                                ->leftJoin('tax', 'payment_suppliers.tax_id', '=', 'tax.tax_id')
                                ->where('payment_suppliers.company_id', session()->get('company_id'));

        return datatables()->of($data)->make(true);
    }

    public function salesCreatePayment($uuid = null)
    {
        $data = null;
        if($uuid){
            $data = InvoiceCustomer::join('customers', 'invoice_customers.customer_id', '=', 'customers.customer_id')
            ->select(
                'invoice_id AS id',
                DB::raw('concat(
                    "INV",
                    serial,
                    coalesce(concat (" - ", customers.company, " - "), " - "),
                    customers.first_name
                ) AS text'),
                'invoice_customers.customer_id',
                DB::raw("CONCAT(customers.company,' - ', customers.first_name, ' ', customers.last_name) AS customer"),
                DB::raw("(SELECT SUM(price*quantity) FROM invoice_customer_items WHERE invoice_customer_items.invoice_id = invoice_customers.invoice_id ) AS price"),
                DB::raw("(SELECT tax.`tax_amount` FROM invoice_tax_customers
                INNER JOIN tax ON invoice_tax_customers.tax_id = tax.tax_id
                WHERE invoice_tax_customers.invoice_id = invoice_customers.invoice_id) AS tax"),
                DB::raw("(SELECT IF(currency > 1, sum(payment_amount)/payment_customers.rate, sum(payment_amount)) FROM payment_customers WHERE payment_customers.invoice_id = invoice_customers.invoice_id AND payment_customers.status = 1) AS bill"),
                "invoice_customers.currency",
                "invoice_customers.rate",
                "invoice_customers.serial",
                "invoice_customers.invoice_date AS paymentDate"
                )
            ->where('invoice_customers.uuid', $uuid)
            ->first();
        }
        return view('dashboard.accounting.sales-payment.create', ['data' => $data]);
    }

    public function purchaseCreatePayment($uuid = null)
    {
        $data = null;
        if($uuid){
            $data = InvoiceSupplier::join('suppliers', 'invoice_suppliers.supplier_id', '=', 'suppliers.supplier_id')
            ->leftJoin('debit_note_suppliers', 'invoice_suppliers.invoice_id', '=', 'debit_note_suppliers.invoice_id')
            ->select(
                'invoice_suppliers.invoice_id AS id',
                DB::raw('concat(
                    "INV",
                    invoice_suppliers.serial,
                    coalesce(concat (" - ", suppliers.company, " - "), " - "),
                    suppliers.first_name
                ) AS text'),
                'invoice_suppliers.supplier_id',
                DB::raw("CONCAT(suppliers.company,' - ', suppliers.first_name, ' ', suppliers.last_name) AS supplier"),
                DB::raw("(SELECT SUM(price*quantity) FROM invoice_supplier_items WHERE invoice_supplier_items.invoice_id = invoice_suppliers.invoice_id ) AS price"),
                DB::raw("(SELECT SUM(payment_amount) FROM debit_note_allocated_credit_suppliers dnac where dnac.debit_note_id = debit_note_suppliers.id AND dnac.status = 1) AS debit_note_allocated"),
                DB::raw("(SELECT tax.`tax_amount` FROM invoice_tax_suppliers
                INNER JOIN tax ON invoice_tax_suppliers.tax_id = tax.tax_id
                WHERE invoice_tax_suppliers.invoice_id = invoice_suppliers.invoice_id) AS tax"),
                DB::raw("(SELECT IF(currency > 1, sum(payment_amount)/payment_suppliers.rate, sum(payment_amount)) FROM payment_suppliers WHERE payment_suppliers.invoice_id = invoice_suppliers.invoice_id AND payment_suppliers.status = 1) AS bill"),
                "invoice_suppliers.currency",
                "invoice_suppliers.rate",
                "invoice_suppliers.serial",
                "invoice_suppliers.invoice_date AS paymentDate"
                )
            ->where('invoice_suppliers.uuid', $uuid)
            ->first();
        }
        return view('dashboard.accounting.purchase-payment.create', ['data' => $data]);
    }

    /**
     * Nanti tambahkan ke jurnal dengan nilai yg terkena pph & juga nominal pphnya dalam bentuk rupiah
     * utk nominal di pembayarannya itu nominal+pph
     *
     */
    public function salesStorePayment(Request $request)
    {
        $serial = PaymentCustomer::withTrashed()->count()+1;
        $payment = new PaymentCustomer;
        $payment->uuid             = Str ::uuid();
        $payment->serial           = $serial;
        $payment->invoice_id       = $request->invoice['id'];
        $payment->currency         = $request->currency;
        $payment->rate             = $request->rate;
        // $payment->discount_type    = $request->currency > 1 ? $request->discount->discount_type: null;
        // $payment->discount_nominal = $request->currency > 1 ? $request->discount->discount_nominal: null;
        $payment->payment_amount   = $request->payment_amount_idr;
        $payment->remark           = $request->remark;
        $payment->status           = 1;
        $payment->user_id          = Auth::id();
        $payment->tax_id           = $request->tax['tax_id'];
        $payment->payment_date     = $request->paymentDate;
        $payment->enter_to_account = $request->cash_account;
        $payment->save();

        // check nominal payment
        $invoice         = InvoiceCustomer::with(['taxes'])->find($request->invoice['id']);
        $invoice_payment = InvoiceCustomerItem::where('invoice_id', $request->invoice['id'])
                            ->select(DB::raw('sum(price * quantity) as subtotal'))
                            ->first();//->sum('price * qty');
        $nominal         = $invoice_payment->subtotal;
        $pay             = $nominal;

        // cek apakah ada pajak di invoice
        if($invoice->taxes){
            $taxCalculate = collect($invoice->taxes)->sum('tax_amount');
            $taxCalculate = ($nominal*($taxCalculate/100));
            $pay = $nominal + $taxCalculate;
        }

        // yg sudah dibayar
        $paidAmount = PaymentCustomer::select(
            DB::raw('SUM(IF(currency > 1, payment_amount / rate, payment_amount)) AS total')
        )
        ->where('invoice_id', $request->invoice['id'])
        ->first();

        // cek mata uang
        $payment = $pay;
        // $profitLossRate = 0;
        if($invoice->currency > 1){
            $payment = $pay / $invoice->rate;
        }

        // perbandingkan
        if($paidAmount->total >= $payment){
            $invoice->payment = 2;
            $invoice->save();
        }
        activity()->causedBy(Auth::id())->log('Payment created!');


        $this->invoiceCustomerToJournalPayment( $request, $payment->id );

        return [$invoice, $invoice_payment, $request->discount_amount, $request->tax_amount_idr, $pay, $payment, $paidAmount, 'taxes' => $taxCalculate];
        // return ['status' => true];
    }

    /**
     * Nanti tambahkan ke jurnal dengan nilai yg terkena pph & juga nominal pphnya dalam bentuk rupiah
     * utk nominal di pembayarannya itu nominal+pph
     *
     */
    public function purchaseStorePayment(Request $request)
    {
        $serial = PaymentSupplier::count()+1;
        $payment = new PaymentSupplier;
        $payment->uuid             = Str ::uuid();
        $payment->serial           = date('mY').$serial;
        $payment->invoice_id       = $request->invoice['id'];
        $payment->currency         = $request->currency;
        $payment->rate             = $request->rate;
        $payment->payment_amount   = $request->payment_amount_idr;
        $payment->remark           = $request->remark;
        $payment->status           = 1;
        $payment->user_id          = Auth::id();
        $payment->tax_id           = $request->tax['tax_id'];
        $payment->payment_date     = $request->paymentDate;
        $payment->enter_to_account = $request->cash_account;
        $payment->save();
        $payment_id = $payment->id;
        // check nominal payment
        $invoice         = InvoiceSupplier::with(['taxes'])->find($request->invoice['id']);
        $invoice_payment = InvoiceSupplierItem::where('invoice_supplier_items.invoice_id', $request->invoice['id'])
                            ->leftJoin('debit_note_suppliers', 'invoice_supplier_items.invoice_id', '=', 'debit_note_suppliers.invoice_id')
                            ->select(
                                DB::raw('sum(price * quantity) as subtotal'),
                                DB::raw("(SELECT SUM(payment_amount) FROM debit_note_allocated_credit_suppliers dnac where dnac.debit_note_id = debit_note_suppliers.id and dnac.status = 1) AS debit_note_allocated")
                            )
                            ->first();
        $nominal         = $invoice_payment->subtotal - $invoice_payment->debit_note_allocated;
        $pay             = $nominal;

        // cek apakah ada pajak di invoice
        if($invoice->taxes){
            $taxCalculate = collect($invoice->taxes)->sum('tax_amount');
            $taxCalculate = ($nominal*($taxCalculate/100));
            $pay = $nominal + $taxCalculate;
        }

        // yg sudah dibayar
        $paidAmount = PaymentSupplier::select(
            DB::raw('SUM(IF(currency > 1, payment_amount / rate, payment_amount)) AS total')
        )
        ->where('invoice_id', $request->invoice['id'])
        ->first();

        // cek mata uang
        $payment = $pay;

        if($invoice->currency > 1){
            $payment = $pay / $invoice->rate;
        }
        Debugbar::info([$paidAmount->total, $payment]);
        // perbandingkan
        if($paidAmount->total >= $payment){
            $invoice->payment = 2;
            $invoice->save();
        }
        activity()->causedBy(Auth::id())->log('Payment created!');


        $this->invoiceSupplierToJournalPayment( $request, $payment_id );

        return [$invoice, $invoice_payment, $request->discount_amount, $request->tax_amount_idr, $pay, $payment, $paidAmount, 'taxes' => $taxCalculate];
        // return ['status' => true];
    }

    public function invoiceCustomerToJournalPayment($request, $payment_id)
    {
        $id           = $request->id;
        $serial       = $request->invoice['serial'];
        $tax          = $request->tax;
        $cash_account = $request->cash_account;
        // $discount     = $request->discount_amount;
        $tax_amount   = $request->tax_amount_idr;
        $amountNet    = $request->payment_amount_net;
        $grandTotal   = $request->payment_amount_idr;
        $paymentDate  = $request->paymentDate;

        $nextId = Journal::withTrashed()->count()+1;

        $journal = new Journal;
        $journal->serial           = $nextId;
        $journal->code             = $nextId.'/'.date_format(date_create(now()), "m/Y");
        $journal->uuid             = Str::uuid();
        $journal->description      = 'Pembayaran Dari Penjualan INV'.$serial;
        $journal->status           = 2;  // Status utk dimasukan ke dalam jurnal
        $journal->transaction_at   = $paymentDate;
        $journal->supplier_payment_id = $payment_id;
        $journal->save();

        $account = SettingAccount::first();
        $accountSales      = $account->sales_account_id;
        $accountDiscount   = $account->sales_discount_account_id;
        $accountReceivable = $account->receivable_account_id;
        $accountProfitRate = $account->sales_profit_rate_account_id;
        $accountLossRate   = $account->sales_loss_rate_account_id;
        if($request->currency > 1){
            $paymentAmount = $request->payment_amount_net / $request->rate;
            $rateInvoice   = $request->invoice['rate'];
            $ratePayment   = $request->rate;

            $payment    = $paymentAmount * $ratePayment;
            $receivable = $paymentAmount * $rateInvoice;

            $amountNet  = $payment;
            $grandTotal = $receivable;

        }
        $journalDetails = [
            [
                'journal_id'  => $journal->id,
                'account_id'  => $cash_account,
                'description' => "Bayar penjualan dari INV{$serial}",
                'debet'       => $amountNet,
                'credit'      => 0
            ],
            [
                'journal_id' => $journal->id,
                'account_id' => $accountReceivable,
                'description'=> "Penjualan dari INV{$serial}",
                'debet'      => 0,
                'credit'     => $grandTotal
            ]
        ];

        // laba rugi atas selisih kurs
        if($request->currency > 1){

            if($payment > $receivable){
                //masuk ke laba
                $journalProfitLoss[] = [
                    'journal_id'  => $journal->id,
                    'account_id'  => $accountProfitRate,
                    'description' => "Laba selisih kurs",
                    'debet'      => 0,
                    'credit'       => $payment - $receivable
                ];
            }elseif($payment < $receivable){
                $profitLoss = $receivable - $payment;
                // ubah
                // masuk ke piutang
                $journalProfitLoss[] = [
                    'journal_id'  => $journal->id,
                    'account_id'  => $accountLossRate,
                    'description' => "Rugi selisih kurs",
                    'debet'       => $profitLoss,
                    'credit'      => 0
                ];
            }
            array_splice($journalDetails, 1, 0, $journalProfitLoss);
        }

        // if ($discount) {
        //     $journalDiscount[] = [
        //         'journal_id' => $journal->id,
        //         'account_id' => $accountDiscount,
        //         'description'=> 'Diskon',
        //         'debet'      => $discount,
        //         'credit'     => 0
        //     ];
        //     array_splice($journalDetails, 1, 0, $journalDiscount);
        // }

        // pajak
        if(isset($tax['tax_id'])){
            $journalTax[] = [
                'journal_id' => $journal->id,
                'account_id' => $tax['receivable_account_id'],
                'description'=> "Pajak PPh",
                'debet'      => $tax_amount,
                'credit'     => 0
            ];

            array_splice($journalDetails, 1, 0, $journalTax);
        }

        JournalDetail::insert($journalDetails);
        activity()->causedBy(Auth::id())->log('Journal Payment created!');
    }

    public function invoiceSupplierToJournalPayment($request, $payment_id)
    {
        $id           = $request->id;
        $serial       = $request->invoice['serial'];
        $tax          = $request->tax;
        $cash_account = $request->cash_account;
        // $discount     = $request->discount_amount;
        $tax_amount   = $request->tax_amount_idr;
        $amountNet    = $request->payment_amount_net;
        $grandTotal   = $request->payment_amount_idr;
        $paymentDate  = $request->paymentDate;

        $nextId = Journal::count()+1;

        $journal = new Journal;
        $journal->serial           = $nextId;
        $journal->code             = $nextId.'/'.date_format(date_create(now()), "m/Y");
        $journal->uuid             = Str::uuid();
        $journal->description      = 'Pembayaran Dari Pembelian INV'.$serial;
        $journal->status           = 2;  // Status utk dimasukan ke dalam jurnal
        $journal->transaction_at   = $paymentDate;
        $journal->supplier_payment_id = $payment_id;
        $journal->save();

        $account = SettingAccount::first();
        $accountDebt = $account->debt_account_id;

        $accountProfitRate = $account->sales_profit_rate_account_id;
        $accountLossRate   = $account->sales_loss_rate_account_id;
        if($request->currency > 1){
            $rateInvoice   = $request->invoice['rate']; // 10.000
            $ratePayment   = $request->rate;  // 12.000

            $paymentAmount = $request->payment_amount * $ratePayment; // 82.5/12.000 = 990.000
            $invoiceAmountToPay = $request->payment_amount * $rateInvoice; // 82.5*10.000 = 825.000
            $selisih = abs($invoiceAmountToPay - $paymentAmount); // 165.00

            $payment    = $paymentAmount * $ratePayment; //
            $receivable = $paymentAmount * $rateInvoice;

            $amountNet  = $payment;
            $grandTotal = $receivable;
        }else{
            $paymentAmount      = $request->payment_amount_idr;
            $invoiceAmountToPay = $request->payment_amount_idr;
        }

        $sum_debit = $invoiceAmountToPay;
        $sum_credit = $paymentAmount;
        $journalDetails = [
            [
                'journal_id' => $journal->id,
                'account_id' => $accountDebt,
                'description'=> "Pembelian dari INV{$serial}",
                'debet'      => $invoiceAmountToPay,
                'credit'     => 0
            ],
            [
                'journal_id'  => $journal->id,
                'account_id'  => $cash_account,
                'description' => "Bayar pembelian dari INV{$serial}",
                'debet'       => 0,
                'credit'      => $paymentAmount
            ]
        ];

        // pajak
        if(isset($tax['tax_id'])){
            $journalDetails[] = [
                'journal_id' => $journal->id,
                'account_id' => $tax['payable_account_id'],
                'description'=> "Pajak PPh",
                'debet'      => 0,
                'credit'     => $tax_amount
            ];
            $sum_credit += $tax_amount;
        }

        // laba rugi atas selisih kurs
        if($request->currency > 1){
            if($sum_debit < $sum_credit){
                //masuk ke laba
                $journalProfitLoss[] = [
                    'journal_id'  => $journal->id,
                    'account_id'  => $accountLossRate,
                    'description' => "Rugi selisih kurs",
                    'debet'       => $selisih,
                    'credit'      => 0
                ];
            }elseif($sum_credit < $sum_debit){
                // masuk ke piutang
                $journalProfitLoss[] = [
                    'journal_id'  => $journal->id,
                    'account_id'  => $accountProfitRate,
                    'description' => "Laba selisih kurs",
                    'debet'       => 0,
                    'credit'      => $selisih
                ];
            }
            array_splice($journalDetails, 1, 0, $journalProfitLoss);
        }

        Debugbar::info(($journalDetails));
        JournalDetail::insert($journalDetails);
        activity()->causedBy(Auth::id())->log('Journal Payment created!');
    }

    public function salesVoidPayment(Request $request)
    {
        PaymentCustomer::where('uuid', $request->uuid)
        ->update([
            'status'      => 0,
            'void_note' => $request->reason,
            'void_by'     => Auth::id(),
            'void_at'     => now(),
        ]);
        $payment_id = PaymentCustomer::where('uuid', $request->uuid)->first();
        Journal::where('customer_payment_id', $payment_id->id)->update(['status' => 3]);

        $inv = InvoiceCustomer::with(['list_items', 'taxes'])->find($request->invoice);
        $taxing = null;
        $price = collect($inv->list_items)->sum(function($items){
            return $items->price * $items->quantity;
        });
        if($inv->taxes){
            $taxing = collect($inv->taxes)->sum('tax_amount');
            $payTax = $price * ($taxing/100);
            $price += $payTax;
        }

        $paidInvoice = PaymentCustomer::where(['invoice_id' => $request->invoice, 'status' => 1])->get();
        $paidInvoice = collect($paidInvoice)->sum(function($inv){
            return $inv->payment_amount;
        });

        if ($paidInvoice < $price) {
            $invoice = InvoiceCustomer::find($request->invoice);
            $invoice->payment = 1;
            $invoice->Save();
        }

        return ['status' => true, 'price' => $price, 'paidInvoice' => $paidInvoice];
    }

    public function salesVoidPaymentInfo(Request $request)
    {
        $payment = PaymentCustomer::with('void_by')->whereUuid($request->uuid)->first();
        return $payment;
    }

    public function deleteJournal(Request $request)
    {
        $journal = Journal::find($request->journal_id);
        if($request->type == 'void'){
            $journal->status = 3;
            $journal->save();
        }elseif($request->type == 'delete'){
            $journal->delete();
        }
        return response()->json(['status' => 'ok']);
    }

    public function salesShowPayment($uuid)
    {
        $data = [];
        $payment = PaymentCustomer::with(['invoice', 'tax'])->where('uuid', $uuid)->first();
        $inv = InvoiceCustomer::join('customers', 'invoice_customers.customer_id', '=', 'customers.customer_id')
            ->select(
                'invoice_id AS id',
                DB::raw('concat(
                    "INV",
                    serial,
                    coalesce(concat (" - ", customers.company, " - "), " - "),
                    customers.first_name
                ) AS text'),
                'invoice_customers.customer_id',
                DB::raw("CONCAT(customers.company,' - ', customers.first_name, ' ', customers.last_name) AS customer"),
                DB::raw("(SELECT SUM(price) FROM invoice_customer_items WHERE invoice_customer_items.invoice_id = invoice_customers.invoice_id ) AS price"),
                DB::raw("(SELECT tax.`tax_amount` FROM invoice_tax_customers
                INNER JOIN tax ON invoice_tax_customers.tax_id = tax.tax_id
                WHERE invoice_tax_customers.invoice_id = invoice_customers.invoice_id) AS tax"),
                DB::raw("(SELECT sum(payment_amount) FROM payment_customers WHERE payment_customers.invoice_id = invoice_customers.invoice_id AND payment_customers.status = 1) AS bill"),
                "invoice_customers.currency",
                "invoice_customers.rate"
                )
            ->where('invoice_customers.invoice_id', $payment->invoice->invoice_id)
            ->first();
        return view('dashboard.accounting.sales-payment.show', ['invoice' => $inv, 'payment' => $payment]);
    }

    public function setting()
    {
        $setting = SettingAccount::with([
            'sales',

            'receivable',
            'sales_return',
            'sales_discount',
            'sales_profit_rate',
            'sales_loss_rate',

            'purchase',
            'debt',
            'purchase_return',
            'purchase_discount',
            'purchase_profit_rate',
            'purchase_loss_rate'
            ])->first();
        return view('dashboard.accounting.setting', ['setting' => $setting]);
    }

    public function storeSetting(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sales_account_id'                    => 'required|exists:chart_accounts,id',
            'receivable_account_id'               => 'required|exists:chart_accounts,id',
            'sales_return_account_id'             => 'required|exists:chart_accounts,id',
            'purchase_account_id'                 => 'required|exists:chart_accounts,id',
            'debt_account_id'                     => 'required|exists:chart_accounts,id',
            'purchase_return_account_id'          => 'required|exists:chart_accounts,id'
        ]);
        if($validator->fails()){
            return redirect('/accounting/setting')->withErrors($validator)->withInput()->with('status-fail', Lang::get('dashboard.fail_saved'));
        }
        $setting = SettingAccount::find($request->id);

        $setting->sales_account_id                = $request->sales_account_id;
        $setting->receivable_account_id           = $request->receivable_account_id;
        $setting->sales_return_account_id         = $request->sales_return_account_id;
        $setting->sales_discount_account_id       = $request->sales_discount_account_id;
        $setting->sales_profit_rate_account_id    = $request->sales_profit_rate_account_id;
        $setting->sales_loss_rate_account_id      = $request->sales_loss_rate_account_id;


        $setting->purchase_account_id             = $request->purchase_account_id;
        $setting->debt_account_id                 = $request->debt_account_id;
        $setting->purchase_return_account_id      = $request->purchase_return_account_id;
        $setting->purchase_discount_account_id    = $request->purchase_discount_account_id;
        $setting->purchase_profit_rate_account_id = $request->purchase_profit_rate_account_id;
        $setting->purchase_loss_rate_account_id   = $request->purchase_loss_rate_account_id;

        $setting->save();
        return redirect('accounting/setting')->with('status-success', Lang::get('dashboard.success_saved'));

    }

}
