<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the users list.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $you = auth()->user();
        $users = User::all();
        return view('dashboard.admin.usersList', compact('users', 'you'));
    }

    /**
     *  Remove user
     *
     *  @param int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function remove( $id )
    {
        $user = User::find($id);
        if($user){
            $user->delete();
        }
        return redirect()->route('adminUsers');
    }

    /**
     *  Show the form for editing the user.
     *
     *  @param int $id
     *  @return \Illuminate\Contracts\Support\Renderable
     */
    // public function editForm( $id )
    // {
    //     $user = User::find($id);
    //     return view('dashboard.admin.userEditForm', compact('user'));
    // }

    // public function edit(){

    // }

    public function editForm()
    {
        // $user = User::find()->first();
        return view('dashboard.admin.editProfile', ['user' => Auth::user()]);
    }

    public function edit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required|max:191',
            'email' => 'required|email|max:191',
            'phone' => 'numeric|digits_between:0,20',
            'password' => 'confirmed|max:191',
        ]);
        if($validator->fails()){
            return redirect("/profile/".Auth::user()->uuid)->withErrors($validator)->withInput();
        }
        $changed = [
            'name'      => $request->name,
            'email'     => $request->email,
            'phone'     => $request->phone,
        ];
        if($request->insert_password == 1){
            $changed['password'] = bcrypt($request->password);
        }
        User::find(Auth::id())->update($changed);
        activity()->causedBy(Auth::user())->log('Profile changed!');
        return redirect('/profile/'.Auth::user()->uuid)->with('status-success', Lang::get(('dashboard.profile_changed')));

    }

}
