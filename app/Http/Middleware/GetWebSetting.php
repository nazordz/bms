<?php

namespace App\Http\Middleware;

use App\Models\WebSetting;
use Closure;

class GetWebSetting
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $setting = WebSetting::find(session()->get('company_id'));
        view()->share('webSetting', $setting);
        return $next($request);
    }
}
