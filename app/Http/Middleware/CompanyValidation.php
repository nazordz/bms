<?php

namespace App\Http\Middleware;

use App\Models\Company;
use Closure;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class CompanyValidation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{

            if(!$request->session()->has('company_id')){
                return redirect('/company')->with('status-fail', Lang::get('dashboard.choose_company'));
            }else{
                $company = Company::find($request->session()->get('company_id'));
                if($company->deleted_at){
                    return redirect('/login')->with('status-fail', Lang::get('dashboard.company_deleted'));
                }
            }
        }catch(Exception $exception){
            return redirect('/login');
        }
        return $next($request);
    }
}
