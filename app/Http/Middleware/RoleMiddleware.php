<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Exceptions\UnauthorizedException;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (Auth::guest()) {
            throw UnauthorizedException::notLoggedIn();
        }

        $roles = is_array($role)
            ? $role
            : explode('|', $role);

        $menus = collect(Auth::user()->getMenuRoles());
        $confirmed = false;
        foreach ($roles as $key => $val) {
            $check = $menus->contains(function($value, $key)use($val) {
                return $value == $val;
            });
            if($check){
                $confirmed = true;
            }
        }
        if(!$confirmed) throw UnauthorizedException::forRoles($roles);

        return $next($request);
    }
}
