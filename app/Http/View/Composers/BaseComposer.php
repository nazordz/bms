<?php

namespace App\Http\View\Composers;

use App\Models\WebSetting;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class BaseComposer
{

    public function compose(View $view)
    {
        if(Auth::check()){

            $setting = WebSetting::first();
            // $setting = DB::table('web_settings')->where('company_id', session()->get('company_id'))->first();
            // dd(Auth::user());
            $view->with('logo', $setting->logo ?? null);
            $view->with('nav_logo', $setting->nav_logo ?? null);
            $view->with('currency', $setting->currency ?? null);
            $view->with('thousand_separator', $setting->thousand_separator ?? null);
            $view->with('decimal_separator', $setting->decimal_separator ?? null);
        }
    }
}
