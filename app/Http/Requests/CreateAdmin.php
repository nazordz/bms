<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateAdmin extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required|unique:users,name|max:191',
            'email' => 'required|unique:users,email|max:191',
            'password' => 'required|confirmed|max:191',
            'role'  => 'required',
        ];
    }

//     public function messages()
//     {
//         return [
//             'name.unique' =>
//         ];
//     }
}
