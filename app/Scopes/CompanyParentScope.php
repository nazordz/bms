<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class CompanyParentScope implements Scope{
    public function __construct($table, $relationTable, $ownerKey, $relationKey) {
        $this->table = $table;
        $this->relationTable = $relationTable;
        $this->ownerKey = $ownerKey;
        $this->relationKey = $relationKey;
    }
    public function apply(Builder $builder, Model $model)
    {
        $builder->whereIn("{$this->table}.{$this->relationKey}", function($query){
            $query->select("{$this->ownerKey}")
                    ->from($this->relationTable)
                    ->where('company_id', session()->get('company_id'));
        });
    }
}
