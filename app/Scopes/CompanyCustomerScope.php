<?php

namespace App\Scopes;

use App\Models\Customer;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class CompanyCustomerScope implements Scope{
    public function __construct($table) {
        $this->table = $table;
    }
    public function apply(Builder $builder, Model $model)
    {
        $builder->whereIn("{$this->table}.customer_id", function($query){
            $query->select('customer_id')
                    ->from(with(new Customer)->getTable())
                    ->where('company_id', session()->get('company_id'));
        });
    }
}
