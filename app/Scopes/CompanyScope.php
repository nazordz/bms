<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class CompanyScope implements Scope{

    public function apply(Builder $builder, Model $model)
    {
        // dd(Auth::check());
        if(Auth::check()){
            $builder->where('company_id', session()->get('company_id'));
        }
    }
}
