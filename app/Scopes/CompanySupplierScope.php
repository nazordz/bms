<?php

namespace App\Scopes;

use App\Models\Supplier;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class CompanySupplierScope implements Scope{
    public function __construct($table) {
        $this->table = $table;
    }
    public function apply(Builder $builder, Model $model)
    {
        $builder->whereIn("{$this->table}.supplier_id", function($query){
            $query->select('supplier_id')
                    ->from(with(new Supplier())->getTable())
                    ->where('company_id', session()->get('company_id'));
        });
    }
}
