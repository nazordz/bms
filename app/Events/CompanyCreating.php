<?php
namespace App\Events;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class CompanyCreating {
    use SerializesModels;

    /*
     * Model Instance
     * @param Model Instance
     */
    public function __construct($table) {
        $this->table = $table;
    }

    public function creating()
    {
        $this->table->company_id = session()->get('company_id');
    }
}
