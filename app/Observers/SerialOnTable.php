<?php

namespace App\Observers;

class SerialOnTable
{
    public function creating($model)
    {
        $model->serial = 1;
    }
}
