List Revisi BMS:
1. Penyesuaian form penawaran dan invoice
2. Mata uang 3 IDR, SGD, USD, RM dan rate
3. Bahasa 2
4. Layout report penawaran dan invoice
5. Keterangan saat melakukan void
6. PPh 23 dipindah d pembayaran
7. Pola penomoran reff pembayaran
8. Pembuatan Voucher untuk pengisian jurnal(tambah memo) dan bisa d export
9. Add COA masih blm bisa. Aga bingung terutama penomorannya
10. ada menu pembayaran pada group akunting.. untuk melakukan pembayaran invoice, munculkan invoice - tgl - nama customer beruba dropdown terlebih dahulu yg statusnya masih unpaid dan gunakan pola split payment.. (edited)

Penjelasan
1.  untuk form input penawaran dan form input invoice mengikuti form booking confirmation,
    jadi ada tambahan beberapa field yg di letakan sebelum pengisian item, tambahan fieldnya seperti Attn, Vessel, Voyage, ETA, B/L No, From, To, dan ada tambahan form term of payment (hari)
    langsung menggenarate tgl jatuh tempo pembayaran..
    misal hari ini senin 23 maret, waktu saya masukin 5 hari di term of paymentnya, maka jatuh temponya langsung terset 28 maret.. begitupun sebaliknya...
    selanjutnya pada pembutan penawaran dan invoice tambahkan juga from mata uang, rate, form term and condition di paling bawah..
    rate ini muncul bila mata uang yg dipilih bukan IDR, misal saya pakai mata uang USD, maka muncul rate, rate di isi dengan harga rupiah terhadap mata uang yg di pilih saat itu.. ini jadi acuan nilai konversi ke IDR..
    misal saya pilih USD maka muncul form rate dan saya isi 16.000.. maka setiap data yg keluar ataupun masuk pada saat add item menjadi terkonversi dengan rate yg sudah di masukan, jd kl saya masukan item dgn harga 1.000.000 maka akan keluar jumlahnya itu 1.000.000/16.000 = USD 62,50

2.  tambahkan term of condition di bawah deskripsi, seperti deskripsi, tapi sudah ada isinya dan bisa di edit.. untuk isinya sementara hardcode dl aja..

    untuk case saat ini minta kolom diskon peritem dan diskon dari total dan di hide dulu.. dan untuk pph 23 nanti masuknya di form pembayaran...

3.  tambahanya munculkan tgl create y, tapi ini bisa di rubah juga, baca tgl saat pembuatan.. dan save juga nama pembuat-nya d db, munculkan nama di list invoice..
4.  Sesuaikan format Export serperti di slack

5.  minta saat melakukan void di invoice atau d jurnal, tambahkan form keterangan.
    jadi misal kita mau void 1 invoice/jurnal, munculkan popup form keterangan keterangan, boleh di isi dan boleh di kosongkan, yg pasti harus tercatat kapan, jam berapa dan siapa yg melakukan void..

untuk 6, 7, 10 ini berhubungan... (dibawah tambahkan tombol pembayaran)
minta tambahkan menu pembayaran di group akunting.. isinya sama seperti yg muncul di pembayaran invoice.. tetapai bedanya kl lewat menu pembayaran ada pilihan invoice yg belum dibayar terlebih dahulu berupa drop down search dengan menampilkan nomor invoice-tgl invoice-nama customer..
munculkan juga pilihan mata uang dan skema rate seperti pada penawaran/invoice
setelah itu form pph 23 yg sebelumnya ada d invoice/penawaran di pindahkan ke pembayaran, jadi pilihannya ada di form pembayaran..
untuk reff pembayaran di buatkan auto number yg tersusun (kodenya menyusul)

8. dibuat cetak journal sepeti yg dikirim mba astin ( ada memo)


**Invoice akan paid bila nominal payment sudah sesuai atau lebih dari invoice
